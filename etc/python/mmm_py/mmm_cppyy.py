import os
import cppyy
import numpy as np

mmmcore_dir = None
mmmcore_build_dir = None
mmmtools_dir = None
mmmtools_build_dir = None

def eigen_to_numpy(matrix) -> np.ndarray:
    """
    :param matrix: A cppyy instance of cppyy.gbl.Eigen.MatrixXY
    """
    dir(matrix)
    array = np.zeros((matrix.rows(), matrix.cols()), dtype=type(matrix(0, 0)))
    for r in range(matrix.rows()):
        for c in range(matrix.cols()):
            array[r, c] = matrix(r, c)
    return array

def find_cmake_package(package_name: str, preferred_repo_dir = None):
    cmake_package_cache = os.path.expanduser("~/.cmake/packages")
    dir = os.path.join(cmake_package_cache, package_name)
    if not os.path.isdir(dir):
        msg = "Could not find CMake package '{}' in CMake package cache'{}'".format(
            package_name, cmake_package_cache)

        # Case mismatch?
        dirs = sorted(os.listdir(cmake_package_cache))
        dirs_lower = [d.lower() for d in dirs]
        try:
            index = dirs_lower.index(package_name.lower())
            msg += "\nDid you mean '{}'?".format(dirs[index])
        except ValueError:
            pass

        raise IOError(msg)

    files = os.listdir(dir)
    
    repos = []
    for f in files:
        with open(os.path.join(dir, f)) as file:
            content = file.read()
        build_dir = content.strip()
        root_dir, build = os.path.split(build_dir)
        if build != "build":
            raise IOError("No folder {}/build".format(package_name))
        repo_dir, project = os.path.split(root_dir)
        repos.append((root_dir, build_dir, repo_dir))
    if len(repos) == 1:
        return repos[0][0], repos[0][1]
    else:
        repo_paths = [x[0] for x in repos]
        if preferred_repo_dir is not None:
            print("Multiple repos found: {}. Trying to load the repository from {}".format(repo_paths, preferred_repo_dir))
            for r in repos:
                if (r[2] == preferred_repo_dir):
                    return r[0], r[1]
        raise IOError("Multiple repos found! {}".format(repo_paths))

def include_header(header_name: str):
    cppyy.include(header_name)

def load_library(path: str):
    cppyy.load_library(path)
    print("Loaded C++ library from {}".format(path))

def load_sensorplugin_lib(name: str):
    load_library(os.path.join(mmmcore_build_dir, "sensorPluginLibs/lib{}.so".format(name)))

def load_mmmtools_project(name: str):
    cppyy.add_include_path(os.path.join(mmmtools_dir, name))
    load_library(os.path.join(mmmtools_build_dir, "lib/lib{}.so".format(name)))

def get_mmm_model_path(name = "mmm"):
    return os.path.join(mmmtools_dir, "data/Model/Winter/{}.xml".format(name))

def set_cppyy_debug(value):
    cppyy.set_debug(value)

def get_mmm_tools_dir():
    return mmmtools_dir

def get_mmm_tools_build_dir():
    return mmmtools_build_dir

def initialize_bindings(preferred_repo_dir = None, init_rbdl = False):
    if preferred_repo_dir is None:
        FILE_DIR = os.path.dirname(os.path.abspath(__file__))
        ROOT_DIR = os.path.abspath(os.path.join(FILE_DIR, "..", "..", ".."))
        preferred_repo_dir = os.path.abspath(os.path.join(ROOT_DIR, ".."))

    # EIGEN
    cppyy.add_include_path("/usr/include/eigen3")
    # SIMOX
    simox_dir, simox_build_dir = find_cmake_package("Simox", preferred_repo_dir)
    cppyy.add_include_path(simox_dir)
    if init_rbdl:   # for <rbdl>
    	cppyy.add_include_path(os.path.join(simox_dir, "3rdParty/rbdl/include"))
    	cppyy.add_include_path(os.path.join(simox_build_dir, "3rdParty/rbdl/include"))
    load_library(os.path.join(simox_build_dir, "lib/libSimoxUtility.so"))
    load_library(os.path.join(simox_build_dir, "lib/libVirtualRobot.so"))
    # MMM
    global mmmcore_dir, mmmcore_build_dir
    mmmcore_dir, mmmcore_build_dir = find_cmake_package("MMMCore", preferred_repo_dir)
    cppyy.add_include_path(mmmcore_dir)
    cppyy.add_include_path(os.path.join(mmmcore_dir, "MMM/3rdParty"))  # for <boost/extension>
    load_library(os.path.join(mmmcore_build_dir, "lib/libMMMCore.so"))
    global mmmtools_dir, mmmtools_build_dir
    mmmtools_dir, mmmtools_build_dir = find_cmake_package("MMMTools", preferred_repo_dir)

