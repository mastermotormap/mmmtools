import cppyy
import mmm_py
import os
FILE_DIR = os.path.dirname(os.path.abspath(__file__))
REPO_DIR = os.path.abspath(os.path.join(FILE_DIR, "..", "..", "..", ".."))
mmm_py.initialize_bindings(REPO_DIR)

def load_mmm_recording(path: str):
    mmm_py.include_header("MMM/Motion/MotionReaderXML.h")
    from cppyy.gbl import MMM
    reader = MMM.MotionReaderXML()
    return reader.loadMotionRecording(path)

def load_mmm_model_motion(path: str):
    mmm_py.include_header("MMM/Motion/MotionRecording.h")
    motions = load_mmm_recording(path)
    motion = motions.getReferenceModelMotion()
    return motion

def run_example():
    motion = load_mmm_model_motion(os.path.join(mmm_py.get_mmm_tools_dir(), "data/Motions/Trial_07_v2.xml"))
    if motion is not cppyy.nullptr:
        mmm_py.include_header("VirtualRobot/Robot.h")
        mmm_py.include_header("MMM/Motion/Motion.h")
        model = motion.getModel().clone()
        mmm_py.load_sensorplugin_lib("IMUSensor")
        mmm_py.include_header("MMM/Motion/Sensor/AttachedSensor.h")
        mmm_py.include_header("MMM/Motion/Plugin/IMUPlugin/IMUSensor.h")
        mmm_py.include_header("MMM/Motion/Plugin/IMUPlugin/IMUSensorMeasurement.h")
        from cppyy.gbl import MMM
        imusensors = motion.getSensorsByType[MMM.IMUSensor]()
        for timestep in motion.getTimesteps():
            motion.initializeModel(model, timestep)
            for imusensor in imusensors:
                imupose = mmm_py.eigen_to_numpy(imusensor.getGlobalPose(model))
                measurement = imusensor.getDerivedMeasurement(timestep)
                if measurement is not cppyy.nullptr:
                    acceleration = mmm_py.eigen_to_numpy(measurement.getAcceleration())
                    print("Timestep: {}\nGlobal pose: {}\nAcceleration: {}\n\n".format(timestep, imupose, acceleration))
                break # only first imu

if __name__ == '__main__':
    run_example()
