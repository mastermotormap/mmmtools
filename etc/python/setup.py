#!/usr/bin/env python3
import os

from os.path import join, dirname, realpath
from setuptools import find_packages, setup

packages = find_packages()
# Ensure that we don't pollute the global namespace.
for p in packages:
    assert p == 'mmm_py' or p.startswith('mmm_py.')

def read_requirements_file(filename):
    req_file_path = '%s/%s' % (dirname(realpath(__file__)), filename)
    with open(req_file_path) as f:
        return [line.strip() for line in f]

setup(
    name="mmm-py", # Replace with your own username
    version="0.0.1",
    author="Andre Meixner",
    author_email="andre.meixner@kit.edu",
    description="A small python package containing bindings for mmm c++ framework via cppyy",
    url="",
    packages=packages,
    include_package_data=True,
    package_dir={'mmm_py': 'mmm_py'},
    package_data={'mmm_py': ['generated/*.so']},
    install_requires=read_requirements_file('requirements.txt'),
    classifiers=[
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3 :: Only',
    ],
    python_requires='>=3.6',
)
