/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMMTools
* @author     Christian Mandery
* @copyright  2015 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include <boost/extension/extension.hpp>

#include "NloptConverterFactory.h"
#include "NloptConverter.h"

namespace MMM
{

// register this factory
ConverterFactory::SubClassRegistry NloptConverterFactory::registry(NloptConverterFactory::getName(), &NloptConverterFactory::createInstance);

NloptConverterFactory::NloptConverterFactory()
    : ConverterFactory()
{}

NloptConverterFactory::~NloptConverterFactory()
{}

ConverterPtr NloptConverterFactory::createConverter()
{
    ConverterPtr converter(new NloptConverter(getName()));
    return converter;
}

std::string NloptConverterFactory::getName()
{
    return "NloptConverter";
}

std::shared_ptr<ConverterFactory> NloptConverterFactory::createInstance(void*)
{
    std::shared_ptr<ConverterFactory> converterFactory(new NloptConverterFactory());
    return converterFactory;
}

}
#ifdef WIN32
#pragma warning(push)
#pragma warning(disable: 4190) // C-linkage warning can be ignored in our case
#endif

extern "C"
BOOST_EXTENSION_EXPORT_DECL MMM::ConverterFactoryPtr getFactory() {
    MMM::NloptConverterFactoryPtr f(new MMM::NloptConverterFactory());
    return f;
}
#ifdef WIN32
#pragma warning(pop)
#endif
