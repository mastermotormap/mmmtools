
#include "ConverterMMM2RobotFactory.h"
#include "ConverterMMM2Robot.h"
#include <boost/extension/shared_library.hpp>
#include <boost/function.hpp>

namespace MMM
{

// register this factory
ConverterFactory::SubClassRegistry ConverterMMM2RobotFactory::registry(ConverterMMM2RobotFactory::getName(), &ConverterMMM2RobotFactory::createInstance);

ConverterMMM2RobotFactory::ConverterMMM2RobotFactory()
	: ConverterFactory()
{

}
ConverterMMM2RobotFactory::~ConverterMMM2RobotFactory()
{

}

ConverterPtr ConverterMMM2RobotFactory::createConverter()
{
	ConverterPtr converter(new ConverterMMM2Robot(getName()));
	return converter;
}

std::string ConverterMMM2RobotFactory::getName()
{
	return "ConverterMMM2Robot";
}

std::shared_ptr<ConverterFactory> ConverterMMM2RobotFactory::createInstance(void*)
{
    std::shared_ptr<ConverterFactory> converterFactory(new ConverterMMM2RobotFactory());
	return converterFactory;
}

}
#ifdef WIN32
#pragma warning(push)
#pragma warning(disable: 4190) // C-linkage warning can be ignored in our case
#endif

extern "C"
BOOST_EXTENSION_EXPORT_DECL MMM::ConverterFactoryPtr getFactory() {
    MMM::ConverterMMM2RobotFactoryPtr f(new MMM::ConverterMMM2RobotFactory());
    return f;
}
#ifdef WIN32
#pragma warning(pop)
#endif
