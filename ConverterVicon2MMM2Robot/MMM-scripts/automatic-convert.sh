#!/bin/bash
FILES1=~/ConverterVicon2MMM2Robot/ViconData/*
FILES2=~/ConverterVicon2MMM2Robot/MMMData/*
for f in $FILES1
do
	filename_base="${f%'.c3d'}" #delete .xml from end of string
	./convert.sh $f ${filename_base}.xml
done

mv ~/ConverterVicon2MMM2Robot/ViconData/*.xml ~/ConverterVicon2MMM2Robot/MMMData

cd ../Robot-scripts

for f in $FILES2
do
	filename_base="${f%'.xml'}" #delete .xml from end of string
	./convertMMM2Robot.sh $f ${filename_base}-Robot.xml
done

mv ~/ConverterVicon2MMM2Robot/MMMData/*-Robot.xml ~/ConverterVicon2MMM2Robot/RobotData

exit
