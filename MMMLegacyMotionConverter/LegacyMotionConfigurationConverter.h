/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_LEGACYMOTIONCONFIGURATIONCONVERTER_H_
#define __MMM_LEGACYMOTIONCONFIGURATIONCONVERTER_H_

#include <filesystem>
#include "../common/MotionListConfiguration.h"
#include <MMM/Motion/LegacyMotionConverter.h>

namespace MMM {

class LegacyMotionConfigurationConverter : public LegacyMotionConverter
{
public:
    LegacyMotionConfigurationConverter(bool mmmOnly = false, const std::vector<std::filesystem::path> &additionalLibPaths = std::vector<std::filesystem::path>(), bool ignoreStandardLibPaths = false);

    std::vector<std::string> getMotionNames(const std::filesystem::path &motionFilePath);

    MotionRecordingPtr convert(std::vector<MotionListConfigurationPtr> configurations, const std::filesystem::path &motionFilePath, const std::function<void(std::filesystem::path&)> &handleMissingModelFile = NULL);
};

typedef std::shared_ptr<LegacyMotionConfigurationConverter> LegacyMotionConfigurationConverterPtr;

}

#endif // __MMM_LEGACYMOTIONCONFIGURATIONCONVERTER_H_
