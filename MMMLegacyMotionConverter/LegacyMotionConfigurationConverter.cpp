#include "LegacyMotionConfigurationConverter.h"

#include <MMM/Exceptions.h>
#include <MMM/Motion/Legacy/LegacyMotion.h>
#include <MMM/Motion/Legacy/LegacyMotionReaderXML.h>
#include <algorithm>

using namespace MMM;

LegacyMotionConfigurationConverter::LegacyMotionConfigurationConverter(bool mmmOnly, const std::vector<std::filesystem::path> &additionalLibPaths, bool ignoreStandardLibPaths)
    : LegacyMotionConverter(mmmOnly, additionalLibPaths, ignoreStandardLibPaths)
{
}

std::vector<std::string> LegacyMotionConfigurationConverter::getMotionNames(const std::filesystem::path &motionFilePath) {
    LegacyMotionReaderXMLPtr legacyMotionReader(new LegacyMotionReaderXML(false, false));
    return legacyMotionReader->getMotionNames(motionFilePath);
}

MotionRecordingPtr LegacyMotionConfigurationConverter::convert(std::vector<MotionListConfigurationPtr> configurations, const std::filesystem::path &motionFilePath, const std::function<void(std::filesystem::path&)> &handleMissingModelFile) {
    std::sort(configurations.begin(), configurations.end(), SortByNewIndexFunctor());
    auto legacyMotions = loadMotions(motionFilePath, true, handleMissingModelFile);
    MotionRecordingPtr motions = MotionRecording::EmptyRecording();
    std::set<std::string> motionNames;
    for (auto configuration : configurations) {
        if (!configuration->isIgnoreMotion()) {
            LegacyMotionPtr legacyMotion = legacyMotions.at(configuration->getIndex());
            legacyMotion->setName(configuration->getMotionName());
            MotionPtr motion = convertMotion(legacyMotion, motionFilePath, true);
            if (motionNames.find(motion->getName()) != motionNames.end()) throw MMM::Exception::MMMException("Motion name '" + motion->getName() + "' is not unique!");
            motionNames.insert(motion->getName());
            motions->addMotion(motion);
        }
    }
    return motions;
}
