#include "LegacyMotionImporterDialog.h"
#include "ui_LegacyMotionImporterDialog.h"

#include <MMM/Model/ModelReaderXML.h>
#include "../../common/HandleMotionsWithoutModel.h"
#include <MMM/Exceptions.h>
#include <QFileDialog>
#include <QCheckBox>
#include <QSettings>
#include <QListWidget>
#include <QMessageBox>
#include <set>

LegacyMotionImporterDialog::LegacyMotionImporterDialog(MMM::LegacyMotionConfigurationConverterPtr converter, const std::filesystem::path &motionFilePath, QWidget* parent) :
    QDialog(parent),
    ui(new Ui::LegacyMotionImporterDialog),
    converter(converter),
    motionFilePath(motionFilePath)
{
    ui->setupUi(this);

    motionList = new MotionListWidget(converter->getMotionNames(motionFilePath));

    ui->motionListLayout->addWidget(motionList);

    connect(ui->importMotionButton, SIGNAL(clicked()), this, SLOT(importMotion()));
    connect(ui->cancelButton, SIGNAL(clicked()), this, SLOT(close()));
}

LegacyMotionImporterDialog::~LegacyMotionImporterDialog() {
    delete ui;
}

void LegacyMotionImporterDialog::importMotion() {
    try {
        motions = converter->convert(motionList->getConfigurations(), motionFilePath, handleMissingModelFilePath);
        if (motions && !motions->empty()) {
            this->close();
        } else {
            QMessageBox* msgBox = new QMessageBox(this);
            msgBox->setText(QString("Please enable minimum one motion!"));
            msgBox->exec();
        }
    } catch (MMM::Exception::MMMException& e) {
        QMessageBox* msgBox = new QMessageBox(this);
        msgBox->setText(QString::fromStdString(e.what()));
        msgBox->exec();
    }
}

MMM::MotionRecordingPtr LegacyMotionImporterDialog::getMotions() {
    exec();
    return motions;
}
