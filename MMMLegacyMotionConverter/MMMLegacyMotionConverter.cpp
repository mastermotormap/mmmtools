/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include <MMM/Motion/LegacyMotionConverter.h>
#include "MMMLegacyMotionConverterConfiguration.h"
#include <MMM/Exceptions.h>

using namespace MMM;

int main(int argc, char *argv[]) {
    MMM_INFO << " --- MMMLegacyMotionConverter --- " << std::endl;
    MMMLegacyMotionConverterConfiguration* configuration = new MMMLegacyMotionConverterConfiguration();
    if (!configuration->processCommandLine(argc, argv))
    {
        MMM_ERROR << "Error while processing command line, aborting..." << std::endl;
        return -1;
    }

    try {
        MMM_INFO << "Reading legacy motion file..." << std::endl;
        MMM::LegacyMotionConverterPtr converter(new MMM::LegacyMotionConverter());

        MMM_INFO << "Converting motions..." << std::endl;
        MMM::MotionRecordingPtr motions = converter->convert(configuration->inputMotionPath, true);

        MMM_INFO << "Writing motions to " << configuration->outputMotionPath << std::endl;
        motions->saveXML(configuration->outputMotionPath);
        return 0;
    } catch (MMM::Exception::MMMException& e) {
        MMM_ERROR << e.what() << std::endl;
        return -2;
    }
}
