/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include "MMMAddSegmentConfiguration.h"
#include <MMM/MMMCore.h>
#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/Segmentation/MotionRecordingSegment.h>
#include <MMM/Motion/Segmentation/Segmentation.h>
#include <MMM/Model/LoadModelStrategy.h>

int main(int argc, char *argv[])
{
    MMM_INFO << "--- MMM DATA GLOVE CONVERTER ---" << std::endl;

    MMMAddSegmentConfiguration *configuration = new MMMAddSegmentConfiguration();
    if (!configuration->processCommandLine(argc,argv)) {
        MMM_ERROR << "Could not process command line, aborting." << std::endl;
        return -1;
    }

    configuration->print();

    try {
        //MMM::MotionReaderXMLPtr motionReader(new MMM::MotionReaderXML(true, false));
        MMM::MotionReaderXMLPtr motionReader(new MMM::MotionReaderXML(true, false, configuration->sensorPluginPaths));

        MMM::LoadModelStrategy::LOAD_MODEL_STRATEGY = MMM::LoadModelStrategy::Strategy::NONE; // do not load model
        MMM_INFO << "Reading motion file '" << configuration->inputMotionPath << "'!" << std::endl;
        MMM::MotionRecordingPtr motions = motionReader->loadMotionRecording(configuration->inputMotionPath);
        MMM::MotionRecordingSegmentPtr segment = MMM::MotionRecordingSegment::createMotionRecordingSegment(configuration->inputSegmentPath, motions);

        if (configuration->timestepDelta != 0.0f) {
            MMM_INFO << "Updating segmentation to motion" << std::endl;
            segment->shiftTimesteps(configuration->timestepDelta);
            segment->updateInterval(motions->getMinTimestep(), motions->getMaxTimestep());
        }
        motions->setMotionSegment(segment);

        MMM_INFO << "Writing motions to " << configuration->outputMotionPath << std::endl;
        motions->saveXML(configuration->outputMotionPath);

        MMM_INFO << "--- END ---" << std::endl;
        return 0;
    }
    catch (MMM::Exception::MMMException &e) {
        MMM_ERROR << e.what() << std::endl;
        return -2;
    }
}
