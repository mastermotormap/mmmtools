#include <VirtualRobot/RuntimeEnvironment.h>
#include <MMM/MMMCore.h>
#include <SimoxUtility/algorithm/string.h>

#include "MMMAddSegmentConfiguration.h"

MMMAddSegmentConfiguration::MMMAddSegmentConfiguration() : ApplicationBaseConfiguration()
{
}

bool MMMAddSegmentConfiguration::processCommandLine(int argc, char *argv[])
{
    VirtualRobot::RuntimeEnvironment::considerKey("inputMotion");
    VirtualRobot::RuntimeEnvironment::considerKey("timestepDelta");
    VirtualRobot::RuntimeEnvironment::considerKey("sensorPluginPaths");
    VirtualRobot::RuntimeEnvironment::considerKey("inputSegment");
    VirtualRobot::RuntimeEnvironment::considerKey("outputMotion");

    VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);

    inputMotionPath = getParameter("inputMotion", true, true);
    inputSegmentPath = getParameter("inputSegment", true, true);
    std::string timestepDeltaStr = getParameter("timestepDelta");
    if (!timestepDeltaStr.empty()) timestepDelta = simox::alg::to_<float>(timestepDeltaStr.c_str());
    else timestepDelta = 0.0f;
    sensorPluginPaths = getPaths("sensorPluginPaths");
    outputMotionPath = getParameter("outputMotion");
    if (outputMotionPath.empty()){
        std::string name = inputMotionPath.stem();
        outputMotionPath = inputMotionPath.parent_path().append(name + "_segmented.xml");
    }

    return valid;
}

void MMMAddSegmentConfiguration::print()
{
    MMM_INFO << "*** MMMDataGlove Configuration ***" << std::endl;
    std::cout << "Input file: " << inputMotionPath << std::endl;
    std::cout << "Output file: " << outputMotionPath << std::endl;
}
