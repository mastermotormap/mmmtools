/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SEGMENTATIONSCENE_H_
#define __MMM_SEGMENTATIONSCENE_H_

#include <QGraphicsScene>
#include <QGraphicsLineItem>
#include <QSettings>

#include <MMM/MMMCore.h>
#include <MMM/Enum.h>
#include <SimoxUtility/color/KellyLUT.h>

#include "SegmentationSlider.h"

#include <deque>
#include <set>

namespace MMM
{
BETTER_ENUM(SceneAnnotationType, int, ActionLabel = 0, BimanualLabel = 1)

class SegmentationRect;
class SegmentationTrack;


class SegmentationScene : public QGraphicsScene
{
    Q_OBJECT

public:
    SegmentationScene(QGraphicsView* parent, qreal minimum = 0, qreal maximum = 1000, qreal resize_margin = 10, qreal trackHeight = 50,
                      size_t previousSegmentsSize = 5);

    ~SegmentationScene();

    bool eventFilter(QObject *obj, QEvent *event) override;

    void annotate(MMM::MotionRecordingPtr motions);

    qreal toScale(float timestep, bool moveIt = false, bool logging = false);

    float toTimestep(qreal value, bool moveIt = false, bool logging = false);

    qreal getResizeMargin();

    void reload(qreal value);

    bool isKeyPressed(int key = Qt::Key_Shift);

    qreal position = 0;

    qreal clear();

    void eraseAll();

    void moveSlider(float timestep);

    void drawForeground(QPainter *painter, const QRectF &rect) override;

    float adjustTimestep(float timestep, bool moveIt = false, bool logging = false);

    QString getAuthorID();

    bool getHighlight();

    void storePreviousSegment(bool fromHelp = false);

    void setPreviousSegment();

    void centerOnCurrentValue();

    qreal getCurrentValue();

    bool isShowAnnotationText();

    void addInitialSegmentation(bool resetSegmentation = false);

    void addBimanualSegmentation(bool initializeWithContacts = false, bool resetSegmentation = false, bool standardActionRecording = false);

    SceneAnnotationType getSceneAnnotationType();

    void setSceneAnnotationType(SceneAnnotationType type);

    void updateVisualisation();

public slots:
    void setAuthorID(QString authorID);

    void setHighlight(bool highlight);

    void setShowAnnotationText(bool value);

    void updateCurrentAnnotation(const QString &annotation);

    void resetSegmentation();

signals:
    void jumpTo(float timestep);
    void showAnnotation(const QString &annotation);
    void saveMotion();

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

private:
    qreal load(MMM::MotionSegmentPtr segment, qreal margin, MMM::SegmentationRect* parent = nullptr);
    void moveSliderLeft(float deltaTimestep = 0.01);
    void moveSliderRight(float deltaTimestep = 0.01);
    void reloadStoredPreviousSegment();
    std::vector<MMM::SegmentationTrack*> getNearestSegmentationTracks(qreal position);
    void saveSegmentationBackup();


    QGraphicsView* view;
    QSettings settings;
    std::map<qreal, std::vector<MMM::SegmentationTrack*>> segTracks;
    SegmentationSlider* slider;
    MMM::MotionRecordingPtr currentRecording;
    qreal minimum;
    qreal maximum;
    qreal resize_margin;
    float minTimestep;
    float maxTimestep;
    std::vector<float> referenceTimesteps;
    std::vector<float>::iterator referenceTimestepsIterator;
    bool highlight = true;
    bool leftMouseButton = false;
    bool draggedSlider = false;
    bool showAnnotationText = true;

    std::deque<MotionRecordingSegmentPtr> previousClonedSegments;
    size_t previousSegmentsSize;
    MotionRecordingSegmentPtr previousSegment_help;

    std::set<int> pressedKeys;
    SceneAnnotationType sceneAnnotationType;


public:
    qreal track_height;
};

}

#endif // __MMM_SEGMENTATIONSCENE_H_
