#include "SegmentationRect.h"

#include <cmath>
#include <iostream>
#include <QKeyEvent>
#include <QTextOption>
#include <QDialog>
#include <QVBoxLayout>

#include <MMM/Motion/MotionRecording.h>
#include <MMM/Motion/Segmentation/AbstractMotionSegment.h>
#include <MMM/Motion/Segmentation/MotionSegment.h>
#include <MMM/Motion/Segmentation/Segmentation.h>

#include <MMM/Motion/Annotation/ActionLabel/ActionLabelAnnotation.h>
#include <SimoxUtility/color/KellyLUT.h>
#include <SimoxUtility/color/GlasbeyLUT.h>
#include <MMM/Motion/Annotation/BimanualLabel/BimanualAnnotation.h>
#include "AddAnnotationWidget.h"

#include "SegmentationTrack.h"
#include "SegmentationScene.h"
#include "ActionLabels/AddActionAnnotationWidget.h"
#include "BimanualLabels/AddBimanualAnnotationWidget.h"

namespace MMM
{

SegmentationRect::SegmentationRect(SegmentationScene* scene, SegmentationTrack* parent, MotionSegmentPtr segment, qreal top, qreal height) :
    QGraphicsItem(),
    top(top),
    height(height),
    color(simox::color::Color::white()),
    highlightColor(simox::color::Color::kit_green()),
    isHighlight(false),
    isResizeLeft(false),
    isResizeRight(false),
    resized(false),
    hovered(false),
    scene(scene),
    observer(std::shared_ptr<MotionSegmentObserver>(new Observer(this))),
    parent(parent),
    segment(segment)
{
    segment->addSegmentObserver(observer);
    setFlag(ItemIsSelectable);
    setAcceptHoverEvents(true);
    setCacheMode( QGraphicsItem::DeviceCoordinateCache );
}

SegmentationRect::~SegmentationRect() {
    segment->removeSegmentObserver(observer);
}

QRectF SegmentationRect::boundingRect() const
{
    float start = scene->toScale(segment->getStartTimestep());
    float end = scene->toScale(segment->getEndTimestep());
    return QRectF(start, top, end - start, height);
}

void SegmentationRect::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    std::string annotationString = std::string();
    QRectF rec = boundingRect();
    //QColor color;
    color = simox::color::Color::white();

    switch(scene->getSceneAnnotationType())
    {
    case SceneAnnotationType::ActionLabel:
    {
        if (auto annotation = segment->getDerivedAnnotation<ActionLabelAnnotation>()) {
            auto label = annotation->getLabel();
            annotationString = label._to_string();
            if (label._value == ActionLabel::Idle) {
                color = simox::color::KellyLUT::kelly_medium_gray;
            }
            else if (label._value == ActionLabel::Approach) {
                color = simox::color::KellyLUT::kelly_very_light_blue;
            }
            else if (label._value == ActionLabel::Hold) {
                color = simox::color::KellyLUT::kelly_strong_blue;
            }
            else if (label._value == ActionLabel::Move) {
                color = simox::color::KellyLUT::kelly_vivid_red;
            }
            else if (label._value == ActionLabel::Place) {
                color = simox::color::KellyLUT::kelly_vivid_orange;
            }
            else if (label._value == ActionLabel::Lift) {
                color = simox::color::KellyLUT::kelly_vivid_yellow;
            }
            else if (label._value == ActionLabel::Pour) {
                color = simox::color::KellyLUT::kelly_strong_purplish_pink;
            }
            else if (label._value == ActionLabel::PostAction) {
                color = simox::color::KellyLUT::kelly_strong_purple;
            }
            else if (label._value == ActionLabel::PreAction) {
                color = simox::color::KellyLUT::kelly_strong_violet;
            }
            else if (label._value == ActionLabel::Retreat) {
                color = simox::color::KellyLUT::kelly_grayish_yellow;
            }
            else if (label._value == ActionLabel::Scoop) {
                color = simox::color::KellyLUT::kelly_vivid_green;
            }
            else color = simox::color::GlasbeyLUT::at(label._to_index());
            break;
        }
    }
    case SceneAnnotationType::BimanualLabel:
    {
        if (auto annotation = segment->getDerivedAnnotation<BimanualAnnotation>()) {
            auto label = annotation->getLabel();
            annotationString = label._to_string();
            if (label._value == BimanualLabel::no_motion) {
                color = simox::color::KellyLUT::kelly_medium_gray;
            }
            else if (label._value == BimanualLabel::unimanual_right) {
                color = simox::color::KellyLUT::kelly_very_light_blue;
            }
            else if (label._value == BimanualLabel::unimanual_left) {
                color = simox::color::KellyLUT::kelly_strong_blue;
            }
            else if (label._value == BimanualLabel::loosely_coupled) {
                color = simox::color::KellyLUT::kelly_vivid_red;
            }
            else if (label._value == BimanualLabel::tightly_coupled_sym) {
                color = simox::color::KellyLUT::kelly_vivid_orange;
            }
            else if (label._value == BimanualLabel::tightly_coupled_asym_r) {
                color = simox::color::KellyLUT::kelly_vivid_yellow;
            }
            else if (label._value == BimanualLabel::tightly_coupled_asym_l) {
                color = simox::color::KellyLUT::kelly_strong_purplish_pink;
            }
            else if (label._value == BimanualLabel::undefined) {
                color = simox::color::KellyLUT::kelly_strong_purple;
            }
            else color = simox::color::GlasbeyLUT::at(label._to_index());
            break;
        }
    }
    } // switch

    if (annotationString.empty()) {
        if (auto annotation = segment->getDerivedAnnotation<ActionLabelAnnotation>()) {
            annotationString = annotation->getLabel()._to_string();
        }
        else if (!segment->getName().empty()) {
            annotationString = "\"" + segment->getName() + "\"";
        }
    }

    if (scene->getHighlight() && isHighlight) {
        color = highlightColor;
    }

    QBrush brush(QColor::fromRgb(color.r, color.g, color.b));
    painter->fillRect(rec, brush);
    painter->drawRect(rec);

    if (!annotationString.empty() && scene->isShowAnnotationText()) {
        double luma = ((0.299 * color.r) + (0.587 * color.g) + (0.114 * color.b)) / 255;
        painter->setPen(QPen(luma > 0.5 ? Qt::black : Qt::white, 3));
        qreal factor = rec.width() / painter->fontMetrics().width(QString::fromStdString(annotationString));
        if ((factor < 1) || (factor > 1))
        {
            QFont f = painter->font();
            f.setPointSizeF(std::max(std::min(f.pointSizeF() * factor, 20.0), 1.0));
            painter->setFont(f);
        }
        painter->drawText(rec, Qt::AlignCenter | Qt::AlignTop, QString::fromStdString(annotationString));
    }
}

void SegmentationRect::highlight(bool value) {
    isHighlight = value;
    this->update(boundingRect());
    auto t = parent;
    while(t) {
        auto s = t->parent;
        if (s) {
            s->isHighlight = value;
            s->update(s->boundingRect());
        }
        else
            break;
        t = s->parent;
    }
}

bool SegmentationRect::keyPressed(QKeyEvent* event, QPointF mousePosScene) {
    if (event->key() == Qt::Key_Delete && hovered && !event->isAutoRepeat()) {
        parent->remove(this, this->mapFromScene(mousePosScene).x());
    }
    else if (event->key() == Qt::Key_C && hovered && !event->isAutoRepeat()) {
        scene->setPreviousSegment();
        if (segment->createChildSegmentation()) {
            scene->storePreviousSegment(true);
            scene->reload(scene->clear());
        }
    }
    else if (event->key() == Qt::Key_S && event->modifiers() == Qt::KeyboardModifier::NoModifier && hovered  && !event->isAutoRepeat()) {
        parent->split(this, this->mapFromScene(mousePosScene).x());
    }
    else {
        return false;
    }
    return true;
}

bool SegmentationRect::keyReleased(QKeyEvent* /*event*/, QPointF /*mousePosScene*/) {
    return false;
}

void SegmentationRect::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    qreal x = event->pos().x();
    if (x - scene->toScale(segment->getStartTimestep()) < scene->getResizeMargin() && segment->getPrevSegment())
    {
        scene->setPreviousSegment();
        isResizeLeft = true;
        resized = false;
    }
    else if (scene->toScale(segment->getEndTimestep()) - x < scene->getResizeMargin() && segment->getNextSegment())
    {
        scene->setPreviousSegment();
        isResizeRight = true;
        resized = false;
    }
}

void SegmentationRect::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (isResizeLeft || isResizeRight) {
        qreal x = event->pos().x();
        if (std::abs(x - scene->getCurrentValue()) < 2 * scene->getResizeMargin()) {
            x = scene->getCurrentValue(); // snap to slider
        }
        if (isResizeLeft)
        {
            segment->resizeLeft(scene->toTimestep(x));
        }
        else if (isResizeRight)
        {
            segment->resizeRight(scene->toTimestep(x));
        }
        resized = true;
    }
}

void SegmentationRect::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QApplication::setOverrideCursor(Qt::ArrowCursor);
    if (isResizeLeft)
    {
        if (resized) scene->storePreviousSegment(true);
        isResizeLeft = false;
    }
    else if (isResizeRight)
    {
        if (resized) scene->storePreviousSegment(true);
        isResizeRight = false;
    }
    else if (scene->isKeyPressed(Qt::Key_Shift)) {
        parent->split(this, event->pos().x());
    }
}

void SegmentationRect::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) {
    QDialog* dialog = new QDialog();
    dialog->move(event->screenPos().x(), event->screenPos().y());
    dialog->setWindowFlags(Qt::Popup);
    QVBoxLayout* layout = new QVBoxLayout();
    dialog->setLayout(layout);
    AddAnnotationWidget* annotationWidget;
    switch (scene->getSceneAnnotationType()) {
    case MMM::SceneAnnotationType::ActionLabel:
    {
        annotationWidget = new AddActionAnnotationWidget(segment, dialog);
        break;
    }
    case MMM::SceneAnnotationType::BimanualLabel:
    {
        annotationWidget = new AddBimanualAnnotationWidget(segment, dialog);
        break;
    }
    }
    layout->addWidget(annotationWidget);
    scene->setPreviousSegment();
    connect(annotationWidget, &AddAnnotationWidget::updated, this, &SegmentationRect::updateAnnotation);
    dialog->open();
}

void SegmentationRect::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    hovered = true;
    updateHovered(event->pos().x(), event->pos().y());
}

void SegmentationRect::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
    hovered = true;
    updateHovered(event->pos().x(), event->pos().y());
}

void SegmentationRect::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    hovered = false;
    updateHovered(event->pos().x(), event->pos().y());
}

void SegmentationRect::updateHovered(qreal x, qreal /*y*/) {
    if (hovered) {
        if (scene->getHighlight()) highlight(true);
        scene->showAnnotation(QString::fromStdString(segment->getAnnotationAsXML()));
        if ((x - scene->toScale(segment->getStartTimestep()) < scene->getResizeMargin() && segment->getPrevSegment())
                || (scene->toScale(segment->getEndTimestep()) - x < scene->getResizeMargin() && segment->getNextSegment()))
        {
            QApplication::setOverrideCursor(Qt::SizeHorCursor);
            return;
        }
        else if (scene->isKeyPressed(Qt::Key_Shift)) {
            QApplication::setOverrideCursor(Qt::CrossCursor);
            return;
        }
    }
    else highlight(false);
    QApplication::setOverrideCursor(Qt::ArrowCursor);
}

void SegmentationRect::updateAnnotation() {
    update();
    scene->storePreviousSegment(true);
}

SegmentationRect::Observer::Observer(SegmentationRect *s) : s(s)
{
}

void SegmentationRect::Observer::notifySegmentResized() {
    s->prepareGeometryChange();
}

}
