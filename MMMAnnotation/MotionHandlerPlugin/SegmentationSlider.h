/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SEGMENTATIONSLIDER_H_
#define __MMM_SEGMENTATIONSLIDER_H_

#include <QGraphicsLineItem>

namespace MMM
{

class SegmentationSlider : public QGraphicsLineItem
{
public:
    SegmentationSlider(qreal pos, qreal size, qreal currentValue = 0, QColor color = Qt::blue, qreal width = 3);

    void setCurrentValue(qreal value);

    qreal getCurrentValue();

    void setCurrentSize(qreal value);

private:
    void updateVis();

    qreal currentValue;
    qreal pos;
    qreal size;
};
}

#endif // __MMM_SEGMENTATIONSLIDER_H_
