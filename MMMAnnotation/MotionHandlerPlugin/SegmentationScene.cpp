﻿#include "SegmentationScene.h"

#include <QKeyEvent>
#include <QGraphicsView>
#include <QScrollBar>
#include <QDir>

#include <MMM/Motion/MotionRecording.h>
#include <MMM/Motion/Segmentation/AbstractMotionSegment.h>
#include <MMM/Motion/Segmentation/MotionSegment.h>
#include <MMM/Motion/Segmentation/MotionRecordingSegment.h>
#include <MMM/Motion/Segmentation/Segmentation.h>
#include <MMM/Motion/Annotation/ActionLabel/ActionLabelAnnotation.h>

#include "SegmentationTrack.h"

namespace MMM
{

SegmentationScene::SegmentationScene(QGraphicsView* parent, qreal minimum, qreal maximum, qreal resize_margin, qreal track_height, size_t previousSegmentsSize) :
    QGraphicsScene(parent),
    view(parent),
    slider(nullptr),
    minimum(minimum),
    maximum(maximum),
    resize_margin(resize_margin),
    previousSegmentsSize(previousSegmentsSize),
    previousSegment_help(nullptr),
    sceneAnnotationType(SceneAnnotationType::ActionLabel),
    track_height(track_height)


{
    view->installEventFilter(this);
    view->setScene(this);
    view->setTransformationAnchor(QGraphicsView::NoAnchor);
    setItemIndexMethod(QGraphicsScene::NoIndex);
}


SegmentationScene::~SegmentationScene() {
    eraseAll();
}

bool SegmentationScene::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::KeyPress)
    {
        QPoint origin = view->mapFromGlobal(QCursor::pos());
        QPointF relativeOrigin = view->mapToScene(origin);
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        pressedKeys.insert(keyEvent->key());
        if (keyEvent->key() == Qt::Key_A) {
            if (keyEvent->modifiers().testFlag(Qt::KeyboardModifier::ShiftModifier)) {
                if (currentRecording) {
                    MotionSegmentPtr prevSegment = nullptr;
                    for (auto t : getNearestSegmentationTracks(relativeOrigin.y())) {
                        auto p = t->getSegmentation()->getPrevSegment(toTimestep(getCurrentValue()), true);
                        if (!prevSegment || (p && p->getEndTimestep() > prevSegment->getEndTimestep()))
                            prevSegment = p;
                    }
                    if (prevSegment)
                        emit jumpTo(prevSegment->getEndTimestep());
                }
            }
            else moveSliderLeft();
            return true;
        }
        else if (keyEvent->key() == Qt::Key_D) {
            if (keyEvent->modifiers().testFlag(Qt::KeyboardModifier::ShiftModifier)) {
                MotionSegmentPtr nextSegment = nullptr;
                for (auto t : getNearestSegmentationTracks(relativeOrigin.y())) {
                    auto n = t->getSegmentation()->getNextSegment(toTimestep(getCurrentValue()), true);
                    if (!nextSegment || (n && n->getStartTimestep() < nextSegment->getStartTimestep()))
                        nextSegment = n;
                }
                if (nextSegment)
                    emit jumpTo(nextSegment->getStartTimestep());
            }
            else moveSliderRight();
            return true;
        }
        else if (keyEvent->key() == Qt::Key_T && !keyEvent->isAutoRepeat()) {
            addInitialSegmentation(false);
            return true;
        }
        else if (keyEvent->key() == Qt::Key_Z && keyEvent->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier) && !keyEvent->isAutoRepeat()) {
            // Strg+Z
            reloadStoredPreviousSegment();
            return true;
        }
        else if (keyEvent->key() == Qt::Key_S && keyEvent->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier) && !keyEvent->isAutoRepeat()) {
            // Strg+S
            emit saveMotion();
            return true;
        }
        else if (keyEvent->key() == Qt::Key_Plus) {
            // Zoom in
            qreal zoomFactor = 1.5f;
            maximum *= zoomFactor;
            slider->setCurrentValue(getCurrentValue() * zoomFactor);
            reload(clear());
            setSceneRect(itemsBoundingRect());
            centerOnCurrentValue();
            return true;
        }
        else if (keyEvent->key() == Qt::Key_Minus) {
            // Zoom out
            qreal zoomFactor = 1.0f / 1.5f;
            maximum *= zoomFactor;
            slider->setCurrentValue(getCurrentValue() * zoomFactor);
            reload(clear());
            setSceneRect(itemsBoundingRect());
            centerOnCurrentValue();
            return true;
        }
        else {
            for (const auto &ts : segTracks)
                for (const auto &t : ts.second)
                    if (t->keyPressed(keyEvent, relativeOrigin)) return true; // already handled
        }
    }
    else if (event->type() == QEvent::KeyRelease)
    {
        QPoint origin = view->mapFromGlobal(QCursor::pos());
        QPointF relativeOrigin = view->mapToScene(origin);
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        pressedKeys.erase(keyEvent->key());
        for (const auto &ts : segTracks)
            for (const auto &t : ts.second)
                if (t->keyReleased(keyEvent, relativeOrigin)) return true; // already handled
    }
    return QObject::eventFilter(obj, event);
}

void SegmentationScene::annotate(MotionRecordingPtr motions) {
    auto currentValue = clear();
    referenceTimesteps.clear();
    previousClonedSegments.clear();
    previousSegment_help = nullptr;

    currentRecording = motions;
    minTimestep = motions->getMinTimestep();
    maxTimestep = motions->getMaxTimestep();
    auto referenceMotion = motions->getReferenceModelMotion();
    if (referenceMotion) {
        referenceTimesteps = referenceMotion->getTimesteps();
        referenceTimestepsIterator = referenceTimesteps.begin();
    }

    reload(currentValue);
}

qreal SegmentationScene::toScale(float timestep, bool moveIt, bool logging) {
    return (adjustTimestep(timestep, moveIt, logging) - minTimestep) / (maxTimestep - minTimestep) * (maximum - minimum) + minimum;
}

float SegmentationScene::toTimestep(qreal value, bool moveIt, bool logging) {
    return adjustTimestep((value - minimum) / (maximum - minimum) * (maxTimestep - minTimestep) + minTimestep, moveIt, logging);
}

qreal SegmentationScene::getResizeMargin() {
    return resize_margin;
}

qreal SegmentationScene::load(MMM::MotionSegmentPtr segment, qreal pos, MMM::SegmentationRect* parent) {
    if (segment) {
        for (auto segmentation : segment->getSegmentations()) {
            auto segmentationTrack = new MMM::SegmentationTrack(this, parent, pos * (track_height + 5), segmentation, false);
            segTracks[pos].push_back(segmentationTrack);
            auto pos2 = pos;
            for (const auto &s : segmentation->getSegments()) {
                auto rec = new MMM::SegmentationRect(this, segmentationTrack, s.second, pos * (track_height + 5), track_height);
                segTracks[pos].back()->addRect(rec);
                pos2 = std::max(load(s.second, pos + 1, rec), pos2);
            }
            pos = pos2;
        }
    }
    return pos;
}

void SegmentationScene::moveSliderLeft(float deltaTimestep) {
    if (draggedSlider)
        draggedSlider = false;
    if (deltaTimestep <= 0) return;
    auto currentTimestep = toTimestep(getCurrentValue());
    auto timestep = currentTimestep - deltaTimestep;
    if (!referenceTimesteps.empty() && timestep >= referenceTimesteps.front() && timestep <= referenceTimesteps.back()) {
        if (currentTimestep > referenceTimesteps.back()) {
            referenceTimestepsIterator = std::prev(referenceTimesteps.end());
        }
        if (referenceTimestepsIterator != referenceTimesteps.begin()) {
            referenceTimestepsIterator--;
            auto timestep = *referenceTimestepsIterator;
            emit jumpTo(timestep);
        }
    }
    else {
        if (timestep < minTimestep) timestep = minTimestep;
        emit jumpTo(timestep);
    }
}

void SegmentationScene::moveSliderRight(float deltaTimestep) {
    if (deltaTimestep <= 0) return;
    if (draggedSlider)
        draggedSlider = false;
    auto currentTimestep = toTimestep(getCurrentValue());
    auto timestep = currentTimestep + deltaTimestep;
    if (!referenceTimesteps.empty() && timestep >= referenceTimesteps.front() && timestep <= referenceTimesteps.back()) {
        if (currentTimestep < referenceTimesteps.front()) {
            referenceTimestepsIterator = referenceTimesteps.begin();
        }
        if (*referenceTimestepsIterator != *referenceTimesteps.rbegin()) {
            referenceTimestepsIterator++;
            auto timestep = *referenceTimestepsIterator;
            emit jumpTo(timestep);
        }
    }
    else {
        if (timestep > maxTimestep) timestep = maxTimestep;
        emit jumpTo(timestep);
    }
}

void SegmentationScene::storePreviousSegment(bool fromHelp) {
    if (!fromHelp)
        setPreviousSegment();
    previousClonedSegments.push_back(previousSegment_help);
    if (previousSegmentsSize < previousClonedSegments.size())
        previousClonedSegments.pop_front();
}

void SegmentationScene::setPreviousSegment() {
    if (currentRecording) {
        auto segment = currentRecording->getSegment();
        if (segment)
            previousSegment_help = segment->clone();
        else previousSegment_help = nullptr;
    }
}

void SegmentationScene::centerOnCurrentValue() {
    if (view->horizontalScrollBar()) {
        auto newScrollBarPos = getCurrentValue() / (maximum-minimum) * (view->horizontalScrollBar()->maximum() - view->horizontalScrollBar()->minimum()) + view->horizontalScrollBar()->minimum();
        //std::cout << "Moving scrollbar to " << newScrollBarPos << std::endl;
        view->horizontalScrollBar()->setValue(newScrollBarPos);
    }
}

qreal SegmentationScene::getCurrentValue() {
    if (!slider) return 0;
    return slider->getCurrentValue();
}

bool SegmentationScene::isShowAnnotationText() {
    return showAnnotationText;
}

void SegmentationScene::reloadStoredPreviousSegment() {
    if (!currentRecording) return;
    if (!previousClonedSegments.empty()) {
        currentRecording->setMotionSegment(previousClonedSegments.back());
        previousClonedSegments.pop_back();
        reload(clear());
    }
    else {
        std::cout << "Maximum amount of reversions exceeded (" << previousSegmentsSize << ")" << std::endl;
    }
}

std::vector<SegmentationTrack *> SegmentationScene::getNearestSegmentationTracks(qreal position) {
    qreal minDistance = std::numeric_limits<qreal>::max();
    std::vector<SegmentationTrack *> segmentationTracks;
    for (const auto &s : segTracks) {
        auto distance = std::abs(s.first * (track_height + 5) + track_height / 2.0f - position);
        if (distance < minDistance) {
            minDistance = distance;
            segmentationTracks = s.second;
        }
    }
    return segmentationTracks;
}

void SegmentationScene::saveSegmentationBackup() {
    auto tempPath = std::filesystem::path(QDir::tempPath().toStdString()) / "h2t-mmmviewer-motion-backups";
    std::filesystem::create_directory(tempPath);
    try {
        auto filePath = tempPath / "segmentation-backup.xml";
        if (currentRecording && currentRecording->getSegment()) {
            currentRecording->getSegment()->saveXML(filePath);
        }
    } catch(MMM::Exception::MMMException &e) {

    }
}

void SegmentationScene::reload(qreal value) {
    auto segment = currentRecording->getSegment();
    if (segment) {
        auto tracks = segment->collectSegmentations();
        std::vector<SegmentationRect*> rects;
        for (auto track : tracks) {
            for (auto segmentation : track) {
                auto pos = position * (track_height + 5);
                MMM::SegmentationRect* parent = nullptr;
                /*for (auto rect : rects) {
                    if (rect->segment == segmentation->getParent())
                        parent = rect;
                }*/
                auto segmentationTrack = new MMM::SegmentationTrack(this, parent, pos, segmentation, false);
                segTracks[position].push_back(segmentationTrack);
                for (const auto &s : segmentation->getSegments()) {
                    auto rec = new MMM::SegmentationRect(this, segmentationTrack, s.second, pos, track_height);
                    rects.push_back(rec);
                    segTracks[position].back()->addRect(rec);
                }
            }
            position += 1;
        }
        // Set parent for segmentation track
        for (const auto &ts : segTracks)
            for (const auto &t : ts.second)
                for (auto rect : rects)
                    if (rect->segment == t->getSegmentation()->getParent())
                        t->parent = rect;
    }
    if (segTracks.size() > 0) {
        slider = new SegmentationSlider(-50, position * (track_height + 5), value);
        addItem(slider);
    }
    this->update();
}

bool SegmentationScene::isKeyPressed(int key) {
    return pressedKeys.find(key) != pressedKeys.end();
}

qreal SegmentationScene::clear() {
    position = 0;
    auto currentValue = getCurrentValue();
    eraseAll();
    slider = nullptr;
    return currentValue;
}

void SegmentationScene::eraseAll() {
    for (auto item : this->items() ) {
        this->removeItem(item);
        delete item;
    }
    for (const auto & s: segTracks) {
        for (auto s : s.second)
            delete s;
    }
    segTracks.clear();
}

void SegmentationScene::moveSlider(float timestep) {
    if (!draggedSlider && slider) {
        std::cout << "Move slider to " << timestep << std::endl;
        slider->setCurrentValue(toScale(timestep, true));
        centerOnCurrentValue();
    }
}

void SegmentationScene::drawForeground(QPainter *painter, const QRectF &rect) {
    if (currentRecording && currentRecording->getSegment()) {
        for (auto t : segTracks) {
            auto target = t.second.front()->getSegmentation()->getDescription();
            if (!target.empty()) {
                painter->setFont(QFont("arial",12));
                painter->drawText(QRect(-100, t.first * (track_height + 5), 100, track_height), Qt::AlignCenter | Qt::AlignTop, QString::fromStdString(target));
            }
        }
    }
    QGraphicsScene::drawForeground(painter, rect);
}

float SegmentationScene::adjustTimestep(float timestep, bool moveIt, bool logging) {
    if (timestep < minTimestep) timestep = minTimestep;
    else if (timestep > maxTimestep) timestep = maxTimestep;
    if (!referenceTimesteps.empty() && timestep >= referenceTimesteps.front() && timestep <= referenceTimesteps.back()) {
        auto it = referenceTimestepsIterator;
        while (it != referenceTimesteps.end() && *it <= timestep) {
            it++;
        }
        if (it != referenceTimesteps.begin()) {
            do {
                it--;
            }
            while (it != referenceTimesteps.begin() && *it > timestep);
        }
        auto nextIt = std::next(it);
        if (nextIt != referenceTimesteps.end() && (*nextIt - timestep < timestep - *it)) {
            it = nextIt;
        }
        auto prevTimestep = timestep;
        timestep = *it;
        if (logging && std::abs(prevTimestep - timestep) > 0.000001)
            std::cout << "Adjusted timestep " << prevTimestep << " to " << timestep << std::endl;
        if (moveIt)
            referenceTimestepsIterator = it;
    }
    return timestep;
}

QString SegmentationScene::getAuthorID() {
    return settings.value("annotation/author", QString::fromStdString(MMM::MotionSegmentation::AUTHOR_ID)).toString();
}

bool SegmentationScene::getHighlight() {
    return highlight;
}

void SegmentationScene::setAuthorID(QString authorID) {
    MMM::MotionSegmentation::AUTHOR_ID = authorID.toStdString();
    settings.setValue("annotation/author", authorID);
}

void SegmentationScene::setHighlight(bool highlight) {
    this->highlight = highlight;
}

void SegmentationScene::setShowAnnotationText(bool value) {
    showAnnotationText = value;
    updateVisualisation();
}

void SegmentationScene::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    if (event->button() == Qt::MouseButton::LeftButton) {
        leftMouseButton = true;
    }
    QGraphicsScene::mousePressEvent(event);
}

void SegmentationScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    if (event->scenePos().y() < 0 && event->scenePos().x() < getCurrentValue() + 3 * resize_margin && event->scenePos().x() > getCurrentValue() - 3 * resize_margin) {
        QApplication::setOverrideCursor(Qt::SizeHorCursor);
        draggedSlider = true;
    }
    else if (!leftMouseButton) {
        QApplication::setOverrideCursor(Qt::ArrowCursor);
        draggedSlider = false;
    }

    if (leftMouseButton && draggedSlider) {
        float timestep = toTimestep(event->scenePos().x());
        emit jumpTo(timestep);
        slider->setCurrentValue(toScale(timestep, true));
        centerOnCurrentValue();
    }
    QGraphicsScene::mouseMoveEvent(event);
}

void SegmentationScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    if (event->button() == Qt::MouseButton::LeftButton) {
        leftMouseButton = false;
    }
    QGraphicsScene::mouseReleaseEvent(event);
}

void SegmentationScene::addInitialSegmentation(bool resetSegmentation) {
    if (!currentRecording) return;
    if (!currentRecording->getSegment() || resetSegmentation)
        this->resetSegmentation();
    else storePreviousSegment();
    auto segment = currentRecording->getSegment();
    MMM::MotionSegmentationPtr segmentation(new MotionSegmentation(segment, MotionSegmentation::MANUAL_SEGMENTATION_TYPE));
    segment->addSegmentation(segmentation);
    segmentation->addSegment(minTimestep, maxTimestep, "");
    reload(clear());
}

void SegmentationScene::addBimanualSegmentation(bool initializeWithContacts, bool resetSegmentation, bool standardActionRecording) {
    if (!currentRecording) return;
    if (!currentRecording->getSegment() || resetSegmentation)
        this->resetSegmentation();
    else storePreviousSegment();
    auto segment = currentRecording->getSegment();
    auto referenceModelMotion = currentRecording->getReferenceModelMotion();
    if (referenceModelMotion) {
        try {
            std::string leftHandNode = "Hand L Root";
            std::string rightHandNode = "Hand R Root";
            MMM::MotionSegmentationPtr leftHandSegmentation(new MotionSegmentation(segment, referenceModelMotion, MotionSegmentation::MANUAL_SEGMENTATION_TYPE, MotionLevel::Action, "Left", leftHandNode));
            MMM::MotionSegmentationPtr rightHandSegmentation(new MotionSegmentation(segment, referenceModelMotion, MotionSegmentation::MANUAL_SEGMENTATION_TYPE, MotionLevel::Action, "Right", rightHandNode));
            segment->addSegmentation(leftHandSegmentation);
            leftHandSegmentation->addSegment(minTimestep, maxTimestep, "");
            segment->addSegmentation(rightHandSegmentation);
            rightHandSegmentation->addSegment(minTimestep, maxTimestep, "");
            if (initializeWithContacts){
                auto distanceMap = currentRecording->calculateDistanceMap(referenceModelMotion->getName(), {"Hand R Palm", "Hand L Palm"}, false);
                float contactDistanceThresholdInMM = 15;
                float allowedTimeDifferenceInS = 0.2;
                std::map<float, std::string> rightHandContactChanges = MotionRecording::calculateContactChanges(distanceMap["Hand R Palm"], contactDistanceThresholdInMM, allowedTimeDifferenceInS);
                for (const auto &contactByTimestep : rightHandContactChanges) {
                    if (contactByTimestep.first > 0)
                        rightHandSegmentation->split(contactByTimestep.first);
                }
                std::map<float, std::string> leftHandContactChanges =  MotionRecording::calculateContactChanges(distanceMap["Hand L Palm"], contactDistanceThresholdInMM, allowedTimeDifferenceInS);
                for (const auto &contactByTimestep : leftHandContactChanges) {
                    if (contactByTimestep.first > 0)
                        leftHandSegmentation->split(contactByTimestep.first);
                }

                if (standardActionRecording) {
                    {
                        auto leftHandSegments = leftHandSegmentation->getSegments();
                        if (leftHandSegments.size() > 4) {
                            unsigned int i = 0;
                            for (auto segment : leftHandSegments) {
                                if (i == 0) segment.second->addAnnotation(MotionAnnotationPtr(new ActionLabelAnnotation(ActionLabel::Idle, leftHandContactChanges.begin()->second)));
                                else if (i == 1) segment.second->addAnnotation(MotionAnnotationPtr(new ActionLabelAnnotation(ActionLabel::Approach, "", "", std::next(leftHandContactChanges.begin(), 2)->second)));
                                else if  (i == leftHandSegments.size() - 1) segment.second->addAnnotation(MotionAnnotationPtr(new ActionLabelAnnotation(ActionLabel::Idle, leftHandContactChanges.rbegin()->second)));
                                else if  (i == leftHandSegments.size() - 2) segment.second->addAnnotation(MotionAnnotationPtr(new ActionLabelAnnotation(ActionLabel::Retreat, "", "", leftHandContactChanges.rbegin()->second)));
                                else segment.second->addAnnotation(MotionAnnotationPtr(new ActionLabelAnnotation(ActionLabel::Unnamed, std::next(leftHandContactChanges.begin(), i)->second)));
                                i++;
                            }
                        }
                    }
                    {
                        auto rightHandSegments = rightHandSegmentation->getSegments();
                        if (rightHandSegments.size() > 4) {
                            unsigned int i = 0;
                            for (auto segment : rightHandSegments) {
                                if (i == 0) segment.second->addAnnotation(MotionAnnotationPtr(new ActionLabelAnnotation(ActionLabel::Idle, rightHandContactChanges.begin()->second)));
                                else if (i == 1) segment.second->addAnnotation(MotionAnnotationPtr(new ActionLabelAnnotation(ActionLabel::Approach, "", "", std::next(rightHandContactChanges.begin(), 2)->second)));
                                else if  (i == rightHandSegments.size() - 1) segment.second->addAnnotation(MotionAnnotationPtr(new ActionLabelAnnotation(ActionLabel::Idle, rightHandContactChanges.rbegin()->second)));
                                else if  (i == rightHandSegments.size() - 2) segment.second->addAnnotation(MotionAnnotationPtr(new ActionLabelAnnotation(ActionLabel::Retreat, "", "", rightHandContactChanges.rbegin()->second)));
                                else segment.second->addAnnotation(MotionAnnotationPtr(new ActionLabelAnnotation(ActionLabel::Unnamed, std::next(rightHandContactChanges.begin(), i)->second)));
                                i++;
                            }
                        }
                    }
                }
            }

            reload(clear());
        } catch (MMM::Exception::MMMException &e) {
            MMM_ERROR << e.what() << std::endl;
        }
    }
    else {
        MMM_WARNING << "No reference model motion found in recording" << std::endl;
    }
}

void SegmentationScene::resetSegmentation() {
    if (currentRecording) {
        storePreviousSegment();
        auto segment = MMM::MotionRecordingSegmentPtr(new MotionRecordingSegment(currentRecording));
        currentRecording->setMotionSegment(segment);
        this->reload(clear());
    }
}

SceneAnnotationType MMM::SegmentationScene::getSceneAnnotationType(){
    return sceneAnnotationType;
}

void MMM::SegmentationScene::setSceneAnnotationType(SceneAnnotationType type){
    sceneAnnotationType = type;
}

void MMM::SegmentationScene::updateCurrentAnnotation(const QString &annotation) {
    setSceneAnnotationType(SceneAnnotationType::_from_string(annotation.toStdString().c_str()));
    updateVisualisation();
}

void MMM::SegmentationScene::updateVisualisation() {
    for (auto item : items()) {
        item->update();
    }
}

}

