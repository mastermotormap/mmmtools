#include "AnnotateMotionHandlerDialog.h"
#include "ui_AnnotateMotionHandlerDialog.h"

#include <MMM/FactoryPluginLoader.h>
#include <QFileDialog>
#include <QSignalMapper>
#include <QLabel>
#include <QTextBrowser>
#include <MMM/Motion/MotionRecording.h>
#include "SegmentationScene.h"

AnnotateMotionHandlerDialog::AnnotateMotionHandlerDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::AnnotateMotionHandlerDialog)
{
    ui->setupUi(this);

    scene = new MMM::SegmentationScene(ui->graphicsView);
    connect(ui->authorEdit, &QLineEdit::textChanged, scene, &MMM::SegmentationScene::setAuthorID);
    connect(ui->highlightBox, &QCheckBox::toggled, scene,  &MMM::SegmentationScene::setHighlight);
    connect(ui->annotationTextBox, &QCheckBox::toggled, scene, &MMM::SegmentationScene::setShowAnnotationText);
    connect(scene, &MMM::SegmentationScene::jumpTo, this, &AnnotateMotionHandlerDialog::jumpTo);
    connect(scene, &MMM::SegmentationScene::saveMotion, this, &AnnotateMotionHandlerDialog::saveMotion);
    connect(ui->infoCheckbox, &QCheckBox::toggled, this, &AnnotateMotionHandlerDialog::showInfoBox);
    ui->infoCheckbox->setChecked(false);
    showInfoBox(false);

    ui->authorEdit->setText(scene->getAuthorID());
    ui->templateComboBox->addItem("Initial Segmentation");
    ui->templateComboBox->addItem("Bimanual Segmentation");
    ui->templateComboBox->addItem("Bimanual Segmentation (Pre-Init with Contacts)");
    ui->templateComboBox->addItem("Bimanual Segmentation (Pre-Init Standard Action Recordings)");
    ui->templateComboBox->setCurrentIndex(ui->templateComboBox->findText(settings.value("annotation/template", "Initial Segmentation").toString()));

    for (const auto &v : MMM::SceneAnnotationType::_values()) {
        ui->annotationTypeComboBox->addItem(v._to_string());
    }
    ui->annotationTypeComboBox->setCurrentText(scene->getSceneAnnotationType()._to_string());
    connect(ui->annotationTypeComboBox, &QComboBox::currentTextChanged, scene, &MMM::SegmentationScene::updateCurrentAnnotation);

    connect(ui->addTemplateButton, &QPushButton::clicked, this, &AnnotateMotionHandlerDialog::addTemplate);
    connect(scene, &MMM::SegmentationScene::showAnnotation, ui->annotationBrowser, &QTextBrowser::setText);
    connect(ui->resetButton, &QPushButton::clicked, scene, &MMM::SegmentationScene::resetSegmentation);
}

AnnotateMotionHandlerDialog::~AnnotateMotionHandlerDialog()
{
    delete scene;
    delete ui;
}

void AnnotateMotionHandlerDialog::segmentMotion(MMM::MotionRecordingPtr motions) {
    scene->annotate(motions);
}

void AnnotateMotionHandlerDialog::moveSlider(float timestep) {
    scene->moveSlider(timestep);
}

void AnnotateMotionHandlerDialog::addTemplate() {
    auto segmenation_template = ui->templateComboBox->currentText();
    if (segmenation_template == "Initial Segmentation") {
        scene->addInitialSegmentation();
    }
    else if (segmenation_template == "Bimanual Segmentation") {
        scene->addBimanualSegmentation(false);
    }
    else if (segmenation_template == "Bimanual Segmentation (Pre-Init with Contacts)") {
        scene->addBimanualSegmentation(true);
    }
    else if (segmenation_template == "Bimanual Segmentation (Pre-Init Standard Action Recordings)") {
        scene->addBimanualSegmentation(true, false, true);
    }
    else {
        MMM_ERROR << "Not valid template " << segmenation_template.toStdString() << std::endl;
        return;
    }
    settings.setValue("annotation/template", segmenation_template);
}

void AnnotateMotionHandlerDialog::showInfoBox(bool value) {
    ui->OthersGroupBox->setVisible(value);
}
