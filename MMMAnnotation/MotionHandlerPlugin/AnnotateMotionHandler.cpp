#include "AnnotateMotionHandler.h"
#include <QFileDialog>

#include <MMM/Motion/MotionRecording.h>

using namespace MMM;

AnnotateMotionHandler::AnnotateMotionHandler(QWidget* widget) :
    MotionHandler(MotionHandlerType::GENERAL, "Annotation", "Ctrl+A"),
    dialog(new AnnotateMotionHandlerDialog(widget))
{
    connect(dialog, &AnnotateMotionHandlerDialog::jumpTo, this, &AnnotateMotionHandler::jumpTo);
    connect(dialog, &AnnotateMotionHandlerDialog::saveMotion, this, &AnnotateMotionHandler::saveMotion);
}

void AnnotateMotionHandler::handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> /*currentRobots*/) {
    if (motions && !motions->empty()) {
        dialog->segmentMotion(motions);
        dialog->show();
    }
    else MMM_ERROR << "Cannot open segment motion dialog, because no motions are present!" << std::endl;
}

std::string AnnotateMotionHandler::getName() {
    return NAME;
}

void AnnotateMotionHandler::timestepChanged(float timestep) {
    if (dialog->isVisible()) dialog->moveSlider(timestep);
}

void AnnotateMotionHandler::motionsOpened(bool opened, MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> /*currentRobots*/) {
    if (opened && motions && dialog->isVisible()) dialog->segmentMotion(motions);
}

