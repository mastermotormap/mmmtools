#include "AnnotateMotionHandlerFactory.h"
#include "AnnotateMotionHandler.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry AnnotateMotionHandlerFactory::registry(AnnotateMotionHandler::NAME, &AnnotateMotionHandlerFactory::createInstance);

AnnotateMotionHandlerFactory::AnnotateMotionHandlerFactory() : MotionHandlerFactory() {}

AnnotateMotionHandlerFactory::~AnnotateMotionHandlerFactory() = default;

std::string AnnotateMotionHandlerFactory::getName() {
    return AnnotateMotionHandler::NAME;
}

MotionHandlerPtr AnnotateMotionHandlerFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new AnnotateMotionHandler(widget));
}

MotionHandlerFactoryPtr AnnotateMotionHandlerFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new AnnotateMotionHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new AnnotateMotionHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
