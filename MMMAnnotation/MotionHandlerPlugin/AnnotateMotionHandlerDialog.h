/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_ANNOTATEMOTIONHANDLERDIALOG_H_
#define __MMM_ANNOTATEMOTIONHANDLERDIALOG_H_

#include <QDialog>
#include <QListWidgetItem>
#include <QSignalMapper>
#include <map>
#include <QGraphicsScene>
#include <QMainWindow>
#include <QGraphicsView>
#include <QMouseEvent>
#include <QObject>
#include <set>
#include <Inventor/sensors/SoTimerSensor.h>
#include <QSettings>

#include "SegmentationScene.h"

#ifndef Q_MOC_RUN
#include <MMM/MMMCore.h>
#endif

namespace Ui {

class AnnotateMotionHandlerDialog;
}

class AnnotateMotionHandlerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AnnotateMotionHandlerDialog(QWidget* parent);
    ~AnnotateMotionHandlerDialog();

    void segmentMotion(MMM::MotionRecordingPtr motions);

    void moveSlider(float timestep);


private slots:
    void addTemplate();
    void showInfoBox(bool value);

signals:
    void jumpTo(float timestep);
    void changeAnnotation(QString& string);
    void saveMotion();

private:
    Ui::AnnotateMotionHandlerDialog *ui;

    MMM::MotionRecordingPtr motions;
    MMM::SegmentationScene* scene;
    QSettings settings;


};

#endif // __MMM_ANNOTATEMOTIONHANDLERDIALOG_H_
