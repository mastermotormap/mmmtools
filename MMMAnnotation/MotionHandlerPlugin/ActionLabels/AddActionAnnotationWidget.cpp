#include "AddActionAnnotationWidget.h"

#include <QGridLayout>
#include <QComboBox>
#include <QLabel>

#include <MMM/Motion/Annotation/ActionLabel/ActionLabelAnnotation.h>
#include <MMM/Motion/Segmentation/MotionSegment.h>
#include <MMM/Motion/MotionRecording.h>

namespace MMM
{

AddActionAnnotationWidget::AddActionAnnotationWidget(MotionSegmentPtr segment, QWidget *parent) : AddAnnotationWidget(parent), segment(segment)
{
    QGridLayout* gridLayout = new QGridLayout();
    setLayout(gridLayout);

    int row = 0;
    QComboBox* annotationBox = new QComboBox();
    gridLayout->addWidget(new QLabel("Action label"), row, 0);
    gridLayout->addWidget(annotationBox, row, 1);
    std::vector<std::string> actionLabels(ActionLabel::_names().begin(), ActionLabel::_names().end());
    std::sort(actionLabels.begin(), actionLabels.end());
    for (const auto &name : actionLabels) {
        annotationBox->addItem(QString::fromStdString(name));
    }

    QComboBox* primaryBox = new QComboBox();
    gridLayout->addWidget(new QLabel("Main object"), ++row, 0);
    gridLayout->addWidget(primaryBox, row, 1);

    QComboBox* secondaryBox = new QComboBox();
    gridLayout->addWidget(new QLabel("Main object 2"), ++row, 0);
    gridLayout->addWidget(secondaryBox, row, 1);

    QComboBox* sourceBox = new QComboBox();
    gridLayout->addWidget(new QLabel("Source"), ++row, 0);
    gridLayout->addWidget(sourceBox, row, 1);

    QComboBox* targetBox = new QComboBox();
    gridLayout->addWidget(new QLabel("Target"), ++row, 0);
    gridLayout->addWidget(targetBox, row, 1);

    QComboBox* failureBox = new QComboBox();
    gridLayout->addWidget(new QLabel("Failure"), ++row, 0);
    gridLayout->addWidget(failureBox, row, 1);

    // Add items
    primaryBox->addItem("None");
    secondaryBox->addItem("None");
    sourceBox->addItem("None");
    targetBox->addItem("None");

    auto motionRecording = segment->getMotionRecording();
    std::vector<MotionPtr> motions(motionRecording->begin(), motionRecording->end());
    std::sort(motions.begin(), motions.end(), [] (const auto &a, const auto &b) { return a->getName() < b->getName(); });
    for (const auto &m : motions) {
        std::vector<std::string> names;
        if (m->isReferenceModelMotion()) {
            // TODO: improve this hard coded stuff!
            names.push_back(m->getName() + ActionObject::SEPARATOR + "Hand L Root");
            names.push_back(m->getName() + ActionObject::SEPARATOR + "Hand R Root");
        }
        else {
            names.push_back(m->getName());
        }
        for (const std::string &name : names) {
            primaryBox->addItem(QString::fromStdString(name));
            secondaryBox->addItem(QString::fromStdString(name));
            sourceBox->addItem(QString::fromStdString(name));
            targetBox->addItem(QString::fromStdString(name));
        }
    }

    for (const auto &name : Failure::_names()) {
        failureBox->addItem(QString::fromStdString(name));
    }
    failureBox->setCurrentText(QString::fromStdString((+Failure::None)._to_string()));

    auto annotation = segment->getDerivedAnnotation<ActionLabelAnnotation>();
    if (annotation) {
        annotationBox->setCurrentText(QString::fromStdString(annotation->getLabel()._to_string()));
        if (auto o = annotation->getMainObject())
            primaryBox->setCurrentText(QString::fromStdString(o->str()));
        if (auto o = annotation->getSecondMainObject())
            secondaryBox->setCurrentText(QString::fromStdString(o->str()));
        if (auto o = annotation->getSource())
            sourceBox->setCurrentText(QString::fromStdString(o->str()));
        if (auto o = annotation->getTarget())
            targetBox->setCurrentText(QString::fromStdString(o->str()));
        failureBox->setCurrentText(QString::fromStdString(annotation->getFailure()._to_string()));
    }
    else {
        annotationBox->setCurrentIndex(-1);
    }

    connect(annotationBox, &QComboBox::currentTextChanged, this, &AddActionAnnotationWidget::addAnnotation);
    connect(primaryBox, &QComboBox::currentTextChanged, this, &AddActionAnnotationWidget::setMainObject);
    connect(secondaryBox, &QComboBox::currentTextChanged, this, &AddActionAnnotationWidget::setSecondaryMainObject);
    connect(sourceBox, &QComboBox::currentTextChanged, this, &AddActionAnnotationWidget::setSource);
    connect(targetBox, &QComboBox::currentTextChanged, this, &AddActionAnnotationWidget::setTarget);
    connect(failureBox, &QComboBox::currentTextChanged, this, &AddActionAnnotationWidget::setFailure);
}

void AddActionAnnotationWidget::addAnnotation(const QString &name)  {
    auto annotations = segment->getDerivedAnnotations<ActionLabelAnnotation>();
    if (annotations.size() > 0) {
        annotations.front()->setLabel(ActionLabel::_from_string(name.toStdString().c_str()));
    }
    else {
        auto annotation = std::shared_ptr<ActionLabelAnnotation>(new ActionLabelAnnotation(ActionLabel::_from_string(name.toStdString().c_str())));
        segment->addAnnotation(annotation);
    }
    emit updated();
}

void AddActionAnnotationWidget::setMainObject(const QString &name) {
    mainObject = name;
    if (auto annotation = segment->getDerivedAnnotation<ActionLabelAnnotation>())
        annotation->setMainObject(name != "None" ? name.toStdString() : std::string());
}

void AddActionAnnotationWidget::setSecondaryMainObject(const QString &name) {
    mainObject2 = name;
    if (auto annotation = segment->getDerivedAnnotation<ActionLabelAnnotation>())
        annotation->setSecondMainObject(name != "None" ? name.toStdString() : std::string());
}

void AddActionAnnotationWidget::setSource(const QString &name) {
    source = name;
    if (auto annotation = segment->getDerivedAnnotation<ActionLabelAnnotation>())
        annotation->setSource(name != "None" ? name.toStdString() : std::string());
}

void AddActionAnnotationWidget::setTarget(const QString &name) {
    target = name;
    if (auto annotation = segment->getDerivedAnnotation<ActionLabelAnnotation>())
        annotation->setTarget(name != "None" ? name.toStdString() : std::string());
}

void AddActionAnnotationWidget::setFailure(const QString &name) {
    failure = name;
    if (auto annotation = segment->getDerivedAnnotation<ActionLabelAnnotation>())
        annotation->setFailure(name.toStdString());
}

}
