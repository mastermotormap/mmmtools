/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SEGMENTATIONRECT_H_
#define __MMM_SEGMENTATIONRECT_H_

#include <QPainter>
#include <QGraphicsItem>
#include <QDebug>

#include <QApplication>
#include <QColor>
#include <QPen>

#include <QGraphicsSceneHoverEvent>
#include <QObject>

#include <SimoxUtility/color/KellyLUT.h>

#include <MMM/Motion/Segmentation/SegmentObserver.h>

#include <MMM/MMMCore.h>

namespace MMM
{

class SegmentationTrack;
class SegmentationScene;

class SegmentationRect : public QObject, public QGraphicsItem
{
    Q_OBJECT

    class Observer : public MotionSegmentObserver {
    public:
        Observer(SegmentationRect* s);

        virtual ~Observer() = default;

        void notifySegmentResized();

        SegmentationRect* s;
    };

    friend class Observer;

public:
    SegmentationRect(SegmentationScene* scene, SegmentationTrack* parent, MMM::MotionSegmentPtr segment, qreal top, qreal height);

    virtual ~SegmentationRect();

    QRectF boundingRect() const override;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

protected:
    qreal   top;
    qreal   height;

    simox::color::Color color;
    simox::color::Color highlightColor;
    bool isHighlight;

    bool isResizeLeft;
    bool isResizeRight;
    bool resized;

    bool hovered;

    SegmentationScene* scene;
    std::shared_ptr<MotionSegmentObserver> observer;

    void highlight(bool value);

public:
    SegmentationTrack* parent;

    MMM::MotionSegmentPtr segment;

    bool keyPressed(QKeyEvent* event, QPointF mousePosScene);

    bool keyReleased(QKeyEvent* event, QPointF mousePosScene);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;

    void hoverEnterEvent(QGraphicsSceneHoverEvent *event) override;
    void hoverMoveEvent(QGraphicsSceneHoverEvent *event) override;
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) override;

    void updateHovered(qreal x, qreal y);

private slots:
    void updateAnnotation();
};

struct seg_rect_compare {
    bool operator() (const SegmentationRect* a, const SegmentationRect* b) const {
        return a < b;
    }
};


}
#endif
