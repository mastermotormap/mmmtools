﻿/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SEGMENTATIONTRACK_H_
#define __MMM_SEGMENTATIONTRACK_H_

#include <QObject>
#include <SimoxUtility/color/KellyLUT.h>
#include <memory>

#include <MMM/MMMCore.h>

#include "SegmentationRect.h"

namespace MMM
{

class SegmentationScene;

class SegmentationTrack : public QObject
{
    Q_OBJECT

public:   
    SegmentationTrack(SegmentationScene* scene, SegmentationRect* parent, qreal position, MMM::MotionSegmentationPtr segmentation, bool createSegmentationRec = true);

    ~SegmentationTrack();

    bool keyPressed(QKeyEvent* key, QPointF mousePosition);

    bool keyReleased(QKeyEvent* key, QPointF mousePosition);

    std::vector<SegmentationTrack*> createChildTracks(qreal position);

    qreal getPosition();

    void addRect(SegmentationRect* rectangle);

    SegmentationRect* parent;

    MMM::MotionSegmentationPtr getSegmentation();

    void split(SegmentationRect* rect, qreal x);

    void remove(SegmentationRect* rect, qreal x);

protected:
    SegmentationScene* scene;
    std::vector<SegmentationRect*> rects;
    qreal position;
    MMM::MotionSegmentationPtr segmentation;


};

}

#endif // SEGMENTATIONTRACK_H
