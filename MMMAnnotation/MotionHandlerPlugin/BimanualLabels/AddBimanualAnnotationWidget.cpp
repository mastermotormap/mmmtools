#include "AddBimanualAnnotationWidget.h"

#include <QGridLayout>
#include <QComboBox>
#include <QLabel>

#include <MMM/Motion/Annotation/BimanualLabel/BimanualAnnotation.h>
#include <MMM/Motion/Segmentation/MotionSegment.h>
#include <MMM/Motion/MotionRecording.h>

namespace MMM
{

AddBimanualAnnotationWidget::AddBimanualAnnotationWidget(MotionSegmentPtr segment, QWidget *parent) : AddAnnotationWidget(parent), segment(segment)
{
    QGridLayout* gridLayout = new QGridLayout();
    setLayout(gridLayout);

    QComboBox* annotationBox = new QComboBox();
    gridLayout->addWidget(new QLabel("Bimanual label"), 0, 0);
    gridLayout->addWidget(annotationBox, 0, 1);
    std::set<std::string> label; // sort labels after name
    for (const auto &name : BimanualLabel::_names()) {
        label.insert(name);
    }
    for (const auto &name : label) {
        annotationBox->addItem(QString::fromStdString(name));
    }

    auto annotation = segment->getDerivedAnnotation<BimanualAnnotation>();
    if (annotation) {
        annotationBox->setCurrentText(QString::fromStdString(annotation->getLabel()._to_string()));
    }
    else {
        annotationBox->setCurrentIndex(-1);
    }

    connect(annotationBox, &QComboBox::currentTextChanged, this, &AddBimanualAnnotationWidget::addAnnotation);
}

void AddBimanualAnnotationWidget::addAnnotation(const QString &name)  {
    auto annotations = segment->getDerivedAnnotations<BimanualAnnotation>();
    if (annotations.size() > 0) {
        annotations.front()->setLabel(BimanualLabel::_from_string(name.toStdString().c_str()));
    }
    else {
        auto annotation = std::shared_ptr<BimanualAnnotation>(new BimanualAnnotation(BimanualLabel::_from_string(name.toStdString().c_str())));
        segment->addAnnotation(annotation);
    }
    emit updated();
}


}
