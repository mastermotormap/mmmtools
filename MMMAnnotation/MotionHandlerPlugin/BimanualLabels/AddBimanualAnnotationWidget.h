/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Franziska Krebs
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#pragma once
#include <QWidget>

#include <MMM/MMMCore.h>
#include <MMM/Motion/Annotation/Annotation.h>
#include <MMM/Motion/Annotation/BimanualLabel/BimanualAnnotation.h>

#include "../AddAnnotationWidget.h"

namespace MMM
{

class BimanualAnnotation;

class AddBimanualAnnotationWidget : public AddAnnotationWidget
{
    Q_OBJECT
public:
    AddBimanualAnnotationWidget(MotionSegmentPtr segment, QWidget *parent = nullptr);

private slots:
    void addAnnotation(const QString &name);


private:
    MotionSegmentPtr segment;
};

}

