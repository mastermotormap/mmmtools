#include "SegmentationSlider.h"

#include <QPainter>

namespace MMM
{

SegmentationSlider::SegmentationSlider(qreal pos, qreal size, qreal currentValue, QColor color, qreal width) :
    QGraphicsLineItem(currentValue, pos, currentValue, size),
    currentValue(currentValue),
    pos(pos),
    size(size)
{
    setPen(QPen(color, width));
}

void SegmentationSlider::setCurrentValue(qreal value) {
    prepareGeometryChange();
    currentValue = value;
    updateVis();
}

qreal SegmentationSlider::getCurrentValue() {
    return currentValue;
}

void SegmentationSlider::setCurrentSize(qreal value) {
    prepareGeometryChange();
    size = value;
    updateVis();
}

void SegmentationSlider::updateVis() {
    setLine(currentValue, pos, currentValue, size);
}




}
