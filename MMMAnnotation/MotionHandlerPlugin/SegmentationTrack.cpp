#include "SegmentationTrack.h"

#include <QGraphicsScene>
#include <iostream>

#include <MMM/Motion/MotionRecording.h>
#include <MMM/Motion/Segmentation/AbstractMotionSegment.h>
#include <MMM/Motion/Segmentation/MotionSegment.h>
#include <MMM/Motion/Segmentation/Segmentation.h>

#include "SegmentationScene.h"

namespace MMM
{

SegmentationTrack::SegmentationTrack(SegmentationScene *scene, SegmentationRect* parent, qreal position, MMM::MotionSegmentationPtr segmentation, bool createSegmentationRec) :
    parent(parent),
    scene(scene),
    position(position),
    segmentation(segmentation)
{
    if (createSegmentationRec) {
        auto segment = segmentation->addSegment(segmentation->getMinimumTimestep(), segmentation->getMaximumTimestep(), "");
        SegmentationRect* rect = new SegmentationRect(scene, this, segment, this->position, 50);
        addRect(rect);
    }
}

SegmentationTrack::~SegmentationTrack()
{
}

bool SegmentationTrack::keyPressed(QKeyEvent *key, QPointF mousePosition) {
    for (auto rec : rects)
        if (rec->keyPressed(key, mousePosition)) return true;
    return false;
}

bool SegmentationTrack::keyReleased(QKeyEvent *key, QPointF mousePosition) {
    for (auto rec : rects)
        if (rec->keyReleased(key, mousePosition)) return true;
    return false;
}

std::vector<SegmentationTrack*> SegmentationTrack::createChildTracks(qreal position) {
    std::vector<SegmentationTrack*> childTracks;

    for (auto rect : rects) {
        auto s = rect->segment->createChildSegmentation();
        auto track = new MMM::SegmentationTrack(scene, rect, position, segmentation, false);
        for (const auto &s : s->getSegments()) {
            auto rec = new MMM::SegmentationRect(scene, track, s.second, position, scene->track_height);
            track->addRect(rec);
        }
        childTracks.push_back(track);
    }

    return childTracks;
}


qreal SegmentationTrack::getPosition() {
    return position;
}

void SegmentationTrack::split(SegmentationRect *rect, qreal x) {
    if (std::abs(x - scene->getCurrentValue()) < 2 * scene->getResizeMargin()) {
        x = scene->getCurrentValue(); // snap to slider
    }
    auto segment = rect->segment;
    auto segmentation = segment->getParentSegmentation();
    if (segmentation) {
        scene->setPreviousSegment();
        if (segmentation->split(scene->toTimestep(x))) {
            scene->storePreviousSegment(true);
            scene->reload(scene->clear());
        }
    }
}

void SegmentationTrack::remove(SegmentationRect *rect, qreal x) {
    auto segment = rect->segment;
    auto segmentation = segment->getParentSegmentation();
    if (segmentation) {
        scene->setPreviousSegment();
        if (segmentation->remove(scene->toTimestep(x))) {
            scene->storePreviousSegment(true);
            scene->reload(scene->clear());
        }
    }
}

void SegmentationTrack::addRect(SegmentationRect* rectangle) {
    rects.push_back(rectangle);
    scene->addItem(rectangle);
}

MotionSegmentationPtr SegmentationTrack::getSegmentation() {
    return segmentation;
}

}
