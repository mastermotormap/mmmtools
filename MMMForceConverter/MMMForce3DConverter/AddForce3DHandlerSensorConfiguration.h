/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2018 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_ADDForce3DHANDLERSENSORCONFIGURATION_H_
#define __MMM_ADDForce3DHANDLERSENSORCONFIGURATION_H_

#include "../../common/AddAttachedSensor/AddAttachedSensorConfiguration.h"
#include <MMM/Motion/Plugin/Force3DPlugin/Force3DSensor.h>

namespace MMM
{

class AddForce3DHandlerSensorConfiguration : public AddAttachedSensorConfiguration<Force3DSensor>
{
public:
    AddForce3DHandlerSensorConfiguration();

protected:
    void addSensorMeasurements(const std::vector<std::map<std::string, std::string> > &values);

};

typedef std::shared_ptr<AddForce3DHandlerSensorConfiguration> AddForce3DHandlerSensorConfigurationPtr;

}

#endif // __MMM_ADDForce3DHANDLERSENSORCONFIGURATION_H_

