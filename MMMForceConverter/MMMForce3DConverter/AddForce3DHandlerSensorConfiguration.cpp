#include "AddForce3DHandlerSensorConfiguration.h"

using namespace MMM;


AddForce3DHandlerSensorConfiguration::AddForce3DHandlerSensorConfiguration() : AddAttachedSensorConfiguration<Force3DSensor>("Force3D", {"ForceX", "ForceY", "ForceZ"})
{
}

void AddForce3DHandlerSensorConfiguration::addSensorMeasurements(const std::vector<std::map<std::string, std::string> > &values) {
    for (std::map<std::string, std::string> map : values) {
        try {
            float timestep = simox::alg::to_<float>(map[TIMESTEP_STR]);
            Eigen::Vector3f force(simox::alg::to_<float>(map["ForceX"]),
                                  simox::alg::to_<float>(map["ForceY"]),
                                  simox::alg::to_<float>(map["ForceZ"]));
            Force3DSensorMeasurementPtr measurement(new Force3DSensorMeasurement(timestep, force));
            sensor->addSensorMeasurement(measurement);
        }
        catch (simox::error::SimoxError &e) {
            throw MMM::Exception::MMMException(e.what());
        }
    }
}

