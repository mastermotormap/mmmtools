#include "AddForce3DHandlerFactory.h"
#include "AddForce3DHandler.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry AddForce3DHandlerFactory::registry(AddForce3DHandler::NAME, &AddForce3DHandlerFactory::createInstance);

AddForce3DHandlerFactory::AddForce3DHandlerFactory() : MotionHandlerFactory() {}

AddForce3DHandlerFactory::~AddForce3DHandlerFactory() {}

std::string AddForce3DHandlerFactory::getName() {
    return AddForce3DHandler::NAME;
}

MotionHandlerPtr AddForce3DHandlerFactory::createMotionHandler(QWidget* parent) {
    return MotionHandlerPtr(new AddForce3DHandler(parent));
}

MotionHandlerFactoryPtr AddForce3DHandlerFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new AddForce3DHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new AddForce3DHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
