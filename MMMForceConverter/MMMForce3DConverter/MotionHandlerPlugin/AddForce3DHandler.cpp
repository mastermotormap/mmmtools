#include "AddForce3DHandler.h"
#include "../AddForce3DHandlerSensorConfiguration.h"

#include <QFileDialog>
#include <QMessageBox>

using namespace MMM;

AddForce3DHandler::AddForce3DHandler(QWidget* parent) :
    AddAttachedSensorHandler<AddForce3DHandlerSensorConfiguration>(parent, "Add 3D force sensor")
{
}

std::string AddForce3DHandler::getName() {
    return NAME;
}
