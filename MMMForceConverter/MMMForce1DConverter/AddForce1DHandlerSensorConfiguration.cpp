#include "AddForce1DHandlerSensorConfiguration.h"

#include <SimoxUtility/algorithm/string.h>

using namespace MMM;


AddForce1DHandlerSensorConfiguration::AddForce1DHandlerSensorConfiguration() : AddAttachedSensorConfiguration<Force1DSensor>("force1D", {"Force"})
{
}

void AddForce1DHandlerSensorConfiguration::addSensorMeasurements(const std::vector<std::map<std::string, std::string> > &values) {
    for (std::map<std::string, std::string> map : values) {
        try {
            float timestep = simox::alg::to_<float>(map[TIMESTEP_STR]);
            float force = simox::alg::to_<float>(map["Force"]);
            Force1DSensorMeasurementPtr measurement(new Force1DSensorMeasurement(timestep, force));
            sensor->addSensorMeasurement(measurement);
        }
        catch (simox::error::SimoxError &e) {
            throw MMM::Exception::MMMException(e.what());
        }
    }
}

