/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2018 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_ADDFORCE1DHANDLER_H_
#define __MMM_ADDFORCE1DHANDLER_H_

#include "../../../MMMViewer/MMM/Viewer/MotionHandler.h"
#include "../../../common/AddAttachedSensor/AddAttachedSensorDialog.h"
#include "../../../common/AddAttachedSensor/AddAttachedSensorHandler.h"
#include "../AddForce1DHandlerSensorConfiguration.h"

namespace MMM
{

class AddForce1DHandler : public AddAttachedSensorHandler<AddForce1DHandlerSensorConfiguration>
{
    Q_OBJECT

public:
    AddForce1DHandler(QWidget* parent);

    virtual std::string getName();

    static constexpr const char* NAME = "AddForce1DHandler";
};

}

#endif
