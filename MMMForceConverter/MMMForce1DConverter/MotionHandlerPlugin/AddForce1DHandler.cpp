#include "AddForce1DHandler.h"

#include <QFileDialog>
#include <QMessageBox>

using namespace MMM;

AddForce1DHandler::AddForce1DHandler(QWidget* parent) :
    AddAttachedSensorHandler<AddForce1DHandlerSensorConfiguration>(parent, "Add 1D force sensor")
{
}

std::string AddForce1DHandler::getName() {
    return NAME;
}
