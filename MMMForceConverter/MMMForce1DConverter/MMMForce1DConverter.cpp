/*
 * This file is part of MMM.
 *
 * MMM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MMM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MMM.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MMM
 * @author     Andre Meixner <andre.meixner@student.kit.edu>
 * @copyright  2018 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
 *
 */

#include "AddForce1DHandlerSensorConfiguration.h"
#include "../../common/AddAttachedSensor/MMMAttachedSensorConverterConfiguration.h"

int main(int argc, char *argv[])
{
    MMM_INFO << " --- MMMForce1DSensorConverter --- " << std::endl;
    return convert(argc, argv, MMM::AddForce1DHandlerSensorConfigurationPtr(new MMM::AddForce1DHandlerSensorConfiguration()));
}

