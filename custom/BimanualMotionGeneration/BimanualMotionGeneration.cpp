﻿/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include "BimanualMotionGenerationConfiguration.h"

#include <string>
#include <vector>
#include <tuple>
#include<fstream>
#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Model/ModelReaderXML.h>
#include <MMM/Model/ModelProcessorWinter.h>
#include <MMM/Motion/Motion.h>
#include <VirtualRobot/Robot.h>
#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensorMeasurement.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensorMeasurement.h>
#include <MMM/Motion/Plugin/CustomPlugin/CustomSensor.h>
#include <MMM/Motion/Plugin/CustomPlugin/CustomSensorMeasurement.h>
#include <VirtualRobot/IK/CompositeDiffIK/CompositeDiffIK.h>
#include <VirtualRobot/XML/RobotIO.h>
#include <MMM/Exceptions.h>
#include <iomanip>
#include <SimoxUtility/math/convert.h>
#include "rapidcsv.h"
#include <SimoxUtility/math/pose/invert.h>

using namespace MMM;
using namespace std;


int main(int argc, char *argv[]) {
    MMM_INFO << "--- Motion Generation from bimanual task space trajectories ---" << std::endl;

    BimanualMotionGenerationConfiguration *configuration = new BimanualMotionGenerationConfiguration();
    if (!configuration->processCommandLine(argc,argv)) {
        MMM_ERROR << "Could not process command line, aborting." << std::endl;
        return -1;
    }

    configuration->print();

   try {
        MMM::MotionRecordingPtr motions = MMM::MotionRecording::EmptyRecording();
        MMM::MotionPtr motion;
        if (configuration->inputMotionPath.empty()) {
            MMM::ModelReaderXMLPtr reader(new MMM::ModelReaderXML());
            MMM::ModelProcessorWinterPtr processor(new MMM::ModelProcessorWinter(configuration->height, configuration->weight));
            std::filesystem::path modelPath = std::string(MMMTools_SRC_DIR) + "/data/Model/Winter/mmm.xml";
            MMM::ProcessedModelWrapperPtr model = reader->loadMotionModel(modelPath, configuration->outputMotionPath, processor, false);
            motion = MMM::MotionPtr(new MMM::Motion("Motion", model, "", MMM::MotionType::MMM));
            motions->addMotion(motion);

            // set default pose
            MMM::ModelPoseSensorPtr modelPoseSensor(new MMM::ModelPoseSensor());
            Eigen::Matrix4f modelPose = Eigen::Matrix4f::Identity();
            modelPose(2,3) = 540.0f * configuration->height;
            MMM::ModelPoseSensorMeasurementPtr modelPoseSensorMeasurement(new MMM::ModelPoseSensorMeasurement(0.0, modelPose));
            modelPoseSensor->addSensorMeasurement(modelPoseSensorMeasurement);
            motion->addSensor(modelPoseSensor);
        }
        else {
            MMM::MotionReaderXMLPtr reader(new MMM::MotionReaderXML());
            motions = reader->loadMotionRecording(configuration->inputMotionPath);
            motion = motions->getReferenceModelMotion();
            if (!motion) {
                MMM_ERROR << "Could not find mmm motion in " << configuration->inputMotionPath << std::endl;
                return -2;
            }
        }

        auto mmmModel = motion->getModel()->cloneScaling();
        VirtualRobot::RobotNodeSetPtr rns_left = mmmModel->getRobotNodeSet(configuration->leftArmRNS);
        VirtualRobot::RobotNodeSetPtr rns_right = mmmModel->getRobotNodeSet(configuration->rightArmRNS);

        if (!configuration->inputMotionPath.empty()) {
            for (MMM::KinematicSensorPtr kinematicSensor : motion->getSensorsByType<MMM::KinematicSensor>()) {
                for (const std::string &jointName : rns_left->getNodeNames())
                    kinematicSensor->removeJoint(jointName);
                for (const std::string &jointName : rns_right->getNodeNames())
                    kinematicSensor->removeJoint(jointName);}
        }

        MMM::KinematicSensorPtr kinematicSensorLeft(new MMM::KinematicSensor(rns_left->getNodeNames()));
        kinematicSensorLeft->setName(configuration->leftArmRNS);
        MMM::KinematicSensorPtr kinematicSensorRight(new MMM::KinematicSensor(rns_right->getNodeNames()));
        kinematicSensorRight->setName(configuration->rightArmRNS);
        motion->addSensor(kinematicSensorLeft);
        motion->addSensor(kinematicSensorRight);
        MMM::CustomSensorPtr customSensor(new MMM::CustomSensor());
        motion->addSensor(customSensor);

        rapidcsv::Document leftHandFile(configuration->inputLeftArmPath, rapidcsv::LabelParams(0, -1));
        rapidcsv::Document rightHandFile(configuration->inputRightArmPath, rapidcsv::LabelParams(0, -1));

        VirtualRobot::CompositeDiffIK::Parameters cp;
        cp.resetRnsValues = false;
        cp.returnIKSteps = true;

        auto sensor = motion->getSensorByType<BasicModelPoseSensor>();
        if (!sensor)  {
            MMM_ERROR << "No model pose in motion!" << std::endl;
            return -3;
        }
        std::vector<float> timesteps = sensor->getTimesteps();

        size_t index = 1;
        while (index < leftHandFile.GetRowCount() && index < rightHandFile.GetRowCount()) {
            cp.steps = index == 1 ? 600 : 10;

            Eigen::Vector3f leftHandPosition = Eigen::Vector3f(leftHandFile.GetCell<float>("RootPosition_X", index), leftHandFile.GetCell<float>("RootPosition_Y", index), leftHandFile.GetCell<float>("RootPosition_Z", index)) * (configuration->meter ? 1000.0f : 1.0f);
            Eigen::Vector3f rightHandPosition = Eigen::Vector3f(rightHandFile.GetCell<float>("RootPosition_X", index), rightHandFile.GetCell<float>("RootPosition_Y", index), rightHandFile.GetCell<float>("RootPosition_Z", index)) * (configuration->meter ? 1000.0f : 1.0f);
            Eigen::Quaternionf leftHandOrientation(leftHandFile.GetCell<float>("RootOrientation_qw", index), leftHandFile.GetCell<float>("RootOrientation_qx", index), leftHandFile.GetCell<float>("RootOrientation_qy", index), leftHandFile.GetCell<float>("RootOrientation_qz", index));
            Eigen::Quaternionf rightHandOrientation(rightHandFile.GetCell<float>("RootOrientation_qw", index), rightHandFile.GetCell<float>("RootOrientation_qx", index), rightHandFile.GetCell<float>("RootOrientation_qy", index), rightHandFile.GetCell<float>("RootOrientation_qz", index));
            //leftHandOrientation *= simox::math::rpy_to_quat(M_PI, 0, 0);
            //rightHandOrientation *= simox::math::rpy_to_quat(M_PI, 0, 0);

            Eigen::Matrix4f leftHandPose = simox::math::pos_quat_to_mat4f(leftHandPosition, leftHandOrientation);
            Eigen::Matrix4f rightHandPose = simox::math::pos_quat_to_mat4f(rightHandPosition, rightHandOrientation);

            // Assumption right and left hand start with same timestep (TODO: test)
            float timestep = configuration->delta > 0.0f ? index * configuration->delta : leftHandFile.GetCell<float>("timestep", index);
            float rootPose_timestep = timesteps.size() > 1 ? timestep : timesteps[0];
            motion->initializeModel(mmmModel, rootPose_timestep);
            if (configuration->global) {
                leftHandPose = mmmModel->toLocalCoordinateSystem(leftHandPose);
                rightHandPose = mmmModel->toLocalCoordinateSystem(rightHandPose);
            }

            std::vector<std::string> nodeNames = rns_left->getNodeNames();
            std::vector<std::string> rightNodeNames = rns_right->getNodeNames();
            nodeNames.insert(nodeNames.end(), rightNodeNames.begin(), rightNodeNames.end());
            auto mergedRNS = VirtualRobot::RobotNodeSet::createRobotNodeSet(mmmModel, "MergedRNS", nodeNames, mmmModel->getRootNode()->getName());

            VirtualRobot::CompositeDiffIKPtr ik(new VirtualRobot::CompositeDiffIK(mergedRNS));
            ik->addTarget(mmmModel->getRobotNode("Hand L Root"), leftHandPose, VirtualRobot::IKSolver::All);
            ik->addTarget(mmmModel->getRobotNode("Hand R Root"), rightHandPose, VirtualRobot::IKSolver::All);
            VirtualRobot::CompositeDiffIK::NullspaceJointLimitAvoidancePtr nsjla(new VirtualRobot::CompositeDiffIK::NullspaceJointLimitAvoidance(rns_left));
            nsjla->kP = 0.1f;
            ik->addNullspaceGradient(nsjla);

            auto result = ik->solve(cp);

            MMM::KinematicSensorMeasurementPtr kinematicSensorLeftMeasurement(new MMM::KinematicSensorMeasurement(timestep, rns_left->getJointValuesEigen()));
            kinematicSensorLeft->addSensorMeasurement(kinematicSensorLeftMeasurement);
            MMM::KinematicSensorMeasurementPtr kinematicSensorRightMeasurement(new MMM::KinematicSensorMeasurement(timestep, rns_right->getJointValuesEigen()));
            kinematicSensorRight->addSensorMeasurement(kinematicSensorRightMeasurement);

            MMM::CustomPosePtr poseLeft_custom(new MMM::CustomPose("Left", leftHandPosition, leftHandOrientation));
            MMM::CustomPosePtr poseRight_custom(new MMM::CustomPose("Right", simox::math::mat4f_to_pos(rightHandPose), simox::math::mat4f_to_quat(rightHandPose), mmmModel->getRootNode()->getName()));
            MMM::CustomSensorMeasurementPtr customMeasurement(new MMM::CustomSensorMeasurement(timestep, {{"Left", poseLeft_custom}, {"Right", poseRight_custom}}));
            customSensor->addSensorMeasurement(customMeasurement);

            MMM_INFO << "Step\t" << index << " - " << timestep << " - reached " << simox::alg::to_string(result.reached) << std::endl;
            index++;
        }


        motions->saveXML(configuration->outputMotionPath);

        MMM_INFO << "--- END ---" << std::endl;
        return 0;
    }
    catch (MMM::Exception::MMMException &e) {
        MMM_ERROR << e.what() << std::endl;
        return -2;
    }
}
