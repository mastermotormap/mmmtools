/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#pragma once

#include <filesystem>
#include <string>

#include <MMM/MMMCore.h>

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <SimoxUtility/algorithm/string.h>

#include "../../common/ApplicationBaseConfiguration.h"

#include <VirtualRobot/RuntimeEnvironment.h>

/*!
    Configuration of BimanualMotionGeneration.
*/
class BimanualMotionGenerationConfiguration : public ApplicationBaseConfiguration
{

public:
    std::filesystem::path inputLeftArmPath;
    std::filesystem::path inputRightArmPath;
    std::filesystem::path inputMotionPath;
    std::filesystem::path outputMotionPath;
    std::string leftArmRNS;
    std::string rightArmRNS;
    float height;
    float weight;
    float delta;
    bool global;
    bool meter;
    bool valid = true;

    BimanualMotionGenerationConfiguration() : ApplicationBaseConfiguration()
    {
    }

    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("leftArm", "left arm task space file path");
        VirtualRobot::RuntimeEnvironment::considerKey("rightArm", "right arm task space file path");
        VirtualRobot::RuntimeEnvironment::considerKey("leftArmRNS", "left arm task space file path");
        VirtualRobot::RuntimeEnvironment::considerKey("rightArmRNS", "right arm task space file path");
        VirtualRobot::RuntimeEnvironment::considerKey("inputMotion", "input mmm motion file path");
        VirtualRobot::RuntimeEnvironment::considerKey("outputMotion", "mmm motion file path");
        VirtualRobot::RuntimeEnvironment::considerKey("weight", "weight of the mmm model (in kg)");
        VirtualRobot::RuntimeEnvironment::considerKey("height", "height of the mmm model (in m)");
        VirtualRobot::RuntimeEnvironment::considerKey("delta", "timestep delta between frames (in s)");
        VirtualRobot::RuntimeEnvironment::considerFlag("global", "specify if the trajectories are in global coordinate system");
        VirtualRobot::RuntimeEnvironment::considerFlag("meter", "specify if the trajectories are meter instead of mm");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);

        inputLeftArmPath = getParameter("leftArm", true, true);
        inputRightArmPath = getParameter("rightArm", true, true);
        inputMotionPath = getParameter("inputMotion", false, true);
        outputMotionPath = getParameter("outputMotion", false, true);
        if (outputMotionPath.empty()) {
            std::string defaultMotionPath = std::string(MMMTools_SRC_DIR) + "/data/Motions/output.xml";
            outputMotionPath = defaultMotionPath;
        }

        leftArmRNS = getParameter("leftArmRNS", false, false);
        if (leftArmRNS.empty()) leftArmRNS = "LeftArm-7dof";
        rightArmRNS = getParameter("rightArmRNS", false, false);
        if (rightArmRNS.empty()) rightArmRNS = "RightArm-7dof";

        if (VirtualRobot::RuntimeEnvironment::hasValue("weight")) {
            weight = simox::alg::to_<float>(VirtualRobot::RuntimeEnvironment::getValue("weight"));
        }
        else weight = 70.0f;

        if (VirtualRobot::RuntimeEnvironment::hasValue("height")) {
            height = simox::alg::to_<float>(VirtualRobot::RuntimeEnvironment::getValue("height"));
        }
        else height = 1.8f;

        if (VirtualRobot::RuntimeEnvironment::hasValue("delta")) {
            delta = simox::alg::to_<float>(VirtualRobot::RuntimeEnvironment::getValue("delta"));
        }
        else delta = 0.0f;

        global = VirtualRobot::RuntimeEnvironment::hasFlag("global");
        meter = VirtualRobot::RuntimeEnvironment::hasFlag("meter");

        VirtualRobot::RuntimeEnvironment::print();

        return valid;
    }

    void print()
    {
        MMM_INFO << "*** BimanualMotionGeneration Configuration ***" << std::endl;
        std::cout << "Input motion: " << outputMotionPath << std::endl;
    }

};
