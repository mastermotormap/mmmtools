/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_CustomTestCONFIGURATION_H_
#define __MMM_CustomTestCONFIGURATION_H_

#include <filesystem>
#include <string>
#include <VirtualRobot/RuntimeEnvironment.h>
#include <MMM/MMMCore.h>

#include "../../common/ApplicationBaseConfiguration.h"

/*!
    Configuration of MotionDataExporter.
*/
class MMMMotionDataExporterConfiguration : public ApplicationBaseConfiguration
{
public:
    MMMMotionDataExporterConfiguration() : ApplicationBaseConfiguration()
    {
    }

    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("inputDirectory");
        VirtualRobot::RuntimeEnvironment::considerKey("outputDirectory");
        VirtualRobot::RuntimeEnvironment::considerKey("localCoordinateSystem");
        VirtualRobot::RuntimeEnvironment::considerKey("jointNames");
        VirtualRobot::RuntimeEnvironment::considerKey("segmentNames");
        VirtualRobot::RuntimeEnvironment::considerFlag("test");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        inputDirectoryPath = getParameter("inputDirectory", true, false);
        std::filesystem::path outputDir = getParameter("outputDirectory", false, false);

        if (VirtualRobot::RuntimeEnvironment::hasFlag("test")) {
            outputDirectoryPath = (outputDir.empty() ? inputDirectoryPath : outputDir) / "mmm_export_rightarm";
            localPosition = "RSsegment_joint";
            jointNames= {"RSx_joint", "RSy_joint", "RSz_joint", "REx_joint", "REz_joint"};
            segmentNames= {"RWsegment_joint", "REsegment_joint"};
        }
        else {
            outputDirectoryPath = (outputDir.empty() ? inputDirectoryPath : outputDir) / "mmm_export";
            localPosition = getParameter("localCoordinateSystem", false, false);
            auto jointNamesVec = getParameters("jointNames");
            jointNames = std::set<std::string>(jointNamesVec.begin(), jointNamesVec.end());
            auto segmentNamesVec = getParameters("segmentNames");
            segmentNames = std::set<std::string>(segmentNamesVec.begin(), segmentNamesVec.end());
        }

        return valid;
    }

    void print()
    {
        MMM_INFO << "*** Motion Data Exporter Configuration ***" << std::endl;
    }

    std::filesystem::path inputDirectoryPath;
    std::filesystem::path outputDirectoryPath;
    std::string localPosition;
    std::set<std::string> jointNames;
    std::set<std::string> segmentNames;
};


#endif // __MMM_CustomTestCONFIGURATION_H_
