/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include <MMM/Motion/Motion.h>





#include <MMM/Model/LoadModelStrategy.h>
#include <MMM/Model/ModelReaderXML.h>
#include <MMM/Model/ModelProcessorWinter.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>
#include <SimoxUtility/math/pose.h>
#include <SimoxUtility/math/distance.h>
#include <SimoxUtility/math/convert.h>
#include <string>
#include <vector>
#include <thread>

#ifndef __MMM_MOTIONDATAEXPORTER_H_
#define __MMM_MOTIONDATAEXPORTER_H_

namespace MMM
{

class MotionDataExporter
{
public:
    MotionDataExporter(const std::set<std::string> &jointNames, const std::set<std::string> &segmentNames, float size = 1.8f, bool addPath = true, bool addName = true,
                       bool addTimestep = true, const std::string &localPosition = "root_joint", bool addOrientation = false, bool mirrorMotion = false, const std::string &delimiter = ";");

    bool exportData(const std::string &directoryPath, const std::string &outputPath, bool recursive = false, unsigned int threads = 4, const std::string &regex = "", int maxNumber = -1, unsigned int startIndex = 0, bool printLog = false);

private:
    std::string getHeader() const;
    std::string getLine(const std::string &fileName, const std::string &motionName, float timestep, VirtualRobot::RobotPtr robot, bool mirrored = false) const;
    void checkJointNames();
    void checkSegmentNames();
    void checkLocalPosition();
    void writeMotion(MMM::MotionPtr motion, VirtualRobot::RobotPtr robot, std::ofstream &myfile, bool mirrored = false);

    std::set<std::string> jointNames;
    std::set<std::string> segmentNames;
    bool addPath;
    bool addName;
    bool addTimestep;
    std::string localPosition;
    bool addOrientation;
    bool mirrorMotion;
    std::string delimiter;
    VirtualRobot::RobotPtr r;
    std::map<std::string, float> initJointValueMap;
    std::set<std::string> relevantJointNames; // make it probably faster to only set needed joints per timestep
};

typedef std::shared_ptr<MotionDataExporter> MotionDataExporterPtr;

}

#endif // __MMM_MOTIONDATAEXPORTER_H_
