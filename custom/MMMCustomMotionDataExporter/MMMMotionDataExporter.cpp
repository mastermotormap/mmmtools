/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include "MMMMotionDataExporterConfiguration.h"

#include "MotionDataExporter.h"

int main(int argc, char *argv[]) {
    MMM_INFO << " --- CustomTest --- " << std::endl;
    MMMMotionDataExporterConfiguration *configuration = new MMMMotionDataExporterConfiguration();
    if (!configuration->processCommandLine(argc, argv))
    {
        MMM_ERROR << "Error while processing command line, aborting..." << std::endl;
        return -1;
    }

    try {
        MMM::MotionDataExporterPtr exporter(new MMM::MotionDataExporter(configuration->jointNames, configuration->segmentNames, 1.8, true, true, true, configuration->localPosition, true, true, ";"));
        exporter->exportData(configuration->inputDirectoryPath, configuration->outputDirectoryPath, true, 12);
        return 1;
    }
    catch (MMM::Exception::MMMException &e) {
        MMM_ERROR << e.what() << std::endl;
        return -2;
    }
}
