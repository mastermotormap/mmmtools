#include "MotionDataExporter.h"

#include <MMM/Motion/Motion.h>
#include <MMM/Motion/MotionReaderXML.h>
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotNodeSet.h>

namespace MMM
{

MotionDataExporter::MotionDataExporter(const std::set<std::string> &jointNames, const std::set<std::string> &segmentNames, float size, bool addPath, bool addName,
                   bool addTimestep, const std::string &localPosition, bool addOrientation, bool mirrorMotion, const std::string &delimiter) :
    jointNames(jointNames),
    segmentNames(segmentNames),
    addPath(addPath),
    addName(addName),
    addTimestep(addTimestep),
    localPosition(localPosition),
    addOrientation(addOrientation),
    mirrorMotion(mirrorMotion),
    delimiter(delimiter)
{
    MMM::ModelReaderXMLPtr modelReader(new MMM::ModelReaderXML());
    auto model = modelReader->loadModel(std::string(MMMTools_SRC_DIR) + "/data/Model/Winter/mmm.xml");
    MMM::ModelProcessorWinterPtr mpw(new MMM::ModelProcessorWinter(size, 100)); // mass is kind of ignored -> has to be scaled
    auto r = mpw->convertModel(model); // use processedModel for every motion

    initJointValueMap = r->getRobotNodeSet("All")->getJointValueMap();

    checkJointNames();
    checkSegmentNames();
    checkLocalPosition();

    for (const auto &segmentName : segmentNames) {
        VirtualRobot::SceneObjectPtr node = r->getRobotNode(segmentName);
        do {
            std::string nodeName = node->getName();
            if (simox::alg::to_lower(nodeName).find("segment") == std::string::npos) {
                relevantJointNames.insert(nodeName);
            }
            node = node->getParent();
        }
        while (node);
    }
}

bool MotionDataExporter::exportData(const std::string &directoryPath, const std::string &outputPath, bool recursive, unsigned int threads, const std::string &regex, int maxNumber, unsigned int startIndex, bool printLog) {
    MMM::MotionReaderXMLPtr mr(new MMM::MotionReaderXML(true, true));
    MMM::LoadModelStrategy::LOAD_MODEL_STRATEGY = MMM::LoadModelStrategy::Strategy::NONE;
    auto motions = mr->loadAllMotionRecordingsFromDirectory(directoryPath, recursive, threads, regex, maxNumber, startIndex, printLog);

    if (threads > std::thread::hardware_concurrency()) threads = std::thread::hardware_concurrency();
    unsigned int split = motions.size() / threads + 1;
    std::vector<std::thread> threadPool;
    std::atomic<int> n = 0;
    auto start = std::chrono::system_clock::now();
    int thread_number = 1;
    std::string header = getHeader();
    for (unsigned int i = 0; i < motions.size(); i += split) {
        threadPool.push_back(std::thread([this,outputPath,thread_number,header,split,i,start]
                                         (const std::vector<MMM::MotionRecordingPtr> &motions, std::atomic<int> &n) {
            auto robot = r->clone();
            std::ofstream myfile;
            myfile.open(outputPath + std::to_string(thread_number) + ".csv");
            myfile.write(header.c_str(), header.size());
            for (unsigned int j = 0; j < split; j++) {
                unsigned int index = i + j;
                if (index >= motions.size()) continue;
                auto m = motions[index];
                if (m->size() == 0) continue;
                auto motion = m->getReferenceModelMotion();
                if (!motion) continue;
                writeMotion(motion, robot, myfile, false);
                //if (mirrorMotion) writeMotion(MMM::MirrorMotion::mirrorMotion(motion), robot, myfile, true);

                int number = ++n;
                if (number % 50 == 0) {
                    auto current = std::chrono::system_clock::now();
                    std::chrono::duration<double> elapsed_seconds = current - start;
                    MMM_INFO << std::to_string(number) + " of " + std::to_string(motions.size()) + " mmm motions processed. Elapsed time: " + std::to_string(elapsed_seconds.count()) << std::endl;
                }
            }
            myfile.close();
        }, std::ref(motions), std::ref(n)));
        thread_number++;
    }
    for (auto &thread : threadPool) {
        thread.join();
    }

    return true;
}

std::string MotionDataExporter::getHeader() const {
    std::stringstream ss;
    if (addPath) ss << "path" << delimiter;
    if (addName) ss << "name" << delimiter;
    if (addTimestep) ss << "timestep" << delimiter;
    for (auto jointName : jointNames){
        ss << jointName << delimiter;
    }
    for (auto segmentName : segmentNames){
        for (auto add : {"", "_2"}) {
            ss << segmentName << "_x" << add << delimiter;
            ss << segmentName << "_y" << add << delimiter;
            ss << segmentName << "_z" << add << delimiter;
        }
        if (addOrientation) {
            ss << segmentName << "_qx" << delimiter;
            ss << segmentName << "_qy" << delimiter;
            ss << segmentName << "_qz" << delimiter;
            ss << segmentName << "_qw" << delimiter;
        }
    }
    if (mirrorMotion) ss << "mirrored" << delimiter;
    std::string header = ss.str();
    return header.substr(0, header.size() - delimiter.size()) + "\n"; // remove last delimiter;
}

std::string MotionDataExporter::getLine(const std::string &fileName, const std::string &motionName, float timestep, VirtualRobot::RobotPtr robot, bool mirrored) const {
    std::stringstream ss;
    if (addPath) ss << fileName << delimiter;
    if (addName) ss << motionName << delimiter;
    if (addTimestep) ss << timestep << delimiter;
    for (auto jointName : jointNames){
        ss << robot->getRobotNode(jointName)->getJointValue() << delimiter;
    }
    auto localCoordSystem = robot->getRobotNode(localPosition);
    for (auto segmentName : segmentNames){
        auto node = robot->getRobotNode(segmentName);

        auto transMatrix1 = localCoordSystem->getGlobalPose();
        auto localPose1 = simox::math::inverted_pose(transMatrix1) * node->getGlobalPose();
        Eigen::Matrix4f transMatrix2 = Eigen::Matrix4f::Identity();
        transMatrix2.block(0,3,3,1) = transMatrix1.block(0,3,3,1);
        auto localPose2 = simox::math::inverted_pose(transMatrix2) * node->getGlobalPose(); // without orientation

        for (auto localPose : {localPose1, localPose2}) {
            ss << localPose(0,3) << delimiter;
            ss << localPose(1,3) << delimiter;
            ss << localPose(2,3) << delimiter;
        }
        if (addOrientation) {
            auto orientation = node->getGlobalOrientation();
            Eigen::Quaternionf q = simox::math::mat3f_to_quat(orientation);
            ss << q.x() << delimiter;
            ss << q.y() << delimiter;
            ss << q.z() << delimiter;
            ss << q.w() << delimiter;
        }
    }
    ss << mirrored << delimiter;
    std::string header = ss.str();
    return header.substr(0, header.size() - delimiter.size()) + "\n"; // remove last delimiter;
}

void MotionDataExporter::checkJointNames() {
    auto vm = r->getRobotNodeSet("Joints_Revolute")->getJointValueMap();
    for (const auto &jointName : jointNames) {
        if (vm.find(jointName) == vm.end())
            throw MMM::Exception::MMMException("Joint " + jointName + " not part of the mmm model!");
    }
}

void MotionDataExporter::checkSegmentNames() {
    auto vm = r->getRobotNodeSet("Joints_Prismatic")->getJointValueMap();
    for (const auto &segmentName : segmentNames) {
        if (vm.find(segmentName) == vm.end())
            throw MMM::Exception::MMMException("Segment " + segmentName + " not part of the mmm model!");
    }
}

void MotionDataExporter::checkLocalPosition() {
    if (initJointValueMap.find(localPosition) == initJointValueMap.end())
        throw MMM::Exception::MMMException("Segment " + localPosition + " not part of the mmm model!");
}

void MotionDataExporter::writeMotion(MMM::MotionPtr motion, VirtualRobot::RobotPtr robot, std::ofstream &myfile, bool mirrored) {
    if (motion) {
        std::stringstream ss;
        std::string filename = motion->getOriginFilePath().stem();
        std::string* fn = &filename;

        auto modelPoseSensor = motion->getSensorByType<MMM::ModelPoseSensor>();
        auto kinematicSensor = motion->getSensorByType<MMM::KinematicSensor>();

        if (!modelPoseSensor || !kinematicSensor) return;
        robot->setJointValues(initJointValueMap); // reset to zero
        auto jointNames = kinematicSensor->getJointNames();

        for (auto timestep : modelPoseSensor->getTimesteps()) {
            auto globalPose = modelPoseSensor->getDerivedMeasurement(timestep)->getRootPose();
            Eigen::Matrix3f rotRot = globalPose.block(0,0,3,3);
            double hip_delta_angle = simox::math::delta_angle(rotRot, Eigen::Matrix3f::Identity());
            if (std::abs(hip_delta_angle) < 0.3) { // refactor upright angle
                robot->setGlobalPose(globalPose);
                auto kinematicSensorMeasurement = kinematicSensor->getDerivedMeasurement(timestep);
                auto jointAngles = kinematicSensorMeasurement->getJointAngles();
                for (unsigned int i = 0; i < jointNames.size(); i++) {
                    if (relevantJointNames.find(jointNames[i]) != relevantJointNames.end())
                        robot->setJointValue(jointNames[i], jointAngles[i]);
                }

                Eigen::Matrix3f collarRot = robot->getRobotNode("collarSegment_joint")->getGlobalOrientation();
                double upper_body_delta_angle = simox::math::delta_angle(collarRot, Eigen::Matrix3f::Identity());
                if (std::abs(upper_body_delta_angle) < 0.5) { // refactor upper body angle
                    ss << getLine(*fn, motion->getName(), timestep, robot, mirrored);
                }
            }
        }
        myfile.write(ss.str().c_str(), ss.str().size());
    }
}

}
