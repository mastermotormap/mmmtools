#include "C3DMotionImporter.h"
#include "../C3DConverter.h"
#include "C3DMotionImporterDialog.h"
#include <QFileDialog>
#include <QSettings>

using namespace MMM;

C3DMotionImporter::C3DMotionImporter(QWidget* widget) :
    MotionHandler(MotionHandlerType::IMPORT, "Import c3d motion file", "Ctrl+I"),
    widget(widget)
{
}

void C3DMotionImporter::handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> currentRobots) {
    QSettings settings;
    std::filesystem::path motionFilePath = QFileDialog::getOpenFileName(widget, QString::fromStdString(getDescription()), settings.value("motion/searchpath", "").toString(), QString::fromStdString("c3d motion files (*.c3d)")).toStdString();
    if (!motionFilePath.empty()) {
        settings.setValue("motion/searchpath", QString::fromStdString(motionFilePath.parent_path()));
        C3DMotionImporterDialog* dialog = new C3DMotionImporterDialog(motionFilePath, widget);
        settings.setValue("motion/path", QString::fromStdString(motionFilePath));
        settings.setValue("motion/type", QString::fromStdString("C3D"));
        emit openMotions(dialog->getMotions());
    }
}

std::string C3DMotionImporter::getName() {
    return NAME;
}
