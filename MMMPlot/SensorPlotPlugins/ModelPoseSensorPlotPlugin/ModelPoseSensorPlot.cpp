#include "ModelPoseSensorPlot.h"

using namespace MMM;

ModelPoseSensorPlot::ModelPoseSensorPlot(BasicModelPoseSensorPtr sensor) :
    sensor(sensor)
{
}

std::vector<std::string> ModelPoseSensorPlot::getNames() {
    std::vector<std::string> modelPoseNames;
    modelPoseNames.push_back("RootPosition_x");
    modelPoseNames.push_back("RootPosition_y");
    modelPoseNames.push_back("RootPosition_z");
    modelPoseNames.push_back("RootRotation_x");
    modelPoseNames.push_back("RootRotation_y");
    modelPoseNames.push_back("RootRotation_z");
    return modelPoseNames;
}

std::tuple<QVector<double>, QVector<double> > ModelPoseSensorPlot::getPlot(std::string name) {
    std::vector<std::string> results = simox::alg::split(name, "_");
    int index = ((int)results[1][0]) - ((int)'x');

    QVector<double> x(sensor->getTimesteps().size()), y(sensor->getTimesteps().size());
    auto values = results[0] == "RootPosition" ? sensor->getPositions(index) : sensor->getAngles(index);

    unsigned int j = 0;
    for (const auto &v : values) {
        x[j] = v.first;
        y[j] = v.second;
        j++;
    }
    return std::make_tuple(x, y);
}
