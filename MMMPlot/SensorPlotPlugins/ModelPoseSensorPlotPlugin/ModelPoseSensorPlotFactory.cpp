#include "ModelPoseSensorPlotFactory.h"
#include "ModelPoseSensorPlot.h"

#include <boost/extension/extension.hpp>

#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>

using namespace MMM;

// register this factory
SensorPlotFactory::SubClassRegistry ModelPoseSensorPlotFactory::registry(ModelPoseSensorPlot::NAME, &ModelPoseSensorPlotFactory::createInstance);

ModelPoseSensorPlotFactory::ModelPoseSensorPlotFactory() : SensorPlotFactory() {}

ModelPoseSensorPlotFactory::~ModelPoseSensorPlotFactory() {}

std::string ModelPoseSensorPlotFactory::getName()
{
    return ModelPoseSensorPlot::NAME;
}

std::string ModelPoseSensorPlotFactory::getUnit() {
    return "Root pose";
}

std::string ModelPoseSensorPlotFactory::getValueName() {
    return "Root pose";
}

SensorPlotList ModelPoseSensorPlotFactory::createSensorPlots(MotionPtr motion, MotionRecordingPtr /*motions*/) {
    SensorPlotList sensorPlots;
    for (auto sensor : motion->getSensorsByType<BasicModelPoseSensor>()) {
        sensorPlots.push_back(SensorPlotPtr(new ModelPoseSensorPlot(sensor)));
    }
    return sensorPlots;
}

bool ModelPoseSensorPlotFactory::hasSensorPlot(MotionPtr motion, MotionRecordingPtr /*motions*/) {
    return motion->hasSensor(BasicModelPoseSensor::TYPE);
}

SensorPlotFactoryPtr ModelPoseSensorPlotFactory::createInstance(void *)
{
    return SensorPlotFactoryPtr(new ModelPoseSensorPlotFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorPlotFactoryPtr getFactory() {
    return SensorPlotFactoryPtr(new ModelPoseSensorPlotFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorPlotFactory::VERSION;
}
