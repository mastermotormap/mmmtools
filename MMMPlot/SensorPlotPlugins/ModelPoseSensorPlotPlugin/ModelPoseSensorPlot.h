/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2019 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_ModelPoseSENSORPLOT_H_
#define __MMM_ModelPoseSENSORPLOT_H_

#include <MMM/MotionPlot/SensorPlot.h>
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>

#include <Inventor/nodes/SoSeparator.h>
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotNodeSet.h>

namespace MMM
{

class ModelPoseSensorPlot : public SensorPlot
{
public:
    ModelPoseSensorPlot(BasicModelPoseSensorPtr sensor);

    std::vector<std::string> getNames();

    std::tuple<QVector<double>, QVector<double> > getPlot(std::string name);

    std::string getName() {
        return sensor->getUniqueName();
    }

    static constexpr const char* NAME = "ModelPosePlot";

private:
    BasicModelPoseSensorPtr sensor;
};

}

#endif
