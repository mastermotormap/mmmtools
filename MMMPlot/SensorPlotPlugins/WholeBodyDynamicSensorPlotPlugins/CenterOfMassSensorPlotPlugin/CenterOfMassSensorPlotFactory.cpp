#include "CenterOfMassSensorPlotFactory.h"
#include "CenterOfMassSensorPlot.h"

#include <boost/extension/extension.hpp>

#include <MMM/Motion/Plugin/WholeBodyDynamicPlugin/WholeBodyDynamicSensor.h>

using namespace MMM;

// register this factory
SensorPlotFactory::SubClassRegistry CenterOfMassSensorPlotFactory::registry(CenterOfMassSensorPlot::NAME, &CenterOfMassSensorPlotFactory::createInstance);

CenterOfMassSensorPlotFactory::CenterOfMassSensorPlotFactory() : SensorPlotFactory() {}

CenterOfMassSensorPlotFactory::~CenterOfMassSensorPlotFactory() {}

std::string CenterOfMassSensorPlotFactory::getName()
{
    return CenterOfMassSensorPlot::NAME;
}

std::string CenterOfMassSensorPlotFactory::getUnit() {
    return "Center of mass position (in cm)";
}

std::string CenterOfMassSensorPlotFactory::getValueName() {
    return "Center of mass";
}

SensorPlotList CenterOfMassSensorPlotFactory::createSensorPlots(MotionPtr motion, MotionRecordingPtr /*motions*/) {
    SensorPlotList sensorPlots;
    for (auto sensor : motion->getSensorsByType<WholeBodyDynamicSensor>()) {
        sensorPlots.push_back(SensorPlotPtr(new CenterOfMassSensorPlot(sensor)));
    }
    return sensorPlots;
}

bool CenterOfMassSensorPlotFactory::hasSensorPlot(MotionPtr motion, MotionRecordingPtr /*motions*/) {
    return motion->hasSensor(WholeBodyDynamicSensor::TYPE);
}

SensorPlotFactoryPtr CenterOfMassSensorPlotFactory::createInstance(void *)
{
    return SensorPlotFactoryPtr(new CenterOfMassSensorPlotFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorPlotFactoryPtr getFactory() {
    return SensorPlotFactoryPtr(new CenterOfMassSensorPlotFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorPlotFactory::VERSION;
}
