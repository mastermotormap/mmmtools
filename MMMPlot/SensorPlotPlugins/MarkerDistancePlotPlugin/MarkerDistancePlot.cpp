#include "MarkerDistancePlot.h"

using namespace MMM;

MarkerDistancePlot::MarkerDistancePlot(MoCapMarkerSensorPtr sensor, MotionPtr motion) :
    sensor(sensor),
    motion(motion)
{
    if (sensor && motion->getModel()) {
        MoCapMarkerSensorMeasurementPtr measurement = sensor->getDerivedMeasurement(sensor->getMinTimestep());
        for (const auto &labeledMarker : measurement->getLabeledMarker()) {
            if (motion->getModel()->getSensor("MARKER_" + labeledMarker.first))
                markerNames.push_back(labeledMarker.first);
        }
        if (!markerNames.empty()) markerNames.push_back("[SUM]");
        else
            MMM_ERROR << "No matching markers found!" << std::endl;
    }
}

std::vector<std::string> MarkerDistancePlot::getNames() {
    return markerNames;
}

std::tuple<QVector<double>, QVector<double> > MarkerDistancePlot::getPlot(std::string name) {
    auto robot = motion->getModel()->clone();
    QVector<double> x(sensor->getTimesteps().size()), y(sensor->getTimesteps().size());
    int i = 0;
    for (auto timestep : sensor->getTimesteps()) {
        motion->initializeModel(robot, timestep);

        x[i] = timestep;
        auto measurement = sensor->getDerivedMeasurement(timestep);
        if (measurement) {
            if (name == "[SUM]") {
                y[i] = 0;
                for (auto markerName : markerNames) {
                    if (markerName == "[SUM]") continue;
                    auto markerPosition2 = measurement->getLabeledMarker(markerName);
                    auto markerPosition = robot->getSensor("MARKER_" + markerName)->getGlobalPosition();
                    y[i] += (markerPosition - markerPosition2).norm();
                }
            }
            else {
                auto markerPosition2 = measurement->getLabeledMarker(name);
                auto markerPosition = robot->getSensor("MARKER_" + name)->getGlobalPosition();
                y[i] = (markerPosition - markerPosition2).norm();
            }
        }
        else {
            MMM_WARNING << "Not mocap measurement at timestep " + std::to_string(timestep) + " for marker " + name;
            y[i] = 0;
        }

        i++;
    }

    return std::make_tuple(x, y);
}
