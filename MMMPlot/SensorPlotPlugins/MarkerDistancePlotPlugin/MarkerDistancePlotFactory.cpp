#include "MarkerDistancePlotFactory.h"
#include "MarkerDistancePlot.h"

#include <boost/extension/extension.hpp>

#include <MMM/Motion/Plugin/MoCapMarkerPlugin/MoCapMarkerSensor.h>

using namespace MMM;

// register this factory
SensorPlotFactory::SubClassRegistry MarkerDistancePlotFactory::registry(MarkerDistancePlot::NAME, &MarkerDistancePlotFactory::createInstance);

MarkerDistancePlotFactory::MarkerDistancePlotFactory() : SensorPlotFactory() {}

MarkerDistancePlotFactory::~MarkerDistancePlotFactory() {}

std::string MarkerDistancePlotFactory::getName()
{
    return MarkerDistancePlot::NAME;
}

std::string MarkerDistancePlotFactory::getUnit() {
    return "Euclidean Distance (mm)";
}

std::string MarkerDistancePlotFactory::getValueName() {
    return "Marker Distance";
}

SensorPlotList MarkerDistancePlotFactory::createSensorPlots(MotionPtr motion, MotionRecordingPtr /*motions*/) {
    SensorPlotList sensorPlots;
    if (motion->getModel()) {
        for (auto sensor : motion->getSensorsByType<MoCapMarkerSensor>()) {
            sensorPlots.push_back(SensorPlotPtr(new MarkerDistancePlot(sensor, motion)));
        }
    }
    return sensorPlots;
}

bool MarkerDistancePlotFactory::hasSensorPlot(MotionPtr motion, MotionRecordingPtr /*motions*/) {
    return motion->getModel() != nullptr && motion->hasSensor(MoCapMarkerSensor::TYPE);
}

SensorPlotFactoryPtr MarkerDistancePlotFactory::createInstance(void *)
{
    return SensorPlotFactoryPtr(new MarkerDistancePlotFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorPlotFactoryPtr getFactory() {
    return SensorPlotFactoryPtr(new MarkerDistancePlotFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorPlotFactory::VERSION;
}
