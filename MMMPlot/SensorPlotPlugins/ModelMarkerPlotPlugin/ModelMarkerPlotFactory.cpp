#include "ModelMarkerPlotFactory.h"
#include "ModelMarkerPlot.h"

#include <boost/extension/extension.hpp>

#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>

using namespace MMM;

// register this factory
SensorPlotFactory::SubClassRegistry ModelMarkerPlotFactory::registry(ModelMarkerPlot::NAME, &ModelMarkerPlotFactory::createInstance);

ModelMarkerPlotFactory::ModelMarkerPlotFactory() : SensorPlotFactory() {}

ModelMarkerPlotFactory::~ModelMarkerPlotFactory() {}

std::string ModelMarkerPlotFactory::getName()
{
    return ModelMarkerPlot::NAME;
}

std::string ModelMarkerPlotFactory::getUnit() {
    return "Marker position";
}

std::string ModelMarkerPlotFactory::getValueName() {
    return "Marker position";
}

SensorPlotList ModelMarkerPlotFactory::createSensorPlots(MotionPtr motion, MotionRecordingPtr /*motions*/) {
    SensorPlotList sensorPlots;
    for (auto sensor : motion->getSensorsByType<BasicModelPoseSensor>()) {
        sensorPlots.push_back(SensorPlotPtr(new ModelMarkerPlot(sensor, motion)));
    }
    return sensorPlots;
}

bool ModelMarkerPlotFactory::hasSensorPlot(MotionPtr motion, MotionRecordingPtr /*motions*/) {
    return motion->hasSensor(BasicModelPoseSensor::TYPE);
}

SensorPlotFactoryPtr ModelMarkerPlotFactory::createInstance(void *)
{
    return SensorPlotFactoryPtr(new ModelMarkerPlotFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorPlotFactoryPtr getFactory() {
    return SensorPlotFactoryPtr(new ModelMarkerPlotFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorPlotFactory::VERSION;
}
