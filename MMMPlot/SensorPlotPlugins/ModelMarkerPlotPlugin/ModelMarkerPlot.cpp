#include "ModelMarkerPlot.h"

using namespace MMM;

ModelMarkerPlot::ModelMarkerPlot(BasicModelPoseSensorPtr sensor, MotionPtr motion) :
    sensor(sensor),
    motion(motion)
{
}

std::vector<std::string> ModelMarkerPlot::getNames() {
    std::vector<std::string> modelMarkerNames;

    if (motion->getModel()) {
        std::vector<VirtualRobot::SensorPtr> sensors = motion->getModel()->getSensors();
        for (const auto &sensor : sensors) {
            if (!sensor->getName().empty()) {
                modelMarkerNames.push_back(sensor->getName() + "_x");
                modelMarkerNames.push_back(sensor->getName() + "_y");
                modelMarkerNames.push_back(sensor->getName() + "_z");
            }
        }
    }
    return modelMarkerNames;
}

std::tuple<QVector<double>, QVector<double> > ModelMarkerPlot::getPlot(std::string name) {
    std::string markerName = name.substr(0, name.size() - 2);   // remove last two chars
    int id = ((int)name[name.size() - 1]) - ((int)'x');         // get index from char

    QVector<double> x(sensor->getTimesteps().size()), y(sensor->getTimesteps().size());
    int i = 0;
    auto robot = motion->getModel()->clone();
    for (float timestep : sensor->getTimesteps()) {
        motion->initializeModel(robot, timestep);
        x[i] = timestep;
        y[i] = robot->getSensor(markerName)->getGlobalPosition()[id];
    }

    return std::make_tuple(x, y);
}
