#include "MoCapMarkerSensorPlotFactory.h"
#include "MoCapMarkerSensorPlot.h"

#include <boost/extension/extension.hpp>

#include <MMM/Motion/Plugin/MoCapMarkerPlugin/MoCapMarkerSensor.h>

using namespace MMM;

// register this factory
SensorPlotFactory::SubClassRegistry MoCapMarkerSensorPlotFactory::registry(MoCapMarkerSensorPlot::NAME, &MoCapMarkerSensorPlotFactory::createInstance);

MoCapMarkerSensorPlotFactory::MoCapMarkerSensorPlotFactory() : SensorPlotFactory() {}

MoCapMarkerSensorPlotFactory::~MoCapMarkerSensorPlotFactory() {}

std::string MoCapMarkerSensorPlotFactory::getName()
{
    return MoCapMarkerSensorPlot::NAME;
}

std::string MoCapMarkerSensorPlotFactory::getType()
{
    return MoCapMarkerSensor::TYPE;
}

std::string MoCapMarkerSensorPlotFactory::getUnit() {
    return "Motion capture marker position (in cm)";
}

std::string MoCapMarkerSensorPlotFactory::getValueName() {
    return "Motion Capture Marker";
}

SensorPlotList MoCapMarkerSensorPlotFactory::createSensorPlots(MotionPtr motion, MotionRecordingPtr /*motions*/) {
    SensorPlotList sensorPlots;
    for (auto sensor : motion->getSensorsByType<MoCapMarkerSensor>()) {
        sensorPlots.push_back(SensorPlotPtr(new MoCapMarkerSensorPlot(sensor)));
    }
    return sensorPlots;
}

bool MoCapMarkerSensorPlotFactory::hasSensorPlot(MotionPtr motion, MotionRecordingPtr motions) {
    return motion->hasSensor(MoCapMarkerSensor::TYPE);
}

SensorPlotFactoryPtr MoCapMarkerSensorPlotFactory::createInstance(void *)
{
    return SensorPlotFactoryPtr(new MoCapMarkerSensorPlotFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorPlotFactoryPtr getFactory() {
    return SensorPlotFactoryPtr(new MoCapMarkerSensorPlotFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorPlotFactory::VERSION;
}
