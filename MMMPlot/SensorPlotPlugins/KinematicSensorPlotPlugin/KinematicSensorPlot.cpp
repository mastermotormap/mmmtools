#include "KinematicSensorPlot.h"

#include <MMM/Exceptions.h>

using namespace MMM;

std::vector<std::string> KinematicSensorPlot::getNames() {
    return sensor->getJointNames();
}

std::tuple<QVector<double>, QVector<double> > KinematicSensorPlot::getPlot(std::string name) {
    if (sensor->hasJoint(name)) {
        QVector<double> x(sensor->getTimesteps().size()), y(sensor->getTimesteps().size());
        int i = 0;
        for (const auto &v : sensor->getJointValues(name)) {
            x[i] = v.first;
            y[i] = v.second;
            i++;
        }
        return std::make_tuple(x,y);
    }
    else
        throw Exception::MMMException("Joint name " + name + " not found.");
}
