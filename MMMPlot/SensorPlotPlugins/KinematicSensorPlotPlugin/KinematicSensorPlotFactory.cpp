#include "KinematicSensorPlotFactory.h"
#include "KinematicSensorPlot.h"

#include <boost/extension/extension.hpp>

#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>

using namespace MMM;

// register this factory
SensorPlotFactory::SubClassRegistry KinematicSensorPlotFactory::registry(KinematicSensorPlot::NAME, &KinematicSensorPlotFactory::createInstance);

KinematicSensorPlotFactory::KinematicSensorPlotFactory() : SensorPlotFactory() {}

KinematicSensorPlotFactory::~KinematicSensorPlotFactory() {}

std::string KinematicSensorPlotFactory::getName()
{
    return KinematicSensorPlot::NAME;
}

std::string KinematicSensorPlotFactory::getUnit() {
    return "Joint angle (radian)";
}

std::string KinematicSensorPlotFactory::getValueName() {
    return "Kinematic joint";
}

SensorPlotList KinematicSensorPlotFactory::createSensorPlots(MotionPtr motion, MotionRecordingPtr /*motions*/) {
    SensorPlotList sensorPlots;
    for (auto sensor : motion->getSensorsByType<BasicKinematicSensor>()) {
        sensorPlots.push_back(SensorPlotPtr(new KinematicSensorPlot(sensor)));
    }
    return sensorPlots;
}

bool KinematicSensorPlotFactory::hasSensorPlot(MotionPtr motion, MotionRecordingPtr /*motions*/) {
    return motion->hasSensor(BasicKinematicSensor::TYPE);
}

SensorPlotFactoryPtr KinematicSensorPlotFactory::createInstance(void *)
{
    return SensorPlotFactoryPtr(new KinematicSensorPlotFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorPlotFactoryPtr getFactory() {
    return SensorPlotFactoryPtr(new KinematicSensorPlotFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorPlotFactory::VERSION;
}
