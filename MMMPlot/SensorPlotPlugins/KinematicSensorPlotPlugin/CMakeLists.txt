project(KinematicSensorPlot)

set(KinematicSensorPlot_Sources
    KinematicSensorPlotFactory.cpp
    KinematicSensorPlot.cpp
)

set(KinematicSensorPlot_Headers
    KinematicSensorPlotFactory.h
    KinematicSensorPlot.h
)

DefineSensorPlotPlugin(${PROJECT_NAME} "${KinematicSensorPlot_Sources}" "${KinematicSensorPlot_Headers}" KinematicSensor)
