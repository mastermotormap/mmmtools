#include "SensorPlotHandlerFactory.h"
#include "SensorPlotHandler.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry SensorPlotHandlerFactory::registry(SensorPlotHandler::NAME, &SensorPlotHandlerFactory::createInstance);

SensorPlotHandlerFactory::SensorPlotHandlerFactory() : MotionHandlerFactory() {}

SensorPlotHandlerFactory::~SensorPlotHandlerFactory() {}

std::string SensorPlotHandlerFactory::getName() {
    return SensorPlotHandler::NAME;
}

MotionHandlerPtr SensorPlotHandlerFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new SensorPlotHandler(widget));
}

MotionHandlerFactoryPtr SensorPlotHandlerFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new SensorPlotHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new SensorPlotHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
