project(SensorPlotHandler)

set(SensorPlotHandler_Sources
    SensorPlotHandlerFactory.cpp
    SensorPlotHandler.cpp
    SensorPlotHandlerDialog.cpp
    ../MMMPlot/PlotWidget.cpp
    ../MMMPlot/SinglePlotWidget.cpp
    ../MMMPlot/qcustomplot.cpp
    ../../common/HandleMotionsWithoutModel.cpp
)

set(SensorPlotHandler_Headers
    SensorPlotHandlerFactory.h
    SensorPlotHandler.h
    SensorPlotHandlerDialog.h
    ../MMMPlot/PlotWidget.h
    ../MMMPlot/SinglePlotWidget.h
    ../MMMPlot/qcustomplot.h
    ../../common/HandleMotionsWithoutModel.h
)

set(SensorPlotHandler_Moc
    SensorPlotHandlerDialog.h
    SensorPlotHandler.h
    ../../MMMViewer/MMM/Viewer/MotionHandler.h
    ../MMMPlot/PlotWidget.h
    ../MMMPlot/SinglePlotWidget.h
    ../MMMPlot/qcustomplot.h
)

set(SensorPlotHandler_Ui
    SensorPlotHandlerDialog.ui
    ../MMMPlot/PlotWidget.ui
    ../MMMPlot/SinglePlotWidget.ui
)

DefineMotionHandlerPlugin(${PROJECT_NAME} "${SensorPlotHandler_Sources}" "${SensorPlotHandler_Headers}" "${SensorPlotHandler_Moc}" "${SensorPlotHandler_Ui}" MotionPlot)
