#include "SensorPlotHandler.h"
#include <QFileDialog>

using namespace MMM;

SensorPlotHandler::SensorPlotHandler(QWidget* widget) :
    MotionHandler(MotionHandlerType::GENERAL, "Plot sensor data", "Ctrl+P"),
    widget(widget),
    dialog(new SensorPlotHandlerDialog())
{
    connect(dialog, &SensorPlotHandlerDialog::openMotions, this, &SensorPlotHandler::openMotions);
    //connect(dialog, SIGNAL(openMotions(MMM::MotionRecordingPtr)), this, SIGNAL(openMotions(MMM::MotionRecordingPtr)));
    connect(dialog, SIGNAL(jumpTo(float)), this, SIGNAL(jumpTo(float)));
}

void SensorPlotHandler::handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> /*currentRobots*/) {
    dialog->loadMotions(motions);
    dialog->show();
}

std::string SensorPlotHandler::getName() {
    return NAME;
}

void SensorPlotHandler::addImportMotionHandler(MotionHandlerPtr motionHandler) {
    dialog->addImportMotionHandler(motionHandler);
}

std::shared_ptr<IPluginHandler> SensorPlotHandler::getPluginHandler() {
    std::shared_ptr<PluginHandler<MMM::SensorPlotFactory> > pluginHandler(new PluginHandler<MMM::SensorPlotFactory>(getPluginHandlerID(), SENSOR_PLOT_PLUGIN_LIB_DIR));
    pluginHandler->updateSignal.connect(std::bind(&PlotWidget::update, dialog->getPlotWidget(), std::placeholders::_1));
    pluginHandler->emitUpdate();
    return pluginHandler;
}

std::string SensorPlotHandler::getPluginHandlerID() {
    return "Sensor Plotter";
}

void SensorPlotHandler::timestepChanged(float timestep) {
    if (dialog->isVisible()) dialog->moveSlider(timestep);
}
