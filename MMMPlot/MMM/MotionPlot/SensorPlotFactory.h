/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2018 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef _MMM_SENSORPLOTFACTORY_H_
#define _MMM_SENSORPLOTFACTORY_H_

#include "SensorPlot.h"

#include <MMM/MMMCore.h>
#include <MMM/AbstractFactoryMethod.h>
#include <MMM/MMMImportExport.h>


namespace MMM
{

class MMM_IMPORT_EXPORT SensorPlotFactory  : public AbstractFactoryMethod<SensorPlotFactory, void*>
{
public:

    SensorPlotFactory() {}

    virtual ~SensorPlotFactory() {}

    virtual SensorPlotList createSensorPlots(MotionPtr motion, MotionRecordingPtr motions) = 0;

    virtual std::string getName() = 0;

    virtual std::string getUnit() = 0;

    virtual std::string getValueName() = 0;

    virtual bool hasSensorPlot(MotionPtr motion, MotionRecordingPtr motions) = 0;

    static constexpr const char* VERSION = "1.0";
};

typedef std::shared_ptr<SensorPlotFactory> SensorPlotFactoryPtr;

} // namespace MMM

#endif // _MMM_SensorPlotFactory_h_
