/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2018 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_PLOTJOINTANGLECONFIGURATION_H_
#define __MMM_PLOTJOINTANGLECONFIGURATION_H_

#include <string>
#include <VirtualRobot/RuntimeEnvironment.h>
#include <MMM/MMMCore.h>

#include "../../common/ApplicationBaseConfiguration.h"

/*!
    Configuration of MMMPlot.
*/
class MMMPlotConfiguration : public ApplicationBaseConfiguration
{

public:
    std::vector<std::filesystem::path> inputMotionPaths;
    std::vector<std::filesystem::path> sensorPluginPaths;
    std::vector<std::filesystem::path> plotPluginPaths;

    MMMPlotConfiguration() : ApplicationBaseConfiguration()
    {
    }

    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("inputMotions");
        VirtualRobot::RuntimeEnvironment::considerKey("sensorPlugins");
        VirtualRobot::RuntimeEnvironment::considerKey("plotPlugins");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        inputMotionPaths = getPaths("inputMotions");
        sensorPluginPaths = getPaths("sensorPlugins");
        plotPluginPaths = getPaths("plotPlugins");

        return valid;
    }

    void print()
    {
        MMM_INFO << "*** MMMPlot Configuration ***" << std::endl;
        printVec("Input motion", inputMotionPaths);
        printVec("Sensor plugin paths", sensorPluginPaths);
        printVec("Sensor plot plugin paths", plotPluginPaths);
    }
};


#endif
