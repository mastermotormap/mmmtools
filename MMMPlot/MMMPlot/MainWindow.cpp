#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "PlotWidget.h"

MainWindow::MainWindow(const std::vector<std::filesystem::path> &motionFilePaths, const std::vector<std::filesystem::path> &sensorPluginPaths, const std::vector<std::filesystem::path> &plotPluginPaths, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    PlotWidget* plotWidget = new PlotWidget(this, sensorPluginPaths, true);
    plotWidget->loadMotions(motionFilePaths);
    auto factoryPluginLoader = plotWidget->getFactoryLoader();
    factoryPluginLoader->addPluginLibs(plotPluginPaths);
    plotWidget->update(factoryPluginLoader->getFactories());
    ui->layout->addWidget(plotWidget);
}

MainWindow::~MainWindow()
{
    delete ui;
}
