cmake_minimum_required(VERSION 3.10.2)

###########################################################
#### Project configuration                             ####
###########################################################

project(MotionPlot)

###########################################################
#### Source configuration                              ####
###########################################################

set(HEADER_FILES
    MMM/MotionPlot/SensorPlot.h
    MMM/MotionPlot/SensorPlotFactory.h
)

###########################################################
#### CMake package configuration                       ####
###########################################################

find_package(MMMCore REQUIRED)
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} MMMCore)

find_package(Simox REQUIRED)
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} VirtualRobot)

###########################################################
#### Project build configuration                       ####
###########################################################

# create the lib
add_library("${PROJECT_NAME}" SHARED ${HEADER_FILES})
target_link_libraries(${PROJECT_NAME} PUBLIC ${EXTERNAL_LIBRARY_DIRS} dl)
set_target_properties(${PROJECT_NAME} PROPERTIES LINKER_LANGUAGE CXX)

# this allows dependant targets to automatically add include-dirs by simply linking against this project
target_include_directories(${PROJECT_NAME} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
    $<INSTALL_INTERFACE:include>
)

install(
    TARGETS ${PROJECT_NAME}
    EXPORT "${CMAKE_PROJECT_NAME}Targets"
    LIBRARY DESTINATION lib
    #ARCHIVE DESTINATION lib
    #RUNTIME DESTINATION bin
    INCLUDES DESTINATION include
    COMPONENT lib
)

add_definitions(-DMMMTools_LIB_DIR="${MMMTools_LIB_DIR}")
add_definitions(-DMMMTools_SRC_DIR="${MMMTools_SOURCE_DIR}")
add_definitions(-DSENSOR_PLOT_PLUGIN_LIB_DIR="${SENSOR_PLOT_PLUGIN_LIB_DIR}")

###########################################################
#### Compiler configuration                            ####
###########################################################

include_directories(${EXTERNAL_INCLUDE_DIRS})
link_directories(${EXTERNAL_LIBRARY_DIRS})
add_definitions(${EXTERNAL_LIBRARY_FLAGS})


###########################################################
#### Sensor Plot Plugins                               ####
###########################################################

# Add sensor plot plugins
function(DefineSensorPlotPlugin SensorPlotPlugin_Name SensorPlotPlugin_Sources SensorPlotPlugin_Headers SensorPlugin_Name)
    add_library(${SensorPlotPlugin_Name} SHARED ${SensorPlotPlugin_Sources} ${SensorPlotPlugin_Headers})
    target_link_libraries(${SensorPlotPlugin_Name} PUBLIC ${EXTERNAL_LIBRARY_DIRS} MotionPlot ${SensorPlugin_Name})
    set_target_properties(${SensorPlotPlugin_Name} PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${SENSOR_PLOT_PLUGIN_LIB_DIR})

    install(
        TARGETS ${SensorPlotPlugin_Name}
        EXPORT "${CMAKE_PROJECT_NAME}Targets"
        LIBRARY DESTINATION lib/mmm_plugins/sensor_plot
        #ARCHIVE DESTINATION lib
        #RUNTIME DESTINATION bin
        #INCLUDES DESTINATION include
        COMPONENT lib
    )
    install(FILES ${SensorPlotPlugin_Headers} DESTINATION "include/MMM/${SensorPlotPlugin_Name}" COMPONENT dev)
endfunction()

add_subdirectory(SensorPlotPlugins)

###########################################################
#### Motion Handler Plugin                             ####
###########################################################

add_subdirectory(MotionHandlerPlugin)



add_subdirectory(MMMPlot)
