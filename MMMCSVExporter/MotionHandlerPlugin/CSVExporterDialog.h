/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2019 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_CSVEXPORTERDIALOG_H_
#define __MMM_CSVEXPORTERDIALOG_H_

#include <QDialog>

#ifndef Q_MOC_RUN
#include <MMM/Motion/MotionRecording.h>
#include <MMM/Motion/Motion.h>
#endif

namespace Ui {
class CSVExporterDialog;
}

class CSVExporterDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CSVExporterDialog(const std::string &exportDirectoryPath, MMM::MotionRecordingPtr motions, QWidget* parent = 0);
    ~CSVExporterDialog();

private slots:
    void exportMotions();

signals:
    void jumpTo(float timestep);
    void saveScreenshot(float timestep, const std::string &directory, const std::string &name = std::string());

private:
    Ui::CSVExporterDialog* ui;
    MMM::MotionRecordingPtr motions;
    std::string exportDirectoryPath;
};

#endif // __MMM_CSVExporterDIALOG_H_
