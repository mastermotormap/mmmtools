#include "CSVExporterPlugin.h"
#include "CSVExporterDialog.h"
#include <QFileDialog>

using namespace MMM;

CSVExportPlugin::CSVExportPlugin(QWidget* widget) :
    MotionHandler(MotionHandlerType::EXPORT, "Export motion as CSV file"),
    searchPath(std::string(MMMTools_SRC_DIR)),
    widget(widget)
{
}

void CSVExportPlugin::handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> /*currentRobots*/) {
    if (!motions || motions->empty()) return;
    std::string exportDirectoryPath = QFileDialog::getExistingDirectory(widget, QString::fromStdString(getDescription()), QString::fromStdString(searchPath)).toStdString();
    if (!exportDirectoryPath.empty()) {
        CSVExporterDialog* dialog = new CSVExporterDialog(exportDirectoryPath, motions, widget);
            MMM_INFO << "we are here" << std::endl;
        connect(dialog, SIGNAL(jumpTo(float)), this, SIGNAL(jumpTo(float)));
        connect(dialog, SIGNAL(saveScreenshot(float,std::string,std::string)), this, SIGNAL(saveScreenshot(float,std::string,std::string)));
        dialog->show();
    }
}

std::string CSVExportPlugin::getName() {
    return NAME;
}
