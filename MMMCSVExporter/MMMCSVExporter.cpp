/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include "MMMCSVExporterConfiguration.h"
#include "CSVExporter.h"

#include <string>
#include <vector>
#include <tuple>

using namespace MMM;

int main(int argc, char *argv[]) {
    MMM_INFO << " --- MMMCSVExporter --- " << std::endl;
    MMMCSVExporterConfiguration* configuration = new MMMCSVExporterConfiguration();
    if (!configuration->processCommandLine(argc, argv)) {
        MMM_ERROR << "Error while processing command line, aborting..." << std::endl;
        return -1;
    }

    try {
        CSVExporter* exporter = new CSVExporter(configuration->sensorPluginPaths);
        if (!configuration->inputMotionPath.empty()) {
            exporter->exportCSV(configuration->inputMotionPath, configuration->outputDir, configuration->localFrame, configuration->exportHands);
        }
        else {
            exporter->exportCSV_dir(configuration->inputMotionDir, configuration->outputDir, configuration->localFrame, configuration->exportHands);
        }

    } catch (MMM::Exception::MMMException& e) {
        MMM_ERROR << e.what() << std::endl;
        return -2;
    }
}
