/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_C3DCONVERTERCONFIGURATION_H_
#define __MMM_C3DCONVERTERCONFIGURATION_H_

#include <filesystem>
#include <string>

#include <MMM/MMMCore.h>

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

#include <VirtualRobot/RuntimeEnvironment.h>

#include "../common/ApplicationBaseConfiguration.h"

/*!
    Configuration of MMMCSVExporter.
*/
class MMMCSVExporterConfiguration : public ApplicationBaseConfiguration
{

public:
    std::string inputMotionPath;
    std::string inputMotionDir;
    std::vector<std::filesystem::path> sensorPluginPaths;
    std::string outputDir;
    bool localFrame;
    bool exportHands;

    MMMCSVExporterConfiguration() : ApplicationBaseConfiguration()
    {
    }

    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("inputMotion", "mmm motion file path");
        VirtualRobot::RuntimeEnvironment::considerKey("inputMotionDir", "mmm motion file directory");
        VirtualRobot::RuntimeEnvironment::considerKey("sensorPlugins");
        VirtualRobot::RuntimeEnvironment::considerKey("outputDir");
        VirtualRobot::RuntimeEnvironment::considerFlag("localFrame");
        VirtualRobot::RuntimeEnvironment::considerFlag("exportHands");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        inputMotionPath = getParameter("inputMotion", false, true);
        inputMotionDir = getParameter("inputMotionDir", false, false);
        if (inputMotionPath.empty() && inputMotionDir.empty())
            valid = false;
        sensorPluginPaths = getPaths("sensorPlugins");
        outputDir = getParameter("outputDir", false, false);
        if (outputDir.empty())
            outputDir = inputMotionPath.empty() ? inputMotionDir : std::filesystem::path(inputMotionPath).parent_path().string();
        localFrame = VirtualRobot::RuntimeEnvironment::hasFlag("localFrame");
        exportHands = VirtualRobot::RuntimeEnvironment::hasFlag("exportHands");

        return valid;
    }

    void print()
    {
        MMM_INFO << "*** MMMCSVExporter Configuration ***" << std::endl;
        std::cout << "Input motion: " << inputMotionPath << std::endl;
        std::cout << "Output motion: " << outputDir << std::endl;
    }
};


#endif
