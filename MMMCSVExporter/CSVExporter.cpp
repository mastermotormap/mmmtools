#include "CSVExporter.h"

#include "LocalTrajExtraction.h"
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensorMeasurement.h>
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>
#include <SimoxUtility/algorithm/string.h>
#include <SimoxUtility/math/convert.h>
#include <MMM/Motion/MotionReaderXML.h>
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotNodeSet.h>

#include <MMM/Exceptions.h>

using namespace MMM;

void CSVExporter::exportCSV_dir(const std::string &directoryPath, const std::string &exportDirectoryPath, bool localFrame, bool exportHands) {
    for(auto &file : std::filesystem::directory_iterator(directoryPath)) {
        auto fileExtension = file.path().extension();
        if (fileExtension == ".xml") {
            exportCSV(file.path(), exportDirectoryPath, localFrame, exportHands);
        }
    }
}

void CSVExporter::exportCSV(const std::string &motionPath, const std::string &exportDirectoryPath, bool localFrame, bool exportHands) {
    try {
        MMM_INFO << "Reading motion from " << motionPath << std::endl;
        MMM::MotionRecordingPtr motions = motionReader->loadMotionRecording(motionPath);
        exportCSV(motions, exportDirectoryPath, localFrame, exportHands, motions->getMinTimestep(), motions->getMaxTimestep(), 100);

    } catch (MMM::Exception::MMMException& e) {
        MMM_ERROR << e.what() << std::endl;
    }
}

void CSVExporter::exportCSV(MotionRecordingPtr motions, const std::string &exportDirectoryPath, bool localFrame, bool exportHands, float minTimestep, float maxTimestep, float framesPerSecond) {
    if (localFrame){
        std::vector<std::string> objNames;
        for(auto motion : *motions)
        {
            if (!motion->getModel()) continue;
            std::string s = std::filesystem::path(motion->getModel()->getFilename()).stem();
            objNames.push_back(s);
        }
        objNames.push_back("MMMHandL");
        objNames.push_back("MMMHandR");

        MMM_INFO << "Calculating local trajectories";
        MMM::MotionRecordingPtr motions_orig = motions;
        for(unsigned int j = 0; j < objNames.size(); j++){
            for(unsigned int k = 0; k < objNames.size(); k++){
                if(j != k){
                    LocalTrajExtractionPtr locTrajEx(new LocalTrajExtraction(motions_orig, minTimestep, maxTimestep, framesPerSecond, objNames[j], objNames[k]));
                    MotionPtr new_motion = locTrajEx->getLocalPoseTraj();
                    motions->addMotion(new_motion);
                }
            }
        }
    }

    unsigned int size = motions->size();
    for(unsigned int i = 0; i < size; i++)
    {
        auto motion = motions->getMotion(i);
        if (!motion) {
            MMM_ERROR << "Motion nullptr!" << std::endl;
            continue;
        }
        //if (!motion->getModel()) continue;
        if (!motion->hasSensor(ModelPoseSensor::TYPE) && !motion->hasSensor(KinematicSensor::TYPE)) {
            MMM_INFO << "Not exporting motion with name '" << motion->getName() << "'. The motion has no model pose or kinematic sensor!" << std::endl;
            continue;
        }
        std::vector<std::string> headers{"timestep"};
        std::vector<std::vector<float>> csvData;
        auto poseSensor = motion->getSensorByType<ModelPoseSensor>();
        if (poseSensor)
        {
            auto names = {"RootPosition_X",
                         "RootPosition_Y",
                         "RootPosition_Z",
                         "RootOrientation_qx",
                         "RootOrientation_qy",
                         "RootOrientation_qz",
                         "RootOrientation_qw"};
            headers.insert(headers.end(), names.begin(), names.end());
        }
        auto kinSensors = motion->getSensorsByType<KinematicSensor>();
        for (auto kinSensor : kinSensors)
        {
            auto names = kinSensor->getJointNames();
            headers.insert(headers.end(), names.begin(), names.end());
        }

        for (float t = minTimestep; t <= maxTimestep; t += 1.0/framesPerSecond)
        {
            std::vector<float> timestepData;
            timestepData.push_back(t);

            if (poseSensor)
            {
                auto data = poseSensor->getDerivedMeasurement(t);
                if (data) {
                    Eigen::Vector3f pos = data->getRootPosition();
                    timestepData.push_back(pos(0));
                    timestepData.push_back(pos(1));
                    timestepData.push_back(pos(2));
                    Eigen::Quaternionf rot = data->getRootRotation();
                    timestepData.push_back(rot.x());
                    timestepData.push_back(rot.y());
                    timestepData.push_back(rot.z());
                    timestepData.push_back(rot.w());
                }
                else {
                    for (unsigned int i = 0; i < 7; i++)
                        timestepData.push_back(std::numeric_limits<double>::quiet_NaN());
                }
            }

            for (auto kinSensor : kinSensors)
            {
                auto data = kinSensor->getDerivedMeasurement(t);
                if (data)
                {
                    Eigen::VectorXf angles = data->getJointAngles();
                    timestepData.insert(timestepData.end(), angles.data(), angles.data() + angles.rows());
                }
                else {
                    for (unsigned int i = 0; i < kinSensor->getJointNames().size(); i++)
                        timestepData.push_back(std::numeric_limits<double>::quiet_NaN());
                }
            }

            csvData.push_back(timestepData);
        }

        auto folderPath = std::filesystem::path(exportDirectoryPath) / std::filesystem::path(motion->getOriginFilePath()).stem();
        if (!std::filesystem::exists(folderPath))
            std::filesystem::create_directory(folderPath);

        std::string filename = folderPath / std::filesystem::path(motion->getName() + ".csv");
        MMM_INFO << "Writing to " << filename << std::endl;
        std::ofstream f(filename.c_str());
        f << simox::alg::join(headers, ",") << "\n";
        for(auto & entry: csvData)
        {
            std::vector<std::string> valueStrings;
            for(auto v : entry)
            {
                valueStrings.push_back(simox::alg::to_string(v));
            }
            f << simox::alg::join(valueStrings, "," ) << "\n";
        }

        if (exportHands && motion->isReferenceModelMotion()) {
            VirtualRobot::RobotPtr robot = motion->getModel()->clone();

            // empty model should be no problem
            ModelPtr emptyModel(new VirtualRobot::LocalRobot("EMPTY"));
            MotionPtr leftHandMotion(new Motion(motion->getName() + "_LeftHand", emptyModel, emptyModel, nullptr, motion->getOriginFilePath()));
            ModelPoseSensorPtr modelPoseSensor_left(new ModelPoseSensor());
            leftHandMotion->addSensor(modelPoseSensor_left);
            MotionPtr rightHandMotion(new Motion(motion->getName() + "_RightHand", emptyModel, emptyModel, nullptr, motion->getOriginFilePath()));
            ModelPoseSensorPtr modelPoseSensor_right(new ModelPoseSensor());
            rightHandMotion->addSensor(modelPoseSensor_right);

            for (float t = minTimestep; t <= maxTimestep; t += 1.0/framesPerSecond)
            {
                motion->initializeModel(robot, t);

                modelPoseSensor_left->addSensorMeasurement(ModelPoseSensorMeasurementPtr(new ModelPoseSensorMeasurement(t, robot->getRobotNode("Hand L TCP")->getGlobalPose())));
                modelPoseSensor_right->addSensorMeasurement(ModelPoseSensorMeasurementPtr(new ModelPoseSensorMeasurement(t, robot->getRobotNode("Hand R TCP")->getGlobalPose())));
            }
            motions->addMotion(leftHandMotion);
            motions->addMotion(rightHandMotion);
            size += 2;
        }
    }
}

