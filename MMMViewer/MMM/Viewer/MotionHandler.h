/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MOTIONHANDLER_H_
#define __MMM_MOTIONHANDLER_H_

#ifndef Q_MOC_RUN
#include <MMM/MMMCore.h>
#include <MMM/Motion/Motion.h>
#include "PluginHandler/PluginHandler.h"
#include <Inventor/nodes/SoSeparator.h>
#endif

#include <QWidget>

Q_DECLARE_METATYPE(SoSeparator*)
Q_DECLARE_METATYPE(MMM::MotionRecordingPtr)

namespace VirtualRobot
{
class Robot; // forward declaration
typedef std::shared_ptr<Robot> RobotPtr;
}

namespace MMM
{

enum MotionHandlerType {
    IMPORT, EXPORT, ADD_SENSOR, GENERAL
};

class MotionHandler;

typedef std::shared_ptr<MotionHandler> MotionHandlerPtr;

class MotionHandler : public QObject
{
    Q_OBJECT

public:
    virtual void handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> currentRobots = {}) = 0;

    virtual std::string getName() = 0;

    virtual MotionHandlerType getType() {
        return type;
    }

    virtual std::string getDescription() {
        return description;
    }

    virtual std::string getShortCut() {
        return shortCut;
    }

    virtual std::shared_ptr<IPluginHandler> getPluginHandler() {
        return nullptr;
    }

    virtual std::string getPluginHandlerID() {
        return "";
    }

    virtual void addImportMotionHandler(MotionHandlerPtr /*motionHandler*/) {
        // do nothing
    }

public slots:
    virtual void timestepChanged(float /*timestep*/) {
        // do nothing
    }

    virtual void motionsOpened(bool /*opened*/, MMM::MotionRecordingPtr /*motions*/, std::map<std::string, VirtualRobot::RobotPtr> /*currentRobots*/) {
        // do nothing
    }

signals:
    void openMotions(MotionRecordingPtr motions);

    void jumpTo(float timestep);

    void updateVisualisation();

    void saveScreenshot(float timestep, const std::string &directory, const std::string &name = std::string());

    void addVisualisation(SoSeparator* sep);

    void saveMotion();

protected:
    MotionHandler(const MotionHandlerType &type, const std::string &description, const std::string &shortCut = std::string()) :
        type(type),
        description(description),
        shortCut(shortCut)
    {
    }

private:
    MotionHandlerType type;
    std::string description;
    std::string shortCut;

};

}

#endif
