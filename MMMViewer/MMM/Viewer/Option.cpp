#include "Option.h"

namespace MMM
{

Option::Option(Option::Type type, OptionAction* action) : QObject(), type(type), action(action)
{
}

void Option::save() {
    settings.setValue(QString::fromStdString("optionsmenu/" + type_names[type]), getValue());
}

}
