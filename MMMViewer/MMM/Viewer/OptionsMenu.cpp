#include "OptionsMenu.h"

#include <QLabel>
#include <QHBoxLayout>

namespace MMM
{

QCheckableAction::QCheckableAction(const std::string &title, Option::Type type, bool initValue, QWidget* parent,
                 const std::string &shortcut, QSignalMapper* mapper)
    : QAction(QString::fromStdString(title), parent), type(type)
{
    setCheckable(true);
    bool value = settings.value(QString::fromStdString("optionsmenu/" + Option::type_names[type]), initValue).toBool();
    setChecked(value);
    if (!shortcut.empty()) setShortcut(QString::fromStdString(shortcut));

    setMapping(mapper);
}

void QCheckableAction::setMapping(QSignalMapper* mapper) {
    connect(this, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(this, new Option(type, this));
    updateOption();
}

QVariant QCheckableAction::getValue() {
    return isChecked();
}

void QCheckableAction::updateOption() {
    emit triggered();
}



QSpinBoxAction::QSpinBoxAction(const std::string &title, Option::Type type, double minimum, double maximum,
                               double initValue, double stepSize, int decimals, QWidget* parent, QSignalMapper* mapper) :
    QWidgetAction(parent), type(type)
{
    QWidget* pWidget = new QWidget(parent);
    QHBoxLayout* pLayout = new QHBoxLayout();
    pLayout->setContentsMargins(8,0,8,0);
    QLabel* pLabel = new QLabel(QString::fromStdString(title));
    pLayout->addWidget(pLabel);
    pSpinBox = new QDoubleSpinBox(pWidget);
    pSpinBox->setMinimum(minimum);
    pSpinBox->setMaximum(maximum);
    pSpinBox->setValue(settings.value(QString::fromStdString("optionsmenu/" + Option::type_names[type]), initValue).toDouble());
    pSpinBox->setSingleStep(stepSize);
    pSpinBox->setDecimals(decimals);
    pLayout->addWidget(pSpinBox);
    pWidget->setLayout(pLayout);
    setDefaultWidget(pWidget);

    connect(pSpinBox, SIGNAL(valueChanged(double)), this, SIGNAL(spinnerChanged()));

    setMapping(mapper);
}


void QSpinBoxAction::setMapping(QSignalMapper* mapper) {
    connect(this, SIGNAL(spinnerChanged()), mapper, SLOT(map()));
    mapper->setMapping(this, new Option(type, this));
    updateOption();
}

QVariant QSpinBoxAction::getValue() {
    return pSpinBox->value();
}

void QSpinBoxAction::updateOption() {
    emit pSpinBox->valueChanged(pSpinBox->value());
}



OptionsMenu::OptionsMenu(QWidget* parent) : QMenu(tr("&Options"), parent) {
    QSignalMapper* mapper = new QSignalMapper(this);
    connect(mapper, SIGNAL(mapped(QObject*)), this, SLOT(m(QObject*)));
    add(new QCheckableAction("Show Visualisation Table", Option::Type::VIS_TABLE, true, this, "Ctrl+T", mapper));
    add(new QCheckableAction("Show Floor", Option::Type::FLOOR, true, this, "Ctrl+Shift+F", mapper));
    add(new QCheckableAction("Show Collision Models", Option::Type::COLLISION_MODEL, false, this, "", mapper));
    addSeparator();
    add(new QSpinBoxAction("AntiAliasing", Option::Type::ANTIALIASING, 0, 8, 0, 1, 0, this, mapper));
    add(new QCheckableAction("Enable interpolation", Option::Type::INTERPOLATION, true, this, "", mapper));
    addSeparator();
    add(new QSpinBoxAction("Font Size", Option::Type::FONT_SIZE, 0, 5, 2, 0.5, 1, this, mapper));
    add(new QSpinBoxAction("Marker Size", Option::Type::MARKER_SIZE, 0, 20, 5, 5, 0, this, mapper));
    addSeparator();
    add(new QSpinBoxAction("MMM Scale", Option::Type::MMM_SCALE, 0.1, 2, 1, 0.1, 2, this, mapper));
    add(new QCheckableAction("Mirror Motions", Option::Type::MIRROR_MOTION, false, this, "", mapper));
}

void OptionsMenu::updateOptions() {
    updating = true;
    for (OptionAction* o : optionActions)
        o->updateOption();
    updating = false;
}

void OptionsMenu::add(QCheckableAction* action) {
    optionActions.push_back(action);
    addAction(action);
}

void OptionsMenu::add(QSpinBoxAction* action) {
    optionActions.push_back(action);
    addAction(action);
}

void OptionsMenu::m(QObject* o) {
    Option* option = dynamic_cast<Option*>(o);
    if (!option) MMM_ERROR << "Something's wrong!" << std::endl; // Should not happen!
    else emit optionChanged(option);
    option->save();
}

bool OptionsMenu::isUpdating() {
    return updating;
}

}
