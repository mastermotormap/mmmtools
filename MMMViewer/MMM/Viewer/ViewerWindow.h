/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/


#ifndef __MMM_ViewerWindow_H_
#define __MMM_ViewerWindow_H_

#include <QList>
#include <QSettings>
#include <QMainWindow>
#include <QWidgetAction>
#include <QSpinBox>
#include <QFrame>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/sensors/SoTimerSensor.h>

#ifndef Q_MOC_RUN
#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/Sensor/SensorFactory.h>
#include "PluginHandler/PluginHandler.h"
#include "PluginHandler/PluginHandlerDialog.h"
#include "SensorVisualisation/SensorVisualisationHandler.h"
#include "MotionHandler.h"
#include "MotionHandlerFactory.h"
#include "OptionsMenu.h"
#include <Inventor/nodes/SoSeparator.h>
#endif

Q_DECLARE_METATYPE(QList<float>)

namespace Ui {

class ViewerWindow;
}

namespace MMM
{

class SegmentationDialog;

class ViewerWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ViewerWindow(QWidget* parent = 0);
    ~ViewerWindow();

    void showWindow();
    void progressMotion();

signals:
    void motionsOpened(bool opened, MMM::MotionRecordingPtr motions = nullptr, std::map<std::string, VirtualRobot::RobotPtr> currentRobots = {});
    void timestepChanged(float timestep);

public slots:
    void jumpTo(float timestep);
    void updateMotionHandler(const std::map<std::string, std::shared_ptr<MMM::MotionHandlerFactory> > &motionHandlerFactories);
    void saveMotion();

private slots:
    bool openMotion();
    bool openMotion(const std::filesystem::path &motionFilePath, bool ignoreError = false);
    bool openMotions(MMM::MotionRecordingPtr motions);
    void optionChanged(Option* option);
    void handleMotion(const QString &handlerName);
    void loadLastMotionFile();
    void saveMotionAs();
    void motionSaved(const std::filesystem::path &motionFilePath);
    void sliderMoved(int position);
    void fpsChanged(int newValue);
    void setVelocity(int index);
    void playStopMotion();
    void saveScreenshot(float timestep, const std::string &directory, const std::string &name = std::string());
    void closeEvent(QCloseEvent *event);
    bool closeWindow();
    void sensorVisualisationLoaded(bool loaded);
    void addVisualisation(SoSeparator* sep);
    void focusCameraOnMotion();
    void updateVisualisation();

private:
    bool openMotionFile(MMM::MotionRecordingPtr motions, bool ignoreError, const std::filesystem::path &motionFilePath = std::string());

    static SoSeparator* createSceneGraph();
    static SoSwitch* createFloorVisualisation(SoSeparator* sceneGraph);
    void addMotionMenu();
    static void progressTimerCallback(void* data, SoSensor* sensor);
    void setupViewer();
    void setupCamera();
    void setupVelocityComboBox();
    bool calculateTimesteps(MMM::MotionRecordingPtr motions);
    void stopTimer();
    bool replaceMotion(MMM::MotionPtr motion);
    void setTitle(const std::filesystem::path &motionFilePath = std::string());
    void errorMessageBox(const std::string &message);
    void updateMotion(const std::map<std::string, std::shared_ptr<MMM::SensorFactory> > &sensorFactories);
    void saveCameraSettings();
    void createTimestampString();
    void saveMotionBackup();

    QMenu* importMotionMenu;
    QMenu* exportMotionMenu;
    QMenu* addSensorMenu;
    QMenu* generalMenu;

    SoSeparator* motionHandlerSep;
    SoSeparator* sceneGraph;
    SoSwitch* floorVisualisation;
    SoTimerSensor* progressTimer;

    PluginHandlerDialog* pluginHandlerDialog;
    SensorVisualisationHandlerPtr sensorVisualisationHandler;
    std::map<std::string, MMM::MotionHandlerPtr> motionHandler;
    MMM::MotionReaderXMLPtr motionReader;
    MMM::MotionRecordingPtr motions;
    bool motionChanged;
    std::string timestamp;

    float minTimestep;
    float maxTimestep;
    int framesPerSecond;
    float currentTimestep;
    float lastTimestep;
    float velocity;

    OptionsMenu* optionsMenu;

    QSettings settings;

    Ui::ViewerWindow* ui;
    SoQtExaminerViewer* viewer;
};

}

#endif // __MMM_ViewerWindow_H_
