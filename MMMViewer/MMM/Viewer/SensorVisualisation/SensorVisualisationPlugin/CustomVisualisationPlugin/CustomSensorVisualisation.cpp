#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <VirtualRobot/SceneObject.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>
#include <Inventor/nodes/SoSphere.h>
#include <SimoxUtility/math/convert.h>
#include "CustomSensorVisualisation.h"

using namespace MMM;

CustomSensorVisualisation::CustomSensorVisualisation(CustomSensorPtr sensor, VirtualRobot::RobotPtr robot, std::shared_ptr<VirtualRobot::CoinVisualization> visualization, SoSeparator* sceneSep) :
    SensorVisualisation(sceneSep),
    sensor(sensor),
    robot(robot),
    lastFontSize(fontSize),
    lastMarkerSize(markerSize)
{
    markerSep = new SoSeparator();
    setVisualisationSep(markerSep);
}

std::string CustomSensorVisualisation::getType() {
    return sensor->getType();
}

int CustomSensorVisualisation::getPriority() {
    return sensor->getPriority();
}

SoSeparator* CustomSensorVisualisation::createLabel(const std::string &label) {
    SoSeparator* labelSep = new SoSeparator;
    if (markerSize > 0 && fontSize > 0) {
        Eigen::Vector3f labelVector;
        labelVector << -markerSize, -markerSize, 0;
        Eigen::Matrix4f localPose = Eigen::Matrix4f::Identity();
        localPose.block(0, 3, 3, 1) += labelVector;
        localPose.block(0, 0, 3, 3) *= fontSize;
        labelSep->addChild(VirtualRobot::CoinVisualizationFactory::getMatrixTransform(localPose));
        labelSep->addChild(VirtualRobot::CoinVisualizationFactory::CreateBillboardText(label));
    }
    return labelSep;
}

SoSeparator* CustomSensorVisualisation::createSphere(const Eigen::Vector3f &pose) {
    SoSeparator* sphereSep = new SoSeparator;
    if (markerSize > 0) {
        Eigen::Matrix4f globalPose = Eigen::Matrix4f::Identity();
        globalPose.block(0, 3, 3, 1) = pose;
        SoMaterial* marked = new SoMaterial;
        marked->ambientColor.setValue(0.2f, 1.0f, 0.2f);
        marked->diffuseColor.setValue(0.2f, 1.0f, 0.2f);
        marked->specularColor.setValue(0.5f, 0.5f, 0.5f);
        sphereSep->addChild(marked);
        sphereSep->addChild(VirtualRobot::CoinVisualizationFactory::getMatrixTransform(globalPose));
        SoSphere* sphere = new SoSphere;
        sphere->radius = markerSize;
        sphereSep->addChild(sphere);
    }

    return sphereSep;
}

void CustomSensorVisualisation::setPose(SoSeparator* s, Eigen::Matrix4f &pose) {
    if (s && s->getNumChildren() > 1) {
        SoMatrixTransform* mt = (SoMatrixTransform*)s->getChild(1);
        SbMatrix m_(reinterpret_cast<SbMat*>(pose.data()));
        mt->matrix.setValue(m_);
    }
}


void CustomSensorVisualisation::update(float timestep, float delta) {
    update(sensor->getDerivedMeasurement(timestep, delta));
}

void CustomSensorVisualisation::update(float timestep) {
    update(sensor->getDerivedMeasurement(timestep));
}

void CustomSensorVisualisation::update(CustomSensorMeasurementPtr measurement) {
    if (!measurement) return;

    if (lastFontSize != fontSize || lastMarkerSize != markerSize) {
        lastMarkerSize = markerSize;
        lastFontSize = fontSize;
        markerSphereSeps.clear();
        markerSep->removeAllChildren();
    }

    std::map<std::string, CustomPosePtr> labeledMarker = measurement->getCustomPoses();
    for (const auto &marker : labeledMarker) {
        Eigen::Matrix4f pose = simox::math::pos_quat_to_mat4f(marker.second->position, marker.second->orientation);
        if (!marker.second->localCoordSystem.empty())
            pose = robot->getRobotNode(marker.second->localCoordSystem)->toGlobalCoordinateSystem(pose);
        if (markerSphereSeps.find(marker.first) != markerSphereSeps.end()) {
            // found sphere, then set pose and maybe add child to visualisation
            SoSeparator* sphereSep = std::get<0>(markerSphereSeps[marker.first]);
            bool* sphereSepAdded = std::get<1>(markerSphereSeps[marker.first]);
            if (!(*sphereSepAdded)) markerSep->addChild(sphereSep);
            *sphereSepAdded = true;
            setPose(sphereSep, pose);
        } else {
            // not found, create pose

            SoSeparator* sphereSep = VirtualRobot::CoinVisualizationFactory::CreateCoordSystemVisualization();
            sphereSep->addChild(createLabel(marker.first));
            sphereSep->ref();
            markerSep->addChild(sphereSep);
            bool* sphereSepAdded = new bool(true);
            markerSphereSeps[marker.first] = std::make_tuple(sphereSep, sphereSepAdded);
        }
    }
    // remove sphere from visualisation if not in current measurement
    for (const auto &markerSphereSep : markerSphereSeps) {
        if (labeledMarker.find(markerSphereSep.first) == labeledMarker.end()) {
            bool* sphereSepAdded = std::get<1>(markerSphereSep.second);
            if (*sphereSepAdded) markerSep->removeChild(std::get<0>(markerSphereSep.second));
            *sphereSepAdded= false;
        }
    }
}
