/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_CustomSensorVISUALISATION_H_
#define __MMM_CustomSensorVISUALISATION_H_

#include "../../SensorVisualisation.h"
#include <MMM/Motion/Plugin/CustomPlugin/CustomSensor.h>

#include <Inventor/nodes/SoSeparator.h>
#include <VirtualRobot/RobotConfig.h>
#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <set>

namespace MMM
{

class CustomSensorVisualisation : public SensorVisualisation
{
public:
    CustomSensorVisualisation(CustomSensorPtr sensor, VirtualRobot::RobotPtr robot, std::shared_ptr<VirtualRobot::CoinVisualization> visualization, SoSeparator* sceneSep);

    std::string getType();

    int getPriority();

private:
    void update(float timestep, float delta);
    void update(float timestep);
    void update(CustomSensorMeasurementPtr measurement);

    SoSeparator* createLabel(const std::string &label);
    SoSeparator* createSphere(const Eigen::Vector3f &pose);
    void setPose(SoSeparator* s, Eigen::Matrix4f &pose);

    CustomSensorPtr sensor;
    VirtualRobot::RobotPtr robot;
    SoSeparator* markerSep;

    double lastFontSize;
    double lastMarkerSize;

    std::map<std::string, std::tuple<SoSeparator*, bool*> > markerSphereSeps;
};

}

#endif
