#include "CustomSensorVisualisationFactory.h"
#include "CustomSensorVisualisation.h"

#include <boost/extension/extension.hpp>

#include <MMM/Motion/Plugin/CustomPlugin/CustomSensor.h>

using namespace MMM;

// register this factory
SensorVisualisationFactory::SubClassRegistry CustomSensorVisualisationFactory::registry(VIS_STR(CustomSensor::TYPE), &CustomSensorVisualisationFactory::createInstance);

CustomSensorVisualisationFactory::CustomSensorVisualisationFactory() : SensorVisualisationFactory() {}

CustomSensorVisualisationFactory::~CustomSensorVisualisationFactory() = default;

std::string CustomSensorVisualisationFactory::getName()
{
    return VIS_STR(CustomSensor::TYPE);
}

SensorVisualisationPtr CustomSensorVisualisationFactory::createSensorVisualisation(SensorPtr sensor, VirtualRobot::RobotPtr robot, std::shared_ptr<VirtualRobot::CoinVisualization> visualization, SoSeparator* sceneSep) {
    CustomSensorPtr s = std::dynamic_pointer_cast<CustomSensor>(sensor);
    if (!s) {
        MMM_ERROR << sensor->getType() << " could not be castet to " << CustomSensor::TYPE << std::endl;
        return nullptr;
    }
    return SensorVisualisationPtr(new CustomSensorVisualisation(s, robot, visualization, sceneSep));
}

SensorVisualisationFactoryPtr CustomSensorVisualisationFactory::createInstance(void *)
{
    return SensorVisualisationFactoryPtr(new CustomSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorVisualisationFactoryPtr getFactory() {
    return SensorVisualisationFactoryPtr(new CustomSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorVisualisationFactory::VERSION;
}

