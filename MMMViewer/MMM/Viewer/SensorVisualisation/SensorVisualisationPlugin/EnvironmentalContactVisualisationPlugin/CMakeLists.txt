project(EnvironmentalContactSensorVisualisation)

set(EnvironmentalContactSensorVisualisation_Sources
    EnvironmentalContactSensorVisualisationFactory.cpp
    EnvironmentalContactSensorVisualisation.cpp
)

set(EnvironmentalContactSensorVisualisation_Headers
    EnvironmentalContactSensorVisualisationFactory.h
    EnvironmentalContactSensorVisualisation.h
)

DefineMotionSensorVisualisationPlugin(${PROJECT_NAME} "${EnvironmentalContactSensorVisualisation_Sources}" "${EnvironmentalContactSensorVisualisation_Headers}" EnvironmentalContactSensor)
