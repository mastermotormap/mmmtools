#include "ModelPoseMarkerSensorVisualisation.h"

#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>
#include <VirtualRobot/SceneObject.h>
#include <Inventor/nodes/SoSphere.h>



using namespace MMM;

ModelPoseMarkerSensorVisualisation::ModelPoseMarkerSensorVisualisation(ModelPoseSensorPtr sensor, VirtualRobot::RobotPtr robot, std::shared_ptr<VirtualRobot::CoinVisualization> visualization, SoSeparator* sceneSep) :
    SensorVisualisation(sceneSep),
    sensor(sensor),
    robot(robot),
    lastFontSize(fontSize),
    lastMarkerSize(markerSize)
{
    SoSeparator* robotSep = new SoSeparator();

    if (visualization) {
        SoNode* visualisationNode = visualization->getCoinVisualization();
        if (visualisationNode) robotSep->addChild(visualisationNode);
    }

    markerSep = new SoSeparator();
    robotSep->addChild(markerSep);

    setVisualisationSep(robotSep);
}

std::string ModelPoseMarkerSensorVisualisation::getType() {
    return sensor->getType();
}

int ModelPoseMarkerSensorVisualisation::getPriority() {
    return sensor->getPriority();
}

SoSeparator* ModelPoseMarkerSensorVisualisation::createLabel(const std::string &label) {
    SoSeparator* labelSep = new SoSeparator;
    if (markerSize > 0 && fontSize > 0) {
        Eigen::Vector3f labelVector;
        labelVector << 0, -markerSize, 0;
        Eigen::Matrix4f localPose = Eigen::Matrix4f::Identity();
        localPose.block(0, 3, 3, 1) += labelVector;
        localPose.block(0, 0, 3, 3) *= fontSize;
        labelSep->addChild(VirtualRobot::CoinVisualizationFactory::getMatrixTransform(localPose));
        labelSep->addChild(VirtualRobot::CoinVisualizationFactory::CreateBillboardText(label));
    }
    return labelSep;
}

SoSeparator* ModelPoseMarkerSensorVisualisation::createSphere(Eigen::Matrix4f globalPose) {
    SoSeparator* sphereSep = new SoSeparator;
    if (markerSize > 0) {
        SoMaterial* marked = new SoMaterial;
        marked->ambientColor.setValue(1.0f, 0.2f, 0.2f);
        marked->diffuseColor.setValue(1.0f, 0.2f, 0.2f);
        marked->specularColor.setValue(0.5f, 0.5f, 0.5f);
        sphereSep->addChild(marked);
        sphereSep->addChild(VirtualRobot::CoinVisualizationFactory::getMatrixTransform(globalPose));
        SoSphere* sphere = new SoSphere;
        sphere->radius = markerSize;
        sphereSep->addChild(sphere);
    }
    return sphereSep;
}

void ModelPoseMarkerSensorVisualisation::setPose(SoSeparator* s, Eigen::Matrix4f globalPose) {
    if (s && s->getNumChildren() > 1) {
        SoMatrixTransform* mt = (SoMatrixTransform*)s->getChild(1);
        SbMatrix m_(reinterpret_cast<SbMat*>(globalPose.data()));
        mt->matrix.setValue(m_);
    }
}

void ModelPoseMarkerSensorVisualisation::update(float timestep, float delta) {
    float minTimestep = sensor->getMinTimestep();
    if (timestep >= minTimestep - delta)
        update(sensor->getDerivedMeasurement(timestep, delta));  // Show visualization also if no sensordata yet (instead of zero pose)
    else
        update(sensor->getDerivedMeasurement(minTimestep));
}

void ModelPoseMarkerSensorVisualisation::update(float timestep) {
    float minTimestep = sensor->getMinTimestep();
    if (timestep >= minTimestep)
        update(sensor->getDerivedMeasurement(timestep)); // Show visualization also if no sensordata yet (instead of zero pose)
    else
        update(sensor->getDerivedMeasurement(minTimestep));
}

void ModelPoseMarkerSensorVisualisation::update(ModelPoseSensorMeasurementPtr measurement) {
    if (measurement) robot->setGlobalPose(measurement->getRootPose());

    if (lastFontSize != fontSize || lastMarkerSize != markerSize) {
        lastMarkerSize = markerSize;
        lastFontSize = fontSize;
        markerSphereSeps.clear();
        markerSep->removeAllChildren();
    }

    std::vector<VirtualRobot::SensorPtr> sensors = robot->getSensors();

    for (const auto &sensor : sensors) {
        if (!sensor->getName().empty()) {
            if (markerSphereSeps.find(sensor->getName()) != markerSphereSeps.end()) {
                SoSeparator* sphereSep = markerSphereSeps[sensor->getName()];
                setPose(sphereSep, sensor->getGlobalPose());
            } else {
                // not found, create sphere
                SoSeparator* sphereSep = createSphere(sensor->getGlobalPose());
                sphereSep->addChild(createLabel(sensor->getName()));
                sphereSep->ref();
                markerSep->addChild(sphereSep);
                markerSphereSeps[sensor->getName()] = sphereSep;
            }
        }
    }
}
