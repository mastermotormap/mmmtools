#include "ModelPoseSensorVisualisation.h"

#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <VirtualRobot/SceneObject.h>


using namespace MMM;

ModelPoseSensorVisualisation::ModelPoseSensorVisualisation(ModelPoseSensorPtr sensor, VirtualRobot::RobotPtr robot, std::shared_ptr<VirtualRobot::CoinVisualization> visualization, SoSeparator* sceneSep) :
    SensorVisualisation(sceneSep),
    sensor(sensor),
    robot(robot)
{
    SoSeparator* robotSep = new SoSeparator();

    if (visualization) {
        SoNode* visualisationNode = visualization->getCoinVisualization();
        if (visualisationNode) robotSep->addChild(visualisationNode);
    }

    setVisualisationSep(robotSep);
}

std::string ModelPoseSensorVisualisation::getType() {
    return sensor->getType();
}

int ModelPoseSensorVisualisation::getPriority() {
    return sensor->getPriority();
}

void ModelPoseSensorVisualisation::update(float timestep, float delta) {
    float minTimestep = sensor->getMinTimestep();
    if (timestep >= minTimestep - delta)
        update(sensor->getDerivedMeasurement(timestep, delta));  // Show visualization also if no sensordata yet (instead of zero pose)
    else
        update(sensor->getDerivedMeasurement(minTimestep));
}

void ModelPoseSensorVisualisation::update(float timestep) {
    float minTimestep = sensor->getMinTimestep();
    if (timestep >= minTimestep)
        update(sensor->getDerivedMeasurement(timestep)); // Show visualization also if no sensordata yet (instead of zero pose)
    else
        update(sensor->getDerivedMeasurement(minTimestep));
}

void ModelPoseSensorVisualisation::update(ModelPoseSensorMeasurementPtr measurement) {
    if (measurement) robot->setGlobalPose(measurement->getRootPose());
}
