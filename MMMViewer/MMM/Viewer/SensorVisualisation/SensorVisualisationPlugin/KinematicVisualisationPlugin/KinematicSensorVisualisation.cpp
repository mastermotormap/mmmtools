#include "KinematicSensorVisualisation.h"

using namespace MMM;

KinematicSensorVisualisation::KinematicSensorVisualisation(KinematicSensorPtr sensor, VirtualRobot::RobotPtr robot, SoSeparator* sceneSep) :
    SensorVisualisation(sceneSep),
    sensor(sensor),
    robot(robot)
{
}

std::string KinematicSensorVisualisation::getType() {
    return sensor->getType();
}

int KinematicSensorVisualisation::getPriority() {
    return sensor->getPriority();
}

void KinematicSensorVisualisation::update(float timestep, float delta) {
    float minTimestep = sensor->getMinTimestep();
    if (timestep >= minTimestep - delta)
        update(sensor->getDerivedMeasurement(timestep, delta));  // Show visualization also if no sensordata yet (instead of zero pose)
    else
        update(sensor->getDerivedMeasurement(minTimestep));
}

void KinematicSensorVisualisation::update(float timestep) {
    float minTimestep = sensor->getMinTimestep();
    if (timestep >= minTimestep)
        update(sensor->getDerivedMeasurement(timestep)); // Show visualization also if no sensordata yet (instead of zero pose)
    else
        update(sensor->getDerivedMeasurement(minTimestep));
}

void KinematicSensorVisualisation::update(KinematicSensorMeasurementPtr measurement) {
     if (!measurement) return;

     if (robot) {
        robot->setJointValues(measurement->getJointAngleMap());
     }
}

void KinematicSensorVisualisation::displayVisualisation(bool display) {
    SensorVisualisation::displayVisualisation(display);
    if (display) update(lastTimestep, lastDelta);
}
