project(KinematicSensorVisualisation)

set(KinematicSensorVisualisation_Sources
    KinematicSensorVisualisationFactory.cpp
    KinematicSensorVisualisation.cpp
)

set(KinematicSensorVisualisation_Headers
    KinematicSensorVisualisationFactory.h
    KinematicSensorVisualisation.h
)

DefineMotionSensorVisualisationPlugin(${PROJECT_NAME} "${KinematicSensorVisualisation_Sources}" "${KinematicSensorVisualisation_Headers}" KinematicSensor)
