#include "SensorVisualisationWidgetItem.h"

namespace MMM
{

SensorVisualisationWidgetItem::SensorVisualisationWidgetItem(std::string name, MMM::SensorVisualisationPtr sensorVisualisation) :
    sensorVisualisation(sensorVisualisation),
    display(sensorVisualisation->isVisible()),
    priority(sensorVisualisation->getPriority())
{
    setCheckState(0, display ? Qt::Checked : Qt::Unchecked);
    setFlags(flags() | Qt::ItemIsUserCheckable);
    setText(1, QString::fromStdString(name));
}

SensorVisualisationWidgetItem::SensorVisualisationWidgetItem(std::string name, int priority) :
    display(true),
    priority(priority)
{
    setCheckState(0, Qt::Checked);
    setFlags(flags() | Qt::ItemIsUserCheckable);
    setText(1, QString::fromStdString(name));
}

MMM::SensorVisualisationPtr SensorVisualisationWidgetItem::getVisualisation() {
   return sensorVisualisation.lock();
}

void SensorVisualisationWidgetItem::minimize() {
    if (this->childCount() == 1) {
        SensorVisualisationWidgetItem* c = static_cast<SensorVisualisationWidgetItem*>(this->child(0));
        MMM::SensorVisualisationPtr vis = c->getVisualisation();
        if (vis) {
            this->sensorVisualisation = vis;
            this->display = vis->isVisible();
            this->setCheckState(0, display ? Qt::Checked : Qt::Unchecked);
            this->priority = c->priority;
            this->removeChild(c);
            delete c;
        }
    }
}

void SensorVisualisationWidgetItem::changeVisualisation(bool display) {
    if (this->display != display) {
        if (this->childCount() > 0)
            for (int i = 0; i < this->childCount(); i++) {
                SensorVisualisationWidgetItem* c = static_cast<SensorVisualisationWidgetItem*>(this->child(i));
                c->changeVisualisation(display);
            }
        else if (auto vis = sensorVisualisation.lock()){
            vis->displayVisualisation(display);
        }
        this->display = display;
        this->setCheckState(0, display ? Qt::Checked : Qt::Unchecked);
    }
}

int SensorVisualisationWidgetItem::getPriority() {
    return priority;
}

}

