#include "SensorVisualisationHandler.h"

#include "../OptionsMenu.h"
#include <VirtualRobot/RobotConfig.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationNode.h>
#include <SimoxUtility/math/convert.h>
#include <MMM/Motion/MotionRecording.h>

namespace MMM
{

SensorVisualisationHandler::SensorVisualisationHandler(SoSeparator* sceneGraph, bool interpolationEnabled, float timestepDelta)
    : sensorVisualisationSeperator(new SoSeparator()),
      interpolationEnabled(interpolationEnabled),
      collisionEnabled(false),
      kinematicStructureEnabled(false),
      timestepDelta(timestepDelta),
      sensorVisualisationTable(new SensorVisualisationTable())
{
    sceneGraph->addChild(sensorVisualisationSeperator);
    sensorVisualisationSeperator->ref();
    sceneGraph->addChild(manipSep);
    manipSep->ref();
    sceneGraph->addChild(bboxSep);
    bboxSep->ref();
}

void SensorVisualisationHandler::updateSensorVisualisation(const std::map<std::string, std::shared_ptr<SensorVisualisationFactory> > &sensorVisualisationFactories) {
    this->sensorVisualisationFactories = sensorVisualisationFactories;
    loadSensorVisualisation(motions);
}

void SensorVisualisationHandler::loadSensorVisualisation(MotionRecordingPtr motions) {
    if (!motions || motions->empty()) return;
    bool visualisationLoaded = false;

    this->motions = motions;

    SoSeparator* sep = new SoSeparator();
    sep->ref();

    sensorVisualisations.clear();
    robots.clear();

    for (const std::string &name : motions->getMotionNames()) {
        auto motion = motions->getMotion(name);
        if (motion->getSensorData().size() == 0) break;

        VirtualRobot::RobotPtr robot;
        std::shared_ptr<VirtualRobot::CoinVisualization> visualization = nullptr;
        if (motion->getModel()) {
            robot = motion->getModel()->cloneScaling();
            robot->reloadVisualizationFromXML();
            robot->showStructure(kinematicStructureEnabled);
            robot->setupVisualization(!kinematicStructureEnabled, true);
            if (collisionEnabled) visualization = robot->getVisualization<VirtualRobot::CoinVisualization>(VirtualRobot::SceneObject::Collision);
            else visualization = robot->getVisualization<VirtualRobot::CoinVisualization>(VirtualRobot::SceneObject::Full);
        }

        robots[motion->getName()] = robot;

        std::map<std::string, SensorVisualisationPtr> motionSensorVis;
        for (auto sensor : motion->getPrioritySortedSensorData()) {
            SensorVisualisationFactoryPtr factory = sensorVisualisationFactories[VIS_STR(sensor->getType().c_str())];
            if (factory) {
                SensorVisualisationPtr visualisation = factory->createSensorVisualisation(sensor, robot, visualization, sep);
                if (visualisation) motionSensorVis[sensor->getUniqueName()] = visualisation;
            } else {
                MMM_INFO << "No sensor visualisation plugin for sensor " << sensor->getType() << " of motion " << motion->getName() << std::endl;
            }
        }

        if (motionSensorVis.size() > 0) {
            sensorVisualisationSeperator->removeAllChildren();
            sensorVisualisationSeperator->addChild(sep);
            sensorVisualisations[motion->getName()] = motionSensorVis;
            visualisationLoaded = true;
        } else {
            MMM_INFO << "No visualisation for motion " << motion->getName() << " possible!" << std::endl;
        }
    }

    if (visualisationLoaded) {
        sensorVisualisationTable->populateTable(motions, sensorVisualisations);
    } else {
        std::string message = "No motion visualisation!";
        sensorVisualisationTable->displayMessage(message);
        MMM_ERROR << message << " Check needed sensor and sensor visualisation plugins!" << std::endl;
    }

    emit sensorVisualisationLoaded(visualisationLoaded);
}

bool SensorVisualisationHandler::changeTimestep(float timestep) {
    if (!motions || motions->empty()) return false;

    manipSep->removeAllChildren();
    bboxSep->removeAllChildren();

    for (const std::string &name : motions->getMotionNames()) {
        auto motion = motions->getMotion(name);
        // TODO: Sort only once, not every timestep!
        std::vector<SensorPtr> sortedSensors = motion->getPrioritySortedSensorData();
        // Set the Kinematicsensors first (needed for some visualisations)!
        std::stable_sort(sortedSensors.begin(), sortedSensors.end(), [] (SensorPtr p1, SensorPtr p2) {
            if (p1->getType() == "Kinematic" && p2->getType() != "Kinematic") return 1;
            else if (p1->getType() != "Kinematic" && p2->getType() == "Kinematic") return -1;
            else return 0;
        });

        //if (robots[name]) motion->initializeModel(robots[name], timestep); // TODO remove line

        for (auto sensor : sortedSensors) {
            SensorVisualisationPtr visualisation = sensorVisualisations[motion->getName()][sensor->getUniqueName()];
            if (visualisation) {
                if (interpolationEnabled) visualisation->updateVisualisation(timestep);
                else visualisation->updateVisualisation(timestep, timestepDelta);
            }
        }
    }

    return true;
}

void SensorVisualisationHandler::setInterpolation(bool enabled) {
    interpolationEnabled = enabled;
}

void SensorVisualisationHandler::setCollisionModel(bool enabled) {
    if (enabled != collisionEnabled) {
        collisionEnabled = enabled;
        loadSensorVisualisation(motions);
    }
}

void SensorVisualisationHandler::setKinematicStructure(bool enabled) {
    kinematicStructureEnabled = enabled;
}

void SensorVisualisationHandler::setTimestepDelta(float delta) {
    timestepDelta = delta;
}

QTreeWidget* SensorVisualisationHandler::getSensorVisualisationTable() {
    return sensorVisualisationTable;
}

void SensorVisualisationHandler::displaySensorVisualisationTable(bool display) {
    if (display) sensorVisualisationTable->show();
    else sensorVisualisationTable->hide();
}

bool SensorVisualisationHandler::optionChanged(Option* option) {
    if(option->getType() == Option::Type::VIS_TABLE) {
        displaySensorVisualisationTable(option->getValue().toBool());
    }
    else if(option->getType() == Option::Type::INTERPOLATION) {
        setInterpolation(option->getValue().toBool());
    }
    else if(option->getType() == Option::Type::COLLISION_MODEL) {
        setCollisionModel(option->getValue().toBool());
    }
    else {
        bool optionFound = false;
        for (auto visualisations : sensorVisualisations) {
            for (auto vis : visualisations.second) {
                if (vis.second)
                    optionFound |= vis.second->optionChanged(option);
            }
        }
        return optionFound;
    }
    return true;
}

}
