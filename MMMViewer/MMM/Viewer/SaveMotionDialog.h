/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SAVEMOTIONDIALOG_H_
#define __MMM_SAVEMOTIONDIALOG_H_

#include <QDialog>

#ifndef Q_MOC_RUN
#include <MMM/Motion/Motion.h>
#include "../common/MotionListWidget.h"
#endif

namespace Ui {
class SaveMotionDialog;
}

namespace MMM
{

class SaveMotionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SaveMotionDialog(MMM::MotionRecordingPtr motions, QWidget *parent = 0);
    ~SaveMotionDialog();

signals:
    void openMotion(const std::filesystem::path &motionFilePath);
    void motionSaved(const std::filesystem::path &motionFilePath);

private slots:
    void saveMotions();

private:
    void enableDragDrop();
    MMM::MotionRecordingPtr getChoosenMotions();
    bool saveMotions(MMM::MotionRecordingPtr motions, const std::filesystem::path &motionFilePath);
    void errorMessageBox(const std::string &message);

    Ui::SaveMotionDialog* ui;
    MotionListWidget* motionList;
    MMM::MotionRecordingPtr motions;
    bool motionChanged;
};

}

#endif // __MMM_SAVEMOTIONDIALOG_H_
