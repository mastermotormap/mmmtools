#include "ImageSequenceExporter.h"
#include "ImageSequenceExporterDialog.h"
#include <QFileDialog>

#include <MMM/Motion/MotionRecording.h>

using namespace MMM;

ImageSequenceExporter::ImageSequenceExporter(QWidget* widget) :
    MotionHandler(MotionHandlerType::EXPORT, "Export motion as image sequence"),
    searchPath(std::string(MMMTools_SRC_DIR)),
    widget(widget)
{
}

void ImageSequenceExporter::handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> currentRobots) {
    if (!motions || motions->empty()) return;

    QSettings settings;
    std::string exportDirectoryPath = QFileDialog::getExistingDirectory(widget, QString::fromStdString(getDescription()), settings.value("exportmotion/imagesequencepath", QString::fromStdString(searchPath)).toString()).toStdString();
    if (!exportDirectoryPath.empty()) {
        settings.setValue("exportmotion/imagesequencepath", QString::fromStdString(exportDirectoryPath));
        ImageSequenceExporterDialog* dialog = new ImageSequenceExporterDialog(exportDirectoryPath, motions, widget);
        connect(dialog, SIGNAL(jumpTo(float)), this, SIGNAL(jumpTo(float)));
        connect(dialog, SIGNAL(saveScreenshot(float,std::string,std::string)), this, SIGNAL(saveScreenshot(float,std::string,std::string)));
        dialog->show();
    }
}

std::string ImageSequenceExporter::getName() {
    return NAME;
}
