#include "CSVExporterDialog.h"
#include "ui_CSVExporterDialog.h"
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <QMessageBox>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensorMeasurement.h>
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>

using namespace MMM;

CSVExporterDialog::CSVExporterDialog(const std::string &exportDirectoryPath, MMM::MotionRecordingPtr motions, QWidget* parent) :
    QDialog(parent),
    ui(new Ui::CSVExporterDialog),
    motions(motions),
    exportDirectoryPath(exportDirectoryPath)
{
    ui->setupUi(this);

    float minTimestep;
    float maxTimestep;
    motions->getMinMaxTimesteps(minTimestep, maxTimestep);

    ui->MinTimestepSpin->setMinimum(minTimestep);
    ui->MaxTimestepSpin->setValue(minTimestep);
    ui->MinTimestepSpin->setMaximum(maxTimestep);
    ui->MaxTimestepSpin->setMinimum(minTimestep);
    ui->MaxTimestepSpin->setValue(maxTimestep);
    ui->MaxTimestepSpin->setMaximum(maxTimestep);

    connect(ui->ExportButton, SIGNAL(clicked()), this, SLOT(exportMotions()));
    connect(ui->CancelButton, SIGNAL(clicked()), this, SLOT(close()));
}

void CSVExporterDialog::exportMotions() {
    float minTimestep = ui->MinTimestepSpin->value();
    float maxTimestep = ui->MaxTimestepSpin->value();
    int framesPerSecond = ui->FPSSpin->value();

    if (minTimestep > maxTimestep) {
        QMessageBox* msgBox = new QMessageBox(this);
        std::string error_message = "Minimum timestep should be smaller than maximum timestep!";
        msgBox->setText(QString::fromStdString(error_message));
        MMM_INFO << error_message << std::endl;
        msgBox->exec();
    }
    for (const std::string &name : motions->getMotionNames()) {
        auto motion = motions->getMotion(name);
        if (!motion->hasSensor("ModelPose") && !motion->hasSensor("Kinematic")) {
            MMM_INFO << "Not exporting motion with name '" << motion->getName() << "'. The motion has no model pose or kinematic sensor!" << std::endl;
            break;
        }
        std::vector<std::string> headers{"timestep"};
        std::vector<std::vector<float>> csvData;
        auto poseSensor = motion->getSensorByType<ModelPoseSensor>();
        if(poseSensor)
        {
            auto names = {"RootPosition_X",
                         "RootPosition_Y",
                         "RootPosition_Z",
                         "RootOrientation_X",
                         "RootOrientation_Y",
                         "RootOrientation_Z"};
            headers.insert(headers.end(), names.begin(), names.end());
        }
        auto kinSensors = motion->getSensorsByType<KinematicSensor>();
        for (auto kinSensor : kinSensors)
        {
            auto names = kinSensor->getJointNames();
            headers.insert(headers.end(), names.begin(), names.end());
        }

        for(float t = minTimestep;
            t <= maxTimestep; t += 1.0/framesPerSecond)
        {
            std::vector<float> timestepData;
            timestepData.push_back(t);

            if(poseSensor)
            {
                auto data = std::dynamic_pointer_cast<ModelPoseSensorMeasurement>(poseSensor->getMeasurement(t));
                Eigen::Vector3f pos = data->getRootPosition();
                timestepData.push_back(pos(0));
                timestepData.push_back(pos(1));
                timestepData.push_back(pos(2));
                Eigen::Vector3f rot = data->getRootRotationRPY();
                timestepData.push_back(rot(0));
                timestepData.push_back(rot(1));
                timestepData.push_back(rot(2));
            }

            for (auto kinSensor : kinSensors)
            {
                auto data = std::dynamic_pointer_cast<KinematicSensorMeasurement>(kinSensor->getMeasurement(t));
                Eigen::VectorXf angles = data->getJointAngles();
                timestepData.insert(timestepData.end(), angles.data(), angles.data()+ angles.rows());
            }

            csvData.push_back(timestepData);
        }

//        auto locale = QLocale(QLocale::English);
        std::string filename = exportDirectoryPath + "/" + motion->getName()+ ".csv";
        MMM_INFO << "Writing to " << filename << std::endl;
        std::ofstream f(filename.c_str());
        f << simox::alg::join(headers, ",") << "\n";
        for(auto & entry: csvData)
        {
            std::vector<std::string> valueStrings;
            for(auto v : entry)
            {
                valueStrings.push_back(QString::number(v).replace(',','.').toStdString());
//                valueStrings.push_back(locale.toString(v,'f', 6).toStdString());
            }
            f << simox::alg::join( valueStrings, "," ) << "\n";
        }
    }


    close();
}

CSVExporterDialog::~CSVExporterDialog() {
    delete ui;
}
