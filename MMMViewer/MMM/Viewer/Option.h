/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_OPTION_H_
#define __MMM_OPTION_H_

#include <QObject>
#include <QSettings>

namespace MMM
{

class OptionAction {
public:
    virtual QVariant getValue() = 0;
    virtual void updateOption() = 0;
};

class Option : public QObject {

public:
    enum Type {
        VIS_TABLE, FLOOR, COLLISION_MODEL, ANTIALIASING, INTERPOLATION, FONT_SIZE, MARKER_SIZE, MMM_SCALE, MIRROR_MOTION
    };

    static inline std::string type_names[9] = { "vistable", "floor", "collision", "antialiasing", "interpolation", "fontsize", "markersize", "mmmscale", "mirror" };

    Option(Option::Type type, OptionAction* action);

    void save();

    Option::Type getType() {
        return type;
    }

    QVariant getValue() {
        return action->getValue();
    }

private:
    QSettings settings;
    Option::Type type;
    OptionAction* action;
};

}

#endif // OPTION_H
