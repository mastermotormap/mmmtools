/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_OPTIONSMENU_H_
#define __MMM_OPTIONSMENU_H_

#include <MMM/MMMCore.h>
#include <QMenu>
#include <QWidgetAction>
#include <QDoubleSpinBox>
#include <QSettings>
#include <QSignalMapper>
#include <QObject>

#include "Option.h"

namespace MMM
{

class QCheckableAction : public QAction, public OptionAction {
    Q_OBJECT

public:
    QCheckableAction(const std::string &title, Option::Type type, bool initValue, QWidget* parent = 0,
                     const std::string &shortcut = "", QSignalMapper* mapper = 0);

    void setMapping(QSignalMapper* mapper);

    QVariant getValue() override;
    void updateOption() override;

private:
    QSettings settings;
    Option::Type type;
};

class QSpinBoxAction : public QWidgetAction, public OptionAction {
    Q_OBJECT

public:
    QSpinBoxAction(const std::string &title, Option::Type type, double minimum, double maximum,
                   double initValue, double stepSize = 1, int decimals = 1, QWidget* parent = 0, QSignalMapper* mapper = 0);

    void setMapping(QSignalMapper* mapper);

    QVariant getValue() override;

    void updateOption() override;

signals:
    void spinnerChanged();

private:
    QSettings settings;
    Option::Type type;
    QDoubleSpinBox* pSpinBox;
};

class OptionsMenu : public QMenu
{
    Q_OBJECT

public:
    OptionsMenu(QWidget* parent = 0);

    void updateOptions();

    void add(QCheckableAction* action);

    void add(QSpinBoxAction* action);

    bool isUpdating();

public slots:
    void m(QObject* o);

signals:
    void optionChanged(Option* option);

private:
   std::vector<OptionAction*> optionActions;
   bool updating;
};

}

#endif // OPTIONSMENU_H
