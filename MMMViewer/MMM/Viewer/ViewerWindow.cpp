#include "ViewerWindow.h"
#include "ui_ViewerWindow.h"

#include "PluginHandler/PluginHandlerDialog.h"
#include "MotionHandlerFactory.h"
#include "SaveMotionDialog.h"

#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/MotionRecording.h>


#include <iostream>
#include <iomanip>
#include <ctime>
#include <qgl.h>
#include <iostream>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>
#include <Inventor/actions/SoLineHighlightRenderAction.h>
#include <Inventor/nodes/SoUnits.h>
#include <Inventor/Qt/SoQt.h>
#include <QCloseEvent>
#include <QMessageBox>
#include <QFileDialog>
#include <QSignalMapper>
#include <QWidgetAction>
#include <QDir>
#include "../common/HandleMotionsWithoutModel.h"
#include "OptionsMenu.h"
#include <MMM/Motion/MotionRecording.h>
#include <SimoxUtility/algorithm/string/string_conversion.h>

namespace MMM
{

ViewerWindow::ViewerWindow(QWidget* parent) :
    QMainWindow(parent),
    motionHandlerSep(new SoSeparator()),
    sceneGraph(createSceneGraph()),
    floorVisualisation(createFloorVisualisation(sceneGraph)),
    progressTimer(new SoTimerSensor(progressTimerCallback, this)),
    pluginHandlerDialog(new PluginHandlerDialog(this)),
    sensorVisualisationHandler(new SensorVisualisationHandler(sceneGraph)),
    motions(nullptr),
    motionChanged(false),
    minTimestep(0.0f),
    maxTimestep(0.0f),
    framesPerSecond(30),
    currentTimestep(0.0f),
    lastTimestep(0.0f),
    velocity(1.0f),
    ui(new Ui::ViewerWindow)
{
    ui->setupUi(this);

    sceneGraph->addChild(motionHandlerSep);

    qRegisterMetaTypeStreamOperators<QList<float> >("QList<float>");
    qRegisterMetaType<SoSeparator*>("SoSeparator*");
    setupViewer();

    setTitle();

    // Add Motion Menu
    addMotionMenu();
    std::shared_ptr<PluginHandler<MMM::MotionHandlerFactory> > motionPluginHandler(new PluginHandler<MMM::MotionHandlerFactory>("Motion Handler", MOTION_HANDLER_PLUGIN_LIB_DIR));
    motionPluginHandler->updateSignal.connect(std::bind(&ViewerWindow::updateMotionHandler, this, std::placeholders::_1));
    std::shared_ptr<PluginHandler<MMM::SensorFactory> > sensorPluginHandler(new PluginHandler<MMM::SensorFactory>("Sensor", MMM::MotionReaderXML::getStandardLibPath()));
    sensorPluginHandler->updateSignal.connect(std::bind(&ViewerWindow::updateMotion, this, std::placeholders::_1));
    std::shared_ptr<PluginHandler<MMM::SensorVisualisationFactory> > sensorVisualisationPluginHandler(new PluginHandler<MMM::SensorVisualisationFactory>("Sensor Visualisation", SENSOR_VISUALISATION_PLUGIN_LIB_DIR));
    sensorVisualisationPluginHandler->updateSignal.connect(std::bind(&SensorVisualisationHandler::updateSensorVisualisation, sensorVisualisationHandler.get(), std::placeholders::_1));

    pluginHandlerDialog->addPluginHandler(motionPluginHandler);
    pluginHandlerDialog->addPluginHandler(sensorPluginHandler);
    pluginHandlerDialog->addPluginHandler(sensorVisualisationPluginHandler);
    motionPluginHandler->emitUpdate(); // need to be called after adding all other plugin handler because of order of plugins
    sensorPluginHandler->emitUpdate();
    sensorVisualisationPluginHandler->emitUpdate();
    ui->viewerLayout->addWidget(sensorVisualisationHandler->getSensorVisualisationTable());
    connect(sensorVisualisationHandler.get(), SIGNAL(sensorVisualisationLoaded(bool)), this, SLOT(sensorVisualisationLoaded(bool)));

    // Add Option Menu
    optionsMenu = new OptionsMenu(this);
    this->menuBar()->addMenu(optionsMenu);
    connect(optionsMenu, SIGNAL(optionChanged(Option*)), this, SLOT(optionChanged(Option*)));

    // Add Extra Menu
    QMenu* extraMenu = new QMenu(tr("Extra"), this);
    QAction* pluginAction = new QAction(tr("&Plugins..."), extraMenu);
    connect(pluginAction, SIGNAL(triggered()), pluginHandlerDialog, SLOT(show()));
    extraMenu->addAction(pluginAction);
    this->menuBar()->addMenu(extraMenu);
    QAction* focusCamAction = new QAction(tr("&Focus camera"), extraMenu);
    focusCamAction->setShortcut(tr("Ctrl+F"));
    connect(focusCamAction, SIGNAL(triggered()), this, SLOT(focusCameraOnMotion()));
    extraMenu->addAction(focusCamAction);

    setupVelocityComboBox();
    connect(ui->timestepSlider, SIGNAL(valueChanged(int)), this, SLOT(sliderMoved(int)));
    connect(ui->fpsSpinBox, SIGNAL(valueChanged(int)), this, SLOT(fpsChanged(int)));
    connect(ui->playStopButton, SIGNAL(clicked()), this, SLOT(playStopMotion()));
    ui->fpsSpinBox->setValue(framesPerSecond);
    createTimestampString();

    MMM::Sensor::handleSensorFilePath = [](std::filesystem::path &path) {
        auto sensorFilePath = QFileDialog::getOpenFileName(0, QString::fromStdString("Sensor specific file could not be loaded from " + std::string(path))).toStdString();
        if (!sensorFilePath.empty()) {
            path = sensorFilePath;
            return true;
        }
        return false;
    };

    loadLastMotionFile();
}

SoSeparator* ViewerWindow::createSceneGraph() {
    SoSeparator* _sceneGraph = new SoSeparator();
    _sceneGraph->ref();

    SoUnits *u = new SoUnits();
    u->units = SoUnits::MILLIMETERS;
    _sceneGraph->addChild(u);
    return _sceneGraph;
}

SoSwitch* ViewerWindow::createFloorVisualisation(SoSeparator* sceneGraph) {
    SoSwitch* floorSwitch = new SoSwitch();
    floorSwitch->whichChild = SO_SWITCH_ALL;

    SoSeparator* _floorVisualisation = VirtualRobot::CoinVisualizationFactory::CreatePlaneVisualization(Eigen::Vector3f(0.0f, 0.0f, 0.0f), Eigen::Vector3f(0.0f, 0.0f, 1.0f), 10000.0f, 0);
    floorSwitch->addChild(_floorVisualisation);
    sceneGraph->addChild(floorSwitch);
    return floorSwitch;
}

void ViewerWindow::setupViewer() {
    viewer = new SoQtExaminerViewer(ui->viewer, "", TRUE, SoQtExaminerViewer::BUILD_POPUP);
    viewer->setBackgroundColor(SbColor(1.0f, 1.0f, 1.0f));
    viewer->setAccumulationBuffer(viewer->getAccumulationBuffer());
    viewer->setGLRenderAction(new SoLineHighlightRenderAction);
    viewer->setTransparencyType(SoGLRenderAction::SORTED_OBJECT_BLEND);
    viewer->setFeedbackVisibility(true);
    viewer->setSceneGraph(sceneGraph);
    viewer->viewAll();
    setupCamera();
}

void ViewerWindow::setupCamera() {
    SoCamera* camera = viewer->getCamera();

    QList<float> qInitPosition;
    qInitPosition.append(-4.0f);
    qInitPosition.append(0.0f);
    qInitPosition.append(1.0f);
    QList<float> qPosition = settings.value("camera/position", QVariant::fromValue(qInitPosition)).value<QList<float> >();
    camera->position.setValue(SbVec3f(qPosition[0], qPosition[1], qPosition[2]));

    QList<float> qInitOrientation;
    qInitOrientation.append(0.5f);
    qInitOrientation.append(-0.5f);
    qInitOrientation.append(-0.5f);
    qInitOrientation.append(0.5f);
    QList<float> qOrientation = settings.value("camera/orientation", QVariant::fromValue(qInitOrientation)).value<QList<float> >();
    camera->orientation.setValue(SbRotation(qOrientation[0], qOrientation[1], qOrientation[2], qOrientation[3]));

    camera->focalDistance.setValue(settings.value("camera/focalDistance", 4.5f).toFloat());
}

void ViewerWindow::setupVelocityComboBox() {
    for (int i = 1; i <= 8; i++) {
        float v = 0.25f * i;
        ui->velocityBox->addItem(QString::fromStdString(std::to_string(v).append("x")), QVariant(v));
        if (std::abs(velocity - v) < 0.00001) ui->velocityBox->setCurrentIndex(i - 1);
    }
    connect(ui->velocityBox, SIGNAL(currentIndexChanged(int)), this, SLOT(setVelocity(int)));
}

void ViewerWindow::updateMotion(const std::map<std::string, std::shared_ptr<MMM::SensorFactory> > &sensorFactories) {
    motionReader = MMM::MotionReaderXMLPtr(new MMM::MotionReaderXML(sensorFactories)); // TODO Reload?
    motionReader->setHandleMissingModelFile(handleMissingModelFilePath);
}

void ViewerWindow::updateMotionHandler(const std::map<std::string, std::shared_ptr<MMM::MotionHandlerFactory> > &motionHandlerFactories) {
    bool resetMenus = false;
    for (auto it = motionHandler.begin(); it != motionHandler.end();) {
        if (motionHandlerFactories.find(it->first) == motionHandlerFactories.end()) {
            resetMenus = true;
            // TODO close motionHandler!
            // TODO remove Plugin Handler
            pluginHandlerDialog->removePluginHandler(it->second->getPluginHandlerID());
            it = motionHandler.erase(it);
        }
        else it++;
    }
    if (resetMenus) {
        importMotionMenu->clear();
        importMotionMenu->setDisabled(true);
        exportMotionMenu->clear();
        exportMotionMenu->setDisabled(true);
        addSensorMenu->clear();
        addSensorMenu->setDisabled(true);
        generalMenu->clear();
        generalMenu->setDisabled(true);
    }
    QSignalMapper* signalMapper = new QSignalMapper(this);
    std::vector<MMM::MotionHandlerPtr> importMotionHandler;
    for (const auto &motionHandlerFactory : motionHandlerFactories) {
        std::string handlerName = motionHandlerFactory.first;
        if (motionHandler.find(handlerName) != motionHandler.end()) {
            if (resetMenus) goto AddAction;
        } else {
            motionHandler[handlerName] = motionHandlerFactory.second->createMotionHandler(this);
            {
                std::shared_ptr<IPluginHandler> pluginHandler = motionHandler[handlerName]->getPluginHandler();
                if (pluginHandler) pluginHandlerDialog->addPluginHandler(pluginHandler);
            }

            AddAction:
            MMM::MotionHandlerPtr handler = motionHandler[handlerName];
            QAction* handlerAction = new QAction(QString::fromStdString(handler->getDescription()), this);
            handlerAction->setShortcut(QString::fromStdString(handler->getShortCut()));
            connect(handlerAction, SIGNAL(triggered()), signalMapper, SLOT(map()));
            signalMapper->setMapping(handlerAction, QString::fromStdString(handlerName));
            switch (handler->getType()) {
            case MMM::MotionHandlerType::IMPORT:
                importMotionMenu->setEnabled(true);
                importMotionMenu->addAction(handlerAction);
                importMotionHandler.push_back(handler);
                break;
            case MMM::MotionHandlerType::EXPORT:
                if (motions) exportMotionMenu->setEnabled(true);
                exportMotionMenu->addAction(handlerAction);
                break;
            case MMM::MotionHandlerType::ADD_SENSOR:
                if (motions) addSensorMenu->setEnabled(true);
                addSensorMenu->addAction(handlerAction);
                break;
            case MMM::MotionHandlerType::GENERAL:
                if (motions) generalMenu->setEnabled(true);
                generalMenu->addAction(handlerAction);
                break;
            }
            connect(this,  &ViewerWindow::timestepChanged, handler.get(), &MMM::MotionHandler::timestepChanged);
            connect(this,  &ViewerWindow::motionsOpened, handler.get(), &MMM::MotionHandler::motionsOpened);
            connect(handler.get(), &MMM::MotionHandler::openMotions, this, &ViewerWindow::openMotions);
            connect(handler.get(), &MMM::MotionHandler::jumpTo, this, &ViewerWindow::jumpTo);
            connect(handler.get(), &MMM::MotionHandler::saveScreenshot, this, &ViewerWindow::saveScreenshot);
            connect(handler.get(), &MMM::MotionHandler::addVisualisation, this, &ViewerWindow::addVisualisation);
            connect(handler.get(), &MMM::MotionHandler::saveMotion, this, &ViewerWindow::saveMotion);
            connect(handler.get(), &MMM::MotionHandler::updateVisualisation, this, &ViewerWindow::updateVisualisation);
        }
    }

    for (MMM::MotionHandlerPtr imh : importMotionHandler) {
        for (const auto &mh : motionHandler) {
            mh.second->addImportMotionHandler(imh);
        }
    }

    connect(signalMapper, SIGNAL(mapped(const QString &)), this, SLOT(handleMotion(const QString &)));

    if (exportMotionMenu->actions().size() > 0) connect(this, SIGNAL(motionsOpened(bool)), exportMotionMenu, SLOT(setEnabled(bool)));
    if (addSensorMenu->actions().size() > 0) connect(this, SIGNAL(motionsOpened(bool)), addSensorMenu, SLOT(setEnabled(bool)));
    if (generalMenu->actions().size() > 0) connect(this, SIGNAL(motionsOpened(bool)), generalMenu, SLOT(setEnabled(bool)));

}

void ViewerWindow::addMotionMenu() {
    QMenu* motionMenu = new QMenu(tr("&Motion"), this);

    QAction* openMotionAction = new QAction(tr("&Open..."), motionMenu);
    openMotionAction->setShortcut(tr("Ctrl+O"));
    connect(openMotionAction, SIGNAL(triggered()), this, SLOT(openMotion()));
    motionMenu->addAction(openMotionAction);

    QAction* reloadMotionAction = new QAction(tr("&Reload..."), motionMenu);
    reloadMotionAction->setShortcut(tr("F5"));
    connect(reloadMotionAction, SIGNAL(triggered()), this, SLOT(loadLastMotionFile()));
    motionMenu->addAction(reloadMotionAction);

    importMotionMenu = new QMenu(tr("&Import"), motionMenu);
    importMotionMenu->setDisabled(true);
    motionMenu->addMenu(importMotionMenu);

    QAction* saveMotionAction = new QAction(tr("&Save"), motionMenu);
    saveMotionAction->setShortcut(tr("Ctrl+S"));
    saveMotionAction->setDisabled(true);
    connect(this, SIGNAL(motionsOpened(bool)), saveMotionAction, SLOT(setEnabled(bool)));
    connect(saveMotionAction, SIGNAL(triggered()), this, SLOT(saveMotion()));
    motionMenu->addAction(saveMotionAction);

    QAction* saveMotionAsAction = new QAction(tr("&Save As..."), motionMenu);
    saveMotionAsAction->setDisabled(true);
    saveMotionAsAction->setShortcut(tr("Ctrl+Shift+S"));
    connect(this, SIGNAL(motionsOpened(bool)), saveMotionAsAction, SLOT(setEnabled(bool)));
    connect(saveMotionAsAction, SIGNAL(triggered()), this, SLOT(saveMotionAs()));
    motionMenu->addAction(saveMotionAsAction);

    exportMotionMenu = new QMenu(tr("&Export"), motionMenu);
    exportMotionMenu->setDisabled(true);
    motionMenu->addMenu(exportMotionMenu);

    motionMenu->addSeparator();

    addSensorMenu = new QMenu(tr("&Add sensor"), motionMenu);
    addSensorMenu->setDisabled(true);
    motionMenu->addMenu(addSensorMenu);

    generalMenu = new QMenu(tr("&General"), motionMenu);
    generalMenu->setDisabled(true);
    motionMenu->addMenu(generalMenu);

    motionMenu->addSeparator();

    QAction* exitApplication = new QAction(tr("&Quit"), motionMenu);
    exitApplication->setShortcut(tr("Ctrl+Q"));
    connect(exitApplication, SIGNAL(triggered()), this, SLOT(closeWindow()));
    motionMenu->addAction(exitApplication);

    this->menuBar()->addMenu(motionMenu);
}

ViewerWindow::~ViewerWindow() {
    delete ui;
}

void ViewerWindow::showWindow() {
    std::cout << "Starting MMMViewer" << std::endl;
    optionsMenu->updateOptions();
    SoQt::show(this);
    SoQt::mainLoop();
}

void ViewerWindow::closeEvent(QCloseEvent *event) {
    event->setAccepted(closeWindow());
}

bool ViewerWindow::closeWindow() {
    if (motionChanged && QMessageBox::Yes == QMessageBox::question(this, tr("Unsaved changes"), tr("The motion contains unsaved changes. Do you want to save?"), QMessageBox::No|QMessageBox::Yes)) {
        saveMotion();
        return false;
    } else {
        std::cout << "Closing MMMViewer" << std::endl;
        saveCameraSettings();
        settings.setValue("motion/backup", false);
        this->close();
        return true;
    }
}

void ViewerWindow::setTitle(const std::filesystem::path &motionFilePath) {
    std::string windowTitle = motionFilePath.filename();
    if (!windowTitle.empty()) windowTitle.append(" - ");
    windowTitle.append("MMMViewer");
    this->setWindowTitle(QString::fromUtf8(windowTitle.c_str()));
}

bool ViewerWindow::openMotion() {
    std::string motionFilePath = QFileDialog::getOpenFileName(this, tr("Open motion"), settings.value("motion/searchpath", "").toString(), tr("Motion files (*.xml)")).toStdString();
    return openMotion(motionFilePath);
}

bool ViewerWindow::openMotion(const std::filesystem::path &motionFilePath, bool ignoreError) {
    if (!motionFilePath.empty()) {
        try {
            settings.setValue("motion/searchpath", QString::fromStdString(motionFilePath.parent_path()));
            MMM::MotionRecordingPtr motions = motionReader->loadMotionRecording(motionFilePath);
            if (openMotionFile(motions, ignoreError, motionFilePath)) {
                settings.setValue("motion/path", QString::fromStdString(motionFilePath));
                settings.setValue("motion/type", QString::fromStdString("MMM"));
                motionChanged = false;
                settings.setValue("motion/backup", false);
                return true;
            }
        } catch (MMM::Exception::MMMException &e) {
            auto message = "Could not open motion '" + motionFilePath.generic_string() + "'! " + e.what();
            if (!ignoreError) errorMessageBox(message);
            else MMM_WARNING << message << std::endl;
        }
    }
    return false;
}

bool ViewerWindow::openMotions(MMM::MotionRecordingPtr motions) {
    return openMotionFile(motions, false);
}

bool ViewerWindow::openMotionFile(MMM::MotionRecordingPtr motions, bool ignoreError, const std::filesystem::path &motionFilePath) {
    if (motions && !motions->empty()) {
        if (!motions->containsMotionWithAnySensor() && !ignoreError) errorMessageBox("Motion not loaded, because no sensors available!");
        else if (calculateTimesteps(motions)) {
            stopTimer();
            this->motions = motions;
            if (motionFilePath.empty()) {
                setTitle(settings.value("motion/path").toString().toStdString());
                motionChanged = true;
                settings.setValue("motion/backup", true);
                saveMotionBackup();
            }
            else setTitle(motionFilePath);
            sensorVisualisationHandler->loadSensorVisualisation(motions);
            optionsMenu->updateOptions(); // options have to be updated for new sensor visualisations
            emit motionsOpened(true, motions, sensorVisualisationHandler->getCurrentRobots());
            emit timestepChanged(currentTimestep);
            return true;
        }
    }
    return false;
}

void ViewerWindow::sensorVisualisationLoaded(bool loaded) {
    if (loaded) {
        ui->currentTimestep->setText(QString::number(currentTimestep));
        ui->maxTimestep->setText(QString::number(maxTimestep));
        ui->timestepSlider->setEnabled(true);
        ui->timestepSlider->setRange(minTimestep * framesPerSecond, maxTimestep * framesPerSecond);
        ui->timestepSlider->setSliderPosition(currentTimestep * framesPerSecond);
        ui->playStopButton->setEnabled(true);
        sensorVisualisationHandler->changeTimestep(currentTimestep);
    } else {
        ui->currentTimestep->setText(QString::number(0.0f));
        ui->maxTimestep->setText(QString::number(0.0f));
        ui->timestepSlider->setEnabled(false);
        ui->timestepSlider->setRange(0,0);
        ui->timestepSlider->setSliderPosition(0);
        ui->playStopButton->setEnabled(false);
    }
}

void ViewerWindow::optionChanged(Option* option) {
    if(option->getType() == Option::Type::FLOOR) {
        if (option->getValue().toBool()) floorVisualisation->whichChild = SO_SWITCH_ALL;
        else floorVisualisation->whichChild = SO_SWITCH_NONE;
    }
    else if(option->getType() == Option::Type::ANTIALIASING) {
        viewer->setAntialiasing(option->getValue().toBool(), option->getValue().toDouble());
    }
    else if (option->getType() == Option::Type::MMM_SCALE) {
        if (!optionsMenu->isUpdating()) {
            for (const std::string &name : motions->getMotionNames()) {
                auto motion = motions->getMotion(name);
                motion->scaleMotion(option->getValue().toFloat(), true);
            }
            sensorVisualisationHandler->loadSensorVisualisation(motions);
            optionsMenu->updateOptions();
        }
    }
    else if (option->getType() == Option::Type::MIRROR_MOTION) {
        if (!optionsMenu->isUpdating()) {
            if (option->getValue().toBool()) {
                auto mirroredMotions = motions->getMirroredMotionRecording();
                if (!mirroredMotions->empty()) sensorVisualisationHandler->loadSensorVisualisation(mirroredMotions);
                else MMM_ERROR << "Motions cannot be mirrored!" << std::endl;
            }
            else sensorVisualisationHandler->loadSensorVisualisation(motions);
            optionsMenu->updateOptions();
        }
    }
    else if (!sensorVisualisationHandler->optionChanged(option))
        MMM_WARNING << "Option type " << option->getType() << " is not valid!";
}

void ViewerWindow::handleMotion(const QString &handlerName) {
    MMM::MotionHandlerPtr handler = motionHandler[handlerName.toStdString()];
    stopTimer();
    motionHandlerSep->removeAllChildren();
    // TODO close other motionHandler;
    handler->handleMotion(motions, sensorVisualisationHandler->getCurrentRobots());
    handler->timestepChanged(currentTimestep);
}

void ViewerWindow::saveMotion() {
    if (motions->getOriginFilePath().empty()) saveMotionAs();
    else {
        try {
            motions->saveXML(motions->getOriginFilePath());
            motionSaved(motions->getOriginFilePath());
        } catch (MMM::Exception::MMMException &e) {
            MMM_ERROR << "Saving motion failed: " << e.what() << std::endl;
        }
    }
}

void ViewerWindow::saveMotionAs() {
    stopTimer();
    SaveMotionDialog* saveMotionDialog = new SaveMotionDialog(motions, this);
    connect(saveMotionDialog, SIGNAL(openMotion(std::filesystem::path)), this, SLOT(openMotion(std::filesystem::path)));
    connect(saveMotionDialog, SIGNAL(motionSaved(std::filesystem::path)), this, SLOT(motionSaved(std::filesystem::path)));
    saveMotionDialog->open();
}

void ViewerWindow::motionSaved(const std::filesystem::path &motionFilePath) {
    MMM_INFO << "Motions saved at " << motionFilePath << std::endl;
    motionChanged = false;
    settings.setValue("motion/backup", false);
}

bool ViewerWindow::calculateTimesteps(MMM::MotionRecordingPtr motions) {
    float min, max;
    motions->getMinMaxTimesteps(min, max);

    if (min < max + 0.000001) {
        minTimestep = min;
        currentTimestep = minTimestep;
        lastTimestep = minTimestep;
        maxTimestep = max;
        return true;
    }

    return false;
}

void ViewerWindow::progressTimerCallback(void* data, SoSensor* /*sensor*/)
{
    ViewerWindow* viewerWindow = static_cast<ViewerWindow*>(data);
    viewerWindow->progressMotion();
}

void ViewerWindow::progressMotion() {
    currentTimestep = lastTimestep + std::roundf(((SbTime::getTimeOfDay() - progressTimer->getBaseTime()).getValue()) * 1000) / 1000 * velocity;
    if (currentTimestep > maxTimestep) {
        progressTimer->unschedule();
        currentTimestep = maxTimestep;
        lastTimestep = minTimestep;
        jumpTo(currentTimestep);
        ui->playStopButton->setText("Play");
        if (ui->repeatMotionCheckBox->isChecked()) {
            currentTimestep = minTimestep;
            playStopMotion();
        }
    } else {
        jumpTo(currentTimestep);
    }
}

void ViewerWindow::fpsChanged(int newValue)
{
    framesPerSecond = newValue;
    ui->timestepSlider->setRange(minTimestep * framesPerSecond, maxTimestep * framesPerSecond);
    ui->timestepSlider->setSliderPosition(currentTimestep * framesPerSecond);

    if (progressTimer->isScheduled()) {
        progressTimer->unschedule();
        progressTimer->setInterval(SbTime(1.0f / framesPerSecond));
        progressTimer->schedule();
    }
}

void ViewerWindow::sliderMoved(int position)
{
    if (!progressTimer->isScheduled()) {
        currentTimestep = ((float) position) / framesPerSecond;
        lastTimestep = currentTimestep;
        jumpTo(currentTimestep);
    }
}

void ViewerWindow::setVelocity(int index) {
    SoCamera* camera = viewer->getCamera();
    std::cout << camera->nearDistance.getValue() << " " << camera->farDistance.getValue() << " " << camera->focalDistance.getValue() << " " << camera->aspectRatio.getValue() << std::endl;
    if (progressTimer->isScheduled()) {
        progressTimer->unschedule();
        lastTimestep = currentTimestep;
        progressTimer->setBaseTime(SbTime::getTimeOfDay());
        progressTimer->schedule();
    }
    velocity = ui->velocityBox->itemData(index).toFloat();
}

void ViewerWindow::jumpTo(float timestep)
{
    //std::cout << "Jump to timestep " << timestep << std::endl;
    ui->currentTimestep->setText(QString::number(timestep));
    disconnect(ui->timestepSlider, SIGNAL(valueChanged(int)), this, SLOT(sliderMoved(int))); // dont emit SliderMoved, because this would execute jumpTo (with an inaccurate position) !
    ui->timestepSlider->setSliderPosition(timestep * framesPerSecond + 0.001); // adding e-3 because of float to int rounding issues
    emit timestepChanged(timestep);
    connect(ui->timestepSlider, SIGNAL(valueChanged(int)), this, SLOT(sliderMoved(int)));
    sensorVisualisationHandler->changeTimestep(timestep);
}

void ViewerWindow::playStopMotion()
{
    if (!progressTimer->isScheduled()) {
        progressTimer->setInterval(SbTime(1.0 / framesPerSecond));
        progressTimer->setBaseTime(SbTime::getTimeOfDay());
        progressTimer->schedule();
        ui->playStopButton->setText("Stop");
    } else {
        lastTimestep = currentTimestep;
        progressTimer->unschedule();
        ui->playStopButton->setText("Play");
    }
}

void ViewerWindow::saveScreenshot(float timestep, const std::string &directory, const std::string &name)
{
    jumpTo(timestep);
    std::string fileNameStr = (name.empty() ? "ViewerWindow_" + simox::alg::to_string(std::roundf(timestep * 1000) / 1000) : name) + ".png";
    QString fileName = (!directory.empty()) ? QString::fromStdString(directory + "/" + fileNameStr) : QString::fromStdString(fileNameStr);

    if (!MMM::xml::isValid(directory)) {
        std::filesystem::create_directory(directory);
    }

    // TODO this will autofollow the motion which would be nice to have for videos (e.g. walking), but currently produces some bad frames!
    // auto camera = viewer->getCamera();
    // camera->viewAll(sceneGraph, viewer->getViewportRegion());

    viewer->getSceneManager()->render(); // TODO check if required
    viewer->render(); // Adapts the SoCamera parameter to render the whole scene
    QGLWidget* w = (QGLWidget*) viewer->getGLWidget();

    QImage i = w->grabFrameBuffer();
    bool bRes = i.save(fileName, "PNG");
    if (bRes) std::cout << "Wrote image " << name << " on timestep " << timestep << std::endl;
    else std::cout << "Failed writing image on timestep " << timestep << std::endl;
}

void ViewerWindow::focusCameraOnMotion() {
    bool floorChanged = false;
    if (floorVisualisation->whichChild.getValue() == SO_SWITCH_ALL) {
        floorChanged = true;
        floorVisualisation->whichChild = SO_SWITCH_NONE;
    }

    // focus camera
    auto camera = viewer->getCamera();
    camera->viewAll(sceneGraph, viewer->getViewportRegion());

    if (floorChanged) floorVisualisation->whichChild = SO_SWITCH_ALL;
}

void ViewerWindow::updateVisualisation() {
    jumpTo(currentTimestep);
}

void ViewerWindow::stopTimer() {
    if (progressTimer->isScheduled()) playStopMotion();
}

void ViewerWindow::errorMessageBox(const std::string &message) {
    QMessageBox* msgBox = new QMessageBox(this);
    msgBox->setText(QString::fromStdString(message));
    MMM_ERROR << message << std::endl;
    msgBox->exec();
}

void ViewerWindow::loadLastMotionFile() {
    if (settings.value("motion/backup", false).toBool()) {
        std::string path = settings.value("motion/backup/path", "").toString().toStdString();
        if (!path.empty()) {
            MMM_INFO << "Loading backup motion from " << path << std::endl;
            if (!openMotion(path, true)) {
                MMM_WARNING << "Could not load last backup motion from " << path << std::endl;
                settings.setValue("motion/backup/path", "");
            }
        }
    }
    else if (settings.value("motion/type", "").toString() == "MMM") {
        std::string path = settings.value("motion/path", "").toString().toStdString();
        if (!path.empty()) {
            MMM_INFO << "Loading last used motion from " << path << std::endl;
            if (!openMotion(path, true))
                MMM_WARNING << "Could not load last motion from " << path << std::endl;
        }
    }
}

void ViewerWindow::saveCameraSettings() {
    SoCamera* camera = viewer->getCamera();

    QList<float> qPosition;
    for (float pos : std::vector<float>(camera->position.getValue().getValue(), camera->position.getValue().getValue() + 3)) qPosition.append(pos);
    settings.setValue("camera/position", QVariant::fromValue(qPosition));

    QList<float> qOrientation;
    for (float ori : std::vector<float>(camera->orientation.getValue().getValue(), camera->orientation.getValue().getValue() + 4)) qOrientation.append(ori);
    settings.setValue("camera/orientation", QVariant::fromValue(qOrientation));

    settings.setValue("camera/focalDistance", camera->focalDistance.getValue());
}

void ViewerWindow::addVisualisation(SoSeparator* sep) {
    motionHandlerSep->removeAllChildren();
    if (sep) {
        motionHandlerSep->addChild(sep);
        MMM_INFO << "Adding motion handler visualisation with " << sep->getNumChildren() << " children" << std::endl;
    }
}

void ViewerWindow::createTimestampString() {
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    char buffer[80];
    std::strftime(buffer,sizeof(buffer),"%d.%m.%Y-%H:%M:%S",&tm);
    std::stringstream ss;
    ss << buffer;
    timestamp = ss.str();
}

void ViewerWindow::saveMotionBackup() {
    auto tempPath = std::filesystem::path(QDir::tempPath().toStdString()) / "h2t-mmmviewer-motion-backups";
    std::filesystem::create_directory(tempPath);
    try {
        auto motionFilePath = tempPath / std::filesystem::path("motion-backup-" + timestamp + ".xml");
        motions->saveXML(motionFilePath);
        settings.setValue("motion/backup/path", QString::fromStdString(motionFilePath));
    } catch(MMM::Exception::MMMException &e) {
        settings.setValue("motion/backup", false);
    }
}

}
