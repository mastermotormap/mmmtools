/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include <Inventor/Qt/SoQt.h>
#include <MMM/Viewer/ViewerWindow.h>
#include <MMM/MMMCore.h>
#include <QApplication>
#include <QSettings>

int main(int argc, char* argv[])
{
    SoDB::init();
    SoQt::init(argc, argv, "MMMViewer");

    QCoreApplication::setOrganizationName("H2T");
    QCoreApplication::setOrganizationDomain("h2t.anthropomatik.kit.edu");
    QCoreApplication::setApplicationName("MMMViewer");
    QCoreApplication::setAttribute(Qt::AA_UseDesktopOpenGL);
    QLocale::setDefault(QLocale::C); // Revert to local C, otherwise screws up CJSON
    setlocale(LC_ALL, "C");
    QSettings settings;

    if (argc > 1 && (std::string(argv[1]) == "-r" || std::string(argv[1]) == "--reset")) {
        MMM_INFO << "Resetting settings at " << settings.fileName().toStdString() << std::endl;
        settings.clear();
    }
    else
        MMM_INFO << "Loading Settings from " << settings.fileName().toStdString() << std::endl;

    MMM::ViewerWindow* mmmViewer(new MMM::ViewerWindow());
    mmmViewer->showWindow();
}
