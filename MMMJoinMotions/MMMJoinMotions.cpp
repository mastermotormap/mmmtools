/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include <MMM/Motion/Motion.h>
#include "MMMJoinMotionsConfiguration.h"
#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/MotionRecording.h>

#include <string>
#include <vector>

using namespace MMM;

int main(int argc, char *argv[]) {
    MMM_INFO << " --- MMMJoinMotions --- " << std::endl;
    MMMJoinMotionsConfiguration *configuration = new MMMJoinMotionsConfiguration();
    if (!configuration->processCommandLine(argc, argv))
    {
        MMM_ERROR << "Error while processing command line, aborting..." << std::endl;
        return -1;
    }

    try {
        MMM::MotionReaderXMLPtr motionReader(new MMM::MotionReaderXML(true, false, configuration->sensorPluginPaths));
        MMM_INFO << "Reading motion file '" << configuration->inputMotionPath1 << "'!" << std::endl;
        MMM::MotionRecordingPtr motions1 = motionReader->loadMotionRecording(configuration->inputMotionPath1);
        MMM::MotionPtr motion1 = motions1->getMotion(configuration->motionName1);
        MMM_INFO << "Reading motion file '" << configuration->inputMotionPath2 << "'!" << std::endl;
        MMM::MotionRecordingPtr motions2 = motionReader->loadMotionRecording(configuration->inputMotionPath2);
        MMM::MotionPtr motion2 = motions2->getMotion(configuration->motionName2);
        MMM::MotionPtr joinedMotion = MMM::Motion::join(motion1, motion2);

        if (!joinedMotion) return -1; // TODO null

        MMM_INFO << "Writing motions to " << configuration->outputMotionPath << std::endl;
        motions1->replaceAndSave(joinedMotion, configuration->outputMotionPath);

        return 0;
    }
    catch (MMM::Exception::MMMException &e) {
        MMM_ERROR << e.what() << std::endl;
        return -1;
    }
}
