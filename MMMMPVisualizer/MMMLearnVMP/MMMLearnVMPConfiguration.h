#ifndef __MMM_MMMMPVISUALIZERCONFIGURATION_H_
#define __MMM_MMMMPVISUALIZERCONFIGURATION_H_

#include <filesystem>
#include <string>

#include <MMM/MMMCore.h>

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <SimoxUtility/algorithm/string.h>

#include <VirtualRobot/RuntimeEnvironment.h>

#include "../common/ApplicationBaseConfiguration.h"

/*!
    Configuration of MPPlugin.
*/
class MMMMPVisualizerConfiguration : public ApplicationBaseConfiguration
{

public:
    std::string tempMotionFile;
    std::string mpFile;
    std::string outputFileName;
    std::string nodeSetName;
    int numSamples = 1000;
    float timeDuration = 10.0f;

    MMMMPVisualizerConfiguration() : ApplicationBaseConfiguration()
    {
    }

    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("tempMotionFile", "mmm template motion file path");
        VirtualRobot::RuntimeEnvironment::considerKey("mpFile", "mp file");
        VirtualRobot::RuntimeEnvironment::considerKey("nodeSetName", "nodeSet for motion analysis");
        VirtualRobot::RuntimeEnvironment::considerKey("outputFileName", "output file name");
        VirtualRobot::RuntimeEnvironment::considerKey("numSamples", "number of samples");
        VirtualRobot::RuntimeEnvironment::considerKey("timeDuration", "motion time duration");

        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        tempMotionFile = getParameter("tempMotionFile", true, true);
        mpFile = getParameter("mpFile", true, true);

        outputFileName = getParameter("outputDir", false, true);
        if (outputFileName.empty())
            outputFileName = std::filesystem::path(mpFile).string() + "_motion.xml";

        if (VirtualRobot::RuntimeEnvironment::hasValue("numSamples"))
            numSamples = simox::alg::to_<int>(getParameter("numSamples"));

        if (VirtualRobot::RuntimeEnvironment::hasValue("timeDuration"))
            timeDuration = simox::alg::to_<float>(getParameter("timeDuration"));

        nodeSetName = getParameter("nodeSetName");
        if (nodeSetName.empty()) nodeSetName = "RightArm";

        return valid;
    }

    void print()
    {
        MMM_INFO << "*** MMMMPVisualizer Configuration ***" << std::endl;
        std::cout << "Template motion: " << tempMotionFile << std::endl;
        std::cout << "MP file: " << mpFile << std::endl;
    }
};


#endif
