#include "MMMLearnVMPConfiguration.h"
#include <MMM/MotionPrimitive/VMPApply.h>
#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/Motion.h>

#include <string>
#include <vector>
#include <tuple>

#include <mplib/factories/VMPFactory.h>

using namespace MMM;

int main(int argc, char *argv[]) {
    MMM_INFO << " --- MMMMPExporter --- " << std::endl;
    MMMMPVisualizerConfiguration* configuration = new MMMMPVisualizerConfiguration();
    if (!configuration->processCommandLine(argc, argv)) {
        MMM_ERROR << "Error while processing command line, aborting..." << std::endl;
        return -1;
    }

    try {
        mplib::factories::VMPFactory factory;
        auto vmp = factory.createDerivedMP(mplib::representation::VMPType::PrincipalComponent);

        if (!vmp) {
            MMM_ERROR << "Empty VMP!" << std::endl;
            return -3;
        }
        vmp->load(configuration->mpFile);

        MMM::MotionReaderXMLPtr reader(new MMM::MotionReaderXML());
        auto motionRecording = reader->loadMotionRecording(configuration->tempMotionFile);
        auto motion = motionRecording->getReferenceModelMotion();
        if (!motion) {
            MMM_ERROR << "No reference model motion!" << std::endl;
            return -4;
        }
        auto timesteps = motion->getTimesteps();
        if (timesteps.size() < 2) {
            MMM_ERROR << "No valid timesteps!" << std::endl;
            return -5;
        }

        VMPApply* visualizer = new VMPApply(motion, configuration->nodeSetName, timesteps.front(), timesteps.back());

        MMM::MotionPtr m = visualizer->getSampledMotion(*vmp.get(), configuration->numSamples, 0, configuration->timeDuration);
        m->saveXML(configuration->outputFileName);

    } catch (MMM::Exception::MMMException& e) {
        MMM_ERROR << e.what() << std::endl;
        return -2;
    }
}
