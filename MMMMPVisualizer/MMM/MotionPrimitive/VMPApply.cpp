#include "VMPApply.h"

#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>
#include <MMM/Motion/Motion.h>
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/IK/CompositeDiffIK/CompositeDiffIK.h>
#include <VirtualRobot/MathTools.h>
#include <mplib/factories/VMPFactory.h>

namespace MMM
{

VMPApply::VMPApply(VirtualRobot::RobotPtr robot, const std::string &nodeSetName) :
    tempMotion(nullptr),
    robot(robot),
    nodeSet(robot->getRobotNodeSet(nodeSetName)),
    minTimestep(-1.0f),
    maxTimestep(-1.0f)
{
}

VMPApply::VMPApply(MotionPtr motion, const std::string &nodeSetName, float minTimestep, float maxTimestep) :
    tempMotion(motion->clone(true, "temp")),
    robot(tempMotion->getModel()),
    nodeSet(robot->getRobotNodeSet(nodeSetName)),
    minTimestep(minTimestep),
    maxTimestep(maxTimestep),
    pose(Eigen::Matrix4f::Zero())
{
    auto sensors = tempMotion->getSensorsByType<BasicKinematicSensor>();
    for (auto sensor : sensors) {
        for (const auto &jointName : nodeSet->getNodeNames())
            sensor->removeJoint(jointName);
    }
}

MotionPtr VMPApply::getTempMotion() {
    return tempMotion;
}

VirtualRobot::RobotPtr VMPApply::getRobot() {
    return robot;
}

Eigen::VectorXf VMPApply::compute(mplib::representation::vmp::VMP &mp, float scaled) {
    double canVal = 1 - (1 * scaled);

    mplib::core::DVec2d currentState = mp.calculateDesiredState(canVal);

    if (mp.getSpace() == mplib::representation::Space::TaskSpace7D)
    {
        Eigen::Matrix4f goalPose = mplib::math::util::toPose<float>(currentState);

        VirtualRobot::CompositeDiffIK::Parameters cp;
        cp.resetRnsValues = false;
        cp.returnIKSteps = true;
        cp.steps = 10;
        VirtualRobot::CompositeDiffIKPtr cik(new VirtualRobot::CompositeDiffIK(nodeSet));
        VirtualRobot::CompositeDiffIK::NullspaceJointLimitAvoidancePtr nsjla(new VirtualRobot::CompositeDiffIK::NullspaceJointLimitAvoidance(nodeSet));
        nsjla->kP = 0.1f;
        cik->addNullspaceGradient(nsjla);
        cik->addTarget(nodeSet->getTCP(), goalPose, VirtualRobot::IKSolver::All);
        cik->solve(cp);

        pose = goalPose;

        return nodeSet->getJointValuesEigen();
    }
    else
    {
        if (currentState.size() != nodeSet->size())
            throw MMM::Exception::MMMException("VMP joint space and nodeset do not match!");

        Eigen::VectorXf jointValues(nodeSet->size());
        for(long j = 0; j < jointValues.rows(); ++j)
            jointValues(j) = currentState[j][0];

        nodeSet->setJointValues(jointValues);
        pose = nodeSet->getTCP()->getPoseInRootFrame();

        return jointValues;
    }
}

Eigen::VectorXf VMPApply::compute(mplib::representation::vmp::VMP &mp, float timestep, float scaled) {
    if (tempMotion) {
        float tempMotionTimestep = timestep;
        if (minTimestep > 0 && maxTimestep > minTimestep) {
            tempMotionTimestep = minTimestep + scaled * (maxTimestep - minTimestep);
        }
        tempMotion->initializeModel(robot, tempMotionTimestep);
    }

    return compute(mp, scaled);
}

Eigen::Matrix4f VMPApply::getLastPose() {
    return pose;
}

MMM::MotionPtr MMM::VMPApply::getSampledMotion(mplib::representation::vmp::VMP &vmp, unsigned int nsamples, float minTimeStep, float maxTimeStep, const std::string &motionName)
{
    if (minTimeStep < 0 || minTimeStep > maxTimeStep) return nullptr;

    MotionPtr motion = tempMotion->clone(true, motionName.empty() ? tempMotion->getName() + "_" + nodeSet->getName() + "_" + vmp.getRepresentationType() : motionName);

    vmp.prepareExecution();

    KinematicSensorPtr kineSensor(new KinematicSensor(nodeSet->getNodeNames()));
    for(unsigned int i = 0; i <= nsamples; ++i)
    {
        float ti = i / (float)nsamples;
        float timestep = minTimeStep + ti * (maxTimeStep - minTimeStep);

        KinematicSensorMeasurementPtr measure(new KinematicSensorMeasurement(timestep, compute(vmp, timestep, ti)));
        kineSensor->addSensorMeasurement(measure);
    }
    motion->addSensor(kineSensor);

    return motion;
}

}
