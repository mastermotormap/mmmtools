#pragma once

#include <MMM/MMMCore.h>

#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/MathTools.h>
#include <VirtualRobot/Import/RobotImporterFactory.h>
#include <mplib/factories/AbstractMPFactory.h>
#include <mplib/representation/AbstractMovementPrimitive.h>
#include <mplib/core/types.h>

namespace MMM
{

BETTER_ENUM(RelativePose, short, RootPose = 0, FirstFrameRootPoseFloor = 10)

class SampledTrajectoryExtractor
{
public:
    SampledTrajectoryExtractor(mplib::representation::Space space,
                               MotionPtr motion,
                               const std::string& nodeSetName,
                               float minTimestep = -1,
                               float maxTimestep = -1,
                               int nsamples = 1000,
                               RelativePose relativePose = RelativePose::RootPose);

    virtual ~SampledTrajectoryExtractor() = default;

    float getMinTimestep();

    float getMaxTimestep();

    std::shared_ptr<mplib::core::SampledTrajectory> getSampledTrajectory();

protected:
    MotionPtr motion;
    const std::string& nodeSetName;
    float minTimestep;
    float maxTimestep;
    int nsamples;
    std::shared_ptr<mplib::core::SampledTrajectory> traj;
    RelativePose relativePose;
    Eigen::Matrix4f initialPose;
};

using AbstractMPLearnerPtr = std::shared_ptr<SampledTrajectoryExtractor>;

template<typename Factory>
class MPLearner : public SampledTrajectoryExtractor
{
public:
    MPLearner(typename Factory::Type type,
              MotionPtr motion,
              const std::string& nodeSetName,
              float minTimestep = -1,
              float maxTimestep = -1,
              int kernelSize = 100,
              int nsamples = 1000,
              RelativePose relativePose = RelativePose::RootPose);

    MPLearner(std::shared_ptr<typename Factory::MovementPrimitive> mp,
              MotionPtr motion,
              const std::string& nodeSetName,
              float minTimestep = -1,
              float maxTimestep = -1,
              int nsamples = 1000,
              RelativePose relativePose = RelativePose::RootPose);

    std::shared_ptr<typename Factory::MovementPrimitive> getMP();

private:
    std::shared_ptr<typename Factory::MovementPrimitive> createMP(typename Factory::Type type, int kernelSize);

    std::shared_ptr<typename Factory::MovementPrimitive> mp;
};

}
