#include "MPLearner.h"

#include <MMM/Motion/Motion.h>
#include <MMM/Motion/Sensor/BasicKinematicSensor.h>
#include <SimoxUtility/algorithm/string.h>
#include <SimoxUtility/math/convert.h>
#include <SimoxUtility/math/pose/invert.h>

#include <mplib/representation/vmp/VMP.h>
#include <mplib/representation/dmp/DiscreteDMP.h>

#include <mplib/factories/VMPFactory.h>
#include <mplib/factories/DMPFactory.h>

namespace MMM
{

SampledTrajectoryExtractor::SampledTrajectoryExtractor(mplib::representation::Space space, MotionPtr motion, const std::string &nodeSetName, float minTimestep, float maxTimestep, int nsamples, RelativePose relativePose) :
    motion(motion),
    nodeSetName(nodeSetName),
    minTimestep(minTimestep),
    maxTimestep(maxTimestep),
    nsamples(nsamples),
    traj(nullptr),
    relativePose(relativePose),
    initialPose(Eigen::Matrix4f::Identity())
{
    VirtualRobot::RobotPtr robot = motion->getModel()->cloneScaling();
    VirtualRobot::RobotNodeSetPtr nodeSet = robot->getRobotNodeSet(nodeSetName);
    VirtualRobot::RobotNodePtr tcp = nodeSet->getTCP();

    auto timesteps = motion->getTimesteps();
    this->minTimestep = std::max(timesteps.front(), minTimestep);
    this->maxTimestep = maxTimestep > 0 ? std::min(timesteps.back(), maxTimestep) : timesteps.back();

    std::map<double, Eigen::Matrix4d> tcpTrajData;
    std::map<double, mplib::core::DVec> jointTrajData;

    for (int i = 0; i <= nsamples; i++)
    {
        float t = this->minTimestep + (this->maxTimestep - this->minTimestep) * (float) i / (float) nsamples;

        motion->initializeModel(robot, t);

        if (i == 0) {
            Eigen::Matrix4f rootPose = robot->getGlobalPose();
            switch (relativePose) {
            case RelativePose::RootPose:
                initialPose = rootPose;
                break;
            case RelativePose::FirstFrameRootPoseFloor:
                Eigen::Vector3f pos = simox::math::mat4f_to_pos(rootPose);
                Eigen::Vector3f rpy = simox::math::mat4f_to_rpy(rootPose);
                pos(2) = 0;
                rpy(0) = 0;
                rpy(1) = 0;
                initialPose = simox::math::pos_rpy_to_mat4f(pos, rpy);
                break;
            }
        }

        Eigen::Matrix4f tcpPose;
        switch (relativePose) {
        case RelativePose::RootPose:
            tcpPose = tcp->getPoseInRootFrame();
            break;
        case RelativePose::FirstFrameRootPoseFloor:
            tcpPose = simox::math::inverted_pose(initialPose) * tcp->getGlobalPose();
            break;
        }

        double ti = (double) i / (double) nsamples;
        tcpTrajData[ti] = tcpPose.cast<double>();

        mplib::core::DVec jdata;
        std::vector<float> jointvals = nodeSet->getJointValues();
        for(size_t j = 0; j < jointvals.size(); ++j)
            jdata.push_back((double) jointvals[j]);

        jointTrajData[ti] = jdata;
    }

    switch (space)
    {
        case mplib::representation::Space::TaskSpace7D:
        {
            traj.reset(new mplib::core::SampledTrajectory(tcpTrajData, true));
            break;
        }
        case mplib::representation::Space::JointSpace:
        case mplib::representation::Space::Custom: // for now
        {
            traj.reset(new mplib::core::SampledTrajectory(jointTrajData));
            break;
        }
    }
}

float SampledTrajectoryExtractor::getMinTimestep() {
    return minTimestep;
}

float SampledTrajectoryExtractor::getMaxTimestep() {
    return maxTimestep;
}

std::shared_ptr<mplib::core::SampledTrajectory> SampledTrajectoryExtractor::getSampledTrajectory() {
    return traj;
}


template <class Factory>
MPLearner<Factory>::MPLearner(typename Factory::Type type, MotionPtr motion, const std::string &nodeSetName, float minTimestep, float maxTimestep, int kernelSize, int nsamples, RelativePose relativePose) :
    MPLearner(createMP(type, kernelSize), motion, nodeSetName, minTimestep, maxTimestep, nsamples, relativePose)
{
}

template<typename Factory>
MPLearner<Factory>::MPLearner(std::shared_ptr<typename Factory::MovementPrimitive> mp, MotionPtr motion, const std::string &nodeSetName, float minTimestep, float maxTimestep, int nsamples, RelativePose relativePose) :
    SampledTrajectoryExtractor(mp->getSpace(), motion, nodeSetName, minTimestep, maxTimestep, nsamples, relativePose),
    mp(mp)
{
    mp->learnFromTrajectories({*traj.get()});
}

template<class Factory>
std::shared_ptr<typename Factory::MovementPrimitive> MPLearner<Factory>::getMP() {
    return mp;
}

template<typename Factory>
std::shared_ptr<typename Factory::MovementPrimitive> MPLearner<Factory>::createMP(typename Factory::Type type, int kernelSize) {
    Factory factory = Factory();
    factory.addConfig("kernelSize", kernelSize);
    return factory.createDerivedMP(type);
}

template class MPLearner<mplib::factories::DMPFactory>;
template class MPLearner<mplib::factories::VMPFactory>;

}
