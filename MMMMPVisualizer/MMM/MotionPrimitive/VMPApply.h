#pragma once

#include <VirtualRobot/VirtualRobot.h>
#include <mplib/representation/vmp/VMP.h>
#include <MMM/MMMCore.h>

namespace MMM {

class VMPApply
{
public:
    VMPApply(VirtualRobot::RobotPtr robot, const std::string &nodeSetName);

    VMPApply(MotionPtr motion, const std::string &nodeSetName, float minTimestep = -1.0f, float maxTimestep = -1.0f);

    MotionPtr getSampledMotion(mplib::representation::vmp::VMP &vmp, unsigned int nsamples, float minTimeStep = -1.0f, float maxTimeStep = -1.0f, const std::string &motionName = std::string());

    MotionPtr getTempMotion();

    VirtualRobot::RobotPtr getRobot();

    Eigen::VectorXf compute(mplib::representation::vmp::VMP &vmp, float scaled);

    Eigen::VectorXf compute(mplib::representation::vmp::VMP &vmp, float timestep, float scaled);

    Eigen::Matrix4f getLastPose();

private:
    MotionPtr tempMotion;
    VirtualRobot::RobotPtr robot;
    VirtualRobot::RobotNodeSetPtr nodeSet;
    float minTimestep;
    float maxTimestep;
    Eigen::Matrix4f pose;
};

typedef std::shared_ptr<VMPApply> VMPApplyPtr;

}
