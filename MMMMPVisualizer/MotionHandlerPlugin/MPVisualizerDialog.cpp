#include "MPVisualizerDialog.h"
#include "ui_MPVisualizerDialog.h"
#include <MMM/MotionPrimitive/VMPApply.h>
#include <MMM/Motion/MotionRecording.h>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <QFileDialog>
#include <QMessageBox>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>
#include <SimoxUtility/math/convert.h>
#include <MMM/Motion/Sensor/BasicKinematicSensor.h>
#include <mplib/factories/VMPFactory.h>

using namespace MMM;

MPVisualizerDialog::MPVisualizerDialog(MotionRecordingPtr motions, QWidget* parent) :
    QDialog(parent),
    ui(new Ui::MPVisualizerDialog),
    motions(nullptr),
    mpVisualizer(nullptr),
    vmpLearner(nullptr),
    vmp(nullptr),
    currentTimestep(-1.0f)
{
    this->parent = parent;
    ui->setupUi(this);

    ui->GenMotionButton->setEnabled(false);
    ui->AddPointButton->setEnabled(false);
    ui->RemovePointButton->setEnabled(false);

    connect(ui->GenMotionButton, SIGNAL(clicked()), this, SLOT(generateMotion()));
    connect(ui->CancelButton, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->LoadMPButton, SIGNAL(clicked()), this, SLOT(loadMPFile()));

    connect(ui->LearnMPButton, SIGNAL(clicked()), this, SLOT(learnMP()));
    connect(ui->SaveMPButton, SIGNAL(clicked()), this, SLOT(saveMP()));

    connect(ui->AddPointButton, SIGNAL(clicked()), this, SLOT(addPoint()));
    connect(ui->RemovePointButton, SIGNAL(clicked()), this, SLOT(removePoint()));
    connect(ui->VisualizeMotion, SIGNAL(toggled(bool)), this, SLOT(previewResult(bool)));

    connect(ui->MinTimestepSpin, SIGNAL(valueChanged(double)), this, SLOT(minTimestepChanged(double)));
    connect(ui->HumanMotionCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(motionChanged(int)));

    connectTable(true);

    openMotion(motions);
}

MPVisualizerDialog::~MPVisualizerDialog() {
    delete ui;
}

void MPVisualizerDialog::openMotion(MotionRecordingPtr motions) {
    if (!motions) return;
    this->motions = motions;

    std::tuple<float, float> timestepTuple = motions->getMinMaxTimesteps();
    float minTimestep = std::get<0>(timestepTuple);
    float maxTimestep = std::get<1>(timestepTuple);

    ui->MinTimestepSpin->setMinimum(minTimestep);
    ui->MinTimestepSpin->setMaximum(maxTimestep);
    ui->MinTimestepSpin->setValue(minTimestep);

    ui->MaxTimestepSpin->setMinimum(minTimestep);
    ui->MaxTimestepSpin->setMaximum(maxTimestep);
    ui->MaxTimestepSpin->setValue(maxTimestep);

    ui->TargetMinTimestepSpin->setMinimum(minTimestep);
    ui->TargetMinTimestepSpin->setMaximum(maxTimestep);
    ui->TargetMinTimestepSpin->setValue(minTimestep);

    ui->TargetMaxTimestepSpin->setMinimum(minTimestep);
    ui->TargetMaxTimestepSpin->setMaximum(maxTimestep);
    ui->TargetMaxTimestepSpin->setValue(maxTimestep);

    findKinematicMotion();
    getAllAvailableNodeSets();

    outfile = motions->getOriginFilePath().filename().stem().string();

    update();
}

void MPVisualizerDialog::setCurrentTimestep(float timestep) {
    this->currentTimestep = timestep;
    visualize();
}

void MPVisualizerDialog::loadMPFile()
{
    std::filesystem::path mpFile = QFileDialog::getOpenFileName(parent, QString::fromStdString("Load MP File"),
                                      QDir::currentPath()).toStdString();
    if (mpFile.empty()) return;

    mplib::factories::VMPFactory factory;
    auto vmp = factory.createDerivedMP(mplib::representation::VMPType::Gaussian);
    if (!vmp) return;


    ui->MPFileEdit->setText(QString::fromStdString(mpFile));

    this->vmp = std::move(vmp);
    this->vmp->load(mpFile);

    nodeSetName = ui->NodeSetComboBox->currentText().toStdString();
    mpVisualizer.reset(new VMPApply(currentMotion, nodeSetName));

    robot = mpVisualizer->getRobot();
    visLoaded = false;
    fillGoalInTable();
    ui->GenMotionButton->setEnabled(true);
}

void MPVisualizerDialog::generateMotion()
{
    if (!vmp) return;

    auto goals = getRowVecFromTable(1, 1);
    vmp->removeViaPoints();
    vmp->setGoals(goals);
    for(int i = 2; i < ui->GoalTableWidget->rowCount(); ++i)
    {
        double canVal = ui->GoalTableWidget->item(i, 0)->text().toDouble();
        if (canVal > 0) {
            auto viapoint = getRowVecFromTable(i, 1);
            vmp->setViaPoint(canVal, viapoint);
        }
        else {
            MMM_ERROR << "ViaPoint at 0. Cannot add motion!" << std::endl;
            return;
        }
    }

    float minTimestep = (float) ui->MinTimestepSpin->value();
    float maxTimestep = (float) ui->MaxTimestepSpin->value();

    MMM::MotionPtr newMotion = mpVisualizer->getSampledMotion(*vmp.get(), 1000, minTimestep, maxTimestep);

    newMotion->setName(ui->MotionNameEdit->text().toStdString());

    motions->addMotion(newMotion);
    emit genMotion(motions);
}

void MPVisualizerDialog::fillGoalInTable()
{
    connectTable(false);
    auto goal = vmp->getGoals();

    ui->GoalTableWidget->clear();
    int nrows = 2;

    switch(vmp->getSpace())
    {
    case mplib::representation::Space::TaskSpace7D:
    {
        int ncols = 8;

        ui->GoalTableWidget->setRowCount(nrows);
        ui->GoalTableWidget->setColumnCount(ncols);

        std::vector<std::string > names{"canVal", "x", "y", "z", "qw", "qx", "qy", "qz"};
        ui->GoalTableWidget->setItem(1, 0, new QTableWidgetItem(QString::number(0, 'f', 3)));

        for(int i = 0; i < ncols; ++i)
        {
            ui->GoalTableWidget->setItem(0, i, new QTableWidgetItem(QString::fromStdString(names[i])));
            if (i < (int) goal.size())
                ui->GoalTableWidget->setItem(1, i+1, new QTableWidgetItem(QString::number(goal[i], 'f', 3)));
        }
        break;
    }
    case mplib::representation::Space::JointSpace:
    case mplib::representation::Space::Custom: // for now
    {
        std::vector<std::string > nodeNames = robot->getRobotNodeSet(nodeSetName)->getNodeNames();
        ui->GoalTableWidget->setRowCount(nrows);
        ui->GoalTableWidget->setColumnCount(nodeNames.size()+1);
        ui->GoalTableWidget->setItem(0, 0, new QTableWidgetItem(QString::fromStdString("canVal")));
        ui->GoalTableWidget->setItem(1, 0, new QTableWidgetItem(QString::number(0, 'f', 3)));

        for(size_t i = 0; i < nodeNames.size(); ++i)
        {
            ui->GoalTableWidget->setItem(0, i+1, new QTableWidgetItem(QString::fromStdString(nodeNames[i])));
            ui->GoalTableWidget->setItem(1, i+1, new QTableWidgetItem(QString::number(goal[i], 'f', 3)));
        }
    }

    }

    ui->AddPointButton->setEnabled(true);
    connectTable(true);
}

mplib::core::DVec MPVisualizerDialog::getRowVecFromTable(int nrow, int jumpCols)
{
    int ncols = ui->GoalTableWidget->columnCount();
    if (nrow == -1)
        nrow = ui->GoalTableWidget->rowCount()-1;

    mplib::core::DVec goal;
    for(int i = jumpCols; i < ncols; i++)
    {
        double val = ui->GoalTableWidget->item(nrow,i)->text().toDouble();
        goal.push_back(val);
    }

    return goal;
}

void MPVisualizerDialog::getAllAvailableNodeSets()
{
    if (!currentMotion || !currentMotion->getModel()) return;

    ui->NodeSetComboBox->clear();

    std::vector<VirtualRobot::RobotNodeSetPtr > nodeSets = currentMotion->getModel()->getRobotNodeSets();

    int index = -1;
    for (size_t i = 0; i < nodeSets.size(); ++i)
    {
        VirtualRobot::RobotNodeSetPtr nodeSet = nodeSets[i];
        if (nodeSetName == nodeSet->getName())
            index = i;
        ui->NodeSetComboBox->addItem(QString::fromStdString(nodeSet->getName()));
    }
    if (index >= 0) ui->NodeSetComboBox->setCurrentIndex(index);
    else nodeSetName = ui->NodeSetComboBox->currentText().toStdString();
}

void MPVisualizerDialog::addRowInTable(const mplib::core::DVec &point)
{
    connectTable(false);
    ui->GoalTableWidget->insertRow(ui->GoalTableWidget->rowCount());
    for (size_t i = 0; i < point.size(); ++i)
    {
        ui->GoalTableWidget->setItem(ui->GoalTableWidget->rowCount()-1, i, new QTableWidgetItem(QString::number(point[i], 'f', 3)));
    }
    connectTable(true);

    ui->RemovePointButton->setEnabled(true);
    visualize();
}

void MPVisualizerDialog::connectTable(bool value) {
    if (!value)
        disconnect(ui->GoalTableWidget, SIGNAL(cellChanged(int, int)), this, SLOT(tableCellChanged(int,int)));
    else
        connect(ui->GoalTableWidget, SIGNAL(cellChanged(int, int)), this, SLOT(tableCellChanged(int,int)));
}

void MPVisualizerDialog::findKinematicMotion()
{
    currentMotion = nullptr;
    ui->HumanMotionCombo->clear();

    for(auto motion : *motions)
    {
        if (motion->hasSensor(BasicKinematicSensor::TYPE))
        {
            ui->HumanMotionCombo->addItem(QString::fromStdString(motion->getName()));
            kinematicMotions.push_back(motion);
        }
    }

    float minTimestep = -1.0f;
    float maxTimestep = std::numeric_limits<float>::max();
    if (!kinematicMotions.empty()) {
        currentMotion = kinematicMotions.at(0);

        auto sensor = currentMotion->getSensorByType(BasicKinematicSensor::TYPE);

        minTimestep = std::max(sensor->getMinTimestep(), minTimestep);
        maxTimestep = std::min(sensor->getMaxTimestep(), maxTimestep);

        ui->HumanMotionCombo->setCurrentIndex(0);
    }

    if (minTimestep >= 0.0f) {
        ui->MinTimestepSpin->setValue(minTimestep);
        ui->TargetMinTimestepSpin->setValue(minTimestep);
    }
    if (maxTimestep < std::numeric_limits<float>::max()) {
        ui->MaxTimestepSpin->setValue(maxTimestep);
        ui->TargetMaxTimestepSpin->setValue(maxTimestep);
    }
}


void MPVisualizerDialog::minTimestepChanged(double val)
{
    double maxval = ui->MaxTimestepSpin->value();
    if (val > maxval)
        ui->MaxTimestepSpin->setValue(val + 0.1);
}

void MPVisualizerDialog::learnMP()
{
    if (!currentMotion) return;

    float minTimestep = ui->MinTimestepSpin->value();
    float maxTimestep = ui->MaxTimestepSpin->value();
    int nsamples = ui->NSamplesSpin->value();
    int kernelSize = ui->KernelSizeSpin->value();

    auto mpType = ui->JointSpaceButton->isChecked() ? mplib::representation::VMPType::Gaussian
                                                    : mplib::representation::VMPType::TaskSpaceGaussian;

    nodeSetName = ui->NodeSetComboBox->currentText().toStdString();

    vmpLearner.reset(new MPLearner<mplib::factories::VMPFactory>(mpType, currentMotion, nodeSetName, minTimestep, maxTimestep, kernelSize, nsamples));
    vmp = vmpLearner->getMP();

    fillGoalInTable();
    ui->GenMotionButton->setEnabled(true);

    update();
}

void MPVisualizerDialog::update() {
    if (!currentMotion) return;
    nodeSetName = ui->NodeSetComboBox->currentText().toStdString();
    ui->MotionNameEdit->setText(QString::fromStdString(currentMotion->getName() + "_" + nodeSetName + "_vmp"));

    if (!vmpLearner) return;
    mpVisualizer.reset(new MMM::VMPApply(currentMotion, nodeSetName, vmpLearner->getMinTimestep(), vmpLearner->getMaxTimestep()));
    robot = mpVisualizer->getRobot();
    previewResult(ui->VisualizeMotion->isChecked());
    visLoaded = false;
}

void MPVisualizerDialog::motionChanged(int ind)
{
    if (kinematicMotions.size() > 0 && (int)kinematicMotions.size() < ind) {
        currentMotion = kinematicMotions.at(ind);
        getAllAvailableNodeSets();
        update();
    }
}

void MPVisualizerDialog::saveMP()
{
    if (!vmp || !currentMotion) return;

    std::string mpSaveDir = QFileDialog::getExistingDirectory(parent, QString::fromStdString("Select Save Directory"), QDir::currentPath()).toStdString();

    if (mpSaveDir.empty())
        mpSaveDir = QDir::currentPath().toStdString();

    std::string motionfile = mpSaveDir + "/" + outfile + "_" + currentMotion->getName() + "_tmp.xml";
    std::string outmpfile = mpSaveDir + "/"+ outfile + "_" + currentMotion->getName() + "_" + nodeSetName + "_vmp.xml";

    vmp->save(outmpfile);
    currentMotion->saveXML(motionfile);
}

void MPVisualizerDialog::addPoint()
{
    addRowInTable(getRowVecFromTable(-1,0));
    visualize();
}

void MPVisualizerDialog::removePoint()
{
    connectTable(false);
    ui->GoalTableWidget->removeRow(ui->GoalTableWidget->rowCount()-1);
    if(ui->GoalTableWidget->rowCount() <= 2)
    {
        ui->RemovePointButton->setEnabled(false);
    }
    connectTable(true);
    visualize();
}

void MPVisualizerDialog::tableCellChanged(int row, int col)
{
    if(col == 0 && row >= 2)
    {
        double canVal = ui->GoalTableWidget->item(row, col)->text().toDouble();
        connectTable(false);
        if (canVal < 0)
            ui->GoalTableWidget->item(row, col)->setText(QString::number(canVal));
        if (canVal > 1)
            ui->GoalTableWidget->item(row, col)->setText(QString::number(1));

        vmp->prepareExecution();
        auto currentState = vmp->calculateDesiredState(canVal);
        for (size_t i = 0; i < currentState.size(); ++i)
        {
            ui->GoalTableWidget->item(row, col+i+1)->setText(QString::number(currentState[i][0]));
        }
        connectTable(true);
    }
    visualize();
}

void MPVisualizerDialog::visualize() {
    if (!mpVisualizer || !vmp || currentTimestep < 0 || !ui->VisualizeMotion->isChecked()) return;

    auto goals = getRowVecFromTable(1, 1);
    vmp->removeViaPoints();
    vmp->setGoals(goals);
    for(int i = 2; i < ui->GoalTableWidget->rowCount(); ++i)
    {
        double canVal = ui->GoalTableWidget->item(i, 0)->text().toDouble();
        if (canVal > 0) {
            auto viapoint = getRowVecFromTable(i, 1);
            vmp->setViaPoint(canVal, viapoint);
        }
    }

    double minTimestep = ui->MinTimestepSpin->value();
    double maxTimestep = ui->MaxTimestepSpin->value();
    double scaled = (currentTimestep - minTimestep) / (maxTimestep - minTimestep);

    vmp->prepareExecution();
    mpVisualizer->compute(*vmp.get(), currentTimestep, scaled);

    if (true) {
        Eigen::Matrix4f pose = mpVisualizer->getLastPose();
        this->sep->removeAllChildren();
        Eigen::Vector3f position = simox::math::mat4f_to_pos(pose);
        auto poseVis = VirtualRobot::CoinVisualizationFactory::CreateSphere(mpVisualizer->getRobot()->toGlobalCoordinateSystemVec(position), 20, 0, 0.5, 0.5);
        this->sep->addChild(poseVis);
    }
}

void MPVisualizerDialog::previewResult(bool checked) {
    if (checked && robot) {
        if (!visLoaded) visLoaded = robot->reloadVisualizationFromXML(false);
        if (visLoaded) {
            SoSeparator* sep = new SoSeparator();
            auto vis = robot->getVisualization<VirtualRobot::CoinVisualization>(VirtualRobot::SceneObject::Full);
            SoNode* visualisationNode = vis->getCoinVisualization();
            if (visualisationNode) sep->addChild(visualisationNode);
            sep->addChild(this->sep);
            addVisualisation(sep);
            visualize();
            return;
        }
    }
    addVisualisation(nullptr);
}


