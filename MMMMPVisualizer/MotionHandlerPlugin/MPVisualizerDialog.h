#ifndef __MMM_MPVISUALIZERialog_H_
#define __MMM_MPVISUALIZERialog_H_

#include <QDialog>

#ifndef Q_MOC_RUN
#include <MMM/Motion/Motion.h>
#endif

#include <MMM/MotionPrimitive/VMPApply.h>
#include <MMM/MotionPrimitive/MPLearner.h>
#include <mplib/factories/VMPFactory.h>
#include <VirtualRobot/VirtualRobot.h>
#include <Inventor/nodes/SoSeparator.h>

namespace Ui {
class MPVisualizerDialog;
}

class MPVisualizerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MPVisualizerDialog(MMM::MotionRecordingPtr motions, QWidget* parent = 0);
    virtual ~MPVisualizerDialog();

    void openMotion(MMM::MotionRecordingPtr motions);

    void setCurrentTimestep(float timestep);

private slots:
    void loadMPFile();
    void generateMotion();
    void minTimestepChanged(double val);
    void learnMP();
    void motionChanged(int ind);
    void saveMP();
    void addPoint();
    void removePoint();
    void tableCellChanged(int row, int col);
    void visualize();
    void previewResult(bool checked);

signals:
    void genMotion(MMM::MotionRecordingPtr);
    void addVisualisation(SoSeparator* sep);

private:
    void fillGoalInTable();
    mplib::core::DVec getRowVecFromTable(int nrow, int jumpCols);
    void getAllAvailableNodeSets();
    void addRowInTable(const mplib::core::DVec& point);
    void connectTable(bool value);


    void findKinematicMotion();
    void update();

    Ui::MPVisualizerDialog* ui;
    MMM::MotionRecordingPtr motions;
    QWidget* parent;

    MMM::VMPApplyPtr mpVisualizer;
    std::shared_ptr<MMM::MPLearner<mplib::factories::VMPFactory>> vmpLearner;
    VirtualRobot::RobotPtr robot;
    std::string nodeSetName;
    std::vector<MMM::MotionPtr> kinematicMotions;
    MMM::MotionPtr currentMotion;
    std::string outfile;
    std::shared_ptr<mplib::representation::vmp::VMP> vmp;
    float currentTimestep;
    bool visLoaded = false;
    SoSeparator* sep = new SoSeparator();
};

#endif // __MMM_MPVisualizerDialog_H_
