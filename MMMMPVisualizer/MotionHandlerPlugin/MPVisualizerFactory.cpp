#include "MPVisualizerFactory.h"
#include "MPVisualizerPlugin.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry MPVisualizerFactory::registry(MPVisualizerPlugin::NAME, &MPVisualizerFactory::createInstance);

MPVisualizerFactory::MPVisualizerFactory() : MotionHandlerFactory() {}

MPVisualizerFactory::~MPVisualizerFactory() {}

std::string MPVisualizerFactory::getName() {
    return MPVisualizerPlugin::NAME;
}

MotionHandlerPtr MPVisualizerFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new MPVisualizerPlugin(widget));
}

MotionHandlerFactoryPtr MPVisualizerFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new MPVisualizerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new MPVisualizerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
