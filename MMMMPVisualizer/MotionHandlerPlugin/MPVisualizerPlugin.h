#ifndef __MMM_MPVISUALIZERPLUGIN_H_
#define __MMM_MPVISUALIZERPLUGIN_H_

#include "../../MMMViewer/MMM/Viewer/MotionHandler.h"
#include "MPVisualizerDialog.h"

namespace MMM
{

class MPVisualizerPlugin : public MotionHandler
{
    Q_OBJECT

public:
    MPVisualizerPlugin(QWidget* widget);

    void handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> currentRobots = {});

    virtual std::string getName();

    static constexpr const char* NAME = "MPVisualizer";

public slots:
    void timestepChanged(float timestep);

private:
    QWidget* widget;
    MPVisualizerDialog* dialog;
};

}

#endif
