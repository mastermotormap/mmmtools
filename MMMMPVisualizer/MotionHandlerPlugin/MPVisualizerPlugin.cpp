#include "MPVisualizerPlugin.h"

#include <QFileDialog>

#include <MMM/Motion/MotionRecording.h>

using namespace MMM;

MPVisualizerPlugin::MPVisualizerPlugin(QWidget* widget) :
    MotionHandler(MotionHandlerType::GENERAL, "Learn Motion Primitives"),
    widget(widget),
    dialog(nullptr)
{
}

void MPVisualizerPlugin::handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> /*currentRobots*/) {
    if (motions->size() == 0) return;

    if (!dialog) {
        dialog = new MPVisualizerDialog(motions, widget);
        connect(dialog, &MPVisualizerDialog::genMotion, this, &MPVisualizerPlugin::openMotions);
        connect(dialog, SIGNAL(addVisualisation(SoSeparator*)), this, SIGNAL(addVisualisation(SoSeparator*)));
    }

    dialog->show();
}

std::string MPVisualizerPlugin::getName() {
    return NAME;
}

void MPVisualizerPlugin::timestepChanged(float timestep) {
    if (dialog) {
        dialog->setCurrentTimestep(timestep);
    }
}
