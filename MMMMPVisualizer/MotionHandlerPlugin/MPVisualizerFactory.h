#ifndef __MMM_MPVISUALIZERFACTORY_H_
#define __MMM_MPVISUALIZERFACTORY_H_

#include <string>

#include "../../MMMViewer/MMM/Viewer/MotionHandlerFactory.h"

namespace MMM
{

class MPVisualizerFactory : public MotionHandlerFactory
{
public:
    MPVisualizerFactory();

    virtual ~MPVisualizerFactory();

    std::string getName();

    MotionHandlerPtr createMotionHandler(QWidget* widget);

    static MotionHandlerFactoryPtr createInstance(void*);

private:
    static SubClassRegistry registry;

};

}

#endif
