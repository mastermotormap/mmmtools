cmake_minimum_required(VERSION 3.10.2)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_LIST_DIR}/CMakeModules)

###########################################################
#### Project configuration                             ####
###########################################################

project(MMMTools)

set(PROJECT_VERSION_MAJOR "0")
set(PROJECT_VERSION_MINOR "1")
set(PROJECT_VERSION_PATCH "0")
set(PROJECT_VERSION "${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH}")
# Legacy $PROJ_VERSION
set(PROJ_VERSION "${PROJECT_VERSION}")

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/etc/cmake")

option(MMM_ENABLE_LEGACY_MOTION_TOOLS "Enable tools for legacy motion operations" OFF)
option(MMM_ENABLE_LEGACY_JACOBIAN_CONVERTER "Enable legacy converter plugins based on the Jacobian (deprecated). Works only if legacy motion tools are enabled" OFF)
option(MMM_CONVERTER_OUTPUT_MARKER_DEVIATION "Enable frame-wise reporting of the marker deviation in converters (always enabled in NloptConverter)" OFF)

###########################################################
#### CMake package configuration                       ####
###########################################################

find_package(Simox QUIET)
find_package(SoQt QUIET)
###########################################################
#### Version configuration                             ####
###########################################################

#set_property(TARGET ${CMAKE_PROJECT_NAME} PROPERTY VERSION ${PROJ_VERSION})
#set_property(TARGET ${CMAKE_PROJECT_NAME} PROPERTY SOVERSION ${PROJ_SO_VERSION})

include(CMakePackageConfigHelpers)
write_basic_package_version_file(
    "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME}/${CMAKE_PROJECT_NAME}ConfigVersion.cmake"
    VERSION ${PROJ_VERSION}
    COMPATIBILITY AnyNewerVersion
)

###########################################################
#### Compiler configuration                            ####
###########################################################

if(MMM_CONVERTER_OUTPUT_MARKER_DEVIATION)
    add_definitions(-DCONVERTER_OUTPUT_MARKER_DEVIATION)
endif(MMM_CONVERTER_OUTPUT_MARKER_DEVIATION)

set(CMAKE_POSITION_INDEPENDENT_CODE ON) # enable -fPIC

if(MSVC)
    add_definitions(
        "/W4" # enable warnings
        "/MP" # enable parallel build
    )
elseif(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
    # cmake 3.10 does not understand c++2a, so we tell it we will handle the standard flag
    set(CMAKE_CXX_STANDARD_DEFAULT)
    add_definitions(-std=c++2a)
    add_definitions(
        "-Wall" # enable warnings
        "-Wno-long-long"
        "-pedantic"
    )
endif()

add_compile_options(-Werror)

###########################################################
#### Motion Handler Plugin                             ####
###########################################################

# Add motion handler plugin
function(DefineMotionHandlerPlugin MotionHandlerPlugin_Name MotionHandlerPlugin_Sources MotionHandlerPlugin_Headers MotionHandlerPlugin_Moc MotionHandlerPlugin_Ui SensorPlugin_Name)
    VirtualRobotQtLibrary(${MotionHandlerPlugin_Name} "${MotionHandlerPlugin_Sources}" "${MotionHandlerPlugin_Headers}" "${MotionHandlerPlugin_Moc}" "${MotionHandlerPlugin_Ui}")
    target_link_libraries(${MotionHandlerPlugin_Name} PUBLIC ${EXTERNAL_LIBRARY_DIRS} ${SensorPlugin_Name})
    set_target_properties(${MotionHandlerPlugin_Name} PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${MOTION_HANDLER_PLUGIN_LIB_DIR})

    install(
        TARGETS ${MotionHandlerPlugin_Name}
        EXPORT "${CMAKE_PROJECT_NAME}Targets"
        LIBRARY DESTINATION lib/mmm_plugins/motion_handler
        #ARCHIVE DESTINATION lib
        #RUNTIME DESTINATION bin
        #INCLUDES DESTINATION include
        COMPONENT lib
    )
    install(FILES ${MotionHandlerPlugin_Headers} DESTINATION "include/MMM/${MotionHandlerPlugin_Name}" COMPONENT dev)
endfunction()

###########################################################
#### Subprojects                                       ####
###########################################################

# set output directories for subprojects
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
# set MMMTools_LIB_DIR (search directory for converter plugins)
set(MMMTools_LIB_DIR ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})

# set SENSOR_VISUALISATION_PLUGIN_LIB_DIR (search directory for sensorvisualisation plugins)
set(SENSOR_VISUALISATION_PLUGIN_LIB_DIR ${CMAKE_BINARY_DIR}/sensorVisualisationPluginLibs)

# set MOTION_CONVERTER_PLUGIN_LIB_DIR (search directory for motion converter plugins)
set(MOTION_CONVERTER_PLUGIN_LIB_DIR ${CMAKE_BINARY_DIR}/motionConverterPluginLibs)

# set MOTION_SEGMENTER_PLUGIN_LIB_DIR (search directory for motion segmenter plugins)
set(MOTION_SEGMENTER_PLUGIN_LIB_DIR ${CMAKE_BINARY_DIR}/segmentationPluginLibs)

# set MOTION_HANDLER_PLUGIN_LIB_DIR (search directory for motion handler plugins)
set(MOTION_HANDLER_PLUGIN_LIB_DIR ${CMAKE_BINARY_DIR}/motionHandlerPluginLibs)

# set SENSOR_PLOT_PLUGIN_LIB_DIR (search directory for sensor plot plugins)
set(SENSOR_PLOT_PLUGIN_LIB_DIR ${CMAKE_BINARY_DIR}/sensorPlotPluginLibs)

add_subdirectory(dae2mmm)
add_subdirectory(MMMLegacyMotionConverter)
add_subdirectory(MMMWholeBodyDynamicCalculator)
add_subdirectory(MMMC3DConverter)
add_subdirectory(MMMViewer)
add_subdirectory(MMMIMUConverter)
add_subdirectory(MMMAccelerometerConverter)
add_subdirectory(MMMJoinMotions)
add_subdirectory(MMMMotionConverter)
add_subdirectory(MMMSegmentation)
add_subdirectory(MMMDataGloveConverter)
add_subdirectory(MMMSynchronize)
add_subdirectory(MMMModelConverter)
add_subdirectory(MMMForceConverter)
add_subdirectory(MMMPlot)
add_subdirectory(MMMCSVExporter)
add_subdirectory(MMMAnnotation)
add_subdirectory(MMMMotionAdaptation)

# maintain a list of projects for exporting
set(SUB_PROJECTS
    dae2mmm
    MMMLegacyMotionConverter
    MMMWholeBodyDynamicCalculator
    MMMC3DConverter
    MMMViewerLib
    MMMViewer
    MMMAccelerometerConverter
    MMMIMUConverter
    MMMJoinMotions
    MMMMotionConverter
    MMMSegmentation
    MMMDataGloveConverter
    MMMSynchronize
    MMMModelConverter
    MotionPlot
    MMMPlot
)

if(MMM_ENABLE_LEGACY_MOTION_TOOLS)
    add_subdirectory(legacy)
endif()

add_subdirectory(custom)

find_package(SemanticObjectRelations QUIET)
if(SemanticObjectRelations_FOUND)
    find_package(PCL 1.8 QUIET REQUIRED COMPONENTS common features io sample_consensus)
    if(PCL_FOUND)
        add_subdirectory(MMMSemanticObjectRelations)
        set(SUB_PROJECTS ${SUB_PROJECTS} MMMSemanticObjectRelations)
     endif(PCL_FOUND)
endif(SemanticObjectRelations_FOUND)

find_package(MPLIB QUIET)
if(MPLIB_FOUND)
    add_subdirectory(MMMMPVisualizer)
    set(SUB_PROJECTS ${SUB_PROJECTS} MMMMotionPrimitiveLib)
    set(SUB_PROJECTS ${SUB_PROJECTS} MMMLearnVMP)
endif()

###########################################################
#### Project build configuration                       ####
###########################################################

export(
    TARGETS ${SUB_PROJECTS}
    FILE "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME}/${CMAKE_PROJECT_NAME}Targets.cmake"
)

configure_file(
    "CMakeModules/${CMAKE_PROJECT_NAME}Config.cmake"
    "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME}/${CMAKE_PROJECT_NAME}Config.cmake"
    COPYONLY
)

set(INSTALL_CMAKE_DIR "share/cmake/${CMAKE_PROJECT_NAME}")
install(
    EXPORT ${CMAKE_PROJECT_NAME}Targets
    DESTINATION ${INSTALL_CMAKE_DIR}
    #COMPONENT dev
)
install(
    FILES
        "CMakeModules/${CMAKE_PROJECT_NAME}Config.cmake"
        "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME}/${CMAKE_PROJECT_NAME}ConfigVersion.cmake"
    DESTINATION ${INSTALL_CMAKE_DIR}
    COMPONENT dev
)
install(DIRECTORY "./data" DESTINATION "share/MMMTools")

export(PACKAGE ${CMAKE_PROJECT_NAME})

###########################################################
#### CPack configuration for deb artefact              ####
###########################################################

# Install to /usr
set(CMAKE_INSTALL_PREFIX "/usr")

if(CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
    set(CPACK_DEBIAN_ARCHITECTURE "amd64")
else()
    set(CPACK_DEBIAN_ARCHITECTURE "i386")
endif()

include(InstallRequiredSystemLibraries)
set(CPACK_SET_DESTDIR "on")
set(CPACK_PACKAGING_INSTALL_PREFIX "/tmp")
set(CPACK_DEBIAN_PACKAGE_NAME "mmmtools")
set(CPACK_GENERATOR "DEB")
set(CPACK_PACKAGE_DESCRIPTION "MMMTools")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Master Motor Map (MMM) Tools")
set(CPACK_PACKAGE_VENDOR "KIT")
set(CPACK_PACKAGE_CONTACT "Master Motor Map Team <h2t_mmm@lists.kit.edu>")
set(CPACK_PACKAGE_VERSION_MAJOR "${PROJECT_VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${PROJECT_VERSION_MINOR}")
set(CPACK_PACKAGE_VERSION_PATCH "${PROJECT_VERSION_PATCH}")
set(CPACK_PACKAGE_FILE_NAME "mmmtools_${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
set(CPACK_SOURCE_PACKAGE_FILE_NAME "mmmtools_${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
set(CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON)

set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

set(CPACK_COMPONENT_APPLICATIONS_DISPLAY_NAME "mmmtools Examples")
set(CPACK_COMPONENT_APPLICATIONDATA_DISPLAY_NAME "mmmtools Data")
set(CPACK_COMPONENT_LIBRARIES_DISPLAY_NAME "mmmtools Libraries")
set(CPACK_COMPONENT_HEADERS_DISPLAY_NAME "mmmtools C++ Headers")

# deb dependencies
set(CPACK_DEBIAN_PACKAGE_DEPENDS "")
list(APPEND CPACK_DEBIAN_PACKAGE_DEPENDS "mmmcore (= 0.0.12)")
list(APPEND CPACK_DEBIAN_PACKAGE_DEPENDS "simox")
string(REPLACE ";" ", " CPACK_DEBIAN_PACKAGE_DEPENDS "${CPACK_DEBIAN_PACKAGE_DEPENDS}")

set(CPACK_COMPONENTS_ALL Applications Headers Libraries ApplicationData)
include(CPack)
