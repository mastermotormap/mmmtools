#include "AddAccelerometerHandlerSensorConfiguration.h"

using namespace MMM;

AddAccelerometerHandlerSensorConfiguration::AddAccelerometerHandlerSensorConfiguration() : AddAttachedSensorConfiguration<IMUSensor>("imu", {"AccelerationX", "AccelerationY", "AccelerationZ"})
{
}

void AddAccelerometerHandlerSensorConfiguration::addSensorMeasurements(const std::vector<std::map<std::string, std::string> > &values) {
    //check if IMU sensor is already present if than substitute
    for (std::map<std::string, std::string> map : values) {
        float timestep;
        Eigen::Vector3f acceleration;


        try {
            timestep = simox::alg::to_<float>(map[TIMESTEP_STR]);
        }
        catch (simox::error::SimoxError &e) {
            throw new MMM::Exception::MMMException(std::string("Timestep invalid! ") + e.what());
        }
        try {
            acceleration << simox::alg::to_<float>(map["AccelerationX"]),
                            simox::alg::to_<float>(map["AccelerationY"]),
                            simox::alg::to_<float>(map["AccelerationZ"]);
        }
        catch (simox::error::SimoxError &e) {
            throw new MMM::Exception::MMMException(std::string("Acceleration invalid at timestep ") + simox::alg::to_string(timestep) + "! " + e.what());
        }
        IMUSensorMeasurementPtr measurement(new IMUSensorMeasurement(timestep, acceleration));
        sensor->addSensorMeasurement(measurement);
    }
}
