#include "AddAccelerometerHandler.h"

using namespace MMM;

AddAccelerometerHandler::AddAccelerometerHandler(QWidget* parent) :
    AddAttachedSensorHandler<AddAccelerometerHandlerSensorConfiguration>(parent, "Add accelerometer sensor")
{
}

std::string AddAccelerometerHandler::getName() {
    return NAME;
}

