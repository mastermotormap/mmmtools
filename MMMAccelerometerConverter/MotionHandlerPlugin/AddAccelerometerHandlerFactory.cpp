#include "AddAccelerometerHandlerFactory.h"
#include "AddAccelerometerHandler.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry AddAccelerationHandlerFactory::registry(AddAccelerometerHandler::NAME, &AddAccelerationHandlerFactory::createInstance);

AddAccelerationHandlerFactory::AddAccelerationHandlerFactory() : MotionHandlerFactory() {}

AddAccelerationHandlerFactory::~AddAccelerationHandlerFactory() {}

std::string AddAccelerationHandlerFactory::getName() {
    return AddAccelerometerHandler::NAME;
}

MotionHandlerPtr AddAccelerationHandlerFactory::createMotionHandler(QWidget* parent) {
    return MotionHandlerPtr(new AddAccelerometerHandler(parent));
}

MotionHandlerFactoryPtr AddAccelerationHandlerFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new AddAccelerationHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new AddAccelerationHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
