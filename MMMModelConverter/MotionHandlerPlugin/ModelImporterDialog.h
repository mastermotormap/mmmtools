/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2018 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MODELIMPORTERDIALOG_H_
#define __MMM_MODELIMPORTERDIALOG_H_

#include <QDialog>
#include <QList>
#include <QSettings>

#ifndef Q_MOC_RUN
#include <MMM/Motion/Motion.h>

#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#endif

Q_DECLARE_METATYPE(QList<float>)

namespace Ui {
class ModelImporterDialog;
}

class ModelImporterDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ModelImporterDialog(MMM::ModelPtr model, QWidget* parent = 0);
    ~ModelImporterDialog();

    MMM::MotionRecordingPtr getMotions();

private slots:
    void importMotion();
    void jointAngleChanged(QWidget*);
    void poseChanged();

private:
    void setupViewer();
    void setupCamera();

    Ui::ModelImporterDialog* ui;
    MMM::MotionRecordingPtr motions;
    VirtualRobot::RobotPtr robot;
    std::map<std::string, float> jointValues;
    SoQtExaminerViewer* viewer;
};

#endif // __MMM_ModelIMPORTERDIALOG_H_
