#include "ModelImporterFactory.h"
#include "ModelImporter.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry ModelImporterFactory::registry(ModelImporter::NAME, &ModelImporterFactory::createInstance);

ModelImporterFactory::ModelImporterFactory() : MotionHandlerFactory() {}

ModelImporterFactory::~ModelImporterFactory() {}

std::string ModelImporterFactory::getName()
{
    return ModelImporter::NAME;
}

MotionHandlerPtr ModelImporterFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new ModelImporter(widget));
}

MotionHandlerFactoryPtr ModelImporterFactory::createInstance(void *)
{
    return MotionHandlerFactoryPtr(new ModelImporterFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new ModelImporterFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
