#include "SemanticObjectRelationsWidget.h"
#include "ui_SemanticObjectRelationsWidget.h"
#include <SemanticObjectRelations/Spatial/SpatialGraph.h>
#include "SemanticGraphEdgeItem.h"
#include <SemanticObjectRelations/Spatial/json.h>
#include "SemanticGraphVertexItem.h"
#include <MMM/Motion/MotionRecording.h>
#include <SimoxUtility/color/KellyLUT.h>
#include <memory>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>
#include <Inventor/nodes/SoSeparator.h>

using namespace semrel;

SemanticObjectRelationsWidget::SemanticObjectRelationsWidget(QWidget* parent) :
    QDialog(parent),
    layoutGraph(nullptr),
    ui(new Ui::SemanticObjectRelationsWidget),
    serializer(new semrel::JsonSimoxShapeSerializer())
{
    ui->setupUi(this);

    semrel::JsonSimoxShapeSerializer::registerSerializer(serializer, true);

    setWindowState(windowState() | Qt::WindowMaximized);

    ui->graphicsView->setScene(&graphicsScene);
    ui->graphicsView->setRenderHints(QPainter::Antialiasing | QPainter::HighQualityAntialiasing);

    std::set<semrel::SpatialRelations::Type> initVisibleTypes = {
        semrel::SpatialRelations::Type::contact,
        semrel::SpatialRelations::Type::static_above,
        semrel::SpatialRelations::Type::static_right_of,
        semrel::SpatialRelations::Type::static_in_front_of,
        semrel::SpatialRelations::Type::static_inside,
        semrel::SpatialRelations::Type::dynamic_moving_apart,
        semrel::SpatialRelations::Type::dynamic_getting_close,
        semrel::SpatialRelations::Type::dynamic_stable
    };

    bidirectionalEdges = {
        semrel::SpatialRelations::names.at(semrel::SpatialRelations::Type::contact),
        semrel::SpatialRelations::names.at(semrel::SpatialRelations::Type::dynamic_moving_together),
        semrel::SpatialRelations::names.at(semrel::SpatialRelations::Type::dynamic_halting_together),
        semrel::SpatialRelations::names.at(semrel::SpatialRelations::Type::dynamic_fixed_moving_together),
        semrel::SpatialRelations::names.at(semrel::SpatialRelations::Type::dynamic_getting_close),
        semrel::SpatialRelations::names.at(semrel::SpatialRelations::Type::dynamic_moving_apart),
        semrel::SpatialRelations::names.at(semrel::SpatialRelations::Type::dynamic_stable)
    };

    unsigned int id = 0;
    for (const auto &relation : semrel::SpatialRelations::names) {
        QListWidgetItem* item = new QListWidgetItem(QString::fromStdString(relation.second), ui->RelationsWidget);
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
        if (initVisibleTypes.find(relation.first) != initVisibleTypes.end()) {
            item->setCheckState(Qt::Checked);
            edgeVis[relation.second] = true;
        }
        else {
            item->setCheckState(Qt::Unchecked);
            edgeVis[relation.second] = false;
        }
        auto color = simox::color::KellyLUT::KELLY_COLORS.at(id);
        auto qcolor = QColor(color.r, color.g, color.b);
        item->setBackgroundColor(qcolor);
        double luma = ((0.299 * color.r) + (0.587 * color.g) + (0.114 * color.b)) / 255;
        item->setTextColor(luma > 0.5 ? Qt::black : Qt::white);
        ui->RelationsWidget->addItem(item);
        id++;
    }
    QObject::connect(ui->RelationsWidget, &QListWidget::itemChanged, this, &SemanticObjectRelationsWidget::relationChanged);

}

SemanticObjectRelationsWidget::~SemanticObjectRelationsWidget()
{
    delete ui;
}

void SemanticObjectRelationsWidget::loadMotions(MMM::MotionRecordingPtr motions) {
    layoutGraph = nullptr;
    sem = MMM::SemanticObjectRelationsPtr(new MMM::SemanticObjectRelations(motions, timeFrequency, true, true));
    timestepChanged(motions->getFirstMotion()->getMinTimestep());
}

void SemanticObjectRelationsWidget::drawGraph(QGraphicsScene* graphicsScene)
{
    graphicsScene->clear();
    for (const auto &n : layoutGraph->nodes)
    {
        SemanticGraphVertexItem* ellipse = new SemanticGraphVertexItem();
        ellipse->setRect(n.second.posX, n.second.posY, n.second.width, n.second.height);
        ellipse->text = QString::fromStdString(n.second.label);
        ellipse->descriptor = n.first;

        //QObject::connect(ellipse, &SemanticGraphVertexItem::onLeftClick, this, &SemanticRelationViewerWidgetController::onLeftClickVertex);

        graphicsScene->addItem(ellipse);
    }

    for (const auto &edges : layoutGraph->edges)
    {
        for (const auto &e : edges.second) {
            if (!e.second.visible) {
                std::cout << "Invisible " << edges.first.first << edges.first.second << e.second.label << std::endl;
                continue;
            }

            QPainterPath path;
            if (e.second.startPoint)
            {
                path.lineTo(*e.second.startPoint);
            }
            if (e.second.controlPoints.size() == 2) {
                path.moveTo(e.second.controlPoints.first());
                path.lineTo(e.second.controlPoints.last());
            }
            else if (e.second.controlPoints.size() == 3) {
                path.moveTo(e.second.controlPoints.first());
                path.quadTo(e.second.controlPoints.at(1),
                            e.second.controlPoints.last());
            }
            else if (e.second.controlPoints.size() > 0){
                QPainterPath path2 {e.second.controlPoints.at(0)};
                for (int i = 1; i < e.second.controlPoints.size(); i += 3) {
                    path2.cubicTo(e.second.controlPoints.at(i), e.second.controlPoints.at(i + 1), e.second.controlPoints.at(i + 2));
                }
                path.addPath(path2);
            }
            else continue;
            if (e.second.endPoint)
            {
                path.lineTo(*e.second.endPoint);
            }

            SemanticGraphEdgeItem* item = new SemanticGraphEdgeItem(edges.first.first, edges.first.second, e.second.label, bidirectionalEdges.find(e.second.label) != bidirectionalEdges.end());
            item->setPath(path);

            //QObject::connect(item, &SemanticGraphEdgeItem::onLeftClick, this, &SemanticRelationViewerWidgetController::onLeftClickEdge);

            graphicsScene->addItem(item);
        }
    }
}

GraphvizLayoutedGraphPtr SemanticObjectRelationsWidget::layoutSpatialGraph(SpatialGraph const& graph)
{
    GraphvizLayout layout;
    for (auto vertex : graph.vertices())
    {
        std::string name = sem->getMotionName(vertex.objectID());
        if (!name.empty())
            layout.addNode(vertex.objectID(), name);
    }
    for (auto edge : graph.edges())
    {
        for (const auto &relation : semrel::SpatialRelations::names) {
            auto it = edgeVis.find(relation.second);
            // only visible edges and edges that are not bidirectional
            if (it != edgeVis.end() && it->second && (edge.sourceDescriptor() < edge.targetDescriptor() || bidirectionalEdges.find(relation.second) == bidirectionalEdges.end() )) {
                auto foundRelations = edge.attrib().relations.getStringList();
                if (std::find(foundRelations.begin(), foundRelations.end(), relation.second) != foundRelations.end())   {
                    layout.addEdge(edge.sourceDescriptor(), edge.targetDescriptor(), relation.second, true);
                }
                else layout.addEdge(edge.sourceDescriptor(), edge.targetDescriptor(), relation.second, false);
            }

        }
    }
    return layout.finish();
}

void SemanticObjectRelationsWidget::relationChanged(QListWidgetItem *item) {
    toggleEdgeVis(item->text().toStdString());
}

void SemanticObjectRelationsWidget::timestepChanged(float timestep) {
    double actualTimestep = sem->getActualTimestep(timestep);
    semrel::SpatialGraph graph = sem->evaluateSpatialRelations(actualTimestep, 1);
    bool newGraph = false;
    if (!layoutGraph) {
        layoutGraph = layoutSpatialGraph(graph);
        newGraph = true;
    }

    layoutGraph->hideEdges();
    for (const auto edge : graph.edges())
    {
        for (const auto &relation : edge.attrib().relations.getStringList()) {
            auto it = edgeVis.find(relation);
            if (it != edgeVis.end() && it->second) {
                layoutGraph->setVisible(edge.sourceObjectID(), edge.targetObjectID(), relation);
                std::cout << "Set visible " << edge.sourceObjectID() << edge.targetObjectID() << relation << std::endl;
            }
        }
    }

    drawGraph(&graphicsScene);

    if (newGraph) {
        ui->graphicsView->fitInView(graphicsScene.itemsBoundingRect(), Qt::KeepAspectRatio);
    }

    currentTimestep = timestep;

    {
        SoSeparator* sep = new SoSeparator();
        auto shapes = sem->getShapes(actualTimestep);
        for (auto shape : shapes) {
            Box* box = static_cast<Box*>(shape);
            if (box) {
                sep->addChild(VirtualRobot::CoinVisualizationFactory::CreateBBoxVisualization(VirtualRobot::BoundingBox(box->getCorners()), true));
            }
        }
        emit addVisualisation(sep);
    }
}

void SemanticObjectRelationsWidget::toggleEdgeVis(const std::string &edgeName) {
    edgeVis[edgeName] = !edgeVis[edgeName];
    layoutGraph = nullptr;
    timestepChanged(currentTimestep);

}
