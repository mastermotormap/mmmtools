/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2019 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/


#pragma once

#include <graphviz/gvc.h>

#include <QPointF>
#include <QList>

#include <string>
#include <map>
#include <memory>
#include <vector>

namespace semrel
{
    struct GraphvizLayoutedNode
    {
        float posX = 0.0f;
        float posY = 0.0f;
        float width = 0.0f;
        float height = 0.0f;
        std::string label;
    };

    struct GraphvizLayoutedEdge
    {
        QList<QPointF> controlPoints;
        std::optional<QPointF> startPoint;
        std::optional<QPointF> endPoint;
        std::string label;
        bool visible = true;
    };

    class GraphvizLayoutedGraph
    {
    public:
        GraphvizLayoutedGraph();
        ~GraphvizLayoutedGraph();

        void hideEdges();

        void setVisible(int start, int end, const std::string &label);

        std::map<int, GraphvizLayoutedNode> nodes;
        std::map<std::pair<int, int>, std::map<std::string, GraphvizLayoutedEdge>> edges;
    };

    typedef std::shared_ptr<GraphvizLayoutedGraph> GraphvizLayoutedGraphPtr;

    class GraphvizLayout
    {
    public:
        GraphvizLayout();

        GraphvizLayout(GraphvizLayout const&) = delete;

        ~GraphvizLayout();

        void addNode(int id, std::string const& label);

        void addEdge(int sourceID, int targetID, const std::string& label = "edge", bool visible = true);

        GraphvizLayoutedGraphPtr finish(std::string const& savePNG = "");

    private:
        GVC_t* context = nullptr;
        graph_t* graph = nullptr;

        std::map<int, node_t*> id2node;
        std::map<std::pair<int, int>, std::vector<edge_t*>> id2edge;
    };

}
