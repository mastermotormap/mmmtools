#include <QFileDialog>
#include <QMessageBox>
#include <MMM/Motion/MotionRecording.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>

#include "SemanticObjectRelationsHandler.h"

using namespace MMM;

SemanticObjectRelationsHandler::SemanticObjectRelationsHandler(QWidget* widget) :
    MotionHandler(MotionHandlerType::GENERAL, "Semantic Object Relations"),
    widget(widget),
    dialog(new SemanticObjectRelationsWidget())
{
    connect(dialog, SIGNAL(addVisualisation(SoSeparator*)), this, SIGNAL(addVisualisation(SoSeparator*)));
}

void SemanticObjectRelationsHandler::handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> currentRobots) {
    if (motions && !motions->empty()) {
        dialog->loadMotions(motions);
        dialog->showMaximized();
    }
    else MMM_ERROR << "Cannot open semantic objects relation dialog, because no motions are present!" << std::endl;
}

std::string SemanticObjectRelationsHandler::getName() {
    return NAME;
}

void SemanticObjectRelationsHandler::timestepChanged(float timestep) {
    if (dialog->isVisible()) dialog->timestepChanged(timestep);
}
