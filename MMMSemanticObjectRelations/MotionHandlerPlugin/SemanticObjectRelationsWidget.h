/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    SEM
* @author     Andre Meixner
* @copyright  2019 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SemanticObjectRelationsWidget_H_
#define __MMM_SemanticObjectRelationsWidget_H_

#include <QDialog>

#ifndef Q_MOC_RUN
#include <QGraphicsScene>
#include "../SemanticObjectRelations.h"
#include <MMM/Motion/Motion.h>
#include "../JsonSimoxShapeSerializer.h"
#include "GraphvizLayout.h"
#include <SemanticObjectRelations/RelationGraph/json/AttributedGraph.h>
#include <QListWidgetItem>
#include <Inventor/nodes/SoSeparator.h>
#endif

namespace Ui {

class SemanticObjectRelationsWidget;
}

class SemanticObjectRelationsWidget : public QDialog
{
    Q_OBJECT

public:
    explicit SemanticObjectRelationsWidget(QWidget* parent = 0);
    ~SemanticObjectRelationsWidget();

    void loadMotions(MMM::MotionRecordingPtr motions);

public slots:
    void timestepChanged(float timestep);

signals:
    void addVisualisation(SoSeparator* sep);

private:
    void toggleEdgeVis(const std::string &edgeName);
    void drawGraph(QGraphicsScene* graphicsScene);

    semrel::GraphvizLayoutedGraphPtr layoutSpatialGraph(semrel::SpatialGraph const& graph);
    void relationChanged(QListWidgetItem* item);

    semrel::GraphvizLayoutedGraphPtr layoutGraph;
    Ui::SemanticObjectRelationsWidget* ui;
    MMM::SemanticObjectRelationsPtr sem;
    QGraphicsScene graphicsScene;
    std::shared_ptr<semrel::JsonSimoxShapeSerializer> serializer;
    std::map<std::string, bool> edgeVis;
    std::set<std::string> bidirectionalEdges;
    float currentTimestep;
    double timeFrequency = 1.0f / 100;

};

#endif // __MMM_SemanticObjectRelationsWidget_H_
