#include "SemanticGraphEdgeItem.h"

#include <iostream>

#include <QGraphicsSceneContextMenuEvent>
#include <QPainter>
#include <SimoxUtility/color/KellyLUT.h>
#include <SemanticObjectRelations/Spatial/SpatialGraph.h>

namespace semrel
{
    static const QColor SEMANTIC_HIGHLIGHT(0x00, 0x97, 0x81);
    //static const QColor SEMANTIC_DEFAULT = Qt::GlobalColor::black;

    SemanticGraphEdgeItem::SemanticGraphEdgeItem(int sourceDescriptor, int targetDescriptor, const std::string semanticRelation, bool bidirectional) :
        sourceDescriptor(sourceDescriptor),
        targetDescriptor(targetDescriptor),
        semanticRelation(semanticRelation),
        bidirectional(bidirectional)
    {
        setColor();
    }

    void SemanticGraphEdgeItem::setColor() {
        color = SEMANTIC_HIGHLIGHT;
        if (!selected) {
            int id = 0;
            for (const auto &relation : semrel::SpatialRelations::names) {
                if (relation.second == semanticRelation)
                    break;
                id++;
            }
            auto c = simox::color::KellyLUT::KELLY_COLORS.at(id);
            color = QColor(c.r, c.g, c.b);
        }
        this->setPen(QPen(color, selected ? 5.0f : 3.0f, Qt::SolidLine, Qt::FlatCap, Qt::MiterJoin));
    }

    void SemanticGraphEdgeItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
    {
        setColor();

        painter->setRenderHint(QPainter::HighQualityAntialiasing, true);

        QGraphicsPathItem::paint(painter, option, widget);

        QPainterPath path = this->path();
        // TODO draw two arrowheads for bidirectional instead of none
        if (path.elementCount() >= 2 && !bidirectional)
        {
            int elementCount = path.elementCount();
            QPointF p1 = path.elementAt(elementCount - 1);
            QPointF p2 = path.elementAt(elementCount - 2);
            QLineF line(p1, p2);

            QPolygonF arrowHead;
            double angle = std::acos(line.dx() / line.length());

            if (line.dy() >= 0)
            {
                angle = (M_PI * 2) - angle;
            }

            float adjustedAngle = M_PI / 2.0f + openingAngle;
            QPointF arrowP1 = line.p1() + QPointF(sin(angle + adjustedAngle) * size,
                                                  cos(angle + adjustedAngle) * size);
            QPointF arrowP2 = line.p1() + QPointF(sin(angle + M_PI - adjustedAngle) * size,
                                                  cos(angle + M_PI - adjustedAngle) * size);

            arrowHead <<  line.p1() << arrowP1 << arrowP2 ;

            painter->drawPolygon(arrowHead);
        }
    }

    void SemanticGraphEdgeItem::mousePressEvent(QGraphicsSceneMouseEvent* event)
    {
        QPointF point = event->pos();

        QPainterPath path = this->path();

        float minDistance2 = 1000.0f;
        for (int i = 0; i < 101; ++i)
        {
            double t = i / 100.0;
            QPointF pointOnPath = path.pointAtPercent(t);
            QPointF diff(pointOnPath - point);
            float length2 = diff.x() * diff.x() + diff.y() * diff.y();
            if (length2 < minDistance2)
            {
                minDistance2 = length2;
            }
        }
        std::cout << "Mouse: " << sourceDescriptor << " -> " << targetDescriptor << ": " << std::sqrt(minDistance2);
        const float CLICK_DISTANCE = 7.0f;
        if (minDistance2 < CLICK_DISTANCE * CLICK_DISTANCE)
        {
            event->setAccepted(true);
            emit onLeftClick(this);
        }
        else
        {
            event->setAccepted(false);
        }
    }

//    QPainterPath SemanticGraphEdgeItem::shape() const
//    {
//        QPainterPath result;
//        QRectF bb = path().boundingRect();
//        double deltaX = 5.0;
//        double deltaY = 5.0;
//        bb.adjust(-deltaX, -deltaY, deltaX, deltaY);

//        result.addRect(bb);
//        return result;
//    }

}


