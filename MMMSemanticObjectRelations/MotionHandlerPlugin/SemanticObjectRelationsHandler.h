/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2019 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SemanticObjectRelationsHandler_H_
#define __MMM_SemanticObjectRelationsHandler_H_

#include "SemanticObjectRelationsWidget.h"

#ifndef Q_MOC_RUN
#include "../../MMMViewer/MMM/Viewer/MotionHandler.h"
#endif

namespace MMM
{

class SemanticObjectRelationsHandler : public MotionHandler
{
    Q_OBJECT

public:
    SemanticObjectRelationsHandler(QWidget* widget);

    void handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> currentRobots);

    virtual std::string getName();

    static constexpr const char* NAME = "SemanticObjectRelationsHandler";

public slots:
    void timestepChanged(float timestep);

private:
    QWidget* widget;
    SemanticObjectRelationsWidget* dialog;
};

}

#endif
