/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2019 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/


#pragma once

#include <QObject>
#include <QGraphicsItem>

#include <cmath>

namespace semrel
{

    class SemanticGraphEdgeItem
        : public QObject
        , public QGraphicsPathItem
    {
        Q_OBJECT
    public:
        SemanticGraphEdgeItem(int sourceDescriptor, int targetDescriptor, const std::string semanticRelation, bool bidirectional = false);

        // QGraphicsItem interface
        void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

        void mousePressEvent(QGraphicsSceneMouseEvent* event) override;

        //QPainterPath shape() const override;

        float openingAngle = M_PI / 8.0f;
        float size = 10.0f;

        bool selected = false;

    signals:
        void onLeftClick(SemanticGraphEdgeItem* selected);

    private:
        void setColor();

        int sourceDescriptor = 0;
        int targetDescriptor = 0;
        std::string semanticRelation = "";
        bool bidirectional = false;
        QColor color;
    };


}
