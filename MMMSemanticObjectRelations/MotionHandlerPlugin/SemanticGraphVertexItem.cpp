#include "SemanticGraphVertexItem.h"

#include <QGraphicsSceneContextMenuEvent>
#include <QPainter>
#include <iostream>

namespace semrel
{

    static const QColor SEMANTIC_BACKGROUND_HIGHLIGHT(0x00, 0x97, 0x81);
    static const QColor SEMANTIC_FOREGROUND_HIGHLIGHT(Qt::GlobalColor::white);
    static const QColor SEMANTIC_BACKGROUND_DEFAULT(Qt::GlobalColor::white);
    static const QColor SEMANTIC_FOREGROUND_DEFAULT(Qt::GlobalColor::black);

    void SemanticGraphVertexItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
    {
        if (selected)
        {
            this->setPen(QPen(SEMANTIC_FOREGROUND_HIGHLIGHT));
            this->setBrush(QBrush(SEMANTIC_BACKGROUND_HIGHLIGHT));
        }
        else
        {
            this->setPen(QPen(SEMANTIC_FOREGROUND_DEFAULT, 2.0f));
            this->setBrush(QBrush(SEMANTIC_BACKGROUND_DEFAULT));
        }
        QGraphicsEllipseItem::paint(painter, option, widget);

        // Add text to the ellipse

        QRectF rect = this->rect();
        painter->drawText(rect, Qt::AlignCenter, text);
    }

    void SemanticGraphVertexItem::mousePressEvent(QGraphicsSceneMouseEvent* event)
    {
        std::cout << "Pressed node: " << text.toStdString();
        if (event->button() == Qt::LeftButton)
        {
            emit onLeftClick(this);
        }
    }

}
