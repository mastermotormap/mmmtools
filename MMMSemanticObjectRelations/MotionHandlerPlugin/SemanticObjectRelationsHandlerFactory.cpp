#include "SemanticObjectRelationsHandlerFactory.h"
#include "SemanticObjectRelationsHandler.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry SemanticObjectRelationsHandlerFactory::registry(SemanticObjectRelationsHandler::NAME, &SemanticObjectRelationsHandlerFactory::createInstance);

SemanticObjectRelationsHandlerFactory::SemanticObjectRelationsHandlerFactory() : MotionHandlerFactory() {}

SemanticObjectRelationsHandlerFactory::~SemanticObjectRelationsHandlerFactory() {}

std::string SemanticObjectRelationsHandlerFactory::getName() {
    return SemanticObjectRelationsHandler::NAME;
}

MotionHandlerPtr SemanticObjectRelationsHandlerFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new SemanticObjectRelationsHandler(widget));
}

MotionHandlerFactoryPtr SemanticObjectRelationsHandlerFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new SemanticObjectRelationsHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new SemanticObjectRelationsHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
