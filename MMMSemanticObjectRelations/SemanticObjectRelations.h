#ifndef SEMANTICOBJECTRELATIONS_H
#define SEMANTICOBJECTRELATIONS_H

#include <MMM/Motion/Motion.h>
#include <SemanticObjectRelations/Shapes/shape_containers.h>
#include <VirtualRobot/VirtualRobot.h>
#include <SemanticObjectRelations/Spatial/SpatialGraph.h>
#include <SemanticObjectRelations/ContactDetection/ContactGraph.h>
#include <VirtualRobot/SceneObject.h>

namespace MMM
{

class SemanticObjectRelations
{
public:
    SemanticObjectRelations(MMM::MotionRecordingPtr motions, double timeFrequency = 0.01, bool endEffectors = true, bool boxShape = true);
    SemanticObjectRelations(const SemanticObjectRelations&) = delete;
    SemanticObjectRelations& operator=(const SemanticObjectRelations&) = delete;
    ~SemanticObjectRelations() {
        for (const auto &s : shapes)
            for (const auto &shape : s.second)
                delete shape;
    }

    semrel::SpatialGraph evaluateSpatialRelations(double timestep, float distanceEqualityThreshold = 30);
    semrel::ContactGraph evaluateContactRelations(double timestep);
    semrel::SpatialGraph getSpatialGraphsFromContactRelation(double timestep);

    std::vector<double> getTimesteps() {
        std::vector<double> timesteps;
        for (auto const &m : shapes) timesteps.push_back(m.first);
        return timesteps;
    }

    double getActualTimestep(double timestep) {
        return getIt(timestep)->first;
    }

    semrel::ShapeMap getObjects(double timestep) {
        return semrel::toShapeMap(getIt(timestep)->second);
    }

    std::map<double, std::vector<semrel::Shape*>>::iterator getIt(double timestep) {
        auto it = shapes.find(timestep);
        if (it == shapes.end()) {
            it = shapes.upper_bound(timestep); // TODO creates unprecise relations
        }
        return it;
    }

    std::vector<semrel::Shape*> getShapes(double timestep) {
        std::vector<semrel::Shape*> shapes;
        auto it = getIt(timestep);
        if (it != this->shapes.end())
            shapes = it->second;
        return shapes;
    }

    std::string getMotionName(int id) const {
        auto it = motionNames.find(id);
        if (it != motionNames.end())
            return it->second;
        else return std::string();
    }

    int getShapeID(std::string motionName) const {
        for (auto &i : motionNames) {
               if (i.second == motionName) {
                  return i.first;
               }
        }
        MMM_ERROR << "No shape id found for " << motionName << std::endl;
        return 0;
    }

private:
    semrel::Shape* getBoxShape(VirtualRobot::SceneObjectPtr node, long id, VirtualRobot::SceneObject::VisualizationType visuType = VirtualRobot::SceneObject::Full, bool includeChildNodes = true);

    void addShape(semrel::Shape* shape, double timestep);

    std::map<double, std::vector<semrel::Shape*>> shapes;
    std::map<int, std::string> motionNames;

    std::vector<VirtualRobot::RobotPtr> robots;
};

typedef std::shared_ptr<SemanticObjectRelations> SemanticObjectRelationsPtr;

}

#endif // SEMANTICOBJECTRELATIONS_H
