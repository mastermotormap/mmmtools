#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include <SemanticObjectRelations/Shapes/Shape.h>
#include <VirtualRobot/Nodes/RobotNode.h>

namespace semrel
{

class SimoxObjectShape : public Shape
{
public:
    SimoxObjectShape() = default;

    SimoxObjectShape(VirtualRobot::SceneObjectPtr object, long id, const std::string& className, const std::string& instanceName);

    // Shape interface
    Eigen::Vector3f getPosition() const override;
    void setPosition(Eigen::Vector3f const& position) override;

    Eigen::Quaternionf getOrientation() const override;
    void setOrientation(Eigen::Quaternionf const& orientation) override;

    TriMesh getTriMeshLocal() const override;

    std::shared_ptr<btCollisionShape> getBulletCollisionShape(float margin) const override;

    void addMargin(float margin) override;

    std::string tagPrefix() const override;

public:
    VirtualRobot::SceneObjectPtr object;
    std::string className = "";
    std::string instanceName = "";
    std::string entityId = "";

};

}
