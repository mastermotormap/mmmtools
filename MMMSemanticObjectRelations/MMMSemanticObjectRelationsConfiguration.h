/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2019 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SemanticObjectRelationsCONFIGURATION_H_
#define __MMM_SemanticObjectRelationsCONFIGURATION_H_

#include <filesystem>
#include <string>
#include <VirtualRobot/RuntimeEnvironment.h>

#include "../common/ApplicationBaseConfiguration.h"

/*!
    Configuration of MMM dynamics calculator.
*/
class MMMSemanticObjectRelationsConfiguration : public ApplicationBaseConfiguration
{
public:
    MMMSemanticObjectRelationsConfiguration() : ApplicationBaseConfiguration()
    {
    }

    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("inputMotion");
        VirtualRobot::RuntimeEnvironment::considerKey("sensorPlugins");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();
        inputMotionPath = getParameter("inputMotion", true, true);
        sensorPluginPaths = getPaths("sensorPlugins");

        return valid;
    }

    void print()
    {
        MMM_INFO << "*** MMMSemanticObjectRelations Configuration ***" << std::endl;
        std::cout << "Input motion: " << inputMotionPath << std::endl;   }

    std::filesystem::path inputMotionPath;
    std::vector<std::filesystem::path> sensorPluginPaths;
};

#endif
