#include "JsonSimoxShapeSerializer.h"

#include <VirtualRobot/ManipulationObject.h>

#include <SemanticObjectRelations/Shapes.h>
#include <SemanticObjectRelations/Shapes/json.h>
#include <SemanticObjectRelations/Shapes/json/ShapeSerializers.h>

#include "SimoxObjectShape.h"


namespace semrel
{

    void JsonSimoxShapeSerializer::to_json(nlohmann::json& j, const SimoxObjectShape& object) const
    {
        semrel::json::to_json_base(j, object);

        j["class"] = object.className;
        j["instance"] = object.instanceName; // TODO only for MMM motion
        j["name"] = object.className + ":" + object.instanceName;
        j["entityId"] = object.entityId;
    }

    void JsonSimoxShapeSerializer::from_json(const nlohmann::json& j, SimoxObjectShape& object) const
    {
        const std::string objectClassName = j.at("class").get<std::string>();

        // TODO
    }

    void JsonSimoxShapeSerializer::registerSerializer(
        const std::shared_ptr<JsonSimoxShapeSerializer>& instance, bool overwrite)
    {
        semrel::json::ShapeSerializers::registerSerializer<SimoxObjectShape>(
            [instance](nlohmann::json & j, const SimoxObjectShape & object)
        {
            instance->to_json(j, object);
        },
        [instance](const nlohmann::json & j, SimoxObjectShape & object)
        {
            instance->from_json(j, object);
        }, overwrite);
    }

}
