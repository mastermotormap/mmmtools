#include "SimoxObjectShape.h"

#include <VirtualRobot/ManipulationObject.h>
#include <VirtualRobot/math/Helpers.h>
#include <VirtualRobot/Visualization/TriMeshModel.h>
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Visualization/VisualizationNode.h>
//#include <BulletCollision/CollisionShapes/btBvhTriangleMeshShape.h>
//#include <BulletCollision/CollisionShapes/btTriangleMesh.h>


using namespace semrel;

SimoxObjectShape::SimoxObjectShape(VirtualRobot::SceneObjectPtr object, long id, const std::string& className, const std::string& instanceName)
    : Shape(ShapeID(id)), object(object), className(className), instanceName(instanceName)
{ }

Eigen::Vector3f SimoxObjectShape::getPosition() const
{
    return object->getGlobalPosition();
}

void SimoxObjectShape::setPosition(const Eigen::Vector3f& position)
{
    Eigen::Matrix4f pose = object->getGlobalPose();
    math::Helpers::Position(pose) = position;
    object->setGlobalPose(pose);
}

Eigen::Quaternionf SimoxObjectShape::getOrientation() const
{
    return Eigen::Quaternionf(object->getGlobalOrientation());
}

void SimoxObjectShape::setOrientation(const Eigen::Quaternionf& orientation)
{
    Eigen::Matrix4f pose = object->getGlobalPose();
    math::Helpers::Orientation(pose) = Eigen::Matrix3f(orientation);
    object->setGlobalPose(pose);
}

semrel::TriMesh SimoxObjectShape::getTriMeshLocal() const
{
    auto vis = object->getVisualization(VirtualRobot::SceneObject::Full);
    vis->createTriMeshModel();

    VirtualRobot::TriMeshModelPtr triMesh = vis->getTriMeshModel();

    std::vector<VirtualRobot::MathTools::TriangleFace> const& faces = triMesh->faces;
    std::vector<Eigen::Vector3f> const& vertices = triMesh->vertices;
    //std::vector<Eigen::Vector3f> const& normals = triMesh->normals;

    semrel::TriMesh result;

    for (VirtualRobot::MathTools::TriangleFace const& face : faces)
    {
        Eigen::Vector3f v0 = vertices.at(face.id1);
        Eigen::Vector3f v1 = vertices.at(face.id2);
        Eigen::Vector3f v2 = vertices.at(face.id3);

        Eigen::Vector3f normal = face.normal;

        semrel::TriMesh::Triangle triangle;
        triangle.v0 = result.numVertices();
        result.addVertex(v0);
        triangle.v1 = result.numVertices();
        result.addVertex(v1);
        triangle.v2 = result.numVertices();
        result.addVertex(v2);

        int n = result.numNormals();
        triangle.n0 = n;
        triangle.n1 = n;
        triangle.n2 = n;
        result.addNormal(normal);

        result.addTriangle(triangle);
    }

    return result;
}

std::shared_ptr<btCollisionShape> SimoxObjectShape::getBulletCollisionShape(float margin) const
{
    (void) margin;
    return nullptr;
    /*TriMesh trimesh = getTriMesh();
    btTriangleMesh* btTrimesh = new btTriangleMesh();

    // add margin to all vertices
    for (Eigen::Vector3f& vertex : trimesh.getVertices())
    {
        vertex += margin * vertex.normalized();
    }

    auto eigenToBullet = [](const Eigen::Vector3f& v)
    {
        auto vc = v.cast<btScalar>();
        return btVector3(vc(0), vc(1), vc(2));
    };

    for (const TriMesh::Triangle& tri : trimesh.getTriangles())
    {
        btTrimesh->addTriangle(eigenToBullet(trimesh.getVertex(tri.v0)),
                               eigenToBullet(trimesh.getVertex(tri.v1)),
                               eigenToBullet(trimesh.getVertex(tri.v2)));
    }

    return std::shared_ptr<btCollisionShape>(new btBvhTriangleMeshShape(btTrimesh, true));*/
}

void SimoxObjectShape::addMargin(float margin)
{
    // TODO: Implement
    (void) margin;
}

std::string SimoxObjectShape::tagPrefix() const
{
    return className + ":" + instanceName;
}
