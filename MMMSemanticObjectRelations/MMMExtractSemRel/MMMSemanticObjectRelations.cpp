/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2019 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include <VirtualRobot/RuntimeEnvironment.h>
#include <MMM/Motion/Motion.h>
#include <MMM/Motion/MotionReaderXML.h>
#include "MMMSemanticObjectRelationsConfiguration.h"
#include <SemanticObjectRelations/RelationGraph/json/AttributedGraph.h>
#include <SemanticObjectRelations/SupportAnalysis/SupportGraph.h>
#include <SemanticObjectRelations/Shapes/json/ShapeSerializers.h>
#include "../SemanticObjectRelations.h"
#include <SemanticObjectRelations/Spatial/json.h>
//#include <SemanticObjectRelations/SupportAnalysis/json.h>

#include "../JsonSimoxShapeSerializer.h"

#include <string>
#include <map>
#include <vector>
#include <exception>
#include <regex>

using namespace semrel;

int main(int argc, char *argv[])
{
    MMM_INFO << " --- MMMSemanticObjectRelations --- " << std::endl;
    MMMSemanticObjectRelationsConfiguration *configuration = new MMMSemanticObjectRelationsConfiguration();
    if (!configuration->processCommandLine(argc, argv))
    {
        MMM_ERROR << "Error while processing command line, aborting..." << std::endl;
        return -1;
    }

    std::shared_ptr<semrel::JsonSimoxShapeSerializer> serializer(new semrel::JsonSimoxShapeSerializer());
    semrel::JsonSimoxShapeSerializer::registerSerializer(serializer, true);

    try {
        MMM::MotionReaderXMLPtr motionReader(new MMM::MotionReaderXML(true, false, configuration->sensorPluginPaths));
        MMM_INFO << "Reading motion file '" << configuration->inputMotionPath << "'!" << std::endl;
        MMM::MotionRecordingPtr motions = motionReader->loadMotionRecording(configuration->inputMotionPath);

        MMM::SemanticObjectRelationsPtr sem = MMM::SemanticObjectRelationsPtr(new MMM::SemanticObjectRelations(motions, 0.01, false, true));

        for (double timestep : sem->getTimesteps()) {
            semrel::SpatialGraph graph = sem->evaluateSpatialRelations(timestep, 30);
            semrel::AttributedGraph attrGraph = semrel::toAttributedGraph(graph, sem->getObjects(timestep));

            std::cout << attrGraph.attrib().json.dump() << std::endl;

            for (auto inputVertex : attrGraph.vertices())
            {
                std::cout <<  "Vertex " << static_cast<long>(inputVertex.descriptor()) << ": " << inputVertex.attrib().json.dump() << std::endl;
            }

            for (auto inputEdge : attrGraph.edges())
            {
                std::cout <<  "Edge " << static_cast<long>(inputEdge.sourceDescriptor()) << " " << static_cast<long>(inputEdge.targetDescriptor())
                           << ": " << inputEdge.attrib().json.dump() << std::endl;
            }
        }
    }
    catch (MMM::Exception::MMMException &e) {
        MMM_ERROR << e.what() << std::endl;
        return -2;
    }

    return 0;
}
