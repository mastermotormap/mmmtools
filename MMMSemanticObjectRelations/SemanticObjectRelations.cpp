#include "SemanticObjectRelations.h"

#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>
#include <VirtualRobot/RobotConfig.h>
#include <VirtualRobot/RobotNodeSet.h>

#include <SemanticObjectRelations/Spatial/evaluate_relations.h>
#include <SemanticObjectRelations/ContactDetection.h>
#include <VirtualRobot/EndEffector/EndEffector.h>
#include "SimoxObjectShape.h"
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <VirtualRobot/SceneObject.h>
#include <VirtualRobot/Visualization/VisualizationNode.h>
#include <SimoxUtility/math/convert.h>
#include <VirtualRobot/Visualization/TriMeshModel.h>
#include <SemanticObjectRelations/SupportAnalysis/GeometricReasoning.h>
#include <SemanticObjectRelations/SupportAnalysis/ActReasoning.h>
#include <SemanticObjectRelations/RelationGraph.h>

using namespace semrel;
using namespace semrel::spatial;
using namespace MMM;

SemanticObjectRelations::SemanticObjectRelations(MMM::MotionRecordingPtr motions, double timeFrequency, bool endEffectors, bool boxShape)
{
    std::vector<float> timesteps;
    auto mmm = motions->getReferenceModelMotion();
    if (mmm) timesteps = mmm->getTimesteps();
    else {
        for (auto motion : *motions) motion->synchronizeSensorMeasurements(timeFrequency);
        if (motions->size() > 0) timesteps = (*motions->begin())->getTimesteps();
    }

    long id = 0;

    std::set<std::string> ignore;// = {"go_pro", "kinect_azure_right", "kinect_azure_left", "kinect_azure_front"};

    for (MMM::MotionPtr motion : *motions) {
        /*if (ignore.find(motion->getName()) != ignore.end()) {
            MMM_INFO << "Ignoring motion " << motion->getName() << " because it is on the ignore list." << std::endl;
            continue;
        }*/
        if (!motion->getModel()) {
            MMM_INFO << "Ignoring motion " << motion->getName() << " because it has no model" << std::endl;
            continue;
        }
        MMM_INFO << motion->getName();
        //todo here!!
        auto robot = motion->getModel()->cloneScaling();

        robot->reloadVisualizationFromXML();
        MMM_INFO << "Robot filename: " << robot->getFilename();

        if (motion->getActuatedJointNames().size() > 0) {
            if (endEffectors) {
                std::vector<std::pair<std::string, std::string>> endEffectorNames =
                    //{};
                    {std::make_pair("Hand L Root", "left_hand"), std::make_pair("Hand R Root", "right_hand")}; // TODO ONLY MMM!
                for (float timestep : timesteps) {
                    motion->initializeModel(robot, timestep);

                    for (unsigned int i = 0; i < endEffectorNames.size(); i++) {
                        auto endEffectorNode = robot->getRobotNode(endEffectorNames[i].first);
                        if (boxShape)
                        {
                            addShape(getBoxShape(endEffectorNode, id + i), timestep);
                        }
                        else
                        {
                            addShape(new SimoxObjectShape(endEffectorNode, id + i, endEffectorNames[i].second, motion->getName()), timestep);
                        }
                        motionNames[id + i] = endEffectorNames[i].second;
                    }
                }
                id += endEffectorNames.size();
            }
        }
        else {
            auto rootNode = robot->getRootNode();
            for (float timestep : timesteps) {
                motion->initializeModel(robot, timestep);
                if (boxShape) addShape(getBoxShape(rootNode, id), timestep);
                else addShape(new SimoxObjectShape(rootNode, id, std::filesystem::path(robot->getFilename()).stem().string(), motion->getName()), timestep);
                motionNames[id] = motion->getName();
            }
            id++;
        }
    }
}


Shape* SemanticObjectRelations::getBoxShape(VirtualRobot::SceneObjectPtr node, long id, VirtualRobot::SceneObject::VisualizationType visuType, bool includeChildNodes) {
    VirtualRobot::VisualizationNodePtr vis = node->getVisualization(visuType);
    if (includeChildNodes) {
        std::vector<VirtualRobot::VisualizationNodePtr> nodes;
        if (vis) nodes.push_back(vis);
        auto children = node->getChildren();
        while (!children.empty()) {
            auto child = *children.rbegin();
            children.pop_back();
            auto childVis = child->getVisualization(visuType);
            if (childVis)
                nodes.push_back(childVis);
            auto childChildren = child->getChildren();
            children.insert(children.begin(), childChildren.begin(), childChildren.end());
        }
        if (nodes.size() == 1)
            vis = nodes.at(0);
        else if (nodes.size() > 1)
            vis = VirtualRobot::VisualizationNode::CreateUnitedVisualization(nodes);
    }

    if (vis) {
        auto boundingBox = vis->getBoundingBox();

        if (false) {
            // Following code computes oobb
            VirtualRobot::TriMeshModelPtr tm = vis->getTriMeshModel();

            if (!tm)
            {
                Eigen::Vector3f minS, maxS;
                tm->getSize(minS, maxS);
                VirtualRobot::MathTools::OOBB bbox(minS, maxS, vis->getGlobalPose());
            }
        }

        //auto pose = node->getGlobalPose();
        AxisAlignedBoundingBox aabb = AxisAlignedBoundingBox(boundingBox.getMin(), boundingBox.getMax());
        return new Box((aabb.min() + aabb.max()) / 2, Eigen::Matrix3f::Identity(), aabb.extents(), ShapeID(id));
    }
    else return nullptr;
}

void SemanticObjectRelations::addShape(Shape* shape, double timestep) {
    if (!shape) return;
    if (shapes.find(timestep) == shapes.end()) shapes[timestep] = std::vector<Shape*>();
    shapes.at(timestep).push_back(shape);
}

SpatialGraph SemanticObjectRelations::evaluateSpatialRelations(double timestep, float distanceEqualityThreshold) {
    auto it = getIt(timestep);
    if (it != shapes.end()) {
        if (it != shapes.begin()) {
            auto currentObjects = it->second;
            auto pastObjects = std::prev(it)->second;
            Parameters params;
            params.distanceEqualityThreshold = distanceEqualityThreshold;
            return evaluateRelations(toShapeMap(currentObjects), toShapeMap(pastObjects), params);
        }
        else {
            return evaluateStaticRelations(toShapeMap(it->second)); // no dynamic relations available
        }
    }
    // TODO else error!
    return SpatialGraph();
}

SpatialGraph SemanticObjectRelations::getSpatialGraphsFromContactRelation(double timestep)
{
    ContactGraph cgraph = evaluateContactRelations(timestep);
    auto it = getIt(timestep);
    //auto objects = it->second;
    std::vector<semrel::Shape*> objects = it->second;
    ShapeMap shapeMap = toShapeMap(objects);
    SpatialGraph sgraph;// = initializeGraph(toShapeMap(objects), {});
    //Vgl evaluate relations
    // contact simply if edge exists
    for(ContactGraph::Vertex vertex: cgraph.vertices())
    {
        // TODO: Maybe add attributes?
        sgraph.addVertex(*shapeMap.at(vertex.objectID()), {});
    }
    for (SpatialGraph::Vertex vertexFrom : sgraph.vertices())
    {
        for (SpatialGraph::Vertex vertexTo : sgraph.vertices())
        {
            ContactGraph::Vertex contactVertexFrom = cgraph.vertex(vertexFrom.attrib().objectID);
            ContactGraph::Vertex contactVertexTo = cgraph.vertex(vertexTo.attrib().objectID);
            SpatialEdge edgeAttribs;
            if (cgraph.hasEdge(contactVertexFrom, contactVertexTo)
                    || cgraph.hasEdge(contactVertexTo, contactVertexFrom))
            {
                edgeAttribs.relations.set(semrel::SpatialRelations::Type::contact, true);
            }
            // Create bidirectional contact graph
            sgraph.addEdge(vertexFrom, vertexTo, edgeAttribs);
            sgraph.addEdge(vertexTo, vertexFrom, edgeAttribs);
        }
    }

    return sgraph;
}

ContactGraph SemanticObjectRelations::evaluateContactRelations(double timestep)
{
    // TODO currently only test!!
    auto it = getIt(timestep);
    if (it == shapes.end())
    {
        throw std::runtime_error("Could not find shape at timestep " + std::to_string(timestep));
    }
    std::vector<semrel::Shape*>& currentObjects = it->second;
    ContactDetection contactDetection;
    return contactDetection.computeContacts(toShapeMap(currentObjects));
}
