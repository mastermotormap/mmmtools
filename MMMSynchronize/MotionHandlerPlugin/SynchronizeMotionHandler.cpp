#include "SynchronizeMotionHandler.h"
#include "SynchronizeMotionHandlerDialog.h"

#include <MMM/Motion/MotionRecording.h>

using namespace MMM;

SynchronizeMotionHandler::SynchronizeMotionHandler(QWidget* widget) :
    MotionHandler(MotionHandlerType::GENERAL, "Synchronize"),
    widget(widget)
{
}

void SynchronizeMotionHandler::handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> /*currentRobots*/) {
    if (motions && !motions->empty()) {
        SynchronizeMotionHandlerDialog* dialog = new SynchronizeMotionHandlerDialog(widget, motions);
        connect(dialog, &SynchronizeMotionHandlerDialog::openMotions, this, &MotionHandler::openMotions);
        connect(dialog, &SynchronizeMotionHandlerDialog::updateVisualisation, this, &MotionHandler::updateVisualisation);
        dialog->show();
    }
    else MMM_ERROR << "Cannot open synchronize motion handler dialog, because no motions are present!" << std::endl;
}

std::string SynchronizeMotionHandler::getName() {
    return NAME;
}
