/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/


#ifndef __MMM_SYNCHRONIZEMOTIONHANDLERDIALOG_H_
#define __MMM_SYNCHRONIZEMOTIONHANDLERDIALOG_H_

#include <QDialog>
#include <QListWidgetItem>

#ifndef Q_MOC_RUN
#include <MMM/MMMCore.h>
#endif

class SensorOffsetItem : public QListWidgetItem
{
public:
    SensorOffsetItem(const std::string &motionName, MMM::SensorPtr sensor);

    void shiftMeasurements(float timestep);

    void updateLabel();

private:
    std::string motionName;
    MMM::SensorPtr sensor;
    float offset;
};

namespace Ui {
class SynchronizeMotionHandlerDialog;
}

class SynchronizeMotionHandlerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SynchronizeMotionHandlerDialog(QWidget* parent, MMM::MotionRecordingPtr motions);
    ~SynchronizeMotionHandlerDialog();

signals:
    void openMotions(MMM::MotionRecordingPtr);
    void updateVisualisation();

private slots:
    void setCurrentMotion(QString name);
    void chooseMotionToggled(bool checked);
    void allMotionToggled(bool checked);
    void synchronize();
    void shiftPlus();
    void shiftMinus();

private:
    void loadMotions();
    void updateSensorOffsetList();
    void shiftSelectedSensors(float delta);

    Ui::SynchronizeMotionHandlerDialog* ui;
    MMM::MotionRecordingPtr motions;
    MMM::MotionPtr currentMotion;
    bool allMotion;
};

#endif // __MMM_SYNCHRONIZEMOTIONHANDLERDIALOG_H_
