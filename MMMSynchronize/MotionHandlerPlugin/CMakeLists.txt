project(SynchronizeMotionHandler)

set(SynchronizeMotionHandler_Sources
    SynchronizeMotionHandlerFactory.cpp
    SynchronizeMotionHandler.cpp
    SynchronizeMotionHandlerDialog.cpp
)

set(SynchronizeMotionHandler_Headers
    SynchronizeMotionHandlerFactory.h
    SynchronizeMotionHandler.h
    SynchronizeMotionHandlerDialog.h
)

set(SynchronizeMotionHandler_Moc
    SynchronizeMotionHandlerDialog.h
    SynchronizeMotionHandler.h
    ../../MMMViewer/MMM/Viewer/MotionHandler.h
)

set(SynchronizeMotionHandler_Ui
    SynchronizeMotionHandlerDialog.ui
)

DefineMotionHandlerPlugin(${PROJECT_NAME} "${SynchronizeMotionHandler_Sources}" "${SynchronizeMotionHandler_Headers}" "${SynchronizeMotionHandler_Moc}" "${SynchronizeMotionHandler_Ui}" "")
