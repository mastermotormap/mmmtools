#include "SynchronizeMotionHandlerDialog.h"
#include "ui_SynchronizeMotionHandlerDialog.h"

#include <MMM/Motion/MotionRecording.h>
#include <MMM/Motion/Sensor/Sensor.h>

#include <QCheckBox>
#include <set>

SynchronizeMotionHandlerDialog::SynchronizeMotionHandlerDialog(QWidget* parent, MMM::MotionRecordingPtr motions) :
    QDialog(parent),
    ui(new Ui::SynchronizeMotionHandlerDialog),
    motions(motions),
    currentMotion(nullptr),
    allMotion(true)
{
    ui->setupUi(this);

    loadMotions();

    connect(ui->AllMotionCheckBox, SIGNAL(toggled(bool)), this, SLOT(allMotionToggled(bool)));
    connect(ui->ChooseMotionGroupBox, SIGNAL(toggled(bool)), this, SLOT(chooseMotionToggled(bool)));
    connect(ui->ChooseMotionGroupBox, SIGNAL(toggled(bool)), ui->ChooseMotionComboBox, SLOT(setEnabled(bool)));
    connect(ui->ChooseMotionComboBox, SIGNAL(currentTextChanged(QString)), this, SLOT(setCurrentMotion(QString)));
    connect(ui->SynchronizeButton, SIGNAL(clicked()), this, SLOT(synchronize()));
    connect(ui->ShiftPlusButton, SIGNAL(clicked()), this, SLOT(shiftPlus()));
    connect(ui->ShiftMinusButton, SIGNAL(clicked()), this, SLOT(shiftMinus()));
}

SynchronizeMotionHandlerDialog::~SynchronizeMotionHandlerDialog() {
    delete ui;
}

void SynchronizeMotionHandlerDialog::loadMotions() {
    for (const std::string &name : motions->getMotionNames()) {
        ui->ChooseMotionComboBox->addItem(QString::fromStdString(name));
    }
    updateSensorOffsetList();
    setCurrentMotion(0);
}

void SynchronizeMotionHandlerDialog::setCurrentMotion(QString name) {
    currentMotion = motions->getMotion(name.toStdString());
}

void SynchronizeMotionHandlerDialog::chooseMotionToggled(bool checked) {
    ui->AllMotionCheckBox->setChecked(!checked);
    allMotion = !checked;
}

void SynchronizeMotionHandlerDialog::allMotionToggled(bool checked) {
    ui->ChooseMotionGroupBox->setChecked(!checked);
    allMotion = checked;
}

void SynchronizeMotionHandlerDialog::synchronize() {
    float timeFrequency = 1.0f / ui->FPSSpin->value();
    if (allMotion) {
        motions->synchronizeSensorMeasurements(timeFrequency);
    } else {
        currentMotion->synchronizeSensorMeasurements(timeFrequency);
    }
    emit openMotions(motions);
    this->close();
}

void SynchronizeMotionHandlerDialog::shiftPlus() {
    shiftSelectedSensors(ui->ShiftTimestep->value());
}

void SynchronizeMotionHandlerDialog::shiftMinus() {
    shiftSelectedSensors(-ui->ShiftTimestep->value());
}

void SynchronizeMotionHandlerDialog::updateSensorOffsetList() {
    ui->SensorOffsetList->clear();
    for (auto motion : *motions) {
        for (auto sensor : motion->getPrioritySortedSensorData()) {
            ui->SensorOffsetList->addItem(new SensorOffsetItem(motion->getName(), sensor));
        }
    }
}

void SynchronizeMotionHandlerDialog::shiftSelectedSensors(float delta) {
    for (auto item : ui->SensorOffsetList->selectedItems()) {
        auto sensorOffsetItem = (SensorOffsetItem*)item;
        if (sensorOffsetItem)
            sensorOffsetItem->shiftMeasurements(delta);
    }
    emit updateVisualisation();
}

SensorOffsetItem::SensorOffsetItem(const std::string &motionName, MMM::SensorPtr sensor) : motionName(motionName), sensor(sensor), offset(0.0f)
{
    updateLabel();
}

void SensorOffsetItem::shiftMeasurements(float timestep) {
    sensor->shiftMeasurements(timestep);
    offset += timestep;
    updateLabel();
}

void SensorOffsetItem::updateLabel() {
    std::string offsetStr = std::abs(offset) > 0.00001 ? "\t" + std::to_string(offset) : "";
    setText(QString::fromStdString(motionName + "\t" + sensor->getUniqueName() + offsetStr));
}
