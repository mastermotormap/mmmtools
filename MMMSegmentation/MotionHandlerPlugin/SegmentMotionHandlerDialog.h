/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_SEGMENTMOTIONHANDLERDIALOG_H_
#define __MMM_SEGMENTMOTIONHANDLERDIALOG_H_

#include <QDialog>
#include <QListWidgetItem>
#include <QSignalMapper>
#include <map>
#include <set>
#include <Inventor/sensors/SoTimerSensor.h>

#ifndef Q_MOC_RUN
#include <MMM/Motion/Motion.h>
#include "../MotionSegmenterFactory.h"
#include "SegmentationPointsWidget.h"
#endif

namespace Ui {

class SegmentMotionHandlerDialog;
}

class SegmentMotionHandlerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SegmentMotionHandlerDialog(QWidget* parent);
    ~SegmentMotionHandlerDialog();

    void segmentMotion(MMM::MotionRecordingPtr motions);

    void update(std::map<std::string, MMM::MotionSegmenterFactoryPtr > motionSegmenterFactories);

signals:
    void jumpTo(float timestep);

private slots:
    void loadAdditionalPlugins();
    void setSegmenter(QString name);
    void setCurrentMotion(QString name);
    void setMMMSegmentMotionHandlerType(bool value);
    void setMMMMarkerSegmentMotionHandlerType(bool value);
    void setC3DMarkerSegmentMotionHandlerType(bool value);
    void segmentMotion();
    void setTimestepChecked(int index);
    void playSegmentMotionHandler();
    void radioButtonClicked(int index);
    void closeEvent(QCloseEvent* event);

private:
    void loadPlugins(std::vector<std::string> segmentationPluginPaths);
    void loadMotions(MMM::MotionRecordingPtr motions);
    void createSegmentationPointsWidget();
    static void playSegmentMotionHandlerTimerCallback(void* data, SoSensor* sensor);
    void playSegmentMotionHandlerStep();
    void stopPlaySegmentMotionHandler();
    void setSegmentMotionHandlerType(MMM::SegmentationType type);
    void enableSegmentMotion();

    Ui::SegmentMotionHandlerDialog* ui;

    MMM::MotionRecordingPtr motions;
    std::string motionName;
    MMM::MotionPtr currentMotion;
    MMM::MotionSegmenterPtr segmenter;
    MMM::SegmentationType segType;
    QWidget* currentSegmentationWidget;
    SegmentationPointsWidget* currentSegmentationPointsWidget;
    std::vector<MMM::SegmentationPtr> segmentedTimesteps;
    SoTimerSensor* playSegmentMotionHandlerTimer;
    unsigned int currentSegment;
    QSignalMapper* signalMapper;

    std::map<std::string, std::shared_ptr<MMM::MotionSegmenterFactory> > motionSegmenterFactories;

};

#endif // __MMM_SEGMENTMOTIONHANDLERDIALOG_H_
