#include "SegmentMotionHandler.h"
#include <QFileDialog>

#include <MMM/Motion/MotionRecording.h>

using namespace MMM;

SegmentMotionHandler::SegmentMotionHandler(QWidget* widget) :
    MotionHandler(MotionHandlerType::GENERAL, "Segment"),
    dialog(new SegmentMotionHandlerDialog(widget))
{
    connect(dialog, SIGNAL(jumpTo(float)), this, SIGNAL(jumpTo(float)));
}

void SegmentMotionHandler::handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> currentRobots) {
    if (motions && !motions->empty()) dialog->segmentMotion(motions);
    else MMM_ERROR << "Cannot open segment motion dialog, because no motions are present!" << std::endl;
}

std::string SegmentMotionHandler::getName() {
    return NAME;
}

std::shared_ptr<IPluginHandler> SegmentMotionHandler::getPluginHandler() {
    std::shared_ptr<PluginHandler<MMM::MotionSegmenterFactory> > pluginHandler(new PluginHandler<MMM::MotionSegmenterFactory>(getPluginHandlerID(), MOTION_SEGMENTER_PLUGIN_LIB_DIR));
    pluginHandler->updateSignal.connect(std::bind(&SegmentMotionHandlerDialog::update, dialog, std::placeholders::_1));
    pluginHandler->emitUpdate();

    return pluginHandler;
}

std::string SegmentMotionHandler::getPluginHandlerID() {
    return "Motion Segmenter";
}

