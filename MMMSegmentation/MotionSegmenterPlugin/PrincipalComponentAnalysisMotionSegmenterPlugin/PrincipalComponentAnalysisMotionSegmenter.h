/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_PRINCIPALCOMPONENTANALYSISMOTIONSEGMENTER_H_
#define __MMM_PRINCIPALCOMPONENTANALYSISMOTIONSEGMENTER_H_

#include "../../MotionSegmenter.h"
#include "PrincipalComponentAnalysisMotionSegmenterConfiguration.h"
#include "PrincipalComponentAnalysisMotionSegmenterWidget.h"

namespace MMM
{

class PrincipalComponentAnalysisMotionSegmenter : public MotionSegmenter
{
public:
    PrincipalComponentAnalysisMotionSegmenter(MotionPtr motion, SegmentationType segType);

    std::vector<SegmentationPtr> segment();

    std::vector<SegmentationPtr> segment(PrincipalComponentAnalysisMotionSegmenterConfigurationPtr configuration);

    void setSegmentationType(SegmentationType segType);

    void setMotion(MMM::MotionPtr motion);

    QWidget* getWidget();

    std::string getName();

    static constexpr const char* NAME = "Principal Component Analysis";

private:
    std::vector<SegmentationPtr> principalComponentAnalysis(Eigen::MatrixXf matrix, const std::vector<float> &timesteps, int reducedDimensions, float alpha, KeyframeType keyframeType);

    PrincipalComponentAnalysisMotionSegmenterWidget* widget;
    MotionPtr motion;
    SegmentationType segType;
};

}

#endif
