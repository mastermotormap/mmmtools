#include "VelocityCrossingMotionSegmenterConfiguration.h"

using namespace MMM;

VelocityCrossingMotionSegmenterConfiguration::VelocityCrossingMotionSegmenterConfiguration(float velocityThreshold, int segmentThreshold, KeyframeType keyframeType, const std::set<std::string> &names) :
    velocityThreshold(velocityThreshold),
    segmentThreshold(segmentThreshold),
    keyframeType(keyframeType),
    segmentNames(names)
{
}

