/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_VELOCITYCROSSINGMOTIONSEGMENTER_H_
#define __MMM_VELOCITYCROSSINGMOTIONSEGMENTER_H_

#include "../../MotionSegmenter.h"
#include "VelocityCrossingMotionSegmenterConfiguration.h"
#include "VelocityCrossingMotionSegmenterWidget.h"

namespace MMM
{

class VelocityCrossingMotionSegmenter : public MotionSegmenter
{
public:
    VelocityCrossingMotionSegmenter(MotionPtr motion, SegmentationType segType);

    std::vector<SegmentationPtr> segment();

    std::vector<SegmentationPtr> segment(VelocityCrossingMotionSegmenterConfigurationPtr configuration);

    void setSegmentationType(SegmentationType segType);

    void setMotion(MMM::MotionPtr motion);

    QWidget* getWidget();

    std::string getName();

    static constexpr const char* NAME = "Velocity Crossing";

private:
    VelocityCrossingMotionSegmenterWidget* widget;
    MotionPtr motion;
    SegmentationType segType;
};

}

#endif
