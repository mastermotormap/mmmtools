#include "Segmentation.h"

using namespace MMM;

Segmentation::Segmentation(float keyTimestep) :
    minTimestep(keyTimestep),
    maxTimestep(keyTimestep),
    keyTimestep(keyTimestep)
{
}

Segmentation::Segmentation(float minTimestep, float maxTimestep, KeyframeType type) :
    minTimestep(minTimestep),
    maxTimestep(maxTimestep),
    keyTimestep(getKeyTimestep(type))
{
}

Segmentation::Segmentation(float minTimestep, float maxTimestep, float keyTimestep) :
    minTimestep(minTimestep),
    maxTimestep(maxTimestep),
    keyTimestep(keyTimestep)
{
}

float Segmentation::getMinTimestep() {
    return minTimestep;
}

float Segmentation::getMaxTimestep() {
    return maxTimestep;
}

float Segmentation::getKeyTimestep() {
    return keyTimestep;
}

void Segmentation::setKeyTimestep(KeyframeType type) {
    keyTimestep = getKeyTimestep(type);
}

float Segmentation::getKeyTimestep(KeyframeType type) {
    switch(type) {
    case KeyframeType::FIRST:
        return minTimestep;
    case KeyframeType::LAST:
        return maxTimestep;
    case KeyframeType::AVERAGE:
    default:
        return (minTimestep + maxTimestep) / 2.0f;
    }
}

