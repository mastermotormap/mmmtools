/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MOTIONSEGMENTER_H_
#define __MMM_MOTIONSEGMENTER_H_

#include <MMM/MMMCore.h>
#include <MMM/MMMImportExport.h>
#include <MMM/Motion/Motion.h>

#include <QWidget>
#include <Eigen/Core>
#include <string> 
#include <vector>
#include <map>

#include "Segmentation.h"

namespace MMM
{

enum MMM_IMPORT_EXPORT SegmentationType
{
    MMM, MMM_MARKER, C3D_MARKER
};

class MMM_IMPORT_EXPORT MotionSegmenter
{
public:   
    virtual std::vector<SegmentationPtr> segment() = 0;

    virtual void setSegmentationType(SegmentationType segType) = 0;

    virtual void setMotion(MMM::MotionPtr motion) = 0;

    virtual QWidget* getWidget() = 0;
};

typedef std::shared_ptr<MotionSegmenter> MotionSegmenterPtr;

}

#endif 
