/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMMTools
* @copyright  2015 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include <VirtualRobot/RuntimeEnvironment.h>
#include <MMM/MMMCore.h>
#include <SimoxUtility/algorithm/string.h>

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

#include "ApplicationBaseConfiguration.h"

ApplicationBaseConfiguration::ApplicationBaseConfiguration() : valid(true)
{
}

std::vector<std::filesystem::path> ApplicationBaseConfiguration::getPaths(const std::string &parameterName, const std::filesystem::path &defaultPath) {
    std::vector<std::filesystem::path> values;
    if (!defaultPath.empty()) values.push_back(defaultPath);
    return getPaths(parameterName, values);
}

std::vector<std::filesystem::path> ApplicationBaseConfiguration::getPaths(const std::string &parameterName, std::vector<std::filesystem::path> defaultPaths)
{
    for (const std::string &path : simox::alg::split(getParameter(parameterName), ";,")) {
        if(!path.empty()) defaultPaths.push_back(path);
    }
    return defaultPaths;
}

std::vector<std::string> ApplicationBaseConfiguration::getParameters(const std::string &parameterName, const std::string &baseValue) {
    std::vector<std::string> baseValues;
    if (!baseValue.empty()) baseValues.push_back(baseValue);
    return getParameters(parameterName, baseValues);
}

std::vector<std::string> ApplicationBaseConfiguration::getParameters(const std::string &parameterName, std::vector<std::string> baseValues)
{
    for (const std::string &parameter : simox::alg::split(getParameter(parameterName), ";,")) {
        if(!parameter.empty()) baseValues.push_back(parameter);
    }
    return baseValues;
}

std::string ApplicationBaseConfiguration::getParameter(const std::string &parameterName, bool required, bool isFilePath)
{
    if (VirtualRobot::RuntimeEnvironment::hasValue(parameterName))
    {
        std::string value = VirtualRobot::RuntimeEnvironment::getValue(parameterName);

        if (isFilePath && !VirtualRobot::RuntimeEnvironment::getDataFileAbsolute(value))
        {
            MMM_ERROR << "Invalid path: " << value << std::endl;
            valid = false;
            return std::string();
        }

        return value;
    }
    else if (required)
    {
        MMM_ERROR << "Missing parameter " << parameterName << "!" << std::endl;
        valid = false;
    }

    return std::string();
}

void ApplicationBaseConfiguration::printVec(const std::string &name, const std::vector<std::string> &v) {
    std::cout << name << ": ";
    for (auto element : v) {
        std::cout << element << ";";
    }
    std::cout << std::endl;
}

void ApplicationBaseConfiguration::printVec(const std::string &name, const std::vector<std::filesystem::path> &v) {
    std::cout << name << ": ";
    for (auto element : v) {
        std::cout << element << ";";
    }
    std::cout << std::endl;
}
