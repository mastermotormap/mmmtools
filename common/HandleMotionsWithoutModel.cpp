#include "HandleMotionsWithoutModel.h"

#include <QSettings>
#include <QFileDialog>
#include <MMM/MMMCore.h>
#include <filesystem>
#include <MMM/Motion/XMLTools.h>
#include "../common/HandleMotionsWithoutModel.h"

void handleMissingModelFilePath(std::filesystem::path &modelFilePath) {
    QSettings settings;
    std::string mmmDefaultPath = std::string(MMMTools_SRC_DIR) + "/data/Model/Winter/mmm.xml";
    std::filesystem::path mmmDefaultFilePath(mmmDefaultPath);

    std::string defaultPath = settings.value("model/searchpath", QString(MMMTools_SRC_DIR) + "/data/Model/Objects").toString().toStdString();

    std::string fileName = modelFilePath.stem();
    std::filesystem::path objectDefaultPath = defaultPath + "/" + fileName + "/" + fileName + ".xml";
    std::string folderName = modelFilePath.parent_path().filename();
    std::filesystem::path objectDefaultPath2 = defaultPath + "/" + folderName + "/" + fileName + ".xml";

    if (modelFilePath.generic_string().find("mmm.xml") != std::string::npos && MMM::xml::isValid(mmmDefaultFilePath)) {
        modelFilePath = mmmDefaultPath;
        MMM_INFO << "Loading missing mmm model file from default path '" + mmmDefaultPath + "'" << std::endl;
    } else if (MMM::xml::isValid(objectDefaultPath)) {
        modelFilePath = objectDefaultPath;
        MMM_INFO << "Loading missing object model file from default path '" + objectDefaultPath.generic_string() + "'" << std::endl;
    } else if (MMM::xml::isValid(objectDefaultPath2)) {
        modelFilePath = objectDefaultPath2;
        MMM_INFO << "Loading missing object model file from default path '" + objectDefaultPath2.generic_string() + "'" << std::endl;
    } else {
        QSettings settings;
        QString storedSearchPath = "model/searchpath/" + QString::fromStdString(fileName);
        if (settings.contains(storedSearchPath)) {
            std::string searchPath = settings.value(storedSearchPath).toString().toStdString();
            if (MMM::xml::isValid(searchPath)) {
                modelFilePath = searchPath;
                return;
            }
        }
        modelFilePath = QFileDialog::getOpenFileName(0, QString::fromStdString("Model file could not be loaded from '" + modelFilePath.generic_string() + "'. Choose a matching model file in your file system!"), QString::fromStdString(defaultPath), QString::fromStdString("model files (*.xml)")).toStdString();
        if (!modelFilePath.empty()) {
            settings.setValue("model/searchpath", QString::fromStdString(modelFilePath.parent_path().parent_path()));
            settings.setValue(storedSearchPath, QString::fromStdString(modelFilePath));
        }
    }
}
