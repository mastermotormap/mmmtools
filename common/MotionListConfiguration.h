/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MOTIONLISTCONFIGURATION_H_
#define __MMM_MOTIONLISTCONFIGURATION_H_

#include <string>
#include <boost/shared_ptr.hpp>

namespace MMM
{

class MotionListConfiguration
{
public:
    MotionListConfiguration(int index, int newIndex, const std::string &motionName, bool ignoreMotion = false);

    int getIndex();

    int getNewIndex();

    std::string getMotionName();

    bool isIgnoreMotion();

private:
    int index;
    int newIndex;
    std::string motionName;
    bool ignoreMotion;
};

typedef std::shared_ptr<MotionListConfiguration> MotionListConfigurationPtr;

struct SortByNewIndexFunctor {
    bool operator()(const MMM::MotionListConfigurationPtr& configuration1, const MMM::MotionListConfigurationPtr& configuration2);
};

}

#endif // MOTIONLISTCONFIGURATION_H
