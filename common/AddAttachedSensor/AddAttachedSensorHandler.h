/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_AddAttachedSensorHandler_H_
#define __MMM_AddAttachedSensorHandler_H_

#include "../../MMMViewer/MMM/Viewer/MotionHandler.h"
#include "../../common/AddAttachedSensor/AddAttachedSensorDialog.h"
#include "../../common/AddAttachedSensor/AddAttachedSensorConfiguration.h"
#include <MMM/Motion/MotionRecording.h>

#include <QMessageBox>

namespace MMM
{

template <typename C, typename std::enable_if<std::is_base_of<BaseAddAttachedSensorConfiguration, C>::value>::type* = nullptr>
class AddAttachedSensorHandler : public MotionHandler
{

public:
    AddAttachedSensorHandler(QWidget* parent, const std::string &title) :
        MotionHandler(MotionHandlerType::ADD_SENSOR, title),
        dialog(0),
        widget(parent)
    {
    }

    void handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> currentRobots) {
        if (true) {
            dialog = new AddAttachedSensorDialog(std::shared_ptr<C>(new C()), widget);
            connect(dialog, SIGNAL(addVisualisation(SoSeparator*)), this, SIGNAL(addVisualisation(SoSeparator*)));
            //connect(dialog, SIGNAL(openMotions(MMM::MotionRecordingPtr)), this, SIGNAL(openMotions(MMM::MotionRecordingPtr)));
            connect(dialog, &AddAttachedSensorDialog::openMotions, this, &MotionHandler::openMotions);
        }
        if (motions && !motions->empty()) {
            MMM::MotionRecordingPtr calculatableMotions = motions->motionsWithModel();
            if (calculatableMotions->size() == 0) {
                QMessageBox* msgBox = new QMessageBox(widget);
                msgBox->setText("No calculatable motions found!");
                msgBox->exec();
            } else {
                dialog->addSensor(calculatableMotions, currentRobots);
            }
        }
        else MMM_ERROR << "Cannot open add imu sensor dialog, because no motions are present!" << std::endl;
    }


public slots:
    void timestepChanged(float timestep) {
        if (dialog) dialog->previewConfigurationFile();
    }

protected:
    std::string searchPath;
    AddAttachedSensorDialog* dialog;
    QWidget* widget;
};

}

#endif
