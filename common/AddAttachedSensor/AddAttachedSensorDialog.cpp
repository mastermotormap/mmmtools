#include "AddAttachedSensorDialog.h"
#include "ui_AddAttachedSensorDialog.h"


#include <VirtualRobot/RobotConfig.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>
#include <SimoxUtility/math/convert.h>
#include <MMM/Motion/MotionRecording.h>

#include <QCheckBox>
#include <QLineEdit>
#include <QMessageBox>
#include <QFileDialog>
#include <QSpinBox>
#include <set>
#include <regex>

AddAttachedSensorDialog::AddAttachedSensorDialog(MMM::BaseAddAttachedSensorConfigurationPtr configuration, QWidget* parent) :
    QDialog(parent),
    configuration(configuration),
    added(false),
    ui(new Ui::AddAttachedSensorDialog)
{
    ui->setupUi(this);

    connect(ui->LoadFileButton, SIGNAL(clicked()), this, SLOT(loadFile()));
    connect(ui->ChooseMotionBox, SIGNAL(currentTextChanged(QString)), this, SLOT(setCurrentMotion(QString)));
    connect(ui->PreviewSensorConfigButton, SIGNAL(clicked()), this, SLOT(previewButtonClicked()));
    connect(ui->LoadSensorConfigButton, SIGNAL(clicked()), this, SLOT(loadConfigurationFile()));
    connect(ui->SaveSensorConfigButton, SIGNAL(clicked()), this, SLOT(saveConfigurationFile()));
    connect(ui->ConvertButton, SIGNAL(clicked()), this, SLOT(convert()));
    connect(ui->AddButton, SIGNAL(clicked()), this, SLOT(add()));
    connect(ui->CancelButton, SIGNAL(clicked()), this, SLOT(hide()));
    connect(ui->ChooseMotionBox, SIGNAL(currentIndexChanged(int)), this, SLOT(previewConfigurationFile()));
    connect(ui->SegmentName, SIGNAL(currentIndexChanged(int)), this, SLOT(previewConfigurationFile()));
    connect(ui->xOffset, SIGNAL(valueChanged(double)), this, SLOT(previewConfigurationFile()));
    connect(ui->yOffset, SIGNAL(valueChanged(double)), this, SLOT(previewConfigurationFile()));
    connect(ui->zOffset, SIGNAL(valueChanged(double)), this, SLOT(previewConfigurationFile()));
    connect(ui->roll, SIGNAL(valueChanged(double)), this, SLOT(previewConfigurationFile()));
    connect(ui->pitch, SIGNAL(valueChanged(double)), this, SLOT(previewConfigurationFile()));
    connect(ui->yaw, SIGNAL(valueChanged(double)), this, SLOT(previewConfigurationFile()));
    connect(ui->IgnoreFirstLine, SIGNAL(stateChanged(int)), this, SLOT(preview()));
    connect(ui->FirstDelimiter, SIGNAL(textChanged(const QString&)), this, SLOT(preview()));
    connect(ui->ByRegex, SIGNAL(stateChanged(int)), this, SLOT(preview()));
    connect(ui->SecondDelimiter, SIGNAL(textChanged(const QString&)), this, SLOT(preview()));
    connect(ui->Regex, SIGNAL(textChanged(const QString&)), this, SLOT(preview()));
    connect(ui->Timestamp, SIGNAL(stateChanged(int)), this, SLOT(preview()));
    connect(ui->TimestampSpecifier, SIGNAL(textChanged(const QString&)), this, SLOT(preview()));


    bool ignoreFirstLine;
    std::string firstSplitChar;
    bool byRegex;
    std::string secondSplitChar;
    std::string regex;
    std::vector<int> indexes;
    configuration->loadConfiguration(ignoreFirstLine, firstSplitChar, byRegex, secondSplitChar, regex, indexes);

    if (ignoreFirstLine) ui->IgnoreFirstLine->setChecked(1);
    ui->FirstDelimiter->setText(QString::fromStdString(firstSplitChar));
    if (byRegex) ui->ByRegex->setChecked(1);
    else ui->ByDelimiter->setChecked(1);
    ui->SecondDelimiter->setText(QString::fromStdString(secondSplitChar));
    ui->Regex->setText(QString::fromStdString(regex));

    QGridLayout* indexGrid = new QGridLayout();
    ui->IndexGroupBox->setLayout(indexGrid);
    int index = 0;
    for(const std::string &name : configuration->getIndexNames()) {
        indexGrid->addWidget(new QLabel(QString::fromStdString(name)), index, 0);
        QSpinBox* spin = new QSpinBox();
        spin->setValue(indexes[index]);
        indexGrid->addWidget(spin, index, 1);
        indexSpinner.push_back(spin);
        connect(spin, SIGNAL(valueChanged(int)), this, SLOT(preview()));
        index++;
    }

    this->setWindowTitle(QString::fromStdString("Add " + configuration->getSensorName() + " sensor"));

    loadFile(settings.value(QString::fromStdString(configuration->getName() + "/sensorDataFilePath"), QString::fromStdString("")).toString().toStdString());

    preview();
}

AddAttachedSensorDialog::~AddAttachedSensorDialog() {
    delete ui;
}

bool AddAttachedSensorDialog::addSensor(MMM::MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> currentRobots) {
    loadMotions(motions);
    std::string configurationFilePath = settings.value(QString::fromStdString(configuration->getName() + "/sensorConfigurationFilePath"), QString::fromStdString("")).toString().toStdString();
    MMM_INFO << "Trying to load sensor configuration from " << configurationFilePath << std::endl;
    loadConfigurationFile(configurationFilePath);
    added = false;
    this->currentRobots = currentRobots;
    show();
    return added;
}

void AddAttachedSensorDialog::loadFile() {
    std::string sensorFilePath = QFileDialog::getOpenFileName(this, tr("Load sensor file"), QString::fromStdString(this->dataFilePath), tr("Sensor file (*.txt *.xml *.csv)")).toStdString();
    loadFile(sensorFilePath);
}

void AddAttachedSensorDialog::loadFile(const std::string &dataFilePath) {
    if (dataFilePath.empty()) return;

    ui->PlainSensorFilePreview->clear();
    sensorFile.clear();
    std::ifstream ifs(dataFilePath);
    sensorFile.assign(std::istreambuf_iterator<char>(ifs), std::istreambuf_iterator<char>());
    if (sensorFile.empty()) return;
    ui->ConvertButton->setEnabled(true);
    ui->PlainSensorFilePreview->appendPlainText(sensorFile.c_str());
    ui->PlainSensorFilePreview->setLineWrapMode(QPlainTextEdit::NoWrap);
    ui->PlainSensorFilePreview->moveCursor(QTextCursor::Start);
    ui->SensorFilePath->setText(QString::fromStdString(dataFilePath));
    ui->SensorFilePath->setToolTip(QString::fromStdString(dataFilePath));
    settings.setValue(QString::fromStdString(configuration->getName() + "/sensorDataFilePath"), QString::fromStdString(dataFilePath));
    this->dataFilePath = dataFilePath;
}

void AddAttachedSensorDialog::previewButtonClicked() {
    if (ui->PreviewSensorConfigButton->text().toStdString() == "Preview") {
        previewConfigurationFile();
        ui->PreviewSensorConfigButton->setText(QString::fromStdString("Dismiss"));
    }
    else {
        ui->PreviewSensorConfigButton->setText(QString::fromStdString("Preview"));
        emit addVisualisation(0);
    }
}

void AddAttachedSensorDialog::previewConfigurationFile() {
    if (currentRobots.size() > 0) {
        Eigen::Matrix4f transformationMatrix = getTransformationMatrix();
        std::string segment = ui->SegmentName->currentText().toStdString();
        std::string motionName = ui->ChooseMotionBox->currentText().toStdString();
        std::string offsetUnit = ui->OffsetUnit->currentText().toStdString();

        if (segment.empty() || motionName.empty()) return;

        auto segmentNode = currentRobots[motionName]->getRobotNode(segment);
        Eigen::Matrix4f matrix = segmentNode->getGlobalPose() * transformationMatrix;

        SoSeparator* sep = new SoSeparator();
        sep->addChild(VirtualRobot::CoinVisualizationFactory::getMatrixTransform(matrix));
        sep->addChild(VirtualRobot::CoinVisualizationFactory::CreateCoordSystemVisualization());

        emit addVisualisation(sep);
    }
}

void AddAttachedSensorDialog::loadConfigurationFile() {
    std::string sensorConfigurationFilePath = QFileDialog::getOpenFileName(this, tr("Load sensor configuration file"), QString::fromStdString(this->configurationFilePath), tr("IMU configuration file (*.xml)")).toStdString();
    loadConfigurationFile(sensorConfigurationFilePath);
}

void AddAttachedSensorDialog::loadConfigurationFile(const std::string &configurationFilePath) {
    if (configurationFilePath.empty()) return;

    try {
        simox::xml::RapidXMLWrapperNodePtr configuration = simox::xml::RapidXMLWrapperRootNode::FromFile(configurationFilePath);
        MMM::IAttachedSensorPtr attachedSensor = this->configuration->loadSensorConfiguration(configuration, configurationFilePath);
        bool segmentNameFound = false;
        for (int i = 0; i < ui->SegmentName->count(); i++) {
            if (ui->SegmentName->itemText(i).toStdString() == attachedSensor->getSegment()) {
                ui->SegmentName->setCurrentIndex(i);
                segmentNameFound = true;
                break;
            }
        }
        if (!segmentNameFound) {
            error("No segment with name " + attachedSensor->getSegment() + " found in model of motion " + currentMotion->getName());
            return;
        }
        for (int i = 0; i < ui->OffsetUnit->count(); i++) {
            if (ui->OffsetUnit->itemText(i).toStdString() == attachedSensor->getOffsetUnit()) {
                ui->OffsetUnit->setCurrentIndex(i);
                break;
            }
        }
        Eigen::Matrix4f transformationMatrix = attachedSensor->getOffset();
        Eigen::Vector3f xyz = simox::math::mat4f_to_pos(transformationMatrix);
        Eigen::Vector3f rpy = simox::math::mat4f_to_rpy(transformationMatrix);

        ui->xOffset->setValue(xyz(0));
        ui->yOffset->setValue(xyz(1));
        ui->zOffset->setValue(xyz(2));
        ui->roll->setValue(rpy(0));
        ui->pitch->setValue(rpy(1));
        ui->yaw->setValue(rpy(2));


        settings.setValue(QString::fromStdString(this->configuration->getName() + "/sensorConfigurationFilePath"), QString::fromStdString(configurationFilePath));
        this->configurationFilePath = configurationFilePath;
    } catch (MMM::Exception::MMMFormatException &e) {
        error(e.what());
    }
}

void AddAttachedSensorDialog::saveConfigurationFile() {
    std::string sensorConfigurationFilePath = QFileDialog::getSaveFileName(this, tr("Save sensor configuration file"), QString::fromStdString("imuConfiguration.xml"), tr("IMU configuration file (*.xml)")).toStdString();
    if (!sensorConfigurationFilePath.empty()) {
        try {
            Eigen::Matrix4f offset = getTransformationMatrix();
            std::string segment = ui->SegmentName->currentText().toStdString();
            std::string offsetUnit = ui->OffsetUnit->currentText().toStdString();
            configuration->setConfiguration(segment, offset, offsetUnit);
            configuration->getAttachedSensor()->saveSensorConfiguration(sensorConfigurationFilePath);
        } catch(MMM::Exception::MMMException &e) {
            error(e.what());
            return;
        }
    }
}

Eigen::Matrix4f AddAttachedSensorDialog::getTransformationMatrix() {
    return simox::math::pos_rpy_to_mat4f(ui->xOffset->value(), ui->yOffset->value(), ui->zOffset->value(),
                                         ui->roll->value(), ui->pitch->value(), ui->yaw->value());
}

void AddAttachedSensorDialog::setSensorConfiguration() {
    try {
        Eigen::Matrix4f offset = getTransformationMatrix();
        std::string segment = ui->SegmentName->currentText().toStdString();
        std::string offsetUnit = ui->OffsetUnit->currentText().toStdString();
        std::string uniqueName = ui->SensorName->text().toStdString();
        std::string description = ui->SensorDescription->text().toStdString();
        configuration->setConfiguration(segment, offset, offsetUnit, description, uniqueName);
    } catch(MMM::Exception::MMMException &e) {
        MMM_ERROR << e.what() << std::endl;
        error(e.what());
    }
}

void AddAttachedSensorDialog::preview() {
    if (dataFilePath.empty()) return;
    ui->ConvertedFilePreview->clear();
    std::vector<int> indexes;
    for (QSpinBox* spin : indexSpinner) {
        indexes.push_back(spin->value());
    }
    std::string previewText;
    try {
        previewText = configuration->getPreview(sensorFile, ui->IgnoreFirstLine->isChecked(), ui->FirstDelimiter->text().toStdString(), ui->ByRegex->isChecked(), ui->SecondDelimiter->text().toStdString(), ui->Regex->text().toStdString(), indexes, ui->Timestamp->isChecked() ? ui->TimestampSpecifier->text().toStdString() : std::string());
    } catch(MMM::Exception::MMMException &e) {
        previewText = e.what();
    }
    ui->AddButton->setEnabled(false);
    ui->ConvertedFilePreview->setLineWrapMode(QPlainTextEdit::NoWrap);
    ui->ConvertedFilePreview->appendPlainText(previewText.c_str());
    ui->ConvertedFilePreview->moveCursor(QTextCursor::Start);
}

void AddAttachedSensorDialog::convert() {
    ui->ConvertedFilePreview->clear();
    std::vector<int> indexes;
    for (QSpinBox* spin : indexSpinner) {
        indexes.push_back(spin->value());
    }
    setSensorConfiguration();
    try {
        std::string previewText = configuration->addSensorMeasurements(sensorFile, ui->IgnoreFirstLine->isChecked(), ui->FirstDelimiter->text().toStdString(), ui->ByRegex->isChecked(), ui->SecondDelimiter->text().toStdString(), ui->Regex->text().toStdString(), indexes, ui->Timestamp->isChecked() ? ui->TimestampSpecifier->text().toStdString() : std::string());
        ui->AddButton->setEnabled(true);
        ui->ConvertedFilePreview->setLineWrapMode(QPlainTextEdit::NoWrap);
        ui->ConvertedFilePreview->appendPlainText(QString::fromStdString(previewText));
    } catch(MMM::Exception::MMMException &e) {
        error(e.what());
        ui->AddButton->setEnabled(false);
    }
}

void AddAttachedSensorDialog::add() {
    try {
        currentMotion->addSensor(configuration->getSensor(), ui->TimestepDelta->value());
        QMessageBox* msgBox = new QMessageBox(this);
        msgBox->setText(QString::fromStdString(configuration->getSensorName() + " sensor added to motion " + currentMotion->getName()));
        msgBox->exec();
        ui->AddButton->setEnabled(false);
        emit openMotions(motions);
    } catch(MMM::Exception::MMMException &e) {
        error(e.what());
    }
}

void AddAttachedSensorDialog::loadMotions(MMM::MotionRecordingPtr motions) {
    ui->ChooseMotionBox->clear();
    currentMotion = nullptr;
    this->motions = motions;

    for (const std::string &name : motions->getMotionNames()) {
        ui->ChooseMotionBox->addItem(QString::fromStdString(name));
    }
    setCurrentMotion(QString::fromStdString(motionName));
    if (!currentMotion) setCurrentMotion(motions->getFirstMotion());
}

void AddAttachedSensorDialog::setCurrentMotion(QString name) {
    setCurrentMotion(motions->getMotion(name.toStdString()));
}

void AddAttachedSensorDialog::setCurrentMotion(MMM::MotionPtr motion) {
    currentMotion = motion;
    if (currentMotion) {
        motionName = currentMotion->getName();

        VirtualRobot::RobotPtr robot = currentMotion->getModel();

        ui->SegmentName->clear();

        for (std::string robotNodeNames : robot->getRobotNodeNames()) {
            ui->SegmentName->addItem(QString::fromStdString(robotNodeNames));
        }
    }
}

void AddAttachedSensorDialog::error(const std::string &errorMsg) {
    MMM_ERROR << errorMsg << std::endl;
    QMessageBox* msgBox = new QMessageBox(this);
    msgBox->setText(QString::fromStdString(errorMsg));
    msgBox->exec();
}
