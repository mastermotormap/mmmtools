#include "AddDataGloveHandler.h"
#include "AddDataGloveHandlerDialog.h"
#include <QFileDialog>
#include <QMessageBox>
#include <MMM/Motion/MotionRecording.h>

using namespace MMM;

AddDataGloveHandler::AddDataGloveHandler(QWidget* widget) :
    MotionHandler(MotionHandlerType::ADD_SENSOR, "Add kinematic sensor from data glove"),
    searchPath(std::string(MMMTools_SRC_DIR)),
    widget(widget)
{
}

void AddDataGloveHandler::handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> /*currentRobots*/) {
    if (motions && !motions->empty()) {
        AddDataGloveHandlerDialog* dialog = new AddDataGloveHandlerDialog(widget, motions);
        if (dialog->convertMotion()) emit openMotions(motions);
    }
    else MMM_ERROR << "Cannot open add data glove dialog, because no motions are present!" << std::endl;
}

std::string AddDataGloveHandler::getName() {
    return NAME;
}
