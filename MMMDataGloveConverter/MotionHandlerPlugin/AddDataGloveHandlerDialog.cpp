#include "AddDataGloveHandlerDialog.h"
#include "ui_AddDataGloveHandlerDialog.h"

#include <MMM/Exceptions.h>
#include <MMM/Motion/MotionRecording.h>
#include <MMM/Model/ModelReaderXML.h>
#include <QFileDialog>
#include <QCheckBox>
#include <QMessageBox>
#include <set>

AddDataGloveHandlerDialog::AddDataGloveHandlerDialog(QWidget* parent, MMM::MotionRecordingPtr motions) :
    QDialog(parent),
    ui(new Ui::AddDataGloveHandlerDialog),
    motions(motions),
    currentMotion(nullptr),
    motionConverted(false)
{
    ui->setupUi(this);

    loadConfiguration(settings.value("datagloveconverter/configfilepath", "").toString().toStdString());
    loadData(settings.value("datagloveconverter/datafilepath", "").toString().toStdString());

    connect(ui->ChooseMotionComboBox, SIGNAL(currentTextChanged(const QString&)), this, SLOT(setCurrentMotion(const QString&)));
    connect(ui->DataButton, SIGNAL(clicked()), this, SLOT(loadData()));
    connect(ui->ConfigButton, SIGNAL(clicked()), this, SLOT(loadConfiguration()));
    connect(ui->ConvertButton, SIGNAL(clicked()), this, SLOT(convert()));
    connect(ui->CancelButton, SIGNAL(clicked()), this, SLOT(close()));

    loadMotions();
    enableConvertButton();
}

AddDataGloveHandlerDialog::~AddDataGloveHandlerDialog() {
    delete ui;
}

bool AddDataGloveHandlerDialog::convertMotion() {
    exec();
    return motionConverted;
}

void AddDataGloveHandlerDialog::loadMotions() {
    for (const std::string &name : motions->getMotionNames()) {
        auto motion = motions->getMotion(name);
        if (!currentMotion) currentMotion = motion;
        ui->ChooseMotionComboBox->addItem(QString::fromStdString(motion->getName()));
    };
}

void AddDataGloveHandlerDialog::setCurrentMotion(const QString &name) {
    currentMotion = motions->getMotion(name.toStdString());
    if (!currentMotion->getModel()) {
        MMM_WARNING << "Current motion does not contain a model. Some functions are therefore not available for this sensor until a model is added." << std::endl;
    }
}

void AddDataGloveHandlerDialog::loadConfiguration() {
    std::filesystem::path converterConfigFilePath = QFileDialog::getOpenFileName(this, tr("Load data glove converter configuration"), settings.value("datagloveconverter/configfilepath", "").toString(), tr("XML files (*.xml)")).toStdString();
    loadConfiguration(converterConfigFilePath);
}

void AddDataGloveHandlerDialog::loadConfiguration(const std::filesystem::path &converterConfigFilePath) {
    if (!converterConfigFilePath.empty()) {
        try {
            converter = MMM::DataGloveConverterPtr(new MMM::DataGloveConverter(converterConfigFilePath));
        }
        catch (MMM::Exception::MMMException &e) {
            QMessageBox* msgBox = new QMessageBox(this);
            std::string error_message = "Could not load data glove configuration file from " + converterConfigFilePath.generic_string() + "! " + e.what();
            msgBox->setText(QString::fromStdString(error_message));
            MMM_ERROR << error_message << std::endl;
            msgBox->exec();
            return;
        }
        setConfigLabel(converterConfigFilePath);
        settings.setValue("datagloveconverter/configfilepath", QString::fromStdString(converterConfigFilePath));
        enableConvertButton();
    }
}

void AddDataGloveHandlerDialog::setConfigLabel(const std::filesystem::path &filePath) {
    QString label = QString::fromStdString(filePath.filename());
    ui->ConfigLabel->setText(label);
    ui->ConfigLabel->setToolTip(label);
}

void AddDataGloveHandlerDialog::loadData() {
    std::string converterDataFilePath = QFileDialog::getOpenFileName(this, tr("Load data glove data"), settings.value("datagloveconverter/datafilepath", "").toString()).toStdString();
    loadData(converterDataFilePath);
}

void AddDataGloveHandlerDialog::loadData(const std::filesystem::path &converterDataFilePath) {
    if (!converterDataFilePath.empty()) {
        this->converterDataFilePath = converterDataFilePath;
        setDataLabel(converterDataFilePath);
        settings.setValue("datagloveconverter/datafilepath", QString::fromStdString(converterDataFilePath));
        enableConvertButton();
    }
}

void AddDataGloveHandlerDialog::setDataLabel(const std::filesystem::path &filePath) {
    QString label = QString::fromStdString(filePath.filename());
    ui->DataLabel->setText(label);
    ui->DataLabel->setToolTip(label);
}

void AddDataGloveHandlerDialog::enableConvertButton() {
    if (currentMotion && converter && !converterDataFilePath.empty()) ui->ConvertButton->setEnabled(true);
}

void AddDataGloveHandlerDialog::convert() {
    try {
        currentMotion->addSensor(converter->convert(converterDataFilePath), ui->TimestepDeltaSpinBox->value(), currentMotion->getModel() != nullptr);
        motionConverted = true;
        this->close();
    }
    catch (MMM::Exception::MMMException &e) {
        QMessageBox* msgBox = new QMessageBox(this);
        std::string error_message = std::string("Could not convert data glove data! ") + e.what();
        msgBox->setText(QString::fromStdString(error_message));
        MMM_ERROR << error_message << std::endl;
        msgBox->exec();
    }
}
