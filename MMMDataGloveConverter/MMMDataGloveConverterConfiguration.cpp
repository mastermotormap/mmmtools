#include <VirtualRobot/RuntimeEnvironment.h>
#include <MMM/MMMCore.h>
#include <SimoxUtility/algorithm/string.h>

#include "MMMDataGloveConverterConfiguration.h"

MMMDataGloveConverterConfiguration::MMMDataGloveConverterConfiguration() : ApplicationBaseConfiguration()
{
}

bool MMMDataGloveConverterConfiguration::processCommandLine(int argc, char *argv[])
{
    VirtualRobot::RuntimeEnvironment::considerKey("mode");
    VirtualRobot::RuntimeEnvironment::considerKey("inputMotion");
    VirtualRobot::RuntimeEnvironment::considerKey("motionName");
    VirtualRobot::RuntimeEnvironment::considerKey("timestepDelta");
    VirtualRobot::RuntimeEnvironment::considerKey("sensorPlugins");

    VirtualRobot::RuntimeEnvironment::considerKey("data");
    VirtualRobot::RuntimeEnvironment::considerKey("config");

    VirtualRobot::RuntimeEnvironment::considerKey("outputMotion");
    VirtualRobot::RuntimeEnvironment::considerFlag("checkModel");
    VirtualRobot::RuntimeEnvironment::considerFlag("replace");
    VirtualRobot::RuntimeEnvironment::printOptions();

    VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);

    testOnly = getParameter("mode") == "test";

    inputMotionPath = getParameter("inputMotion", !testOnly, true);
    motionName = getParameter("motionName");
    sensorPluginPaths = getPaths("sensorPlugins");
    std::string timestepDeltaStr = getParameter("timestepDelta");
    if (!timestepDeltaStr.empty()) timestepDelta = simox::alg::to_<float>(timestepDeltaStr.c_str());
    else timestepDelta = 0.0f;

    dataGloveDataPath = getParameter("data", true, true);
    dataGloveConfigPath = getParameter("config", true, true);

    if (VirtualRobot::RuntimeEnvironment::hasFlag("checkModel")) checkModel = true;
    else checkModel = false;

    outputMotionPath = getParameter("outputMotion");
    if (outputMotionPath.empty()){
        std::string name = inputMotionPath.stem();
        outputMotionPath = inputMotionPath.parent_path().append(name + "_Output.xml");
        //outputMotionPath = "MMMDataGloveConverter_Output.xml";
    }
    replace = VirtualRobot::RuntimeEnvironment::hasFlag("replace");

    return valid;
}

void MMMDataGloveConverterConfiguration::print()
{
    MMM_INFO << "*** MMMDataGlove Configuration ***" << std::endl;
    std::cout << "Input file: " << inputMotionPath << std::endl;
    std::cout << "Output file: " << outputMotionPath << std::endl;
}
