#include <fstream>
#include <MMM/Exceptions.h>
#include <SimoxUtility/algorithm/string.h>

#include "DataGloveConverter.h"

using namespace MMM;

FingerMapping::FingerMapping(unsigned int index, const std::string &jointName, const std::string &description, float multiplyBy) :
    index(index),
    jointName(jointName),
    description(description),
    multiplyBy(multiplyBy)
{
}

std::vector<std::string> FingerMapping::getJointNames(const FingerMappingList &mappings) {
    std::vector<std::string> jointNames;
    for (auto mapping : mappings) {
        jointNames.push_back(mapping->jointName);
    }
    return jointNames;
}

float FingerMapping::getJointAngle(const std::vector<std::string> &values, int row) {
    if (index >= values.size()) throw Exception::MMMException("Index " + std::to_string(index) + " needs to be smaller than column size " + std::to_string(values.size()));
    std::string jointAngleStr = values[index];
    std::replace(jointAngleStr.begin(), jointAngleStr.end(), ',', '.');
    try {
        return simox::alg::to_<float>(jointAngleStr) * multiplyBy;
    }
    catch (simox::error::SimoxError &e) {
        throw MMM::Exception::MMMException(std::string(e.what()) + "'" + values[index] + "' is no valid float at row " + std::to_string(row) + " and column " + std::to_string(index) + "!");
    }
}

DataGloveConverter::DataGloveConverter(const std::string &dataGloveConfigPath) : separator(";"), hasHeader(false), useTimestepIndex(true) {
    loadConfig(dataGloveConfigPath);
}

/** @throws MMMException */
KinematicSensorPtr DataGloveConverter::convert(const std::string &dataGloveDataPath) {
    KinematicSensorPtr sensor(new KinematicSensor(FingerMapping::getJointNames(fingerMapping)));
    std::ifstream infile(dataGloveDataPath);
    if (infile.is_open()) {
        std::string line;
        if (hasHeader) std::getline(infile, line); // Ignore first line
        int count = 0; // used for Delta
        bool firstvalue = true;
        double offset_time = 0;
        while (std::getline(infile, line))
        {
            std::vector<std::string> values = simox::alg::split(line, separator);
            if (timestepIndex >= values.size()) throw Exception::MMMException("Index " + std::to_string(timestepIndex) + " needs to be smaller than column size " + std::to_string(values.size()));
            std::string timestepStr = values[timestepIndex];

            std::replace(timestepStr.begin(), timestepStr.end(), ',', '.');
            try {
                // glove system time to milliseconds
                std::vector<std::string> splittedTime;
                std::string delimiter = ":";
                size_t pos = 0;
                while ((pos = timestepStr.find(delimiter)) != std::string::npos) {
                    splittedTime.push_back(timestepStr.substr(0, pos));
                    timestepStr.erase(0, pos + delimiter.length());
                }
                splittedTime.push_back(timestepStr.substr(0, 1));
                float timeInMs = (simox::alg::to_<int>(splittedTime.at(0)) * 60 * 60 * 1000 + simox::alg::to_<int>(splittedTime.at(1)) * 60 * 1000 + simox::alg::to_<int>(splittedTime.at(2)) * 1000 + simox::alg::to_<int>(splittedTime.at(3)) * 1000/30  + simox::alg::to_<int>(splittedTime.at(4)) * 1000/90);
                float timestep = useTimestepIndex ? timeInMs/1000//XML::convertTo<float>(timestepStr, "'" + values[timestepIndex] + " is no valid float for time at row " + std::to_string(count + hasHeader) + " and column " + std::to_string(timestepIndex) + "!")
                                                  : ++count * timestepDelta;
                if (useTimestepIndex && firstvalue){
                    offset_time = timestep;
                    firstvalue = false;
                }
                if(useTimestepIndex){
                    timestep = timestep- offset_time;
                }

                Eigen::VectorXf jointAngles(fingerMapping.size());
                int i = 0;
                for (auto mapping : fingerMapping) {
                    jointAngles(i++) = mapping->getJointAngle(values, count + hasHeader);
                }

                KinematicSensorMeasurementPtr sensorMeasurement(new KinematicSensorMeasurement(timestep, jointAngles));
                sensor->addSensorMeasurement(sensorMeasurement);
            }
            catch (simox::error::SimoxError &e) {
                throw MMM::Exception::MMMException(std::string(e.what()) + "'" + values[timestepIndex] + " is no valid float for time at row " + std::to_string(count + hasHeader) + " and column " + std::to_string(timestepIndex) + "!");
            }
        }
    }
    else throw MMM::Exception::MMMException(dataGloveDataPath + " could not be opened!");

    if (sensor->getTimesteps().size() == 0) throw MMM::Exception::MMMException("No measurements were added!");

    return sensor;
}

void DataGloveConverter::loadConfig(const std::string &dataGloveConfigPath) {
    simox::xml::RapidXMLWrapperRootNodePtr rootNode = simox::xml::RapidXMLWrapperRootNode::FromFile(dataGloveConfigPath);
    if (rootNode->has_node("DataGloveFileConfig")) {
        simox::xml::RapidXMLWrapperNodePtr dataFileConfigNode = rootNode->first_node("DataGloveFileConfig");
        if (dataFileConfigNode->has_attribute("separator")) {
            separator = dataFileConfigNode->attribute_value("separator");
        }
        if (dataFileConfigNode->has_attribute("hasHeader")) {
            std::string hasHeaderStr = dataFileConfigNode->attribute_value("hasHeader");
            try {
                hasHeader = simox::alg::to_<bool>(hasHeaderStr);
            }
            catch(simox::error::SimoxError &e) {
                throw Exception::MMMException(std::string(e.what()) + " The attribute value of 'hasHeader' needs to be a valid bool not '" + hasHeaderStr + "'");
            }
        }
    }
    simox::xml::RapidXMLWrapperNodePtr timestepNode = rootNode->first_node("Timestep");
    std::string timestepType = simox::alg::to_lower(timestepNode->attribute_value("type").c_str());
    std::string timestepValue = timestepNode->attribute_value("value");
    if (timestepType == "byindex") {
        try {
            timestepIndex = simox::alg::to_<unsigned int>(timestepValue);
        }
        catch(simox::error::SimoxError &e) {
            throw Exception::MMMException(std::string(e.what()) + " If the type 'ByIndex' is used, the value attribute need to be a valid unsigned integer!");
        }
        useTimestepIndex = true;
    } else if (timestepType == "bydelta") {
        try {
            timestepDelta = simox::alg::to_<float>(timestepValue);
            timestepIndex = 0;
        }
        catch(simox::error::SimoxError &e) {
            throw Exception::MMMException(std::string(e.what()) + " If the type 'ByDelta' is used, the value attribute need to be a valid float!");
        }
        useTimestepIndex = false;
    }
    else throw Exception::MMMException("'" + timestepType + "' is no valid timestep type attribute. Use 'ByIndex' or 'ByDelta'!");
    std::set<std::string> jointNames;
    for (simox::xml::RapidXMLWrapperNodePtr mappingNode : rootNode->first_node("FingerMapping")->nodes("Mapping")) {
        float multiplyBy = 1.0f;
        std::string jointName = mappingNode->attribute_value("jointName");
        if (jointNames.find(jointName) != jointNames.end())
            throw Exception::MMMException("Duplicate of mapping with jointName '" + jointName + "'");
        if (mappingNode->has_attribute("multiplyBy"))
        try {
            multiplyBy = simox::alg::to_<float>(mappingNode->attribute_value("multiplyBy"));
        }
        catch(simox::error::SimoxError &e) {
            throw Exception::MMMException(std::string(e.what()) + "The attribute value of 'multiplyBy' on mapping with joint name '" + jointName +"' need to be a valid float!");
        }
        try {
            fingerMapping.push_back(FingerMappingPtr(new FingerMapping(simox::alg::to_<unsigned int>(mappingNode->attribute_value("index")),
                                                                       jointName,
                                                                       mappingNode->attribute_value_or_default("description", ""),
                                                                       multiplyBy)));
        }
        catch(simox::error::SimoxError &e) {
            throw Exception::MMMException(std::string(e.what()) + " Index attribute for jointName " + jointName + " no valid unsigned integer!");
        }
        jointNames.insert(jointName);
    }
}
