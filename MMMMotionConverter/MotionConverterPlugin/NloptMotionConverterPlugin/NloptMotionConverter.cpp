/*k
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMMTools
* @author     Christian Mandery
* @copyright  2015 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/


#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>
#include <MMM/Motion/Plugin/MoCapMarkerPlugin/MoCapMarkerSensor.h>
#include <MMM/Exceptions.h>
#include <MMM/Motion/XMLTools.h>

#include "FramewiseLocalStrategy.h"
#include "MinimumJerkStrategy.h"
#include "GradientSmoothStrategy.h"

#include <map>

#include "NloptMotionConverter.h"

using namespace MMM;

NloptMotionConverter::NloptMotionConverter(MotionPtr inputMotion, ModelPtr outputModel, ModelPtr outputModelUnprocessed, ModelProcessorPtr
                                           outputModelProcessor, const std::string &configFile, const std::string &convertedMotionName,
                                           OptimizationStrategy optimizationStrategy)
    :   MotionConverter(inputMotion, outputModel, outputModelUnprocessed, outputModelProcessor, configFile, convertedMotionName),
        optimizationStrategy(optimizationStrategy),
        markerScaleFactor(1.0f),
        targetModelSize(1.0f)
{
    if (!inputMotion || !outputModel) throw Exception::MMMException("Input motion and output model should not be null");

    try {
        readConverterConfig();
    } catch (Exception::MMMException &e) {
        throw Exception::MMMException("Exception while reading nlopt converter config file! " + std::string(e.what()));
    }
}

void NloptMotionConverter::readConverterConfig() {
    MMM_INFO << "Reading XML converter config." << std::endl;

    simox::xml::RapidXMLWrapperNodePtr configNode = simox::xml::RapidXMLWrapperRootNode::FromFile(configFile);

    std::string type = configNode->attribute_value(xml::attribute::TYPE);
    if (type != NAME) throw Exception::MMMException("ConverterConfig type: Expecting " + std::string(NAME) + ", got " + type);

    // Read Model Node
    simox::xml::RapidXMLWrapperNodePtr modelNode = configNode->first_node("Model");
    if (outputModelProcessor) targetModelSize = outputModel->getScaling();
    else if (modelNode->has_attribute("size")) targetModelSize = modelNode->attribute_value_<float>("size");

    // Read Marker Mapping Node
    simox::xml::RapidXMLWrapperNodePtr markerMappingNode = modelNode->first_node("MarkerMapping");
    for (simox::xml::RapidXMLWrapperNodePtr mappingNode : markerMappingNode->nodes("Mapping")) {
        std::string inputMarkerName;
        if (mappingNode->has_attribute("c3d")) inputMarkerName = mappingNode->attribute_value("c3d"); // old term
        else inputMarkerName = mappingNode->attribute_value("source");
        std::string outputMarkerName;
        if (mappingNode->has_attribute("mmm")) outputMarkerName = mappingNode->attribute_value("mmm"); // old term
        else outputMarkerName = mappingNode->attribute_value("target");
        if (inputMarkerName.empty() || outputMarkerName.empty()) throw Exception::MMMException("Empty Marker names not allowed!");
        for (const auto &mapping : markerMapping) {
            if (mapping.first == inputMarkerName) throw Exception::MMMException("Mapping: Input marker name " + inputMarkerName + " is mapped twice!");
            else if (mapping.second == outputMarkerName) throw Exception::MMMException("Mapping: Output marker name " + outputMarkerName + " is mapped twice!");
        }
        if (!outputModel->getSensor(outputMarkerName)) throw Exception::MMMException("Mapping: Output marker name " + outputMarkerName + " is invalid!");
        markerMapping[inputMarkerName] = outputMarkerName;
        float markerWeight = 1.0f;
        if (mappingNode->has_attribute("weight")) markerWeight = simox::alg::to_<float>(mappingNode->attribute_value("weight"));
            markerWeights[inputMarkerName] = markerWeight;
    }
    // TODO check inputMarkerName in inputMotion/Model?

    // Read Joint Set Node
    if (modelNode->has_node("JointSet")) {
        simox::xml::RapidXMLWrapperNodePtr jointSetNode = modelNode->first_node("JointSet");
        for (simox::xml::RapidXMLWrapperNodePtr jointNode : jointSetNode->nodes("Joint")) {
            std::string jointName = jointNode->attribute_value(xml::attribute::NAME);
            joints.push_back(jointName);
        }
    }

    // TODO OptimizationNode
}

void NloptMotionConverter::cancel() {
    if (strategy) strategy->cancel();
}

float NloptMotionConverter::getCurrentTimestep() {
    return strategy ? strategy->getCurrentTimestep() : 0;
}

MotionPtr NloptMotionConverter::convertMotion() {
    MMM_INFO << "Converting motion" << std::endl;

    // Create resulting output motion
    MotionPtr outputMotion(new Motion(convertedMotionName, outputModelUnprocessed, outputModel, outputModelProcessor));

    std::map<float, std::map<std::string, Eigen::Vector3f> > labeledMarkerData = getLabeledMarkerData();
//    std::map<float, std::map<std::string, Eigen::Vector3f> > _10framesData;

//    for(auto & d : labeledMarkerData)
//    {
//        _10framesData[d.first] = d.second;
//        if(_10framesData.size() >= 30)
//            break;
//    }
//    labeledMarkerData = _10framesData;
    // Use concrete Strategy
    KinematicSensorList additionalKinSensors = inputMotion->getSensorsByType<KinematicSensor>(); // additional kinematic sensors, e.g. from cyberglove, that can be used for conversion
    auto it = additionalKinSensors.begin();
    while (it != additionalKinSensors.end()) {
        bool erased = false;
        for (const auto &jointName : joints) {
            if ((*it)->hasJoint(jointName)) {
                it = additionalKinSensors.erase(it);
                erased = true;
                break;
            }
        }
        if (!erased) it++;
    }

    if (!logFilePath.empty()) {
        std::ofstream ofs(logFilePath, std::ofstream::out);
        if (ofs.is_open()) {
            ofs << "Converter: NloptMotionConverter" << std::endl;
            ofs << "Optimization strategy: " << (optimizationStrategy == OptimizationStrategy::FRAMEWISE_LOCAL ? "Framewise" : "MinimumJerk") << std::endl;
            ofs << "Configuration file: " << simox::xml::RapidXMLWrapperRootNode::FromFile(configFile)->print(false) << std::endl;
            ofs << "Target model: " << simox::xml::RapidXMLWrapperRootNode::FromFile(outputModel->getFilename())->print(false) << std::endl;
            ofs.close();
        }
    }

    switch (optimizationStrategy) {
    case FRAMEWISE_LOCAL:
        strategy = FramewiseLocalStrategyPtr(new FramewiseLocalStrategy(labeledMarkerData, outputMotion, outputModel, joints, markerMapping, markerWeights, additionalKinSensors));
        strategy->openLogFile(logFilePath);
        break;
    case GRADIENT_SMOOTH:
        strategy = std::make_shared<GradientSmoothStrategy>(labeledMarkerData, outputMotion, outputModel, joints, markerMapping, markerWeights, additionalKinSensors);
        strategy->openLogFile(logFilePath);
        break;
    case WHOLE_MOTION:
        MotionPtr localOutputMotion(new Motion(convertedMotionName, outputModelUnprocessed, outputModel, outputModelProcessor));

        strategy = FramewiseLocalStrategyPtr(new FramewiseLocalStrategy(labeledMarkerData, localOutputMotion, outputModel, joints, markerMapping, markerWeights, additionalKinSensors));
        strategy->openLogFile(logFilePath);
        strategy->convert();
        strategy->closeLogFile();

        auto globalStrategy = MinimumJerkStrategyPtr(new MinimumJerkStrategy(labeledMarkerData, outputMotion, outputModel, joints, markerMapping, markerWeights, additionalKinSensors));
        globalStrategy->setInputMotion(localOutputMotion);
        globalStrategy->openLogFile(logFilePath);
        strategy = globalStrategy;
    }
    strategy->convert();
    strategy->closeLogFile();

    // Add other Sensors
    if (isConvertFromModel()) {
        // Add only MoCapMarker sensor as others such as center of mass or imu depend on model
        SensorPtr mocapSensor = inputMotion->getSensorByType(MoCapMarkerSensor::TYPE);
        if (mocapSensor) outputMotion->addSensor(mocapSensor);
    }
    else {
        for (const auto &s : inputMotion->getSensorData()) {
            SensorPtr sensor = s.second;
            if (sensor->getType() != ModelPoseSensor::TYPE) {
                bool addSensor = true;
                if (sensor->getType() == KinematicSensor::TYPE && inputMotion->getName() == convertedMotionName) {
                    auto kinematicSensor = std::dynamic_pointer_cast<KinematicSensor>(sensor);
                    for (const auto &jointName : joints) {
                        if (kinematicSensor->hasJoint(jointName)) {
                            MMM_INFO << "Did not add kinematic sensor " << sensor->getUniqueName() << " to converted motion " << std::endl;
                            addSensor = false; // do not add kinematic sensor as he is newly created
                            break;
                        }
                    }
                }
                if (addSensor) outputMotion->addSensor(sensor); // exception should hopefully not occure!
            }
        }
    }

    return outputMotion;
}

bool NloptMotionConverter::isConvertFromModel() {
    return inputMotion->getModel() && inputMotion->hasSensor(ModelPoseSensor::TYPE) &&
            inputMotion->getSensorByType(ModelPoseSensor::TYPE)->getTimesteps().size() > 1 && inputMotion->getName() != convertedMotionName;
}

std::map<float, std::map<std::string, Eigen::Vector3f> > NloptMotionConverter::getLabeledMarkerData() {
    labeledMarkerData = {};
    ModelPtr inputModel = inputMotion->getModel();
    if (isConvertFromModel()) {
        MMM_INFO << "Using motion's kinematic data." << std::endl;

        VirtualRobot::RobotPtr inputRobot = inputModel->cloneScaling();
        inputRobot->setUpdateCollisionModel(false);
        inputRobot->setUpdateVisualization(false);
        inputRobot->setupVisualization(false, false);
        inputRobot->setThreadsafe(false);

        markerScaleFactor = targetModelSize / inputModel->getScaling();
        MMM_INFO << "Marker scale factor is " << markerScaleFactor << "." << std::endl;

        // Retrieve labeled marker data from modelPose and joints
        ModelPoseSensorPtr modelPoseSensor = inputMotion->getSensorByType<ModelPoseSensor>();
        std::vector<float> timesteps = modelPoseSensor->getTimesteps();

        KinematicSensorPtr kinematicSensor = KinematicSensor::join(inputMotion->getSensorsByType<KinematicSensor>(), timesteps);
        const std::vector<std::string>& jointNames = kinematicSensor->getJointNames();

        for (auto timestep : timesteps)
        {
            ModelPoseSensorMeasurementPtr modelPoseSensorMeasurement = modelPoseSensor->getDerivedMeasurement(timestep);
            KinematicSensorMeasurementPtr kinematicSensorMeasurement = kinematicSensor->getDerivedMeasurement(timestep);
            if (!kinematicSensorMeasurement) continue;

            inputRobot->setGlobalPose(modelPoseSensorMeasurement->getRootPose());

            for (unsigned int i = 0; i < jointNames.size(); ++i) {
                inputRobot->setJointValue(jointNames[i], kinematicSensorMeasurement->getJointAngles()[i]);
            }

            std::map<std::string, Eigen::Vector3f> labeledMarker;

            for (auto it = markerMapping.begin(); it != markerMapping.end(); ) {
                const std::string& inputMarker = it->first;
                VirtualRobot::SensorPtr sensor = inputRobot->getSensor(inputMarker);
                if (sensor) {
                    labeledMarker[inputMarker] = sensor->getGlobalPose().block(0, 3, 3, 1) * markerScaleFactor;
                    it++;
                }
                else {
                    MMM_WARNING << "The robot for the input motion doesn't contain the following input marker '" + inputMarker + "'. The marker is ignored." << std::endl;
                    it = markerMapping.erase(it);
                }
            }
            if (markerMapping.empty()) {
                throw new Exception::MMMException("No valid marker in configuration!");
            }

            labeledMarkerData[timestep] = labeledMarker;
        }

    } else if (inputMotion->hasSensor(MoCapMarkerSensor::TYPE)){
        MMM_INFO << "Using motion's marker data." << std::endl;

        MoCapMarkerSensorPtr moCapMarkerSensor = inputMotion->getSensorByType<MoCapMarkerSensor>();
        labeledMarkerData = moCapMarkerSensor->getLabeledMarkerData();

        int missingMarker = 0;
        for (const auto &markerData : labeledMarkerData) {
            std::map<std::string, Eigen::Vector3f> m = markerData.second;
            for (auto it = markerMapping.begin(); it != markerMapping.end();) {
                if (m.find(it->first) == m.end()) {
                    MMM_WARNING << "Ignoring motion capture marker "<< it->first << ", because isn't contained in the motion (timestep: " << markerData.first << ")!" << std::endl;
                    it = markerMapping.erase(it);
                    missingMarker++;
                }
                else it++;
            }
        }
        if (missingMarker > 3) throw Exception::MMMException("Too much motion capture marker are missing in the motion!");
    }
    else throw Exception::MMMException("Either MoCapMarkerSensor or a Model + ModelPoseSensor is needed!");
    return labeledMarkerData;
}

