#include "GradientSmoothStrategy.h"

#include <queue>

#include <SimoxUtility/math/convert/quat_to_rpy.h>

using namespace MMM;

GradientSmoothStrategy::GradientSmoothStrategy(const std::map<float, std::map<std::string, Eigen::Vector3f> > &labeledMarkerData, MotionPtr outputMotion, ModelPtr outputModel, const std::vector<std::string> &joints, const std::map<std::string, std::string> &markerMapping, const std::map<std::string, float> &markerWeights, KinematicSensorList availableKinematicSensors) :
    ConvertingStrategy(labeledMarkerData, outputMotion, outputModel, joints, markerMapping, markerWeights, availableKinematicSensors),
    cancelled(false),
    state(nullptr),
    robotNodeSet(VirtualRobot::RobotNodeSet::createRobotNodeSet(outputRobot, "NLOPT", joints))
{
    frameDimension = 7 + joints.size();


    std::queue<VirtualRobot::RobotNodePtr> q;
    q.push(outputModel->getRootNode());
    while (!q.empty()) {
        const VirtualRobot::RobotNodePtr& node = q.back();
        q.pop();
        for (const VirtualRobot::SensorPtr& sensor : node->getSensors()) {
            const std::string name = sensor->getName();
            for (const auto &m : markerMapping) {
                if (m.second == name)
                    rootJointMarkers.push_back(m.first);
            }
        }
        for (const auto& childNode : node->getChildren()) {
            VirtualRobot::RobotNodePtr childRobotNode = std::dynamic_pointer_cast<VirtualRobot::RobotNode>(childNode);
            if (childRobotNode && childRobotNode->getType() != VirtualRobot::RobotNode::Joint) {
                q.push(childRobotNode);
            }
        }
    }
}

void GradientSmoothStrategy::cancel() {
    cancelled = true;
}

float GradientSmoothStrategy::getCurrentTimestep() {
    return currentTimestep;
}

Eigen::Matrix4f GradientSmoothStrategy::Registration3d(const Eigen::Ref<const Eigen::MatrixXf> A,
                                                       const Eigen::Ref<const Eigen::MatrixXf> B)
{
    Eigen::Vector3f centroid_A = A.block(0,0,A.rows(),3).colwise().mean();
    Eigen::Vector3f centroid_B = B.block(0,0,A.rows(),3).colwise().mean();

    Eigen::MatrixXf H = (A.block(0,0,A.rows(),3).rowwise() - centroid_A.transpose()).transpose() * (B.block(0,0,A.rows(),3).rowwise() - centroid_B.transpose());

    Eigen::JacobiSVD<Eigen::MatrixXf> svd(H, Eigen::ComputeThinU | Eigen::ComputeThinV);
    Eigen::Matrix3f U = svd.matrixU();
    Eigen::Matrix3f V = svd.matrixV();
    Eigen::Matrix3f R = V*U.transpose();

    if (R.determinant()<0) {
        V.col(2) *= -1;
        R = V*U.transpose();
    }

    Eigen::Vector3f t = -R*centroid_A+centroid_B;
    Eigen::Matrix4f T = Eigen::Matrix4f::Identity();
    T.block(0,0,3,3) = R;
    T.block(0,3,3,1) = t;

    return T;
}

Eigen::Matrix4f GradientSmoothStrategy::Registration3d(const std::map<std::string, Eigen::Vector3f>& markerPositions){
    Eigen::MatrixXf A(rootJointMarkers.size(), 3);
    Eigen::MatrixXf B(rootJointMarkers.size(), 3);
    int row=0;
    for (const auto &name : rootJointMarkers){
        if (inputMarkersToOutputRobotSensors.find(name) == inputMarkersToOutputRobotSensors.end()) {
            MMM_ERROR << "Unknown virtual marker " << name;
            return Eigen::Matrix4f::Identity();
        }
        Eigen::Vector3f estimatedMarkerPosition = markerPositions.at(name);
        Eigen::Vector3f virtualMarkerPosition = inputMarkersToOutputRobotSensors.at(name)->getGlobalPosition();
        A.row(row) = virtualMarkerPosition;
        B.row(row) = estimatedMarkerPosition;

        MMM_INFO << "Estimated marker position for " << name << " " << estimatedMarkerPosition;
        MMM_INFO << "Virtual marker position for " << name << " " << virtualMarkerPosition;

        row++;
    }
    return Registration3d(A,B);
}


void GradientSmoothStrategy::convert() {
    if (labeledMarkerData.empty()) return;

    const std::map<std::string, Eigen::Vector3f> firstMarkerData = labeledMarkerData.begin()->second;
    Eigen::Matrix4f transformation = Registration3d(firstMarkerData);
    Eigen::Matrix4f globalPose = transformation * outputRobot->getGlobalPose();
    outputRobot->setGlobalPose(globalPose);

    unsigned int frameNum = 0;
    std::map<float, std::vector<double>> optimizedFrames;
    bool logging = logFile.is_open();

    soft_constraints.clear();

    double sum_weights = 0.0;
    for (const auto& s : inputMarkersToOutputRobotSensors)
        sum_weights += markerWeights.at(s.first);

    double factor = 10 / sum_weights;
    for (const auto& s : inputMarkersToOutputRobotSensors) {
        const double weight = markerWeights.at(s.first);
        soft_constraints.push_back(std::make_unique<Position>(s.second, weight * factor));
    }

    std::vector<double> configuration;
    const Eigen::Vector3f rootPos = outputRobot->getGlobalPosition();
    configuration.push_back(rootPos[0] / 1000);
    configuration.push_back(rootPos[1] / 1000);
    configuration.push_back(rootPos[2] / 1000);
    const Eigen::Quaternionf rootRot = simox::math::mat3f_to_quat(outputRobot->getGlobalOrientation()).normalized();
    configuration.push_back(rootRot.x());
    configuration.push_back(rootRot.y());
    configuration.push_back(rootRot.z());
    configuration.push_back(rootRot.w());

    for (const auto &jointName : joints) {
        configuration.push_back(outputRobot->getRobotNode(jointName)->getJointValue());
    }

    state = new State(configuration);


    // Initialize optimization
    nlopt::opt optimizer(nlopt::LD_SLSQP, frameDimension);
    optimizer.set_min_objective(ConvertingStrategy::objectiveFunctionWrapperStatic, this);
    optimizer.set_maxeval(2000);
    optimizer.set_ftol_rel(1e-10);
    //optimizer.add_equality_constraint(ConvertingStrategy::quaternionEqualityConstraint, NULL, 1e-6);
    for (const auto &labeledMarker : labeledMarkerData) {
        if (cancelled) throw MMM::Exception::ForcedCancelException();

        if (frameNum == 1) {
            optimizer.set_maxeval(300);
            soft_constraints.push_back(std::make_unique<MinimizeVelocity>(5.0));
        }
        else if (frameNum == 2)
            soft_constraints.push_back(std::make_unique<MinimizeAcceleration>(3.0));
        else if (frameNum == 3)
            soft_constraints.push_back(std::make_unique<MinimizeJerk>(2.0));

        currentTimestep = labeledMarker.first;
        markerPositions.clear();
        for (const auto &m : labeledMarker.second) {
            auto it = inputMarkersToOutputRobotSensors.find(m.first);
            if (it != inputMarkersToOutputRobotSensors.end())
                markerPositions[it->second->getName()] = m.second;
        }

        // Run optimization
        double objectiveValue;
        try {
            setOptimizationBounds(optimizer, configuration);
            nlopt::result resultCode = optimizer.optimize(configuration, objectiveValue);
            MMM_INFO << "Optimization for frame " << frameNum << " at timestep " << currentTimestep << " finished with object value " << objectiveValue << " code " << resultCode << ".\n";
        }
        catch (nlopt::roundoff_limited&) {
            MMM_INFO << "Optimization for frame " << frameNum << " at timestep " << currentTimestep << " finished by throwing nlopt::roundoff_limited (the result should be usable)." << "\n";
        }

        // Create ModelPoseSensorMeasurement
        Eigen::Vector3f rootPos(configuration[0], configuration[1], configuration[2]);
        Eigen::Quaternionf rootRot(configuration[6], configuration[3], configuration[4], configuration[5]);
        rootRot.normalize();
        configuration[6] = rootRot.w();
        configuration[3] = rootRot.x();
        configuration[4] = rootRot.y();
        configuration[5] = rootRot.z();

        outputModelPoseSensor->addSensorMeasurement(std::make_shared<ModelPoseSensorMeasurement>(currentTimestep, rootPos * 1000, rootRot));

        // Create KinematicSensorMeasurement
        Eigen::VectorXf jointValues(joints.size());
        for (int i = 0; i < jointValues.rows(); ++i) {
            jointValues[i] = configuration[7 + i];
        }
        outputKinematicSensor->addSensorMeasurement(std::make_shared<KinematicSensorMeasurement>(currentTimestep, jointValues));

        optimizedFrames[currentTimestep] = configuration;
        state->update(configuration);

        //state->print();

        // Output error - expensive!
        if (frameNum % 25 == 0 || logging) {
            setOutputModelConfiguration(configuration);

            double avgDistance, maxDistance;
            calculateMarkerDistancesAverageMaximum(labeledMarkerData[currentTimestep], avgDistance, maxDistance);
            if (frameNum % 25 == 0)
                std::cout << "Frame #" << frameNum << " at timestep " << currentTimestep << " finished: max error = " << maxDistance << ", avg error = " << avgDistance << std::endl;
            if (logging) {
                logFile << "Frame #" << frameNum << " at timestep " << currentTimestep << " finished: max error = " << maxDistance << ", avg error = " << avgDistance << "\n";
            }
        }

        frameNum++;
    }
    //double maxJerk, avgJerk;
    //std::tie(maxJerk,avgJerk) = calculateMaxAndAverageJerk(optimizedFrames);
    //MMM_INFO << "Max jerk: " << maxJerk << " avg jerk: " << avgJerk << std::endl;
}

void GradientSmoothStrategy::computeGradient(Eigen::VectorXd optValues, const State &state,
                     const std::map<std::string, Eigen::Vector3f> &targetPositions, double &objectiveValue, std::vector<double> &gradient) {
    std::vector<double> f_0s;
    objectiveValue = 0;
    gradient.clear();
    for (unsigned int i = 0; i < optValues.rows(); i++)
        gradient.push_back(0.0);

    for (const std::unique_ptr<Constraint> &c : soft_constraints) {
        double localObjValue = c->computeObjectiveValue(optValues, state, targetPositions);
        objectiveValue += c->getWeight() * localObjValue;
        f_0s.push_back(localObjValue);
    }

    double theta = 2*sqrt(std::numeric_limits<float>::epsilon());

    unsigned i = 0;
    Eigen::Matrix4f pos = outputRobot->getGlobalPose();
    Eigen::Matrix3f rot = simox::math::mat4f_to_mat3f(pos);
    for (; i < 3; i++) {
        double positionValue = optValues(i);
        theta = make_xph_representable<double>(positionValue, theta);
        optValues(i) += theta;
        pos(i, 3) = optValues(i) * 1000.0;
        outputRobot->setGlobalPose(pos);
        for (unsigned int index = 0; index < soft_constraints.size(); index++) {
            double objValue = soft_constraints[index]->computeObjectiveValue(optValues, state, targetPositions);
            gradient[i] += soft_constraints[index]->getWeight() * ((-f_0s[index] + objValue) / theta);
        }
        pos(i, 3) = positionValue * 1000.0;
        optValues(i) = positionValue;
        gradient[i] *= 0.005 / joints.size(); // Has influence on every joint
    }
    for (; i < 7; i++) {
        Eigen::Quaterniond q(optValues(6), optValues(3), optValues(4), optValues(5));
        double value = q.coeffs()(i - 3);
        theta = make_xph_representable<double>(value, theta);
        q.coeffs()(i - 3) += theta;
        pos.block(0, 0, 3, 3) = q.normalized().toRotationMatrix().cast<float>();
        optValues(i) = q.coeffs()(i - 3);
        outputRobot->setGlobalPose(pos);
        for (unsigned int index = 0; index < soft_constraints.size(); index++) {
            double objValue = soft_constraints[index]->computeObjectiveValue(optValues, state, targetPositions);
            gradient[i] += soft_constraints[index]->getWeight() * ((-f_0s[index] + objValue) / theta);
        }
        optValues(i) = value;
        gradient[i] *= 0.005 / joints.size(); // Has influence on every joint
    }
    pos.block(0, 0, 3, 3) = rot;
    outputRobot->setGlobalPose(pos);
    for (const VirtualRobot::RobotNodePtr &node : robotNodeSet->getAllRobotNodes()) {
        float jointValue = node->getJointValue();
        theta = make_xph_representable<double>(jointValue, theta);
        double jointValue_help = jointValue + theta;
        node->setJointValueNoUpdate(jointValue_help);
        optValues(i) = jointValue_help;
        node->updatePose();
        for (unsigned int index = 0; index < soft_constraints.size(); index++) {
            double objValue = soft_constraints[index]->computeObjectiveValue(optValues, state, targetPositions);
            gradient[i] += soft_constraints[index]->getWeight() * ((-f_0s[index] + objValue) / theta);
        }
        optValues(i) = jointValue;
        node->setJointValueNoUpdate(jointValue);
        node->updatePose();
        i++;
    }
}

Eigen::Quaternionf GradientSmoothStrategy::setOutputModelConfiguration(const std::vector<double> &configuration) {
    Eigen::Vector3f rootPos;
    rootPos[0] = configuration[0];
    rootPos[1] = configuration[1];
    rootPos[2] = configuration[2];

    Eigen::Quaternionf rootRot(configuration[6], configuration[3], configuration[4], configuration[5]);
    rootRot.normalize();

    Eigen::Matrix4f globalPose = simox::math::pos_quat_to_mat4f(1000 * rootPos, rootRot);
    outputRobot->setGlobalPose(globalPose, false);

    for (unsigned int i = 0; i < joints.size(); ++i) {
        if (const auto node = outputRobot->getRobotNode(joints[i]))
            node->setJointValueNoUpdate(configuration[7 + i]);
        else MMM_ERROR << "Robot has no joint " << joints[i] << std::endl;
    }
    for (const auto &additionalKinematicSensor : availableKinematicSensors) {
        additionalKinematicSensor->initializeModel(outputRobot, currentTimestep, true, false);
    }
    outputRobot->updatePose();

    return rootRot;
}

double GradientSmoothStrategy::quaternionEqualityConstraint(const std::vector<double> &configuration, std::vector<double> &grad, void *) {
    double norm = 0;
    for (unsigned int i = 3; i < 7; i++) {
        norm += configuration[i] * configuration[i];
    }
    norm = norm - 1;
    if (norm > 1e-6) {
        for (unsigned int i = 0; i < grad.size(); i++) {
            if (i < 3 || i > 6)
                grad[i] = 0.;
            else
                grad[i] = 2 * configuration[i] * (norm / abs(norm));
        }
    }
    return norm;
}

double GradientSmoothStrategy::objectiveFunction(const std::vector<double> &configuration, std::vector<double> &grad) {
    if (configuration.size() != frameDimension) {
        MMM_ERROR << "NloptConverter: x has wrong number of frameDimensionensions (" << configuration.size() << ")!" << std::endl;
        return 0.0;
    }

    double objectiveValue;
    Eigen::VectorXd configurationEigen = Eigen::Map<const Eigen::VectorXd>(configuration.data(), configuration.size());
    //setOutputModelConfiguration(configuration)
    configurationEigen.block(3,0,4,1) = setOutputModelConfiguration(configuration).coeffs().cast<double>();
    computeGradient(configurationEigen, *state, markerPositions, objectiveValue, grad);
    return objectiveValue;
}

void GradientSmoothStrategy::setOptimizationBounds(nlopt::opt& optimizer, const std::vector<double> &current) const {
    std::vector<double> lowerBounds, upperBounds;
    for (int i = 0; i < 3; ++i) {
        lowerBounds.push_back(current[i] - 0.1);
        upperBounds.push_back(current[i] + 0.1);
    }
    for (int i = 3; i < 7; ++i) {
        lowerBounds.push_back(std::max(-1.0, current[i] - 0.1));
        upperBounds.push_back(std::min(1.0, current[i] + 0.1));
    }
//    for (int i = 0; i < 3; ++i) {
//        lowerBounds.push_back(-HUGE_VAL);
//        upperBounds.push_back(+HUGE_VAL);
//    }
//    for (int i = 3; i < 7; ++i) {
//        lowerBounds.push_back(-1.0);
//        upperBounds.push_back(1.0);
//    }
    for (const auto& jointName : joints) {
        auto joint = outputModel->getRobotNode(jointName);
        if (joint->isLimitless()) {
            lowerBounds.push_back(-HUGE_VAL);
            upperBounds.push_back(+HUGE_VAL);
        }
        else {
            lowerBounds.push_back(joint->getJointLimitLo());
            upperBounds.push_back(joint->getJointLimitHi());
        }
    }
    optimizer.set_lower_bounds(lowerBounds);
    optimizer.set_upper_bounds(upperBounds);
}
