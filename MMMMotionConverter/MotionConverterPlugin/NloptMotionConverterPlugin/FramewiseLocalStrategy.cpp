#include "FramewiseLocalStrategy.h"

#include <SimoxUtility/math/convert/quat_to_rpy.h>

using namespace MMM;

FramewiseLocalStrategy::FramewiseLocalStrategy(const std::map<float, std::map<std::string, Eigen::Vector3f> > &labeledMarkerData, MotionPtr outputMotion, ModelPtr outputModel, const std::vector<std::string> &joints, const std::map<std::string, std::string> &markerMapping, const std::map<std::string, float> &markerWeights, KinematicSensorList availableKinematicSensors) :
    ConvertingStrategy(labeledMarkerData, outputMotion, outputModel, joints, markerMapping, markerWeights, availableKinematicSensors),
    cancelled(false)
{
}

void FramewiseLocalStrategy::cancel() {
    cancelled = true;
}

float FramewiseLocalStrategy::getCurrentTimestep() {
    return currentTimestep;
}

void FramewiseLocalStrategy::convert() {

    unsigned int frameNum = 0;
    std::map<float, std::vector<double>> optimizedFrames;
    bool logging = logFile.is_open();
    for (const auto &labeledMarker : labeledMarkerData) {
        if (cancelled) throw MMM::Exception::ForcedCancelException();

        // Build initial configuration for optimization
        std::vector<double> configuration;
        if (frameNum > 0) {
            KinematicSensorMeasurementPtr kinematicSensorMeasurement = outputKinematicSensor->getDerivedMeasurement(currentTimestep);
            ModelPoseSensorMeasurementPtr modelPoseSensorMeasurement = outputModelPoseSensor->getDerivedMeasurement(currentTimestep);

            Eigen::Vector3f rootPos = modelPoseSensorMeasurement->getRootPosition();
            configuration.push_back(rootPos[0]); configuration.push_back(rootPos[1]); configuration.push_back(rootPos[2]);

            Eigen::Vector3f rootRot = simox::math::quat_to_rpy(modelPoseSensorMeasurement->getRootRotation());
            configuration.push_back(rootRot[0]); configuration.push_back(rootRot[1]); configuration.push_back(rootRot[2]);

            for (int i = 0; i < kinematicSensorMeasurement->getJointAngles().rows(); ++i) {
                configuration.push_back(kinematicSensorMeasurement->getJointAngles()[i]);
            }
        } else {
            configuration = std::vector<double>(frameDimension, 0.0);

            // Initial optimization
            currentTimestep = labeledMarker.first;
            nlopt::opt optimizer(nloptAlgorithm, frameDimension);
            optimizer.set_min_objective(ConvertingStrategy::objectiveFunctionWrapperStatic, this);
            optimizer.set_ftol_rel(0.0000001);
            setOptimizationBounds(optimizer, true);

            double objectiveValue;
            // compute initial configuration
            try {
                nlopt::result resultCode = optimizer.optimize(configuration, objectiveValue);
                MMM_INFO << "Initial Optimization for frame " << frameNum << " at timestep " << currentTimestep << " finished with code " << resultCode << ". " << std::endl;
            }
            catch (nlopt::roundoff_limited&) {
                MMM_INFO << "Initial Optimization for frame " << frameNum << " at timestep " << currentTimestep << " finished by throwing nlopt::roundoff_limited (the result should be usable)." << std::endl;
            }

            normalizeConfiguration(configuration);
            setOutputModelConfiguration(configuration);

            double avgDistance, maxDistance;
            calculateMarkerDistancesAverageMaximum(labeledMarkerData[currentTimestep], avgDistance, maxDistance);
            std::cout << "Initialization finished: max error = " << maxDistance << ", avg error = " << avgDistance << std::endl;

            if (frameDimension > 6) {
                // Also compute initial configuration with joint limit avoidance to prevent getting stuck in local minima near a joint limit
                std::vector<double> configurationJLA = std::vector<double>(frameDimension, 0.0);

                constraintJointLimits = true;
                try {
                    nlopt::result resultCode = optimizer.optimize(configurationJLA, objectiveValue);
                    MMM_INFO << "Initial Optimization (constraint joint limits) for frame " << frameNum << " at timestep " << currentTimestep << " finished with code " << resultCode << ". " << std::endl;
                }
                catch (nlopt::roundoff_limited&) {
                    MMM_INFO << "Initial Optimization (constraint joint limits) for frame " << frameNum << " at timestep " << currentTimestep << " finished by throwing nlopt::roundoff_limited (the result should be usable)." << std::endl;
                }
                constraintJointLimits = false;

                normalizeConfiguration(configurationJLA);

                try {
                    nlopt::result resultCode = optimizer.optimize(configurationJLA, objectiveValue);
                    MMM_INFO << "Initial Optimization (constraint joint limits 2) for frame " << frameNum << " at timestep " << currentTimestep << " finished with code " << resultCode << ". " << std::endl;
                }
                catch (nlopt::roundoff_limited&) {
                    MMM_INFO << "Initial Optimization (constraint joint limits 2) for frame " << frameNum << " at timestep " << currentTimestep << " finished by throwing nlopt::roundoff_limited (the result should be usable)." << std::endl;
                }

                normalizeConfiguration(configurationJLA);
                setOutputModelConfiguration(configurationJLA);

                double avgDistanceJLA, maxDistanceJLA;
                calculateMarkerDistancesAverageMaximum(labeledMarkerData[currentTimestep], avgDistanceJLA, maxDistanceJLA);
                std::cout << "Initialization finished (constraint joint limits): max error = " << maxDistanceJLA << ", avg error = " << avgDistanceJLA << std::endl;

                if (avgDistanceJLA < avgDistance) {
                    configuration = configurationJLA;
                }
            }
        }
        currentTimestep = labeledMarker.first;

        // Initialize optimization
        nlopt::opt optimizer(nloptAlgorithm, frameDimension);
        optimizer.set_min_objective(ConvertingStrategy::objectiveFunctionWrapperStatic, this);
        optimizer.set_ftol_rel(0.0000001);

        setOptimizationBounds(optimizer, false);

        // Run optimization
        double objectiveValue;
        try {
            nlopt::result resultCode = optimizer.optimize(configuration, objectiveValue);
            MMM_INFO << "Optimization for frame " << frameNum << " at timestep " << currentTimestep << " finished with code " << resultCode << ". " << "\n";
        }
        catch (nlopt::roundoff_limited&) {
            MMM_INFO << "Optimization for frame " << frameNum << " at timestep " << currentTimestep << " finished by throwing nlopt::roundoff_limited (the result should be usable)." << "\n";
        }

        normalizeConfiguration(configuration);

        optimizedFrames[currentTimestep] = configuration;

        // Create ModelPoseSensorMeasurement
        Eigen::Vector3f rootPos(configuration[0], configuration[1], configuration[2]);
        Eigen::Vector3f rootRot(configuration[3], configuration[4], configuration[5]);
        ModelPoseSensorMeasurementPtr modelPoseSensorMeasurement(new ModelPoseSensorMeasurement(currentTimestep, rootPos, rootRot));
        outputModelPoseSensor->addSensorMeasurement(modelPoseSensorMeasurement);

        // Create KinematicSensorMeasurement
        Eigen::VectorXf jointValues(joints.size());
        for (int i = 0; i < jointValues.rows(); ++i) {
            jointValues[i] = configuration[6 + i];
        }
        KinematicSensorMeasurementPtr kinematicSensorMeasurement(new KinematicSensorMeasurement(currentTimestep, jointValues));
        outputKinematicSensor->addSensorMeasurement(kinematicSensorMeasurement);

        // Output error - expensive!
        if (frameNum % 25 == 0 || logging) {
            setOutputModelConfiguration(configuration);

            double avgDistance, maxDistance;
            calculateMarkerDistancesAverageMaximum(labeledMarkerData[currentTimestep], avgDistance, maxDistance);
            if (frameNum % 25 == 0)
                std::cout << "Frame #" << frameNum << " at timestep " << currentTimestep << " finished: max error = " << maxDistance << ", avg error = " << avgDistance << std::endl;
            if (logging) {
                logFile << "Frame #" << frameNum << " at timestep " << currentTimestep << " finished: max error = " << maxDistance << ", avg error = " << avgDistance << "\n";
            }
        }

        frameNum++;
    }
    double maxJerk, avgJerk;
    std::tie(maxJerk,avgJerk) = calculateMaxAndAverageJerk(optimizedFrames);
    MMM_INFO << "Max jerk: " << maxJerk << " avg jerk: " << avgJerk << std::endl;
}


double FramewiseLocalStrategy::objectiveFunction(const std::vector<double> &configuration, std::vector<double> &grad) {
    if (!grad.empty()) {
        MMM_ERROR << "NloptConverter: Gradient computation not supported!" << std::endl;
        return 0.0;
    }

    if (configuration.size() != frameDimension) {
        MMM_ERROR << "NloptConverter: x has wrong number of frameDimensionensions (" << configuration.size() << ")!" << std::endl;
        return 0.0;
    }

    setOutputModelConfiguration(configuration);

    return calculateMarkerDistancesSquaresSum(labeledMarkerData[currentTimestep]);
}





void FramewiseLocalStrategy::setOptimizationBounds(nlopt::opt& optimizer, bool initial) const {
    // Some algorithms cannot handle unconstraint components (i.e. upper/lower limit of +/- infinity)
    const double positionLowerBound = -10000.0, positionUpperBound = 10000.0;  // 10m

    // We must not limit the rotation strictly at +- pi because otherwise a local optimization algorithm can get stuck when the rotation angle overflows/underflows
    const double rotationOverflowBorder = 0.2;
    const double rotationLowerBound = -2*M_PI - rotationOverflowBorder, rotationUpperBound = 2*M_PI + rotationOverflowBorder;

    std::vector<double> lowerBounds, upperBounds;

    for (int i = 0; i < 2; ++i) {  // translation & rotation vectors
        for (int j = 0; j < 3; ++j) {
            lowerBounds.push_back(i ? rotationLowerBound : positionLowerBound);
            upperBounds.push_back(i ? rotationUpperBound : positionUpperBound);
        }
    }

    for (auto jointName : joints) {
        auto joint = outputModel->getRobotNode(jointName);
        lowerBounds.push_back(joint->getJointLimitLo());
        upperBounds.push_back(joint->getJointLimitHi());
    }

    optimizer.set_lower_bounds(lowerBounds);
    optimizer.set_upper_bounds(upperBounds);
}
