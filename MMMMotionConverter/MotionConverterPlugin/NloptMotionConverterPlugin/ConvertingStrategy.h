/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef _MMM_CONVERTINGSTRATEGY_h_
#define _MMM_CONVERTINGSTRATEGY_h_

#include <MMM/MMMCore.h>
#include <MMM/Motion/Motion.h>

#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>
#include <MMM/Exceptions.h>
#include <VirtualRobot/VirtualRobotCommon.h>
#include <Eigen/Core>
#include <SimoxUtility/math/convert.h>
#include "../../MotionConverter.h"

#include <nlopt.hpp>

namespace MMM
{

class MMM_IMPORT_EXPORT ConvertingStrategy
{
public:
    virtual void cancel() = 0;

    virtual float getCurrentTimestep() = 0;

    virtual void convert() = 0;

    void openLogFile(const std::filesystem::path &logFilePath) {
        if (logFilePath.empty()) return;
        closeLogFile();
        logFile = std::ofstream(logFilePath, std::ofstream::app);
    }

    bool closeLogFile() {
        if (logFile.is_open())  {
            logFile.close();
            return true;
        }
        else return false;
    }

protected:
    ConvertingStrategy(const std::map<float, std::map<std::string, Eigen::Vector3f> > &labeledMarkerData, MotionPtr outputMotion, ModelPtr outputModel, const std::vector<std::string> &joints, const std::map<std::string, std::string> &markerMapping, const std::map<std::string, float> &markerWeights, KinematicSensorList availableKinematicSensors = KinematicSensorList()) :
        nloptAlgorithm(nlopt::LN_SBPLX),
        labeledMarkerData(labeledMarkerData),
        outputMotion(outputMotion),
        outputModel(outputModel),
        outputKinematicSensor(new KinematicSensor(joints)),
        outputModelPoseSensor(new ModelPoseSensor()),
        joints(joints),
        frameDimension(6 + joints.size()),
        markerWeights(markerWeights),
        availableKinematicSensors(availableKinematicSensors)
    {
        auto joints_all = joints;
        for (auto kinematicSensor : availableKinematicSensors) {
            auto j = kinematicSensor->getJointNames();
            joints_all.insert(joints_all.end(), j.begin(), j.end());
        }
        // Improve performance by using reduced model
        outputRobot = ProcessedModelWrapper::createReducedModel(outputModel, joints_all);
        outputRobot->setUpdateCollisionModel(false);
        outputRobot->setUpdateVisualization(false);
        outputRobot->setupVisualization(false, false);
        outputRobot->setThreadsafe(false);

        for (const auto &mapping : markerMapping) {
            inputMarkersToOutputRobotSensors[mapping.first] = outputRobot->getSensor(mapping.second);
        }

        for (auto joint : joints) {
            jointValuesToSet[joint] = 0.0f;
        }

        // exceptions will be forwarded
        outputMotion->addSensor(outputKinematicSensor);
        outputMotion->addSensor(outputModelPoseSensor);
    }

    ~ConvertingStrategy() {
        closeLogFile();
    }

    static double objectiveFunctionWrapperStatic(const std::vector<double> &configuration, std::vector<double> &grad, void *data) {
        return static_cast<ConvertingStrategy*>(data)->objectiveFunctionWrapper(configuration, grad);
    }

    double objectiveFunctionWrapper(const std::vector<double> &configuration, std::vector<double> &grad) {
        return objectiveFunction(configuration, grad);
    }

    virtual double objectiveFunction(const std::vector<double> &configuration, std::vector<double> &grad) = 0;

    void normalizeConfiguration(std::vector<double> &configuration) {
        while (configuration[3] > M_PI)
            configuration[3] -= 2 * M_PI;
        while (configuration[3] < -M_PI)
            configuration[3] += 2 * M_PI;
        while (configuration[4] > M_PI)
            configuration[4] -= 2 * M_PI;
        while (configuration[4] < -M_PI)
            configuration[4] += 2 * M_PI;
        while (configuration[5] > M_PI)
            configuration[5] -= 2 * M_PI;
        while (configuration[5] < -M_PI)
            configuration[5] += 2 * M_PI;

    }

    void setOutputModelConfiguration(const std::vector<double> &configuration) {
        Eigen::Vector3f rootPos;
        rootPos[0] = configuration[0]; rootPos[1] = configuration[1]; rootPos[2] = configuration[2];

        Eigen::Vector3f rootRot;
        rootRot[0] = configuration[3]; rootRot[1] = configuration[4]; rootRot[2] = configuration[5];

        Eigen::Matrix4f globalPose = simox::math::pos_rpy_to_mat4f(rootPos, rootRot);
        outputRobot->setGlobalPose(globalPose, false);

        for (unsigned int i = 0; i < joints.size(); ++i) {
            auto value = configuration[6 + i];
            jointValuesToSet[joints[i]] = value;
        }

        outputRobot->setPropagatingJointValuesEnabled(false);
        outputRobot->setJointValues(jointValuesToSet);
        for (auto additionalKinematicSensor : availableKinematicSensors) {
            additionalKinematicSensor->initializeModel(outputRobot, currentTimestep, true);
        }
        outputRobot->setPropagatingJointValuesEnabled(true);
        outputRobot->updatePose();
    }

    double calculateMarkerDistancesSquaresSum(const std::map<std::string, Eigen::Vector3f> &labeledMarker) const {
        double sumDistancesSquares = 0.0;

        for (const auto &inputMarkersToOutputRobotSensor : inputMarkersToOutputRobotSensors) {
            const std::string &inputMarker = inputMarkersToOutputRobotSensor.first;
            const VirtualRobot::SensorPtr& outputRobotSensor = inputMarkersToOutputRobotSensor.second;

            const Eigen::Vector3f& posInputMarker = labeledMarker.at(inputMarker);
            if(posInputMarker.norm() < 0.0001f) // marker exactly at zero means that it is not valid
            {
                continue;
            }
            const Eigen::Vector3f& posOutputMarker = outputRobotSensor->getGlobalPose().block(0, 3, 3, 1);

            sumDistancesSquares += (posInputMarker - posOutputMarker).squaredNorm() * markerWeights.at(inputMarker);
        }
        if (constraintJointLimits) {
            for (const auto &value : jointValuesToSet) {
                auto joint = outputRobot->getRobotNode(value.first);

                auto normalized = (value.second - joint->getJointLimitLo()) / (joint->getJointLimitHi() - joint->getJointLimitLo());
                if ((normalized > 0.5 && std::abs(joint->getJointLimitHi()) > 0.1) || (normalized < 0.5 && std::abs(joint->getJointLimitLo()) > 0.1)) {
                    sumDistancesSquares *= std::pow(joint->getJointLimitHi() - joint->getJointLimitLo(), 2) / (50 * (joint->getJointLimitHi() - value.second) * (value.second - joint->getJointLimitLo())) + 0.92;
                }
            }
        }
        return sumDistancesSquares;
    }

    void calculateMarkerDistancesAverageMaximum(const std::map<std::string, Eigen::Vector3f> &labeledMarker, double &avgDistance, double &maxDistance) const {
        double sumDistances = 0.0;
        maxDistance = 0.0;

        int count = 0;

        for (const auto &inputMarkersToOutputRobotSensor : inputMarkersToOutputRobotSensors) {
            const std::string& inputMarker = inputMarkersToOutputRobotSensor.first;
            const VirtualRobot::SensorPtr& outputRobotSensor = inputMarkersToOutputRobotSensor.second;

            const Eigen::Vector3f& posInputMarker = labeledMarker.at(inputMarker);
            if(posInputMarker.norm() < 0.0001f) // marker exactly at zero means that it is not valid
            {
                continue;
            }
            const Eigen::Vector3f& posOutputMarker = outputRobotSensor->getGlobalPose().block(0, 3, 3, 1);

            double distance = (posInputMarker - posOutputMarker).norm();
            sumDistances += distance;

            if (distance > maxDistance) {
                maxDistance = distance;
            }

            count++;
        }

        avgDistance = sumDistances / count;
    }

    std::pair<double, double> calculateMaxAndAverageJerk(const std::map<float, std::vector<double>>& configurations)
    {
        double jerkSum = 0.0;
        double maxJerk = 0.0;
        std::unique_ptr<Eigen::VectorXd> previousJointPos, previousJointVel, previousJointAcc;
        int frameNum = 0;

        float previousTimestep = 0.0f;
        for(auto& pair : configurations)
        {
            auto timestep = pair.first;
            auto tDelta = timestep - previousTimestep;
            auto frameConfiguration = pair.second;
            setOutputModelConfiguration(frameConfiguration);
            Eigen::VectorXd jointPos = Eigen::Map<Eigen::VectorXd>(frameConfiguration.data(), frameConfiguration.size());
            Eigen::VectorXd orientation = jointPos.block<3,1>(3,0);
            Eigen::VectorXd jointPosWithoutOrientation(jointPos.rows() - 3);
            jointPosWithoutOrientation << jointPos.head(3), jointPos.tail(jointPos.rows()-6); // remove orientation since it cannot be derived like in the following
            jointPosWithoutOrientation.head(3) /= 100; // make position smaller to fit to radian for optimization
            if(previousJointPos)
            {
                /*if(previousJointPos->rows() != (int)frameConfiguration.size())
                {
                    MMM_ERROR << "Different joint vector sizes!" << std::endl;
                    throw std::runtime_error("Different joint vector sizes!");
                }*/

                Eigen::VectorXd jointVel = (jointPosWithoutOrientation - *previousJointPos)/tDelta;
                if(previousJointVel)
                {
                    Eigen::VectorXd jointAcc = (jointVel - *previousJointVel)/tDelta;
                    if(previousJointAcc)
                    {
                        Eigen::VectorXd jointJerk = (jointAcc-*previousJointAcc)/tDelta;
                        if(std::abs(jointJerk.maxCoeff()) > maxJerk)
                        {
                            maxJerk = std::abs(jointJerk.maxCoeff());
                        }
                        jerkSum += jointJerk.cwiseAbs().mean();
                    }
                    else
                    {
                        previousJointAcc.reset(new Eigen::VectorXd());
                    }
                    *previousJointAcc = jointAcc;
                }
                else
                {
                    previousJointVel.reset(new Eigen::VectorXd());
                }
                *previousJointVel = jointVel;
            }
            else
            {
                previousJointPos.reset(new Eigen::VectorXd());
            }
            previousTimestep = timestep;
            *previousJointPos = jointPosWithoutOrientation;
            frameNum++;
        }
        return std::make_pair(maxJerk, jerkSum/frameNum);
    }

    nlopt::algorithm nloptAlgorithm;

    std::map<float, std::map<std::string, Eigen::Vector3f> > labeledMarkerData;
    MotionPtr outputMotion;
    ModelPtr outputModel;
    KinematicSensorPtr outputKinematicSensor;
    ModelPoseSensorPtr outputModelPoseSensor;
    VirtualRobot::RobotPtr outputRobot;
    std::vector<std::string> joints;
    unsigned int frameDimension;
    std::map<std::string, float> markerWeights;
    KinematicSensorList availableKinematicSensors;
    float currentTimestep;
    bool constraintJointLimits = false;
    std::filesystem::path logFilePath;
    std::ofstream logFile;

    // The following members are not strictly necessary but used for performance optimizations
    std::map<std::string, VirtualRobot::SensorPtr> inputMarkersToOutputRobotSensors;
    std::map<std::string, float> jointValuesToSet;

};

typedef std::shared_ptr<ConvertingStrategy> ConvertingStrategyPtr;

}

#endif
