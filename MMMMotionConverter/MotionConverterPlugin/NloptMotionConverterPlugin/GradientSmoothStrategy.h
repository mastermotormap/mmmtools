/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#pragma once

#include <MMM/MMMCore.h>

#include "ConvertingStrategy.h"
#include <atomic>
#include <boost/math/special_functions/next.hpp>
#include <Eigen/Core>

namespace MMM
{

// Optimization inspired by "RelaxedIK: Real-time Synthesis of Accurate and Feasible Robot Arm Motion", D. Rakita (2018)

template <typename T>
T groove_loss(T x_val, T t, int d, T c, T f, int g) {
    return -std::exp(-std::pow(x_val - t, d) / 2 * std::pow(c, 2)) + f * std::pow(x_val - t, g);
}

class LossFunction
{
public:
    virtual double compute(double &value) const {
        return value;
    }
};

class Groove final : public LossFunction
{
public:
    Groove(double t, int d, double c, double f, int g) :
        t(t), d(d), c(c), f(f), g(g)
    {
    }

    double compute(double &value) const final {
        return groove_loss<double>(value, t, d, c, f, g);
    }

private:
    double t;
    int d;
    double c;
    double f;
    int g;
};

// from boost/finite_differences.hpp which is not available here
template<class Real>
Real make_xph_representable(Real x, Real h)
{
    using std::numeric_limits;
    // Redefine h so that x + h is representable. Not using this trick leads to large error.
    // The compiler flag -ffast-math evaporates these operations . . .
    Real temp = x + h;
    h = temp - x;
    // Handle the case x + h == x:
    if (h == 0)
    {
        h = boost::math::nextafter(x, (numeric_limits<Real>::max)()) - x;
    }
    return h;
}


struct State {
    State(const Eigen::Ref<const Eigen::VectorXd> values) :
        initial_state(values),
        xopt(values),
        previous(values),
        previous2(values)
    {
    }

    State(const std::vector<float> values) :
        State(Eigen::Map<const Eigen::VectorXf>(values.data(), values.size()).cast<double>())
    {
    }

    State(const std::vector<double> values) :
        State(Eigen::Map<const Eigen::VectorXd>(values.data(), values.size()))
    {
    }

    void update(const Eigen::Ref<const Eigen::VectorXd> values) {
        previous2 = previous;
        previous = xopt;
        xopt = values;
    }

    void update(const std::vector<double> values) {
        update(Eigen::Map<const Eigen::VectorXd>(values.data(), values.size()));
    }

    inline Eigen::VectorXd velocity(const Eigen::Ref<const Eigen::VectorXd> values) const {
        //print();
        //std::cout << "Values:    " << values << std::endl;
        return values - xopt;
    }

    inline Eigen::VectorXd acceleration(const Eigen::Ref<const Eigen::VectorXd> values) const {
        return values - 2*xopt + previous;
    }

    inline Eigen::VectorXd jerk(const Eigen::Ref<const Eigen::VectorXd> values) const {
        return values - 3*xopt + 3*previous - previous2;
    }

    void print() const {
        std::stringstream ss;
        ss << "STATE\n";
        ss << "Initial State:   " << initial_state.transpose() << "\n";
        ss << "Current State:   " << xopt.transpose() << "\n";
        ss << "Previous State:  " << previous.transpose() << "\n";
        ss << "Previous State2: " << previous2.transpose() << "\n";
        std::cout << ss.str() << std::endl;
    }

private:
    Eigen::VectorXd initial_state;
    Eigen::VectorXd xopt;
    Eigen::VectorXd previous;
    Eigen::VectorXd previous2;
};

class Constraint {
public:
    Constraint(std::unique_ptr<LossFunction> loss = std::make_unique<LossFunction>(), double weight = 1.0) :
        weight(weight), loss(std::move(loss))
    {
    }

    inline double computeObjectiveValue(const Eigen::Ref<const Eigen::VectorXd> values, const State &state,
                                 const std::map<std::string, Eigen::Vector3f> &targetPositions) const {
        double objectiveValue = objectiveFunction(values, state, targetPositions);
        return loss->compute(objectiveValue);
    }

    inline double computeWeightedObjectiveValue(const Eigen::Ref<const Eigen::VectorXd> values, const State &state,
                                         const std::map<std::string, Eigen::Vector3f> &targetPositions) const {
        return weight * computeObjectiveValue(values, state, targetPositions);
    }

    double getWeight() const {
        return weight;
    }

protected:
    virtual double objectiveFunction(const Eigen::Ref<const Eigen::VectorXd> values, const State &state,
                                     const std::map<std::string, Eigen::Vector3f> &targetPositions) const = 0;

private:
    double weight;
    std::unique_ptr<LossFunction> loss;
};

class Position : public Constraint
{
public:
    Position(const VirtualRobot::SceneObjectPtr &node, double weight = 1.0)
        : Constraint(std::make_unique<Groove>(0., 2, 0.1, 10.0, 2), weight), node(node)
    {
    }

    static inline double error(const VirtualRobot::SceneObjectPtr &node, const Eigen::Ref<const Eigen::Vector3f> globalPosition) {
        return ((node->getGlobalPosition() - globalPosition) / 100.0).norm();
    }

protected:
    double objectiveFunction(const Eigen::Ref<const Eigen::VectorXd> /*values*/, const State &/*state*/,
                             const std::map<std::string, Eigen::Vector3f> &targetPositions) const override {
        auto it = targetPositions.find(node->getName());
        if (it == targetPositions.end())
            return 0;
        return error(node, it->second);
    }

private:
    VirtualRobot::SceneObjectPtr node;
};

class MinimizeVelocity : public Constraint
{
public:
    MinimizeVelocity(double weight = 1.0) :
        Constraint(std::make_unique<Groove>(0., 2, 0.1, 10.0, 2), weight)
    {
    }

protected:
    double objectiveFunction(const Eigen::Ref<const Eigen::VectorXd> values, const State &state,
                             const std::map<std::string, Eigen::Vector3f> &/*targetPositions*/) const override {
        Eigen::VectorXd w = Eigen::VectorXd::Ones(values.size());
        return state.velocity(values).cwiseProduct(w).norm();
    }
};

class MinimizeAcceleration : public Constraint
{
public:
    MinimizeAcceleration(double weight = 1.0) :
        Constraint(std::make_unique<Groove>(0., 2, 0.1, 10.0, 2), weight)
    {
    }

protected:
    double objectiveFunction(const Eigen::Ref<const Eigen::VectorXd> values, const State &state,
                             const std::map<std::string, Eigen::Vector3f> &/*targetPositions*/) const override {
        Eigen::VectorXd w = Eigen::VectorXd::Ones(values.size());
        return state.acceleration(values).cwiseProduct(w).norm();
    }
};

class MinimizeJerk : public Constraint
{
public:
    MinimizeJerk(double weight = 1.0) :
        Constraint(std::make_unique<Groove>(0., 2, 0.1, 10.0, 2), weight)
    {
    }

protected:
    double objectiveFunction(const Eigen::Ref<const Eigen::VectorXd> values, const State &state,
                             const std::map<std::string, Eigen::Vector3f> &/*targetPositions*/) const override {
        Eigen::VectorXd w = Eigen::VectorXd::Ones(values.size());
        return state.jerk(values).cwiseProduct(w).norm();
    }
};


class MMM_IMPORT_EXPORT GradientSmoothStrategy : public ConvertingStrategy
{
public:
    GradientSmoothStrategy(const std::map<float, std::map<std::string, Eigen::Vector3f> > &labeledMarkerData, MotionPtr outputMotion, ModelPtr outputModel, const std::vector<std::string> &joints, const std::map<std::string, std::string> &markerMapping, const std::map<std::string, float> &markerWeights, KinematicSensorList availableKinematicSensors = KinematicSensorList());

    void cancel();

    float getCurrentTimestep();

    void convert();

protected:
    double objectiveFunction(const std::vector<double> &configuration, std::vector<double> &grad);

private:
    Eigen::Matrix4f Registration3d(const Eigen::Ref<const Eigen::MatrixXf> A,
                                   const Eigen::Ref<const Eigen::MatrixXf> B);
    Eigen::Matrix4f Registration3d(const std::map<std::string, Eigen::Vector3f> &markerPositions);

    void setOptimizationBounds(nlopt::opt& optimizer, const std::vector<double> &current) const;

    void computeGradient(Eigen::VectorXd jointValues, const State &state,
                         const std::map<std::string, Eigen::Vector3f> &targetPositions,
                         double &objectiveValue, std::vector<double> &gradient);

    Eigen::Quaternionf setOutputModelConfiguration(const std::vector<double> &configuration);

    static double quaternionEqualityConstraint(const std::vector<double> &configuration, std::vector<double> &grad, void */*data*/);

    bool cancelled;

    State* state;
    std::vector<std::string> rootJointMarkers;
    std::vector<std::unique_ptr<Constraint>> soft_constraints;
    std::map<std::string, Eigen::Vector3f> markerPositions;
    VirtualRobot::RobotNodeSetPtr robotNodeSet;
};

}
