/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_CONVERTMOTIONHANDLER_H_
#define __MMM_CONVERTMOTIONHANDLER_H_

#include "ConvertMotionHandlerDialog.h"
#include "../../MMMViewer/MMM/Viewer/MotionHandler.h"

namespace MMM
{

class ConvertMotionHandler : public MotionHandler
{
    Q_OBJECT

public:
    ConvertMotionHandler(QWidget* widget);

    void handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> currentRobots);

    virtual std::string getName();

    static constexpr const char* NAME = "ConvertMotion";

    std::shared_ptr<IPluginHandler> getPluginHandler();

    std::string getPluginHandlerID();

private:
    ConvertMotionHandlerDialog* dialog;
};

}

#endif
