#include "ConvertMotionHandlerDialog.h"
#include "ui_ConvertMotionHandlerDialog.h"

#include <MMM/FactoryPluginLoader.h>

#include <MMM/Model/ModelReaderXML.h>
#include <MMM/Exceptions.h>
#include <MMM/Motion/MotionRecording.h>

#include "QFileDialog"
#include "QMessageBox"
#include "ConvertingMotionHandlerDialog.h"

ConvertMotionHandlerDialog::ConvertMotionHandlerDialog(QWidget* parent)
    : QDialog(parent),
      ui(new Ui::ConvertMotionHandlerDialog)
{
    ui->setupUi(this);

    connect(ui->chooseMotionBox, SIGNAL(currentTextChanged(QString)), this, SLOT(setCurrentMotion(QString)));
    connect(ui->loadTargetModelButton, SIGNAL(clicked()), this, SLOT(loadTargetModel()));
    connect(ui->loadConverterConfigButton, SIGNAL(clicked()), this, SLOT(loadConverterConfiguration()));
    connect(ui->convertMotionButton, SIGNAL(clicked()), this, SLOT(convertMotion()));
    connect(ui->cancelButton, SIGNAL(clicked()), this, SLOT(close()));

    loadTargetModel(settings.value("converter/modelfilepath", "").toString().toStdString(), true);
    loadConverterConfiguration(settings.value("converter/configfilepath", "").toString().toStdString());

    ui->chooseConverterBox->setCurrentText(settings.value("converter/converter", "NloptConverter").toString());
}

ConvertMotionHandlerDialog::~ConvertMotionHandlerDialog()
{
    delete ui;
}

void ConvertMotionHandlerDialog::loadMotions(MMM::MotionRecordingPtr motions) {
    ui->chooseMotionBox->clear();
    convertedMotion = nullptr;
    currentMotion = nullptr;
    this->motions = motions;

    int i = 0;
    for (const std::string &name : motions->getMotionNames()) {
        auto motion = motions->getMotion(name);
        if (!currentMotion) setCurrentMotion(QString::fromStdString(motion->getName()));
        ui->chooseMotionBox->addItem(QString::fromStdString(motion->getName()));
        if (motion->getName() == motionName) ui->chooseMotionBox->setCurrentIndex(i);
        i++;
    }
}

MMM::MotionPtr ConvertMotionHandlerDialog::convertMotion(MMM::MotionRecordingPtr motions) {
    loadMotions(motions);
    exec();
    return convertedMotion;
}

void ConvertMotionHandlerDialog::setConvertedMotion(MMM::MotionPtr motion) {
    convertedMotion = motion;
    if (convertedMotion) this->close();
}

void ConvertMotionHandlerDialog::reject() {
    this->hide();
}

void ConvertMotionHandlerDialog::setCurrentMotion(QString name) {
    currentMotion = motions->getMotion(name.toStdString());
    if (currentMotion) {
        motionName = currentMotion->getName();
        setConverterOptions();
    }
    enableConvertMotionButton();
}

void ConvertMotionHandlerDialog::loadTargetModel() {
    std::filesystem::path modelFilePath = QFileDialog::getOpenFileName(this, tr("Load target model"), settings.value("converter/filesearchpath", "").toString(), tr("XML files (*.xml)")).toStdString();
    settings.setValue("converter/filesearchpath", QString::fromStdString(modelFilePath.parent_path()));
    loadTargetModel(modelFilePath);
}

void ConvertMotionHandlerDialog::loadTargetModel(const std::filesystem::path &modelFilePath, bool ignoreError) {
    if (!modelFilePath.empty()) {
        MMM::ModelReaderXMLPtr r(new MMM::ModelReaderXML());
        targetModel = r->loadModel(modelFilePath);
        if (!targetModel && !ignoreError) {
            QMessageBox* msgBox = new QMessageBox(this);
            std::string error_message = "Could not read model from " + modelFilePath.generic_string() + "!";
            msgBox->setText(QString::fromStdString(error_message));
            std::cout << error_message << std::endl;
            msgBox->exec();
        }
        setTargetModelLabel(modelFilePath);
        settings.setValue("converter/modelfilepath", QString::fromStdString(modelFilePath));
        enableConvertMotionButton();
    }
}

void ConvertMotionHandlerDialog::loadConverterConfiguration() {
    std::filesystem::path converterConfigFilePath = QFileDialog::getOpenFileName(this, tr("Load converter configuration"), settings.value("converter/filesearchpath", "").toString(), tr("XML files (*.xml)")).toStdString();
    settings.setValue("converter/filesearchpath", QString::fromStdString(converterConfigFilePath.parent_path()));
    loadConverterConfiguration(converterConfigFilePath);
}

void ConvertMotionHandlerDialog::loadConverterConfiguration(const std::filesystem::path &converterConfigFilePath) {
    if (!converterConfigFilePath.empty()) {
        this->converterConfigFilePath = converterConfigFilePath;
        setConverterConfigLabel(converterConfigFilePath);
        settings.setValue("converter/configfilepath", QString::fromStdString(converterConfigFilePath));
        enableConvertMotionButton();
    }
}

void ConvertMotionHandlerDialog::update(std::map<std::string, MMM::MotionConverterFactoryPtr > motionConverterFactories) {
    this->motionConverterFactories = motionConverterFactories;

    ui->chooseConverterBox->clear();
    for (const auto &converter : motionConverterFactories) ui->chooseConverterBox->addItem(QString::fromStdString(converter.first));
    enableConvertMotionButton();
}

void ConvertMotionHandlerDialog::convertMotion() {
    MMM::ModelPtr targetModelProcessed = targetModel;
    MMM::ModelProcessorWinterPtr targetModelProcessor = getModelProcessor();
    if (targetModelProcessor) targetModelProcessed = targetModelProcessor->convertModel(targetModel);

    try {
        MMM::MotionConverterPtr motionConverter = getMotionConverterFactory()->createMotionConverter(currentMotion, targetModelProcessed, targetModel, targetModelProcessor, converterConfigFilePath, getMotionName());
        motionConverter->setAdditionalMotions(motions);
        ConvertingMotionHandlerDialog* convertingDialog = new ConvertingMotionHandlerDialog(this, motionConverter, currentMotion);
        connect(convertingDialog, SIGNAL(motionConverted(MMM::MotionPtr)), this, SLOT(setConvertedMotion(MMM::MotionPtr)));
        convertingDialog->open();
    } catch (MMM::Exception::MMMException &e) {
        QMessageBox* msgBox = new QMessageBox(this);
        msgBox->setText(e.what());
        std::cout << e.what() << std::endl;
        msgBox->exec();
    }
}

bool ConvertMotionHandlerDialog::enableConvertMotionButton() {
    if (targetModel && !converterConfigFilePath.empty() && currentMotion && motionConverterFactories.size() > 0) {
        ui->convertMotionButton->setEnabled(true);
        return true;
    }
    ui->convertMotionButton->setEnabled(false);
    return false;
}

MMM::ModelProcessorWinterPtr ConvertMotionHandlerDialog::getModelProcessor() {
    MMM::ModelProcessorWinterPtr targetModelProcessor;
    if (ui->winterProcessorGroupBox->isChecked()) {
        targetModelProcessor = MMM::ModelProcessorWinterPtr(new MMM::ModelProcessorWinter(ui->heightSpinner->value(), ui->weightSpinner->value(), ui->useHandLength->isChecked() ? ui->handLengthSpinner->value() : -1.0f));
    }
    return targetModelProcessor;
}

MMM::MotionConverterFactoryPtr ConvertMotionHandlerDialog::getMotionConverterFactory() {
    QString converterName = ui->chooseConverterBox->currentText();
    settings.setValue("converter/converter", converterName);
    return motionConverterFactories[converterName.toStdString()];
}

void ConvertMotionHandlerDialog::setTargetModelLabel(const std::filesystem::path &filePath) {
    QString label = QString::fromStdString(filePath.filename());
    ui->loadedTargetModel->setText(label);
    ui->loadedTargetModel->setToolTip(label);
}

void ConvertMotionHandlerDialog::setConverterConfigLabel(const std::filesystem::path &filePath) {
    QString label = QString::fromStdString(filePath.filename());
    ui->loadedConverterConfig->setText(label);
    ui->loadedConverterConfig->setToolTip(label);
}

void ConvertMotionHandlerDialog::setConverterOptions() {
    ui->convertedMotionName->setText(QString::fromStdString("converted_" + motionName));

    if (currentMotion->getModel(false)) {
        ui->newMotionRadioButton->setChecked(true);
    }
}

std::string ConvertMotionHandlerDialog::getMotionName() {
    if (ui->newMotionRadioButton->isChecked()) {
        std::string convertedMotionName = ui->convertedMotionName->text().toStdString();
        if (convertedMotionName.empty()) throw MMM::Exception::MMMException("Motion name for converted motion should not be empty!");
        else return convertedMotionName;
    }
    else return motionName;
}

