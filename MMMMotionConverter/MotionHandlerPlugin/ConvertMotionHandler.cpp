#include "ConvertMotionHandler.h"
#include <QFileDialog>
#include <MMM/Motion/MotionRecording.h>

using namespace MMM;

ConvertMotionHandler::ConvertMotionHandler(QWidget* widget) :
    MotionHandler(MotionHandlerType::GENERAL, "Convert", "Ctrl+C"),
    dialog(new ConvertMotionHandlerDialog(widget))
{
}

void ConvertMotionHandler::handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> currentRobots) {
    if (motions && !motions->empty()) {
        MotionPtr convertedMotion = dialog->convertMotion(motions);
        if (convertedMotion) {
            motions->addReplaceMotion(convertedMotion);
            emit openMotions(motions);
        }
    }
    else MMM_ERROR << "Cannot open convert motion dialog, because no motions are present!" << std::endl;
}

std::string ConvertMotionHandler::getName() {
    return NAME;
}

std::shared_ptr<IPluginHandler> ConvertMotionHandler::getPluginHandler() {
    std::shared_ptr<PluginHandler<MMM::MotionConverterFactory> > pluginHandler(new PluginHandler<MMM::MotionConverterFactory>(getPluginHandlerID(), MOTION_CONVERTER_PLUGIN_LIB_DIR));
    pluginHandler->updateSignal.connect(std::bind(&ConvertMotionHandlerDialog::update, dialog, std::placeholders::_1));
    pluginHandler->emitUpdate();
    return pluginHandler;
}

std::string ConvertMotionHandler::getPluginHandlerID() {
    return "Motion Converter";
}
