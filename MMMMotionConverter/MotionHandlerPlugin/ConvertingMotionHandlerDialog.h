/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2017 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef _MMM_CONVERTINGMOTIONHANDLERDIALOG_H_
#define _MMM_CONVERTINGMOTIONHANDLERDIALOG_H_

#include <Inventor/sensors/SoTimerSensor.h>
#include <QDialog>
#include <thread>
#include <exception>
#include <stdexcept>

#ifndef Q_MOC_RUN
#include "../MotionConverter.h"
#endif

namespace Ui {
class ConvertingMotionHandlerDialog;
}

class ConvertingMotionHandlerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ConvertingMotionHandlerDialog(QWidget* parent, MMM::MotionConverterPtr motionConverter, MMM::MotionPtr motion);
    ~ConvertingMotionHandlerDialog();

    void update();

signals:
    void motionConverted(MMM::MotionPtr convertedMotion);

private slots:
    void cancel();

private:
    static void updateTimerCallback(void* data, SoSensor* sensor);
    static void convert(MMM::MotionConverterPtr motionConverter);

    Ui::ConvertingMotionHandlerDialog* ui;
    MMM::MotionConverterPtr motionConverter;
    std::thread convertingThread;
    SoTimerSensor* updateTimer;
    float minTimestep;
    float maxTimestep;
    int scaling;
    float updateInterval;
    SbTime startingTime;
};

#endif // _MMM_CONVERTINGMOTIONHANDLERDIALOG_H_
