#include "AddWholeBodyDynamicHandlerDialog.h"
#include "ui_AddWholeBodyDynamicHandlerDialog.h"

#include <MMM/Motion/MotionRecording.h>
#include "../WholeBodyDynamicCalculator.h"
#include <QCheckBox>
#include <QMessageBox>
#include <set>

AddWholeBodyDynamicHandlerDialog::AddWholeBodyDynamicHandlerDialog(QWidget* parent, MMM::MotionRecordingPtr motions) :
    QDialog(parent),
    ui(new Ui::AddWholeBodyDynamicHandlerDialog),
    motions(motions),
    currentMotion(nullptr),
    allMotion(true),
    calculated(false)
{
    ui->setupUi(this);

    loadMotions();

    connect(ui->AllMotionCheckBox, SIGNAL(toggled(bool)), this, SLOT(allMotionToggled(bool)));
    connect(ui->ChooseMotionGroupBox, SIGNAL(toggled(bool)), this, SLOT(chooseMotionToggled(bool)));
    connect(ui->ChooseMotionGroupBox, SIGNAL(toggled(bool)), ui->ChooseMotionComboBox, SLOT(setEnabled(bool)));
    connect(ui->ChooseMotionComboBox, SIGNAL(currentTextChanged(QString)), this, SLOT(setCurrentMotion(QString)));
    connect(ui->CalculateButton, SIGNAL(clicked()), this, SLOT(calculate()));
    connect(ui->CancelButton, SIGNAL(clicked()), this, SLOT(close()));
}

AddWholeBodyDynamicHandlerDialog::~AddWholeBodyDynamicHandlerDialog() {
    delete ui;
}

bool AddWholeBodyDynamicHandlerDialog::calculateWholeBodyDynamic() {
    exec();
    return calculated;
}

void AddWholeBodyDynamicHandlerDialog::loadMotions() {
    for (const std::string &name : motions->getMotionNames()) {
        ui->ChooseMotionComboBox->addItem(QString::fromStdString(name));
    }
    setCurrentMotion(0);
}

void AddWholeBodyDynamicHandlerDialog::setCurrentMotion(QString name) {
    currentMotion = motions->getMotion(name.toStdString());
}

void AddWholeBodyDynamicHandlerDialog::chooseMotionToggled(bool checked) {
    ui->AllMotionCheckBox->setChecked(!checked);
    allMotion = !checked;
}

void AddWholeBodyDynamicHandlerDialog::allMotionToggled(bool checked) {
    ui->ChooseMotionGroupBox->setChecked(!checked);
    allMotion = checked;
}

void AddWholeBodyDynamicHandlerDialog::calculate() {
    if (allMotion) {
        for (const std::string &name : motions->getMotionNames()) {
            auto motion = motions->getMotion(name);
            MMM::WholeBodyDynamicCalculator::calculate(motion);
        }
    } else {
        MMM::WholeBodyDynamicCalculator::calculate(currentMotion);
    }
    calculated = true;
    this->close();
}
