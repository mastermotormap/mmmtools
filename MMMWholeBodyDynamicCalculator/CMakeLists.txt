cmake_minimum_required(VERSION 3.10.2)

###########################################################
#### Project configuration                             ####
###########################################################

project(MMMWholeBodyDynamicCalculator)

###########################################################
#### Source configuration                              ####
###########################################################

set(SOURCE_FILES
    MMMWholeBodyDynamicCalculator.cpp
    WholeBodyDynamicCalculator.cpp
    ../common/ApplicationBaseConfiguration.cpp
)

set(HEADER_FILES
    MMMWholeBodyDynamicCalculatorConfiguration.h
    WholeBodyDynamicCalculator.h
    ../common/ApplicationBaseConfiguration.h
)

###########################################################
#### CMake package configuration                       ####
###########################################################

find_package(MMMCore REQUIRED)
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} MMMCore ModelPoseSensor KinematicSensor WholeBodyDynamicSensor)

###########################################################
#### Project build configuration                       ####
###########################################################

add_executable(${PROJECT_NAME} ${SOURCE_FILES} ${HEADER_FILES})
target_link_libraries(${PROJECT_NAME} PUBLIC ${EXTERNAL_LIBRARY_DIRS} dl)

set(MMM_WHOLEBODYDYNAMIC_CALCULATOR_BASE_DIR ${PROJECT_SOURCE_DIR} CACHE INTERNAL "" )
add_definitions(-DMMM_WHOLEBODYDYNAMIC_CALCULATOR_BASE_DIR="${MMM_WHOLEBODYDYNAMIC_CALCULATOR_BASE_DIR}" -D_SCL_SECURE_NO_WARNINGS)

install(
    TARGETS ${PROJECT_NAME}
    EXPORT "${CMAKE_PROJECT_NAME}Targets"
    #LIBRARY DESTINATION lib
    #ARCHIVE DESTINATION lib
    RUNTIME DESTINATION bin
    #INCLUDES DESTINATION include
    COMPONENT bin
)

add_definitions(-DMMMTools_LIB_DIR="${MMMTools_LIB_DIR}")
add_definitions(-DMMMTools_SRC_DIR="${MMMTools_SOURCE_DIR}")

###########################################################
#### Compiler configuration                            ####
###########################################################

include_directories(${EXTERNAL_INCLUDE_DIRS})
link_directories(${EXTERNAL_LIBRARY_DIRS})
add_definitions(${EXTERNAL_LIBRARY_FLAGS})

###########################################################
#### Motion Handler Plugin                             ####
###########################################################

add_subdirectory(MotionHandlerPlugin)
