cmake_minimum_required(VERSION 3.10.2)

###########################################################
#### Project configuration                             ####
###########################################################

project(MotionAdaptation)

###########################################################
#### MMMMotationAdaptation configuration               ####
###########################################################

set(SOURCE_FILES
	MMMMotionAdaptation.cpp
)

set(HEADER_FILES
)

###########################################################
#### CMake package configuration                       ####
###########################################################

find_package(MMMCore REQUIRED)
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} MMMCore)

###########################################################
#### Project build configuration                       ####
###########################################################

include_directories(${EXTERNAL_INCLUDE_DIRS})
add_library(${PROJECT_NAME} SHARED ${SOURCE_FILES} ${HEADER_FILES})
target_link_libraries(${PROJECT_NAME} PUBLIC ${EXTERNAL_LIBRARY_DIRS} dl)

# this allows dependant targets to automatically add include-dirs by simply linking against this project
target_include_directories(${PROJECT_NAME} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/..> # TODO: other code requires headers like "#include <MMMSegmentation/xyz.h>" so we need
    $<INSTALL_INTERFACE:include>                      # TODO: the parent dir for BUILD_INTERFACE. find a more elegant way i guess?
)

install(
    TARGETS ${PROJECT_NAME}
    EXPORT "${CMAKE_PROJECT_NAME}Targets"
    LIBRARY DESTINATION lib
    #ARCHIVE DESTINATION lib
    #RUNTIME DESTINATION bin
    #INCLUDES DESTINATION include # seems unnecessary because of target_include_directories()
    COMPONENT lib
)

install(FILES ${HEADER_FILES} DESTINATION "include/${PROJECT_NAME}" COMPONENT dev)

add_definitions(-DMMMTools_LIB_DIR="${MMMTools_LIB_DIR}")
add_definitions(-DMMMTools_SRC_DIR="${MMMTools_SOURCE_DIR}")

###########################################################
#### Compiler configuration                            ####
###########################################################

include_directories(${EXTERNAL_INCLUDE_DIRS})
link_directories(${EXTERNAL_LIBRARY_DIRS})
add_definitions(${EXTERNAL_LIBRARY_FLAGS})

###########################################################
#### Motion Handler Plugin                             ####
###########################################################

add_subdirectory(MotionHandlerPlugin)
