/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MotionAdaptationHandlerDIALOG_H_
#define __MMM_MotionAdaptationHandlerDIALOG_H_

#include <QDialog>
#include <QSettings>
#include <Inventor/nodes/SoSeparator.h>

#ifndef Q_MOC_RUN
#include <MMM/Motion/Motion.h>
#endif

namespace Ui {
class MotionAdaptationHandlerDialog;
}

class MotionAdaptationHandlerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MotionAdaptationHandlerDialog(QWidget* parent, MMM::MotionRecordingPtr motions);
    ~MotionAdaptationHandlerDialog();

    bool convertMotion();

    void previewTrajectory();

    Eigen::Matrix4f getTransformation();

    VirtualRobot::RobotPtr getAdaptedModel();

    void setCurrentTimestep(float timestep);

private slots:
    void setCurrentMotion(const QString &name);
    void offsetUpdated(double value);
    void setFixPose();
    void printPose();
    void printModel();
    void saveModel();
    void previewTrajectory(bool value);
    void setJoint(const QString &name);
    void setJointOffset(double value);

signals:
    void updateVisualisation();
    void addVisualisation(SoSeparator* sep);

private:
    void loadMotions();
    bool addPreviewVisualisation(float stepSize = 1, float transparency = 0.3);

    Ui::MotionAdaptationHandlerDialog* ui;
    MMM::MotionRecordingPtr motions;
    MMM::MotionPtr currentMotion;
    bool motionConverted;
    bool update;
    QSettings settings;
    std::map<float, VirtualRobot::RobotPtr> previewModels;
    float stepSize;
    float currentTimestep;
};

#endif // __MMM_MotionAdaptationHandlerDIALOG_H_
