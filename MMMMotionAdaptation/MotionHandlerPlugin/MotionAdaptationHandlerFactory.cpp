#include "MotionAdaptationHandlerFactory.h"
#include "MotionAdaptationHandler.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry MotionAdaptationHandlerFactory::registry(MotionAdaptationHandler::NAME, &MotionAdaptationHandlerFactory::createInstance);

MotionAdaptationHandlerFactory::MotionAdaptationHandlerFactory() : MotionHandlerFactory() {}

MotionAdaptationHandlerFactory::~MotionAdaptationHandlerFactory() = default;

std::string MotionAdaptationHandlerFactory::getName() {
    return MotionAdaptationHandler::NAME;
}

MotionHandlerPtr MotionAdaptationHandlerFactory::createMotionHandler(QWidget* widget) {
    return MotionHandlerPtr(new MotionAdaptationHandler(widget));
}

MotionHandlerFactoryPtr MotionAdaptationHandlerFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new MotionAdaptationHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new MotionAdaptationHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
