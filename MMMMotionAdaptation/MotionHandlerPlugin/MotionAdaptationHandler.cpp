#include "MotionAdaptationHandler.h"
#include <QFileDialog>
#include <QMessageBox>
#include <MMM/Motion/MotionRecording.h>
#include <MMM/Motion/Sensor/BasicModelPoseSensor.h>

using namespace MMM;

MotionAdaptationHandler::MotionAdaptationHandler(QWidget* widget) :
    MotionHandler(MotionHandlerType::GENERAL, "Adaptation"),
    searchPath(std::string(MMMTools_SRC_DIR)),
    widget(widget),
    dialog(nullptr)
{
}

void MotionAdaptationHandler::handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> /*currentRobots*/) {
    auto m = motions->motionsWithModel();
    if (m && !m->empty()) {
        dialog = new MotionAdaptationHandlerDialog(widget, m);
        connect(dialog, SIGNAL(updateVisualisation()), this, SIGNAL(updateVisualisation()));
        connect(dialog, SIGNAL(addVisualisation(SoSeparator*)), this, SIGNAL(addVisualisation(SoSeparator*)));
        if (dialog->convertMotion()) emit openMotions(m);
    }
    else MMM_ERROR << "Cannot open add data glove dialog, because no motions are present!" << std::endl;
}

std::string MotionAdaptationHandler::getName() {
    return NAME;
}

void MotionAdaptationHandler::timestepChanged(float timestep) {
    if (dialog) {
        dialog->setCurrentTimestep(timestep);
    }
}
