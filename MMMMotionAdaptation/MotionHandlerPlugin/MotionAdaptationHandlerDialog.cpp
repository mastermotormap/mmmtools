#include "MotionAdaptationHandlerDialog.h"
#include "ui_MotionAdaptationHandlerDialog.h"

#include <MMM/Exceptions.h>
#include <MMM/Motion/MotionRecording.h>
#include <MMM/Model/ModelReaderXML.h>
#include <VirtualRobot/Robot.h>
#include <QFileDialog>
#include <QCheckBox>
#include <QDoubleSpinBox>
#include <SimoxUtility/math/convert.h>
#include <MMM/Motion/Sensor/BasicModelPoseSensor.h>
#include <MMM/Motion/Sensor/BasicKinematicSensor.h>
#include <QMessageBox>
#include <VirtualRobot/XML/RobotIO.h>
#include <VirtualRobot/Nodes/PositionSensor.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <set>

MotionAdaptationHandlerDialog::MotionAdaptationHandlerDialog(QWidget* parent, MMM::MotionRecordingPtr motions) :
    QDialog(parent),
    ui(new Ui::MotionAdaptationHandlerDialog),
    motions(motions),
    currentMotion(nullptr),
    motionConverted(false),
    update(false),
    stepSize(0.0f)
{
    ui->setupUi(this);

    connect(ui->ChooseMotionComboBox, SIGNAL(currentTextChanged(const QString&)), this, SLOT(setCurrentMotion(const QString&)));
    connect(ui->xOffset, SIGNAL(valueChanged(double)), this, SLOT(offsetUpdated(double)));
    connect(ui->yOffset, SIGNAL(valueChanged(double)), this, SLOT(offsetUpdated(double)));
    connect(ui->zOffset, SIGNAL(valueChanged(double)), this, SLOT(offsetUpdated(double)));
    connect(ui->rollOffset, SIGNAL(valueChanged(double)), this, SLOT(offsetUpdated(double)));
    connect(ui->pitchOffset, SIGNAL(valueChanged(double)), this, SLOT(offsetUpdated(double)));
    connect(ui->yawOffset, SIGNAL(valueChanged(double)), this, SLOT(offsetUpdated(double)));
    connect(ui->SetPoseButton, SIGNAL(clicked()), this, SLOT(setFixPose()));
    connect(ui->PrintPoseButton, SIGNAL(clicked()), this, SLOT(printPose()));
    connect(ui->PrintModelButton, SIGNAL(clicked()), this, SLOT(printModel()));
    connect(ui->SaveModelButton, SIGNAL(clicked()), this, SLOT(saveModel()));
    connect(ui->SaveModelButton, SIGNAL(clicked()), this, SLOT(saveModel()));
    connect(ui->previewTraj, SIGNAL(toggled(bool)), this, SLOT(previewTrajectory(bool)));
    connect(ui->jointComboBox, SIGNAL(currentTextChanged(const QString&)), this, SLOT(setJoint(const QString&)));
    connect(ui->jointValueOffset, SIGNAL(valueChanged(double)), this, SLOT(setJointOffset(double)));
    //connect(ui->yOffset, &QDoubleSpinBox::valueChanged, this, &MotionAdaptationHandlerDialog::offsetUpdated);

    loadMotions();
}

MotionAdaptationHandlerDialog::~MotionAdaptationHandlerDialog() {
    delete ui;
}

bool MotionAdaptationHandlerDialog::convertMotion() {
    show();
    return motionConverted;
}

void MotionAdaptationHandlerDialog::previewTrajectory() {
    if (ui->previewTraj->isChecked()) previewTrajectory(ui->previewTraj->isChecked());
}

Eigen::Matrix4f MotionAdaptationHandlerDialog::getTransformation() {
    return simox::math::pos_rpy_to_mat4f(ui->xOffset->value(), ui->yOffset->value(), ui->zOffset->value(),
                                         ui->rollOffset->value(), ui->pitchOffset->value(), ui->yawOffset->value());
}

VirtualRobot::RobotPtr MotionAdaptationHandlerDialog::getAdaptedModel() {
    if (currentMotion) {
        auto model = currentMotion->getModel(false)->clone();
        if (model) {
            auto offsetTransformation = getTransformation();
            auto root = model->getRootNode();
            for (auto sensor : root->getSensors()) {
                if (std::dynamic_pointer_cast<VirtualRobot::PositionSensor>(sensor)) {
                    Eigen::Matrix4f transformation = sensor->getParentNodeToSensorTransformation();
                    if (true) // transform position only
                        transformation.block(0,3,4,1) = offsetTransformation.inverse() * transformation.block(0,3,4,1); // probably wrong
                    else
                        transformation = offsetTransformation.inverse() * transformation;
                    sensor->setRobotNodeToSensorTransformation(transformation);
                }
            }
        }
        return model;
    }
    return nullptr;
}

void MotionAdaptationHandlerDialog::setCurrentTimestep(float timestep) {
    currentTimestep = timestep;
}

void MotionAdaptationHandlerDialog::loadMotions() {
    for (const std::string &name : motions->getMotionNames()) {
        auto motion = motions->getMotion(name);
        if (!currentMotion) currentMotion = motion;
        ui->ChooseMotionComboBox->addItem(QString::fromStdString(motion->getName()));
    };
    previewModels.clear();
    previewTrajectory();
}

bool MotionAdaptationHandlerDialog::addPreviewVisualisation(float stepSize, float transparency) {
    if (currentMotion) {
        auto model = currentMotion->getModel();
        if (model) {
            auto maxTimestep = currentMotion->getMaxTimestep();
            if (stepSize != this->stepSize)
                previewModels.clear();
            if (previewModels.size() == 0) {
                SoSeparator* sep = new SoSeparator();
                VirtualRobot::RobotPtr visModel = model->cloneScaling();
                visModel->reloadVisualizationFromXML(false);
                for (float timestep = currentMotion->getMinTimestep(); timestep < maxTimestep; timestep += stepSize) {
                    VirtualRobot::RobotPtr previewModel = visModel->clone();
                    auto vis = previewModel->getVisualization<VirtualRobot::CoinVisualization>(VirtualRobot::SceneObject::Full);
                    vis->setTransparency(transparency);
                    SoNode* visualisationNode = vis->getCoinVisualization();
                    if (visualisationNode) sep->addChild(visualisationNode);
                    this->stepSize = stepSize;
                    previewModels[timestep] = previewModel;
                }
                emit addVisualisation(sep);
            }
            for (float timestep = currentMotion->getMinTimestep(); timestep < maxTimestep; timestep += stepSize) {
                if (previewModels.find(timestep) == previewModels.end())
                    MMM_ERROR << "This should not occur!" << std::endl;
                else
                    currentMotion->initializeModel(previewModels.at(timestep), timestep);
            }
            return true;
        }
    }
    emit addVisualisation(nullptr);
    return false;
}

void MotionAdaptationHandlerDialog::setCurrentMotion(const QString &name) {
    currentMotion = motions->getMotion(name.toStdString());
    if (currentMotion) {
        std::shared_ptr<MMM::BasicModelPoseSensor> modelPoseSensor = currentMotion->getSensorByType<MMM::BasicModelPoseSensor>();
        if (modelPoseSensor) {
            Eigen::Matrix4f offset = modelPoseSensor->getOffset();
            Eigen::Vector3f rpy = simox::math::mat4f_to_rpy(offset);
            Eigen::Vector3f pos = simox::math::mat4f_to_pos(offset);
            update = false;
            ui->xOffset->setValue(pos(0));
            ui->yOffset->setValue(pos(1));
            ui->zOffset->setValue(pos(2));
            ui->rollOffset->setValue(rpy(0));
            ui->pitchOffset->setValue(rpy(1));
            ui->yawOffset->setValue(rpy(2));
            update = true;
        }
        ui->jointComboBox->clear();
        ui->jointValueOffset->setValue(0.0);
        auto kinematicSensors = currentMotion->getSensorsByType<MMM::BasicKinematicSensor>();
        for (auto kinematicSensor : kinematicSensors) {
            for (const std::string &jointName : kinematicSensor->getJointNames()) {
                ui->jointComboBox->addItem(QString::fromStdString(jointName));
            }
        }
    }
    previewModels.clear();
    previewTrajectory();
}

void MotionAdaptationHandlerDialog::offsetUpdated(double /*value*/) {
    if (!update) return;
    //Eigen::Matrix4f offsetTransformation = simox::math::pos_rpy_to_mat4f(ui->xOffset->value() - lastX, ui->yOffset->value() - lastY, ui->zOffset->value() - lastZ,
    //                              ui->rollOffset->value() - lastRoll, ui->pitchOffset->value() - lastPitch, ui->yawOffset->value() - lastYaw);
    Eigen::Matrix4f offsetTransformation = simox::math::pos_rpy_to_mat4f(ui->xOffset->value(), ui->yOffset->value(), ui->zOffset->value(),
                                      ui->rollOffset->value(), ui->pitchOffset->value(), ui->yawOffset->value());

    std::shared_ptr<MMM::BasicModelPoseSensor> modelPoseSensor = currentMotion->getSensorByType<MMM::BasicModelPoseSensor>();
    if (!modelPoseSensor) return;
    modelPoseSensor->setOffset(offsetTransformation);

    emit updateVisualisation();
    previewTrajectory();
}

void MotionAdaptationHandlerDialog::setFixPose() {
    if (currentMotion) {
        auto modelPoseSensor = currentMotion->getSensorByType<MMM::BasicModelPoseSensor>();
        Eigen::Matrix4f rootPose;
        if (currentTimestep < modelPoseSensor->getMinTimestep())
            rootPose = modelPoseSensor->getRootPose(modelPoseSensor->getMinTimestep());
        else if (currentTimestep > modelPoseSensor->getMaxTimestep())
            rootPose = modelPoseSensor->getRootPose(modelPoseSensor->getMaxTimestep());
        else
            rootPose = modelPoseSensor->getRootPose(currentTimestep);
        modelPoseSensor->setFixedRootPose(rootPose, true);
    }
}

void MotionAdaptationHandlerDialog::printPose() {
    if (currentMotion) {
        std::cout << "Root pose of " << currentMotion->getName() << " at timestep " << currentTimestep << ":\n";
        std::cout << currentMotion->getRootPose(currentTimestep) << std::endl;
    }
}

void MotionAdaptationHandlerDialog::printModel() {
    if (currentMotion) {
        auto model = getAdaptedModel();
        if (model)
            std::cout << model->toXML(model->getFilename()) << std::endl;
    }
}

void MotionAdaptationHandlerDialog::saveModel() {
    if (currentMotion) {
        auto model = getAdaptedModel();
        if (model) {
            std::filesystem::path modelFilePath = QFileDialog::getSaveFileName(this, tr("Save model"), QString::fromStdString(model->getFilename()), tr("XML files (*.xml)")).toStdString();
            if (!modelFilePath.empty()) {
                std::string fn = modelFilePath.filename().generic_string();
                std::string fnPath = modelFilePath.parent_path().generic_string();
                VirtualRobot::RobotIO::saveXML(model, fn, fnPath);
            }
        }
    }
}

void MotionAdaptationHandlerDialog::previewTrajectory(bool value) {
    if (value)
        addPreviewVisualisation(ui->previewTrajTimestep->value());
    else {
        previewModels.clear();
        emit addVisualisation(nullptr);
    }
}

void MotionAdaptationHandlerDialog::setJoint(const QString &name) {
    if (currentMotion) {
        auto jointName = name.toStdString();
        auto kinematicSensors = currentMotion->getSensorsByType<MMM::BasicKinematicSensor>();
        for (auto kinematicSensor : kinematicSensors) {
            if (kinematicSensor->hasJoint(jointName)) {
                int index = kinematicSensor->getJointIndex(jointName);
                ui->jointValueOffset->setValue(kinematicSensor->getJointOffset()(index));
                return;
            }
        }
    }
    ui->jointValueOffset->setValue(0.0);
}

void MotionAdaptationHandlerDialog::setJointOffset(double value) {
    if (currentMotion && !ui->jointComboBox->size().isEmpty()) {
        auto jointName = ui->jointComboBox->currentText().toStdString();
        auto kinematicSensors = currentMotion->getSensorsByType<MMM::BasicKinematicSensor>();
        for (auto kinematicSensor : kinematicSensors) {
            if (kinematicSensor->hasJoint(jointName)) {
                kinematicSensor->setJointOffset(jointName, value);
                emit updateVisualisation();
                return;
            }
        }
    }
}
