/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2021 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_MotionAdaptationHandler_H_
#define __MMM_MotionAdaptationHandler_H_

#include "../../MMMViewer/MMM/Viewer/MotionHandler.h"
#include "MotionAdaptationHandlerDialog.h"

namespace MMM
{

class MotionAdaptationHandler : public MotionHandler
{
    Q_OBJECT

public:
    MotionAdaptationHandler(QWidget* widget);

    void handleMotion(MotionRecordingPtr motions, std::map<std::string, VirtualRobot::RobotPtr> currentRobots);

    virtual std::string getName();

    static constexpr const char* NAME = "MotionAdaptationHandler";

public slots:
    void timestepChanged(float timestep);

private:
    std::string searchPath;
    QWidget* widget;
    MotionAdaptationHandlerDialog* dialog;
};

}

#endif
