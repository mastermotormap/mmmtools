#include "AddIMUHandler.h"

using namespace MMM;

AddIMUHandler::AddIMUHandler(QWidget* parent) :
    AddAttachedSensorHandler<AddIMUHandlerSensorConfiguration>(parent, "Add IMU sensor")
{
}

std::string AddIMUHandler::getName() {
    return NAME;
}

