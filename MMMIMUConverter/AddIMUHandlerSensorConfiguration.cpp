#include "AddIMUHandlerSensorConfiguration.h"

using namespace MMM;

AddIMUHandlerSensorConfiguration::AddIMUHandlerSensorConfiguration() : AddAttachedSensorConfiguration<IMUSensor>("imu", {"AccelerationX", "AccelerationY", "AccelerationZ", "GyroscopeX", "GyroscopeY", "GyroscopeZ", "MagnetometerX", "MagnetometerY", "MagnetometerZ"})
{
}

void AddIMUHandlerSensorConfiguration::addSensorMeasurements(const std::vector<std::map<std::string, std::string> > &values) {
    for (std::map<std::string, std::string> map : values) {
        float timestep;
        Eigen::Vector3f acceleration;
        Eigen::Vector3f gyroscope;
        Eigen::Vector3f magnetometer;
        try {
            timestep = simox::alg::to_<float>(map[TIMESTEP_STR]);
        }
        catch (simox::error::SimoxError &e) {
            throw new MMM::Exception::MMMException(std::string("Timestep invalid! ") + e.what());
        }
        try {
            acceleration << simox::alg::to_<float>(map["AccelerationX"]),
                            simox::alg::to_<float>(map["AccelerationY"]),
                            simox::alg::to_<float>(map["AccelerationZ"]),
            gyroscope << simox::alg::to_<float>(map["GyroscopeX"]),
                         simox::alg::to_<float>(map["GyroscopeY"]),
                         simox::alg::to_<float>(map["GyroscopeZ"]);
            magnetometer << simox::alg::to_<float>(map["MagnetometerX"]),
                         simox::alg::to_<float>(map["MagnetometerY"]),
                         simox::alg::to_<float>(map["MagnetometerZ"]);;
        }
        catch (simox::error::SimoxError &e) {
            throw new MMM::Exception::MMMException(std::string("Acceleration invalid at timestep ") + simox::alg::to_string(timestep) + "! " + e.what());
        }
        IMUSensorMeasurementPtr measurement(new IMUSensorMeasurement(timestep, acceleration, gyroscope, magnetometer));
        sensor->addSensorMeasurement(measurement);
    }
}
